<script src="https://kaana.io/resources/js/jquery.validate.min.js"></script>
<script>
  
  $('body').addClass('PF-dark');
  
  var numberSteps = $('#testform .step').length;
  var currentStep = 0; // Current step is set to be the first step (0)
  processstep(); // Display the crurrent step

  $(document).on("click", "#testform .next", function(event) {
    processstep();
    event.preventDefault();
  });

  $(document).on("click", "#testform .back", function(event) {
    processstep("back");
    event.preventDefault();
  });

  $(document).on("submit", "#testform", function(event) {
    processstep();
    event.preventDefault();
  });

  var processtimeoutstep = null;

  function processstep(nextback) {
    $('.login_container .PF-progress.linear.loading').show();
    clearTimeout(processtimeoutstep);
    processtimeoutstep = setTimeout(function() {
      if (currentStep < numberSteps) {
        if (nextback == "back") {
          currentStep = currentStep - 1;
        } else {
          currentStep = currentStep + 1;
        }
        var step = $(".step:nth-child(" + currentStep + ")");
        $('.step').hide();
        step.show();

        if(!$(".step:nth-child(" + currentStep + ")").hasClass('auto')){
          step.find('input').first().focus();
        } else {
          setTimeout(function() {
            processstep();
          }, Math.floor((Math.random() * 3000) + 2000));
        }
      } else {
        alertdd.show('finite');
      }
      if(!$(".step:nth-child(" + currentStep + ")").hasClass('auto')){
      $('.login_container .PF-progress.linear.loading').hide();
      }
    }, 500);
    
  }
</script>