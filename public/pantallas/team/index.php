<html>

<head>
  <style>
    .team_container {
      max-width: 55rem;
      margin: auto;
      border: none;
    }
    
    .team_container>header>.info>h1 {
      font-size: 2.5em;
      position: relative;
      width: fit-content;
    }
    
    .team_container>header>.info>h1:before {
      content: "";
      position: absolute;
      left: 0;
      right: 0;
      top: 0.75em;
      bottom: 0.3em;
      -webkit-transform: rotateZ(-2deg);
      -ms-transform: rotate(-2deg);
      transform: rotateZ(-2deg);
      background: rgba(var(--PF-color-primary), .4);
      z-index: -1;
    }
    
    .team_container>header>.info>p {
      font-size: 1.5em;
    }
    
    .team_container .PF-grid {
      grid-template-columns: repeat(auto-fill, minmax(14em, 1fr));
    }

    .team_container .PF-grid .PF-card .info p {
      -webkit-line-clamp: initial;
    }
    
    .team_container .PF-grid .PF-card {
      flex: 1 1 20%;
    }

    .team_container .PF-grid.circle>.PF-card>.PF-image {
      background-color: rgba(var(--PF-color-primary), 0.1);
      box-shadow: none;
    }
    
    .team_container .PF-grid .PF-card .info h1 {
      font-size: 1.5em;
      position: relative;
    }
    
    .team_container .PF-grid .PF-card .info h1:before {
      content: "";
      position: absolute;
      left: 0;
      right: 0;
      top: 0.65em;
      bottom: 0.3em;
      -webkit-transform: rotateZ(-2deg);
      -ms-transform: rotate(-2deg);
      transform: rotateZ(-2deg);
      background: rgba(var(--PF-color-primary), .4);
      z-index: -1;
    }

    @media (min-width: 28em) {
      .team_container .founder-grid {
        width:fit-content;
        margin: auto;
      }
    }

    @media (max-width: 28em) {
      .team_container .PF-grid.other-team {
        grid-template-columns:repeat(auto-fill, minmax(10em, 1fr));
      }
    }
    
  </style>
</head>

<body>

  <div id="team_container" class="team_container PF-page">
    <header>
      <div class="info">
        <h1 t-dd>Team</h1>
        <p t-dd>We are a small team united by a common passion, to make a better world. Most of us come from long adventures and experiences that have made us think in such a way that we want to do our bit, and we are all very passionate about solving real problems
          of users in this space. As a team, we also love electric cars and dogs, but otherwise we have a very diverse set of passions outside of work.</p>
      </div>
    </header>

    <div class="content">
      <div class="PF PF-grid circle center founder-grid">

        <div class="PF PF-card PFC-red" opendd-href="?p=profile&id=108">
          <div class="PF PF-image ripple" style=" background-image: url('./pantallas/team/photos/108.png');" rgdd></div>
          <div class="info">
            <h1>Albert Isern Alvarez</h1>
            <p t-dd>CEO, Founder & Developer.</p>
          </div>
        </div>
        
      </div>
      <div class="PF PF-grid circle center other-team">


        <?php
$user_team = mysqli_query($con,"SELECT * FROM internal_team WHERE hide IS NULL ORDER BY id ASC");
while($row_user_team = mysqli_fetch_array($user_team))
  {
    $name_team = $row_user_team['name'];
    $lastname_team = $row_user_team['lastname'];
    $user_id_team = $row_user_team['user_id'];
    $description_team = $row_user_team['description'];
    $color_team = $row_user_team['color'];
    if($name_team){$name_show_team = $name_team . " " . $lastname_team;} else {$name_show_team = $user_username_team;}
    $name_show_team = ucwords($name_show_team);
    if(file_exists(__DIR__."/photos/" . $user_id_team . ".png")){
      $avatar_team = "//kaana.io/pantallas/team/photos/" . $user_id_team . ".png";
    } else {
      $avatar_team = "//user.kaana.io/user.svg";
    }
    ?>
        <div class="PF PF-card <?php if($color_team){?>PFC-<?=$color_team?><?}?>" opendd-href="?p=profile&id=<?=$user_id_team?>">
          <div class="PF PF-image ripple" style=" background-image: url('<?=$avatar_team?>');" rgdd></div>
          <div class="info">
            <h1><?=$name_show_team?></h1>
            <p t-dd><?=$description_team?></p>
          </div>
        </div>

        <?}?>

      </div>
    </div>

  </div>


</body>

</html>