<html>

<head>
  <style>
  .files_container.PF-tabs>.tabs>.tab {
  	overflow: visible;
  }
  .files_container .PF-grid {
  	margin: auto;
    max-width: 80vw;
  }
  </style>
</head>

<body>
  
  <div class="PF-tabs files_container">
    <div class="tabs">
      <div class="tab" data-name="tabsfiles-home"></div>
      <div class="tab" data-name="tabsfiles-recent"></div>
      <div class="tab" data-name="tabsfiles-saved"></div>
      <div class="tab" data-name="tabsfiles-upload"></div>
    </div>

    <div class="PF PF-tabbar bottom shadow sh64">
      <div class="container center">
        <ul id="bottombar">
        	
          <li class="ripple" data-for="tabsfiles-home" data-taburl="./pantallas/files/pages/home/index.php?<?=$server_querystring?>">
            <i class="material-icons">home</i>
            <span t-dd>Home</span>
          </li>

          <li class="ripple active" data-for="tabsfiles-recent" data-taburl="./pantallas/files/pages/recent/index.php?<?=$server_querystring?>" tab-allowreload>
            <i class="material-icons">cloud</i>
            <span t-dd>Recent</span>
          </li>

					<li class="ripple" data-for="tabsfiles-saved" data-taburl="./pantallas/files/pages/trash/index.php?<?=$server_querystring?>">
            <i class="material-icons">delete</i>
            <span t-dd>Trash</span>
          </li>

          <li class="ripple" data-for="tabsfiles-upload" data-taburl="./pantallas/files/pages/upload/index.php?<?=$server_querystring?>">
            <i class="material-icons">cloud_upload</i>
            <span t-dd>Upload</span>
          </li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
  </div>

</body>

</html>