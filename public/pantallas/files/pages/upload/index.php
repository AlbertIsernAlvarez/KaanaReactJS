<form class="PF PF-form" id="uploadfilesform" action="./pantallas/files/manager/upload.php" method="post" enctype="multipart/form-data">
    <input type="file" name="files[]" multiple id="imageUpload" required onchange="$('#uploadfilesform').submit();">
    <div class="PF PF-buttons full">
      <label for="imageUpload" class="PF PF-button" type="submit" t-dd>Upload</label>
    </div>
  </form>

  <script>

  $("#uploadfilesform").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Uploading files');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
      alert(data);
    }
  });
  
</script>