<html>

<head>
  <style>
    .invitation-container {
      max-width: 35em;
      margin: auto;
      padding-top: 1em;
      padding-bottom: 1em;
    }
  </style>
</head>

<body>

  <div class="invitation-container">
    <div class="PF PF-content full noborder center">
      <div class="container">
        <div class="PF PF-image transparent" style="background-image:url('./pantallas/files/assets/images/trash.png');height: 3em;"></div>
        <h1 t-dd>You haven't deleted any files recently.</h1>
        <p t-dd>You have up to 30 days to restore a deleted file.</p>
      </div>
    </div>
    

  </div>

</body>

</html>