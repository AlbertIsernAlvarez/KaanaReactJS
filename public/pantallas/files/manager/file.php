<?php
if ($id_get) {
    $filesmanager = mysqli_query($con, "SELECT * FROM files WHERE fileid='$id_get' AND status IS NULL");
    if ($row_filesmanager = mysqli_fetch_array($filesmanager)) {
        $userid_filesmanager       = $row_filesmanager['user_id'];
        $fileid_filesmanager       = $row_filesmanager['fileid'];
        $originalname_filesmanager = $row_filesmanager['original_name'];
        $mime_filesmanager         = $row_filesmanager['mime'];
        $type_filesmanager         = $row_filesmanager['type'];
        
        $mime_type = strtok($mime_filesmanager, '/');
        
        $file_to_open = __DIR__ . "/../../../user/" . $userid_filesmanager . "/" . $mime_type . "/" . $fileid_filesmanager . "." . substr($mime_filesmanager, strpos($mime_filesmanager, "/") + 1);
        
        if ($mime_type == "image" and "$mime_filesmanager" != "image/svg") {
            $requested_uri = str_replace(__DIR__ . "/../../..","",$file_to_open);
            include_once(__DIR__ . "/../../../resources/adaptive-images/adaptive-images.php");
        } elseif (file_exists($file_to_open) and $user_mismo_id) {
            header('Content-type: ' . $type_filesmanager);
            echo file_get_contents($file_to_open);
        }
    } else {
        header("HTTP/1.0 404 Not Found");
    }
}
?>