<div class="PF PF-toolbar transparent window-toolbar">
    <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  </div>

<div class="PF-tabs">
    <div class="PF PF-tabbar shadow">
      <div class="container">
        <ul>
        	<li class="ripple active" data-for="home-settingstab" data-taburl="./pantallas/files/manager/selector/upload.php?<?=$server_querystring?>" preload><span t-dd>Upload</span></li>
          <li class="ripple" data-for="display-settingstab" data-taburl="./pantallas/files/pages/recent/index.php?<?=$server_querystring?>" preload><span t-dd>Select</span></li>
          <li class="ripple" data-for="about-settingstab" data-taburl="./pantallas/settings/pantallas/about.php?<?=$server_querystring?>" preload><span t-dd>From url</span></li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
    <div class="tabs">
    	<div class="tab" data-name="home-settingstab"></div>
      <div class="tab" data-name="display-settingstab"></div>
      <div class="tab" data-name="about-settingstab"></div>
    </div>
  </div>