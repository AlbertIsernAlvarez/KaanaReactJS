<?php
  
$uploaded_files = array();
$files_uploaded = array();

if(isset($_FILES['files']['name']) and $user_mismo_id){
  //echo $_FILES['files']['name'][$key];
  foreach($_FILES['files']['name'] as $key=>$val){
    $errors = null;
    $errors = array();
    $file_name = $_FILES['files']['name'][$key];
    $file_size = $_FILES['files']['size'][$key];
    $file_tmp = $_FILES['files']['tmp_name'][$key];
    $file_type = $_FILES['files']['type'][$key];
    $file_mime = mime_content_type($_FILES['files']['tmp_name'][$key]);
    
    $file_rename = generateFileLargeName();
    
    //$extensions = array("image/gif","image/jpg","image/png","image/bmp","image/jpeg");
    //if(in_array($file_mime,$extensions)=== false){
      //$errors[]="extension not allowed, please choose a JPEG or PNG file.";
    //}
    
    if($file_size > 50000000){
      $errors[]='File size have to be less than 50 MB';
    }
    
    if(empty($errors)===true){
      $mime_type = strtok($file_mime, '/');
      $mime_upload = str_replace($mime_type."/", ".", $file_mime);
      if (!file_exists(__DIR__."/../../../user/" . $user_mismo_id . "/".$mime_type."/")) {
        mkdir(__DIR__."/../../../user/" . $user_mismo_id . "/".$mime_type."/", 0755, true);
      }
      move_uploaded_file($file_tmp, __DIR__."/../../../user/" . $user_mismo_id . "/".$mime_type."/".$file_rename.$mime_upload);
      mysqli_query($con, "INSERT INTO files (original_name, user_id, fileid, mime, type) VALUES('$file_name', '$user_mismo_id', '$file_rename', '$file_mime', '$file_type')") or die();
      $last_id_uploaded_file = mysqli_insert_id($con);

      if(empty($errors)===false){
        $errors[]= "File not inserted in database";
      } else {
        $files_uploaded[] = $file_rename;
      }
      
    }
    
    if($errors){
      $errors_debug = implode(",", $errors);
      userErrorLog($errors_debug);
      echo false;
    } else {
      echo implode(",", $files_uploaded);
    }
  }
  
}

function generateFileLargeName(){
  $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  $file_rename = null;
  $length = strlen($charset);
  $count = 91;
  while ($count--) {
    $file_rename .= $charset[mt_rand(0, $length-1)];
  }
  return $file_rename;
}

?>