<html>

<head>
  <style>
    .feedback-support-container {
      max-width: calc(500px + 2rem);
      margin:auto;
    }
    
    .feedback-support-container .feed .post .top {
      display:none;
    }
    
  </style>
</head>

<body>

  <div class="feedback-support-container" >
    <div class="PF PF-content full noborder center transparent">
    <div class="container">
      <div class="PF-image" style="background-image:url('./pantallas/lastest-updates/lastest-updates.svg'); background-size: contain; min-height: 6em; background-color: transparent;" ></div>
      <h1 t-dd >Lastest Updates</h1>
      <div follow-button data-id="639" data-type="user" data-size="" ></div>
    </div>
  </div>
    
    <div class="PF PF-card">
      <div class="info" >
        <h1><span t-dd>The last Kaana version is</span> <b><?=$last_version_number?></b></h1>
        <p t-dd >Here you will see the latest news that are being integrated into Kaana.</p>
        <p t-dd>To be more informed click on the follow button.</p>
      </div>
    </div>
    
    <div feed data-type="all" data-order="lastest" data-classes="shadow" data-showform="no" data-currenturl="?p=profile&id=639" ></div>
    
    <div class="PF PF-card" >
      <div class="info">
      	<h1 t-dd>Donate</h1>
      <p t-dd>If you like Kaana - and we sincerely hope you do - consider donating. It will help the development process and make it easier for us to improve it for everyone.</p>
      </div>
      <a class="PF PF-button transparent ripple" opendd-href="?p=donate">
        <div class="inside">
          <p t-dd>Donate</p>
        </div>
      </a>
    </div>

  </div>
  
</body>

</html>