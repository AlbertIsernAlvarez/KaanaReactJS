<div class="PF PF-toolbar">
  <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  <h1>Ethereum</h1>
</div>

<div class="PF PF-card">
  <header>
    <h1>ETH <span t-dd>Adress</span>:</h1>
    <p style="user-select: all;">0xaa75b84c942f7125584c53a8986ebf6c60c76670</p>
  </header>
  <div class="content">
    <img style="width:10em; height:10em;" class="PFC-orange PF-image ripple" src="//img.kaana.io/donaciones/ethereum-qr.png"/>
  </div>
</div>