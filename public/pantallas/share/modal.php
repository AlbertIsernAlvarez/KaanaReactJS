<div class="PF PF-toolbar">
	<h1 t-dd>Share</h1>
</div>

<label class="PF-textfield filled" style="margin: .5em; width: calc(100% - 1em);">
	<input type="text" placeholder=" " value="<?=$_GET['share']?>" id="shareinput" readonly>
	<span t-dd>Copy link</span>
</label>

<?php
if($_GET['content']){?>
<label class="PF-textfield filled" style="margin: .5em; width: calc(100% - 1em);">
		<textarea type="text" placeholder=" " name="state" id="sharecontent" readonly><?=$_GET['content']?></textarea>
		<span t-dd>Copy content</span>
	</label>
<?}?>

<script>
$('#shareinput').focus(function() {
		$(this).select();
	});
	$('#sharecontent').focus(function() {
		$(this).select();
	});
</script>