<html>

<head>
	<style>
		body:not(.PF-dark) .dd_screen {
			background-image: url(//img.kaana.io/backgrounds/city_banner_background.svg);
			background-repeat: no-repeat;
			background-position: center bottom;
			background-attachment: fixed;
		}
		
		.Kavatar_container {
			display: flex;
			flex-direction: column;
			margin: auto;
			max-width: 50em;
		}
		
		.Kavatar_container>.PF-content>.text {
			margin: 0;
			padding: .5rem;
		}
	</style>
</head>

<body>
	<div class="Kavatar_container">
		<div>
			<div class="PF PF-content noborder center transparent">
				<div class="container">
					<div class="PF-image PF-avatar ddbg-avatar circle" rgdd onclick="windowdd('./resources/window/cambiar-avatar.php');"></div>
					<h1><?=$user_mismo_name?><span t-dd>, this is your Kavatar</span></h1>
				</div>
				</d>
			</div>
			<div class="PF PF-content" style="text-align: left;">
				<div class="container">
					<p class="text" t-dd>Your Kavatar is an image that appears next to your name when you write comments or news on a blog. Avatars help identify your posts on blogs and forums, why not anywhere?</p>
					<p class="text" t-dd>An 'avatar' is an image that represents you online—a little picture that appears next to your name when you interact with websites.</p>
					<p class="text" t-dd>A Kavatar is a Globally Avatar. You upload it and create your profile just once, and then when you participate in any Kavatar-enabled site, your Kavatar image will automatically follow you there.</p>
					<p class="text" t-dd>Kavatar is a free service for site owners, developers, and users.</p>
				</div>
			</div>
		</div>
</body>

</html>