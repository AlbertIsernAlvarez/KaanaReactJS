<div class="search_empty">
  <div class="PF PF-content full noborder center">
			<div class="container">
				<div class="PF PF-image transparent" style="background-image:url('./pantallas/search/search.svg'); background-size: contain; min-height: 6em;"></div>
				<h1 t-dd>Search something</h1>
				<p t-dd>Search a friend, topic, what's trending and much more</p>
			</div>
		</div>
</div>