<html>

<head>
	<style>
	
		.market-container {
			max-width: calc(500px + 2rem);
			margin: auto;
		}
		
	</style>
</head>

<body>

	<div class="market-container">
		<div class="PF PF-content full noborder center">
			<div class="container">
				<div class="PF PF-image transparent" style="background-image:url('./apps/market/market.svg'); background-size: contain; min-height: 6em;"></div>
				<h1 t-dd>Search for a product</h1>
				<p t-dd>Search food, a bike, TV, everything you want</p>
			</div>
		</div>

	</div>

</body>

</html>