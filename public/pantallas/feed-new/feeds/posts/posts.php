<?php

$feed['blocks']['2'][] = array(
  		'title' => 'posts',
  		'fullcoverage' => array(),
  		'cards' => array());

include(__DIR__.'/algorithm.php');
include(__DIR__.'/process-text.php');
  		
$posts = mysqli_query($con,"SELECT * FROM posts WHERE id IN($ids_select_post) LIMIT 10");
while($row_posts = mysqli_fetch_array($posts))
{

	$ids_feedsloaded[] =  $row_posts['id'];

	$user_id_posts = $row_posts['user_id'];

	//data user
  $posts_user_post = mysqli_query($con,"SELECT * FROM users WHERE user_id='$user_id_posts'");
  while($row_posts_user_post = mysqli_fetch_array($posts_user_post)){
    $user_id_post = $row_posts_user_post["user_id"];
    $user_username_post = $row_posts_user_post["username"];
    $user_name_post = $row_posts_user_post["name"];
    $user_lastname_post = $row_posts_user_post["lastname"];
    $user_avatar_post = $row_posts_user_post["avatar"];
    if($user_avatar_post == null) {
      $user_avatar_post = "user.svg";
      $user_avatar_id_post = '0';
    }
   }
 

	$post_image = null;
	if($row_posts['type'] == 'photo'){
		$post_image = "//kaana.io/apps/photos/see.php?id=" . $row_posts['contentid'];
	}
	
  
  $feed['blocks']['2'][0]['cards'][] = array(
  	'id' => $row_posts['id'],
  	'topic' => null,
  	'category' => null,
  	'title' => $row_posts['title'],
  	'description' => null,
  	'date' => $row_posts['date'],
  	'source' => array(
  		'name' => null,
  		'logo' => null,
  		'link' => null),
  	'author' => array(
  	  'username' => $user_username_post,
  		'name' => $user_name_post,
  		'lastname' => $user_lastname_post,
  		'image' => null),
  	'content' => array(
  		'text' => processtext_post($row_posts['text']),
  		'images' => array($post_image),
  		'links' => array()
  		),
  		'tags' => $row_posts["etiquetas"]
  	);

}

updatefeedsloaded($con, $user_id_mismo, $ids_feedsloaded, "posts");

?>