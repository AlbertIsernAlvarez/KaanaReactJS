<?php

$feed['blocks']['1'][] = array(
  		'title' => 'news',
  		'cards' => array(),
  		'fullcoverage' => array());


$news = mysqli_query($con,"SELECT * FROM kiosco_news ORDER BY fecha DESC LIMIT 10");
while($row_news = mysqli_fetch_array($news))
{
  
  $feed['blocks']['1']['cards'][] = array(
  	'id' => $row_news['id'],
  	'topic' => null,
  	'category' => $row_news['categoria'],
  	'title' => $row_news['titulo'],
  	'description' => null,
  	'date' => $row_news['fecha'],
  	'source' => array(
  		'name' => $row_news['fuente'],
  		'logo' => '',
  		'link' => ''),
  	'author' => array(
  		'name' => $row_news['autor'],
  		'lastname' => '',
  		'image' => ''),
  	'content' => array(
  		'text' => strip_tags($row_news['texto']),
  		'images' => array($row_news['imagen']),
  		'links' => array()
  		),
  		'tags' => $row_news["etiquetas"]
  	);

}

?>