<html>

<head>
  <style>
    
  </style>
</head>

<body>

  <div class="fakefeed">



<?php

function read_feedsloaded($con, $user_mismo_id, $feed){
  $feedsloaded=mysqli_query($con, "SELECT * FROM feeds_loaded WHERE user_id='$user_id_mismo' AND feed='$feed' ORDER BY id DESC LIMIT 1");
  while($row_feedsloaded=mysqli_fetch_array($feedsloaded)) {
    return implode(unserialize($row_feedsloaded["ids"]), ",");
  }
}

function updatefeedsloaded($con, $user_mismo_id, $ids_feedsloaded, $feed){
  
  $ids_feedsloaded = serialize($ids_feedsloaded);
  
  if(isset($update)) {
    mysqli_query($con,"UPDATE feeds_loaded SET ids='$ids_feedsloaded' WHERE feed='$feed' AND user_id='$user_mismo_id' ");
  } else {
    mysqli_query($con,"INSERT INTO feeds_loaded (user_id, ids, feed) VALUES ('$user_mismo_id', '$ids_feedsloaded', '$feed')");
  }
  
}


include(__DIR__.'/feeds/posts/posts.php');

foreach ($feed['blocks'] as &$block) {?>

<div class="block">

	<?php foreach ($block[0]['cards'] as &$card) {?>
	<div class="card" data-type="<?=$card['type']?>">
        <div class="content">
          <?php if($card['content']['images'][0]){ foreach ($card['content']['images'] as &$post_image){?><img class="PF PF-image ripple" src="<?=$post_image?>" rgdd/><?}}?>
          <div class="data">
            <?php if($card['source']['name']){?>
            <span><?=$card['source']['name']?></span>
            <?} elseif($card['author']['name']){?>
            <span><?=$card['author']['name']?> <?=$card['author']['lastname']?></span>
            <?} elseif($card['author']['username']){?>
            <span><?=$card['author']['username']?></span>
            <?}?>
            <?php if($card['title']){?><h1><?=$card['title']?></h1><?}?>
            <?php if($card['content']['text'] and !$card['title']){?><p><?=$card['content']['text']?></p><?}?>
          </div>
        </div>
        <div class="footer">
          <span prettydate><?=$card['date']?></span>
          <div class="space"></div>
          <div more-less data-type="post" data-id="<?=$publicacion_id_p?>"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>
      <?}?>

    <?php foreach ($block[0]['fullcoverage'] as &$fullcoverage) {?>
      <div class="fullcoverage" data-id="<?=$fullcoverage['id']?>">
        <div class="content">
          <div class="pages">
            <?php foreach ($fullcoverage['screens'] as &$fullcoverage_screens) {?>
            <div class="screen" data-id="<?=$fullcoverage_screens['id']?>"></div>
            <?}?>
          </div>
          <div class="progress-container">
            <?php foreach ($fullcoverage['screens'] as &$fullcoverage_progress) {?>
            <progress value="100" max="100" class="PF-progress linear ripple"></progress>
            <?}?>
          </div>
        </div>
        <div class="PF button ripple">View full coverage</div>
      </div>
    <?}?>
      
</div>
<?}?>

    <div class="block" style="display:none;">

      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>Yesterday</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">thumbs_up_down</i></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="card mini">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>Yesterday</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="card mini">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>Yesterday</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="card">
        <div class="content">
          <div class="PF link shadow">
            <div class="PF PF-image"></div>
            <div class="info">
              <h1>Hello to this website</h1>
              <p>Aloha caracoloha</p>
            </div>
          </div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>Yesterday</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="fullcoverage">
        <div class="content">
          <div class="pages">
            <div class="screen" style="background-image: url('https://kaana.io/apps/photos/see.php?id=QjadGE2p9arrg1VwPftP42Gyo3DF4HDT8BX1yYTCestJfQFvYel8C2S4RSkhAkarJGjXAg0bq2vAMZtWp66amRxlyAe');"></div>
            <div class="screen"></div>
            <div class="screen"></div>
            <div class="screen"></div>
            <div class="screen"></div>
          </div>
          <div class="progress-container">
            <progress value="100" max="100" class="PF-progress linear ripple"></progress>
            <progress value="100" max="100" class="PF-progress linear ripple"></progress>
            <progress value="70" max="100" class="PF-progress linear ripple"></progress>
            <progress value="0" max="100" class="PF-progress linear ripple"></progress>
            <progress value="0" max="100" class="PF-progress linear ripple"></progress>
          </div>
        </div>
        <div class="PF button ripple">View full coverage</div>
      </div>

      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>11 hours ago</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

    </div>



    <div class="block" style="display:none;">

      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>Yesterday</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>Yesterday</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>11 hours ago</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>11 hours ago</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

      <div class="fullcoverage">
        <div class="content"></div>
        <div class="PF button ripple">View full coverage</div>
      </div>

      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <span>CNN en Español</span>
            <h1>Tras sobrevivir a un balazo en la garganta, este niño de 2 años espera a la puerta de EEUU para pedir asilo</h1>
          </div>
        </div>
        <div class="footer">
          <span>11 hours ago</span>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>

    </div>


  </div>

</body>

</html>