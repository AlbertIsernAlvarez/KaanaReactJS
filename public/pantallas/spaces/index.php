<html>

<head>
	<style>
		.spaces_container {}
		
		.spaces_container .space-info {
			width: 100%;
			padding: .5em;
			text-align: center;
			background-color: rgb(var(--PF-color-surface));
			z-index: 5;
			position: relative;
		}
		
		.spaces_container .space-info .PF-image {
			width: 5em;
			height: 5em;
			margin: auto;
		}
		
		.spaces_container .space-info .follow-button-container {
			margin: .5em auto;
		}
		
		.spaces_container h1.title {
			padding: .5em;
		}
		
		.spaces_container > .PF-tabs > .tabs > .tab > .PF-grid {
			max-width: 50em;
			margin: auto;
		}
	</style>
</head>

<body>

	<div class="spaces_container">
<?php
if($id_get){
	include(__DIR__."/pantallas/space/space.php");
} else {
	include(__DIR__."/pantallas/home/home.php");
}
?>
	</div>

</body>

</html>