<?php
if($event_id){?>
<div class="PF event-info">
  <div class="container">
    <div class="PF-image circle ripple <?=$event_avatar?>" style="background-image:url('<?=$event_avatar?>');" rgdd></div>
    <?php if($event_title){?><h1 <?php if($event_user_id === $user_mismo_id){?>class="edit"<?}?> ><?=$event_title?></h1><?}?>
    <span>By Albert · 0 People</span>
    <div class="PF PF-avatars"></div>
  </div>
</div>


<div class="PF-tabs">
			<div class="PF PF-tabbar shadow">
				<div class="container">
					<ul>
						<li class="ripple active" data-for="eventtabs-about"><span t-dd>About</span></li>
						<li class="ripple" data-for="eventtabs-media" data-taburl="./pantallas/events/pantallas/event/media.php?<?=$server_querystring?>"><span t-dd>Media</span></li>
						<li class="ripple" data-for="eventtabs-discussion" data-taburl="./pantallas/events/pantallas/event/discussion.php?<?=$server_querystring?>"><span t-dd>Discussion</span></li>
						<?php if($event_user_id === $user_mismo_id){?><li class="ripple" data-for="eventtabs-edit" data-taburl="./pantallas/events/pantallas/event/edit.php?<?=$server_querystring?>"><span t-dd>Edit</span></li><?}?>
					</ul>
					<div class="slider"></div>
				</div>
			</div>
			<div class="tabs">
				<div class="tab" data-name="eventtabs-about">
					<?php include(__DIR__."/about.php"); ?>
				</div>
				<div class="tab" data-name="eventtabs-media"></div>
				<div class="tab" data-name="eventtabs-discussion"></div>
				<?php if($event_user_id === $user_mismo_id){?><div class="tab" data-name="eventtabs-edit"></div><?}?>
			</div>
		</div>
<?}?>