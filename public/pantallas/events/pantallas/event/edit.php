<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Edit event</h1>
			<p><span t-dd>Information to edit your event.</span></p>
		</div>
	</header>
	<div class="grid-info">

		<div class="item ripple" onclick="windowdd('./pantallas/events/edit/avatar.php?<?=$server_querystring?>');">
			<div class="left">
				<span t-dd>Photo</span>
			</div>
			<div class="right">
				<div class="double">
					<p t-dd>A photo helps personalize your event</p>
				</div>
				<div class="PF-avatar circle" rgdd></div>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/events/edit/title.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Title</span>
			</div>
			<div class="right">
				<p><?=replaceemojis($event_title)?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/events/edit/description.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Description</span>
			</div>
			<div class="right">
				<p><?=replaceemojis($event_description)?></p>
			</div>
		</div>
		
		<div class="item ripple" onclick="windowdd('./pantallas/events/edit/location.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Location</span>
			</div>
			<div class="right">
				<p><?=$event_location?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/events/edit/topic.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Topic</span>
			</div>
			<div class="right">
				<p user-data-color><?=$event_topic?></p>
			</div>
		</div>

	</div>
</div>


<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Co-hosts</h1>
			<p>You can add multiple friends or Pages, like artists and sponsors, to help spread the word. They'll have editing privileges and can add it to their calendars.</p>
		</div>
	</header>
	<div class="grid-info"></div>
</div>