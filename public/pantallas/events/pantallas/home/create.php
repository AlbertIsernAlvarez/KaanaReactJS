<html>

<head>
	<style>

	</style>
</head>

<body>
	<div>
		<div class="PF PF-content full center transparent">
			<div class="container">
				<div class="image" style="background-image:url('./pantallas/events/create.svg');"></div>
				<h1 t-dd>Create a new event</h1>
				<p t-dd>Connect with more people</p>
			</div>
		</div>
		<div class="content">
			<form class="PF PF-form" id="neweventform" action="./pantallas/events/resources/create.php" method="post" enctype="multipart/form-data">

				<label class="PF-textfield filled">
					<input placeholder=" " type="text" required name="name" validate="name">
					<span t-dd>Title</span>
				</label>

				<label class="PF-textfield filled">
					<textarea placeholder=" " type="text" required="" id="input-field"></textarea>
					<span t-dd>Description</span>
				</label>


				<div class="PF PF-buttons full">
					<button class="PF PF-button" type="submit">
						<div class="inside">
							<p t-dd>Create</p>
						</div>
					</button>
				</div>

			</form>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$("#neweventform").ajaxForm(function(idevent) {
				openddgo("?p=events&id=" + idevent);
			});
		});
	</script>

</body>

</html>