<html>

<head>
	<style>

	</style>
</head>

<body>

	<div class="PF-tabs">
		<div class="tabs">
			<div class="tab" data-name="tabsevents-1"></div>
			<div class="tab" data-name="tabsevents-2"></div>
			<div class="tab" data-name="tabsevents-3"></div>
		</div>

		<div class="PF PF-tabbar bottom shadow">
			<div class="container center">
				<ul id="bottombar">
					<li class="ripple active" data-for="tabsevents-1" data-taburl="./pantallas/events/pantallas/home/explore.php">
						<i class="material-icons bd">local_library</i>
						<span t-dd>Explore</span>
					</li>

					<li class="ripple" data-for="tabsevents-2" data-taburl="./pantallas/events/pantallas/home/my-events.php">
						<i class="material-icons">language</i>
						<span t-dd>My Events</span>
					</li>

					<li class="ripple" data-for="tabsevents-3" data-taburl="./pantallas/events/pantallas/home/create.php">
						<i class="material-icons">language</i>
						<span t-dd>Create</span>
					</li>

				</ul>
				<div class="slider"></div>
			</div>
		</div>
	</div>

</body>

</html>