<html>

<head>
	<style>
		.events_container {
			flex: 1 auto;
			display: flex;
			flex-direction: column;
		}
		
		.events_container .event-info {
			width: 100%;
			padding: .5em;
			text-align: center;
			background-color: rgb(var(--PF-color-surface));
			z-index: 5;
			position: relative;
		}
		
		.events_container .event-info .PF-image {
			width: 5em;
			height: 5em;
			margin: auto;
		}
		
		.events_container .event-info .follow-button-container {
			margin: .5em auto;
		}
		
		.events_container h1.title {
			padding: .5em;
		}
		
		.events_container > .PF-tabs > .tabs > .tab > .PF-grid {
			max-width: 50em;
			margin: auto;
		}
		
		.events_container .PF-card,
		.events_container .PF-chips {
			max-width: 50em;
			margin: .5em auto;
		}
	</style>
</head>

<body>

	<div class="events_container">
		<?php
if($event_id){
	include(__DIR__."/pantallas/event/event.php");
} else {
	include(__DIR__."/pantallas/home/home.php");
}
?>
	</div>

</body>

</html>