<html>

<head>
  <style>
    .PF-window .container.eventimagebgcontainer {
      position: relative;
      overflow: hidden;
      background-color: var(--PF-color-bg-first-default);
    }

    .eventimage-upload {
      position: relative;
      max-width: 10rem;
      margin: auto;
      text-align: center;
    }

    .eventimage-upload .eventimage-edit {
    position: absolute;
    right: 0.5rem;
    z-index: 1;
    bottom: 0.5rem;
    border-radius: 2em;
    background: rgb(var(--PF-color-primary));
    color: rgb(var(--PF-color-surface));
    padding: 0.5rem;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 5px 0 rgba(0, 0, 0, .23);
    transition: 0.5s;
    cursor: pointer;
    }

    .eventimage-upload .eventimage-edit i {
      cursor: pointer;
    }

    .eventimage-upload .eventimage-edit:hover {
      box-shadow: 0 10px 10px 0 rgba(0, 0, 0, .19), 0 6px 3px 0 rgba(0, 0, 0, .23);
    }

    .eventimage-upload .eventimage-edit input {
      display: none;
    }

    .eventimage-upload .eventimage-edit input+label {
      display: inline-block;
      width: 2rem;
      height: 2rem;
      margin-bottom: 0;
      border-radius: 100%;
      cursor: pointer;
      transition: 0.5s;
    }

    .eventimage-upload .eventimage-edit input+label {
      text-align: center;
      margin: auto;
      display: inline-flex;
      align-items: center;
      text-align: center;
    }

    .eventimage-upload .eventimage-edit input+label i {
      margin: auto;
    }

    .eventimage-upload .eventimage-preview {
    width: 10rem;
    height: 10rem;
    position: relative;
    border-radius: 5em;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    overflow: hidden;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }
  </style>
</head>

<body>
  <div class="PF PF-toolbar">
    <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
    <h1 t-dd>Change event image</h1>
  </div>

  <div class="content">
    <div class="container full middle eventimagebgcontainer">
      <div class="eventimage-upload">
        <div class="eventimage-edit">
          <form id="subireventimage" method="post" enctype="multipart/form-data" action="./resources/actualizar/eventimage.php">
            <input name="files[]" accept="image/*;capture=camera" type="file" id="imageUpload">
          </form>
          <label for="imageUpload"><i class="material-icons">&#xE2C3;</i></label>
        </div>
        <div class="eventimage-preview ddbg-eventimage imagePreview" id="imagePreview"></div>
      </div>
    </div>
    <div class="container">
      <div class="PF PF-buttons full">
        <button class="PF PF-button" onclick="$('#subireventimage').submit();" t-dd>
          <div class="inside">
            <p t-dd>Upload</p>
          </div>
        </button>
      </div>
    </div>
  </div>

  <script>
    
    
    $("#subireventimage").ajaxForm(function(data) {
      $('#ddwindow').removeClass('open');
      $('.closewindowdd').removeClass('show');
      $('body').removeClass('overflowhidden');
      if(data){
        $("#ddbg-eventimage").load('./resources/ddbg-eventimage.php');
        $(".ddbg-eventimage").css("background-image", "");
        $(".ddbg-eventimage").attr("data-bg", "");
        alertdd.show('Event image updated correctly');
      } else {
        alertdd.show('The event image cannot be updated');
      }
    });
    
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.imagePreview').css('background-image', 'url(' + e.target.result + ')');
          $('.imagePreview').hide();
          $('.imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imageUpload").change(function() {
        readURL(this);
      }

    );
  </script>
</body>

</html>