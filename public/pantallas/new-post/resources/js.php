<script>
var allowsendformnewpost = null;
<?php if($usuario_color != null){?>$('body').addClass('PFC-<?=$usuario_color?>');<?}?>

setTimeout(function(){
$("#typed").on('blur keyup paste input', function() {
  var leghttyped = $('#typed').text().length;
  $("#nuevapublicacion").removeClass('big');
  $("#nuevapublicacion").removeClass('large');
  if (leghttyped <= 20 && leghttyped >= 1 && $("#nuevapublicacion.big.large").length === 0) {
    $("#nuevapublicacion").addClass('big').addClass('large');
  } else if (leghttyped <= 180 && leghttyped >= 1 && $("#nuevapublicacion.big").length === 0) {
    $("#nuevapublicacion").addClass('big');
  }
});
}, 1000);

  if (!$(this).attr('data-draft') && !timeoutprocessform) {
    allowsendformnewpost = "no";
    $('#nuevapublicacion').attr("data-draft", "notempty");
    $.post("./pantallas/new-post/resources/create-draft.php", {
        action: "create"
      })
      .done(function(data) {
        $('#nuevapublicacion').attr("data-draft", data);
        alertdd.show('Draft created');
        allowsendformnewpost = null;
      });
  }

  var timeoutprocessform = null;
  $("#nuevapublicacion, #nuevapublicacion input, #nuevapublicacion select, #nuevapublicacion textarea").on("blur keyup paste input", function() {
    $('.newpost_container input[name=title]').val($('.newpost_container .textarea-container .textarea .writtor>.title').text());
    $('#nuevapublicacion textarea[name=text]').val(document.querySelector('.newpost_container .textarea-container .textarea .writtor>.text').innerText);
    $('#newpost-publishbutton').attr('disabled', true);
    $('#newpost-previewbutton').attr('disabled', true);

    if (!allowsendformnewpost) {
      allowsendformnewpost = "no";
      clearTimeout(timeoutprocessform);
      timeoutprocessform = setTimeout(function() {
        $('#loading').show();
        var draftid = $('#nuevapublicacion').attr('data-draft');
        var datadraft = new FormData($('#nuevapublicacion')[0]);
        datadraft.append('id', draftid);
        datadraft.append('action', 'update');

        $.ajax({
          url: './pantallas/new-post/resources/process-text.php',
          data: datadraft,
          type: "POST",
          enctype: "multipart/form-data",
          processData: false, // Important!
          contentType: false,
          cache: false,
          success: function(data) {
            eval(data);
            $('#loading').hide();
            /*alertdd.show('Draft processed');*/
          }
        });

        $.ajax({
          url: './pantallas/new-post/resources/create-draft.php',
          data: datadraft,
          type: "POST",
          enctype: "multipart/form-data",
          processData: false, // Important!
          contentType: false,
          cache: false,
          success: function(data) {
            $('#loading').hide();
            alertdd.show('Draft saved');
            $('#newpost-publishbutton').removeAttr('disabled');
            $('#newpost-previewbutton').removeAttr('disabled');
            allowsendformnewpost = null;
          }
        });

        if ($("#typed").text().length === 0) {
          $('#newpost-publishbutton').attr('disabled', true);
          $('#newpost-previewbutton').attr('disabled', true);
        }
      }, 1000);
    }
  });



  var timeoutpublishpost = null;
  $("#nuevapublicacion").submit(function(e) {
    if (!allowsendformnewpost) {
      clearTimeout(timeoutpublishpost);
      timeoutpublishpost = setTimeout(function() {
        var draftid = $('#nuevapublicacion').attr('data-draft');
        var datadraft = $('#nuevapublicacion').serializeArray();
        datadraft.push({
          name: 'id',
          value: draftid
        });
        datadraft.push({
          name: 'action',
          value: 'publish'
        });
        $.ajax({
          url: "./pantallas/new-post/resources/create-draft.php",
          data: datadraft,
          type: "POST",
          enctype: "multipart/form-data",
          processData: false, // Important!
          contentType: false,
          cache: false,
          success: function(data) {
            $('#loading').hide();
            $('.side-menu-overlay').removeClass('show');
            alertdd.show('Post created correctly');
            $('#nuevapublicacion').attr('data-draft', '');
            $('#nuevapublicacion').empty().append("<div post data-id='"+draftid+"'></div>");
          }
        });
      }, 1000);
    } else {
      alertdd.show('The post is being saved');
    }
    e.preventDefault();
  });

  var timeoutpostpreview = null;
  $(document).on("click", "#nuevapublicacion .preview", function() {
    if (!allowsendformnewpost) {
      clearTimeout(timeoutpostpreview);
      timeoutpostpreview = setTimeout(function() {
        windowdd('./pantallas/publication/index.php?post=' + $('#nuevapublicacion').attr('data-draft') + '&<?=$server_querystring;?>', 'fit');
      }, 1000);
    } else {
      alertdd.show('The post is being saved');
    }
  });


  function successnuevapublicacionform(postid) {
    $.get("./resources/feed/feeds/posts/feed.php?post=" + postid, function(data) {
      if (data) {
        if ($('#feed .block').last().attr('data-feed') == 'posts') {
          $("#feed .block").last().children('.PF-toolbar').after(data);
        } else {
          $("#feed").last().prepend("<div class='block'>" + data + "</div>");
        }
      }
    });
    openddgo('?p=publication&post=' + postid);
    alertdd.show('Publication created correctly');
    $('.side-menu-overlay').removeClass('show').removeClass('white');
    $("#nuevapublicacion").removeClass('open');
    $('#loading').hide();
    $('#nuevapublicacion .bottom .container .downbar').hide();
    $("#typed").text('');
    document.getElementById("textareapublicacion").value = "";
    $('#nuevapublicacion .content-to-share').empty().hide();
    $("#nuevapublicacion")[0].reset();
    $('#nuevapublicacion .bottom .container .downbar .delete').hide();
    $('#nuevapublicacion .bottom .container .downbar .post').attr('disabled', true);
  }
</script>