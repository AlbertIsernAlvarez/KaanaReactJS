<?php
include_once(__DIR__."/../../../../resources/url-info.php");
$links_item_url = $_GET['url'];
if($links_item_url){

  $links_query = mysqli_query($con, "SELECT * FROM links WHERE url='$links_item_url' ORDER BY date DESC LIMIT 1");
  while($row_links = mysqli_fetch_array($links_query)){
  	$links_item_url = $row_links["url"];
  	$links_item_url_domain = str_ireplace('www.', '', parse_url($links_item_url, PHP_URL_HOST));
    $links_item_title = $row_links["title"];
    $links_item_description = $row_links["description"];
    $links_item_image = $row_links["image"];
  }

  if(!$links_item_url_domain){
    $link_info = urlinfo($links_item_url);
    $links_item_title = $link_info["title"];
    $links_item_url_domain = $link_info["domain"];
    $links_item_image = $link_info["image"];
  }

  if(preg_match('/http:\/\/(www\.)*youtube\.com\/.*/',$links_item_url)){
    $links_item_embed_video = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$links_item_url);
  }
?>
<a class="PF PF-link flex shadow" href="<?=$links_item_url?>" target="_blank">
  <?php if($links_item_embed_video){
    echo $links_item_embed_video;
  } else {?>
  <div class="PF PF-image" style="background-image: url('<?=$links_item_image?>');"></div>
  <?}?>
  <div class="data">
    <div class="source"><p><?=$links_item_url_domain?></p></div>
    <h1><?=$links_item_title?></h1>
  </div>
</a>
<?}?>