<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Schendule</h1>
</div>
<form class="PF PF-form" id="update-schendule" action="./pantallas/new-post/resources/update/schendule.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<input type="date" placeholder=" " name="date">
		<span t-dd>Date</span>
	</label>
	<label class="PF-textfield filled">
		<input type="time" placeholder=" " name="time">
		<span t-dd>Time</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

$(".PF-window [name=schendule]").val(new Date().toJSON().slice(0,19));

  $("#update-schendule").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>