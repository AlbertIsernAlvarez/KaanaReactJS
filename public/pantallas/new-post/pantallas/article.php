<div class=" PF PF-form content">
  <div class="textarea-container">
    <div class="PF PF-avatar ddbg-avatar ripple" rgdd></div>
    <div class="textarea" onclick="$('#typed').focus();">
      <div class="writtor">
        <div class="title" contenteditable="true"></div>
        <div class="text" contenteditable="true" id="typed"></div>
        <div class="placeholder" t-dd>
          <?php include(__DIR__."/../resources/text-form.php"); ?>
        </div>
      </div>
      <div class="PF PF-chips sugestions" style=" padding: 0; ">
        <div class="PF PF-chip ripple" onclick="$('.newpost_container .textarea-container .textarea .writtor>.title').show();" t-dd>Add title</div>
        <div class="PF PF-chip ripple" onclick="$('#filepost').click();" t-dd>Add an image</div>
        <div class="PF PF-chip ripple" t-dd>Create poll</div>
      </div>
    </div>
  </div>
  <div class="resources"></div>
  <div class="PF PF-buttons flex">
    <button class="PF PF-button transparent ripple preview" disabled id="newpost-previewbutton">
      <div class="inside">
        <p t-dd>Preview</p>
      </div>
    </button>
    <button class="PF PF-button transparent ripple save" disabled id="newpost-savebutton">
      <div class="inside">
        <p t-dd>Save</p>
      </div>
    </button>
    <div class="space"></div>
    <div class="PF PF-icon PF-button schendule"><i class="material-icons"> access_time </i></div>
    <button class="PF PF-button ripple" onclick="$('#nuevapublicacion').submit();" disabled id="newpost-publishbutton">
      <div class="inside">
        <p t-dd>Publish</p>
      </div>
    </button>
  </div>
</div>


<div style="display: none;">
  <input type="number" name="post" value="" />
  <input type="text" name="reply" />
  <input name="files[]" accept="*;capture=camera" type="file" id="filepost" multiple>
  <input name="title" type="text" />
  <textarea type="text" name="text" id="textareapublicacion" class="textareapublicacion"></textarea>
  <input type="submit" name="submit" id="enviarnewpost" style="display:none;" />
</div>

<script>
  $('#typed').focus();

  $("#filepost").on('change',function(){

        $(".resources").empty().append("<div class='images'></div>");//you can remove this code if you want previous user input
        for(let i=0;i<this.files.length;++i){
            let filereader = new FileReader();
            let $img=jQuery.parseHTML("<img kZoom class='PF PF-image' src=''>");
            filereader.onload = function(){
                $img[0].src=this.result;
            };
            filereader.readAsDataURL(this.files[i]);
            $(".resources>.images").append($img);
        }


    });

    $("#nuevapublicacion .schendule").on('click',function(){
      var draftid = $('#nuevapublicacion').attr('data-draft');
      windowdd('./pantallas/new-post/resources/form/schendule.php?post='+draftid+'&<?=$server_querystring?>', 'fit');
    });

  <?php if(isset($post)){?>
  $("[name='post']").val('<?=$post?>');
  $('.resources').append("<div post data-id='<?=$post?>'></div>");
  <?}
  if(isset($post) && isset($action) && $action === 'reply'){?>
  $("[name='reply']").val(true);
  <?}?>
</script>