<?php if($pagina == 'new-post'){?>
<h1 class="PF" style="text-align: center;padding: 1em;background: rgb(var(--PF-color-primary)); margin: 0; color: rgb(var(--PF-color-surface));font-size: 1.5em;" t-dd >Explore the world by watching and making live broadcasts.</h1>
<?}?>
<div class="PF PF-card" <?php if($pagina == 'new-post'){?>style="max-width: 40rem;flex-direction: row;margin: .5em;position: fixed;left:0;bottom: 0;"<?}?> >
  <div class="info">
    <h1 t-dd >This function is not yet available.</h1>
    <p t-dd >Follow Kaana Developers to be updated about the latest functions.</p>
  </div>
  <div follow-button data-id="639" data-type="user" data-size="" ></div>
</div>
<video id="livewebcamvideo" autoplay style="flex: 1 auto; background-color: black; max-width: 100%;"></video>
<script>
var videoDiv = document.getElementById("livewebcamvideo"),
    vendorUrl = window.URL || window.webkitURL;
    navigator.getMedia =    navigator.getUserMedia ||
                            navigator.webkitGetUserMedia ||
                            navigator.mozGetUserMedia ||
                            navigator.oGetUserMedia ||
                            navigator.msGetUserMedia;

var MediaStream;
function captureWebcam(video, audio){
    navigator.getMedia({
        video: video,
        audio: audio
    }, function(stream){
        videoDiv.srcObject=stream;
        MediaStream = stream.getTracks()[0]; // create the stream tracker
    }, function(error){
        // An error occured
        // error.code
        console.log(error)
    }); 
}       

captureWebcam(true, false);
</script>