<html>

<head>
  <style>
    .ayuda_container {
      width: 100%;
      flex: 1;
      display: flex;
      flex-direction: column;
    }

    .ayuda_container .content {
      max-width:60rem;
      width:100%;
      margin:auto;
      flex: 1 auto;
    }
    
    .ayuda_container .content .PF-card {
      flex-basis: calc(100%/3 - 1rem);
    }
    
  </style>
</head>

<body>
  <div class="ayuda_container">
    <div class="PF PF-content full noborder center">
    <div class="container">
      <div class="PF-image" style="background-image:url('./pantallas/center/center.svg'); background-size: contain; min-height: 5em; background-color: transparent;" ></div>
    </div>
    <div class="content" >
      <div class="PF PF-grid selector" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); ">
        
        <div class="PF-card" opendd-href="?p=help" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/center/illustrations/help.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Get help</h1>
          <p t-dd >Open Help Center</p>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=contributors" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/contributors/contributors.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Contributors platform</h1>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=account">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/center/illustrations/account-center.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >My account</h1>
          <p t-dd >Open the account center</p>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=social-justice" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/social-justice/social-justice.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Social Justice</h1>
          <p t-dd >Help to make the world a better place</p>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=resource-directory" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/resource-directory/resource-directory.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Resource Directory</h1>
          <p t-dd >Find things you need, a place where they offer free food, housing, etc.</p>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=events" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/events/events.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Events</h1>
          <p t-dd>Discover events near you</p>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=donate" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/donate/donate.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Donate</h1>
          <p t-dd >Make a donation, it will go to electronic infrastructure, developers, etc ...</p>
        </div>
      </div>

    </div>
    </div>
  </div>

  </div>



</body>

</html>