<html>

<head>
	<style>
		.traductor_container {
			width: calc(100% - 1em);
			margin: .5em;
		}
		
		.traductor_container .translate_translated {
			width: 100%;
			min-height: 108px;
			display: flex;
		}
		
		.traductor_container .translate_translated .box {
			flex: 1 auto;
			width: 100%;
			min-height: 12rem;
			font-size: 2rem;
			display: flex;
			flex-direction: column;
			user-select: text;
			background-color: rgba(var(--PF-color-surface), .5);
		}
		
		.traductor_container .translate_translated .box:first-child {
			border-right: 1px solid rgba(0, 0, 0, 0.12);
		}
		
		.traductor_container .translate_translated .box:last-child {
			color: rgba(var(--PF-color-primary));
			background: rgba(var(--PF-color-primary), 0.22);
		}
		
		.traductor_container .translate_translated .box .textarea {
			padding: 1rem;
			flex: 1;
			word-break: break-word;
		}

		.traductor_container .translate_translated .box .textarea * {
			word-break: break-word;
		}
		
		.translate_zone {
			margin: auto;
			max-width: 80rem;
			border-radius: 1em;
			overflow: hidden;
			margin-top: .5em;
			position: relative;
		}
		
		.translate_zone .PF-buttons {
			padding: .5em;
			position: relative;
			z-index: 4;
			background: rgb(var(--PF-color-surface));
		}
		
		@media (max-width: 75em) {
			.traductor_container .translate_translated {
				flex-direction: column;
			}
			.traductor_container .translate_translated .box:first-child {
				border-right: 0;
				border-bottom: 1px solid rgba(0, 0, 0, 0.12);
			}
		}

		#nomorecontenttotranslate {
			display: none;
		}
	</style>
</head>

<body>

	<div class="traductor_container">

		<form class="PF shadow translate_zone" id="translate-contribute" method="post" action="./apps/translate/resources/contribute-update.php">
			<progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; z-index: 5; display: none;"></progress>
			<div class="translate_translated">
				<div class="PF box" id="nomorecontenttotranslate">
					<div class="textarea" t-dd>No more content to translate</div>
				</div>
				<div class="PF box" id="translate1">
					<div class="PF PF-toolbar">
						<h1><span t-dd>From</span> <span id="language1" language-name ></span></h1>
					</div>
					<div class="textarea" name="text" id="translate-contribute-select" style="user-select: all;"></div>
					<textarea style="display:none;" name="text" id="translate-contribute-select-text"></textarea>
					<input style="display:none;" name="id" id="translate-contribute-select-id" />
				</div>
				<div class="box">
					<div class="PF PF-toolbar">
						<h1><span t-dd>To</span> <span id="language2" language-name ></span></h1>
					</div>
					<div contenteditable="true" class="textarea" id="translate-contribute-update"></div>
					<textarea style="display:none;" name="update" id="translate-contribute-update-textarea"></textarea>
				</div>
			</div>

			<div class="PF PF-guidenav shadow">
				<a class="link link-back PF ripple" onclick="window.history.back();">
					<i class="material-icons">arrow_back</i>
					<div class="text">
						<span t-dd>Go to the previous screen</span>
						<p t-dd>Go back</p>
					</div>
				</a>
				<a class="link link-forward PF ripple" onclick="submitcontribute();">
					<i class="material-icons">arrow_forward</i>
					<div class="text">
						<span t-dd>Show more content</span>
						<p t-dd>Next</p>
					</div>
				</a>
			</div>

		</form>

		<script>
			$('#language2').text('<?=$language?>');

			$.initialize("#translate-contribute-select", function() {

				$(this).load('./apps/translate/resources/contribute-select.php?<?=$server_querystring?>');

			});

			$('#translate-contribute').ajaxForm(function(data) {

				$('#translate-contribute-select').load('./apps/translate/resources/contribute-select.php?<?=$server_querystring?>');
				$('#translate-contribute .PF-progress.loading').hide();
				eval(data);

			});

  $(document).on("keydown", function(e) {
  	if (e.keyCode == 9) {
  		e.preventDefault();
  		e.stopPropagation();
  		submitcontribute();
  	}
  });

			var timeoutcontributetranslate;
			function submitcontribute() {
				$('#translate-contribute .PF-progress.loading').show();
				$('#translate-contribute-update-textarea').val($('#translate-contribute-update').text());

				clearTimeout(timeoutcontributetranslate);
				timeoutcontributetranslate = setTimeout(function() {
					$('#translate-contribute').submit();
					$('#translate-contribute-select').empty();
					$('#translate-contribute-select-id').val('');
					$('#translate-contribute-update').empty();
					$('#translate-contribute-update-textarea').val('');
				}, 1000);
			}
		</script>

	</div>

</body>

</html>