<html>

<head>
	<style>
		.moderate_container {
			width: calc(100% - 1em);
			margin: .5em;
		}
		
		.moderate_container .moderate_moderated {
			width: 100%;
			min-height: 108px;
			display: flex;
		}
		
		.moderate_container .moderate_moderated .box {
			flex: 1 auto;
			width: 100%;
			min-height: 12rem;
			display: flex;
			flex-direction: column;
			background-color: rgba(var(--PF-color-surface), .5);
		}
		
		
		.moderate_container .moderate_moderated .box .post>.container>.content>.text {
			user-select: text;
		}
		
		.moderate_container .moderate_moderated .box .post>.container>.bottom {
			display: none;
		}
		
		.moderate_container .moderate_moderated .box:first-child {
			border-right: 1px solid rgba(0, 0, 0, 0.12);
		}
		
		.moderate_container .moderate_moderated .box:last-child {
			color: rgba(var(--PF-color-primary));
			background: rgba(var(--PF-color-primary), 0.22);
		}
		
		.moderate_container .moderate_moderated .box .textarea {
			padding: 1rem;
			flex: 1;
			word-break: break-word;
		}

		.moderate_container .moderate_moderated .box .textarea * {
			word-break: break-word;
		}
		
		.moderate_zone {
			margin: auto;
			max-width: 80rem;
			border-radius: 1em;
			overflow: hidden;
			margin-top: .5em;
			position: relative;
		}
		
		.moderate_zone .PF-buttons {
			padding: .5em;
			position: relative;
			z-index: 4;
			background: rgb(var(--PF-color-surface));
		}
		
		@media (max-width: 75em) {
			.moderate_container .moderate_moderated {
				flex-direction: column;
			}
			.moderate_container .moderate_moderated .box:first-child {
				border-right: 0;
				border-bottom: 1px solid rgba(0, 0, 0, 0.12);
			}
		}

		#nomorecontenttomoderate {
			display: none;
		}
	</style>
</head>

<body>

	<div class="moderate_container">

		<form class="PF shadow moderate_zone" id="moderate-contribute" method="post" action="./pantallas/contributors/pantallas/moderate-posts/resources/moderate-update.php">
			<progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; z-index: 5; display: none;"></progress>
			<div class="moderate_moderated">
				<div class="PF box" id="nomorecontenttomoderate">
					<div class="textarea" t-dd>No more content to moderate</div>
				</div>
				<div class="PF box" id="moderate1">
					<div class="PF PF-toolbar">
						<h1><span t-dd>Content</span></h1>
					</div>
					<div class="textarea" name="text" id="moderate-contribute-select"></div>
					<textarea style="display:none;" name="text" id="moderate-contribute-select-text"></textarea>
					<input style="display:none;" name="id" id="moderate-contribute-select-id" />
				</div>
				<div class="box">
					<div class="PF PF-toolbar">
						<h1><span t-dd>To moderate</span></h1>
					</div>
					<div contenteditable="true" class="textarea" id="moderate-contribute-update"></div>
					<textarea style="display:none;" name="update" id="moderate-contribute-update-textarea"></textarea>
				</div>
			</div>

			<div class="PF PF-guidenav shadow">
				<a class="link link-back PF ripple" onclick="window.history.back();">
					<i class="material-icons">arrow_back</i>
					<div class="text">
						<span t-dd>Go to the previous screen</span>
						<p t-dd>Go back</p>
					</div>
				</a>
				<a class="link link-forward PF ripple" onclick="submitcontribute();">
					<i class="material-icons">arrow_forward</i>
					<div class="text">
						<span t-dd>Show more content</span>
						<p t-dd>Next</p>
					</div>
				</a>
			</div>

		</form>

		<script>
			$('#language2').text('<?=$language?>');

			$.initialize("#moderate-contribute-select", function() {

				$(this).load('./pantallas/contributors/pantallas/moderate-posts/resources/moderate-select.php?<?=$server_querystring?>');

			});

			$('#moderate-contribute').ajaxForm(function(data) {

				$('#moderate-contribute-select').load('./pantallas/contributors/pantallas/moderate-posts/resources/moderate-select.php?<?=$server_querystring?>');
				$('#moderate-contribute .PF-progress.loading').hide();
				eval(data);

			});

			var timeoutcontributemoderate;
			function submitcontribute() {
				$('#moderate-contribute .PF-progress.loading').show();
				$('#moderate-contribute-update-textarea').val($('#moderate-contribute-update').text());

				clearTimeout(timeoutcontributemoderate);
				timeoutcontributemoderate = setTimeout(function() {
					$('#moderate-contribute').submit();
					$('#moderate-contribute-select').empty();
					$('#moderate-contribute-select-id').val('');
					$('#moderate-contribute-update').empty();
					$('#moderate-contribute-update-textarea').val('');
				}, 1000);
			}
		</script>

	</div>

</body>

</html>