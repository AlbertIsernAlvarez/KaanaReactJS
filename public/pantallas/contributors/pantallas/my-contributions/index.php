<ul class="PF PF-list">
<?php
$karma_contributions_list = mysqli_query($con,"SELECT * FROM karma WHERE user_id='$user_mismo_id' ORDER BY date DESC");
while($row_karma_contributions_list = mysqli_fetch_array($karma_contributions_list))
  {
    $karma_contributions_list_action = $row_karma_contributions_list['action'];
    $karma_contributions_list_date = $row_karma_contributions_list['date'];

		switch ($karma_contributions_list_action) {
    case "translate_text":
        $karma_contributions_list_title = "Translation";
        break;
    case "special":
        $karma_contributions_list_title = "Special case";
        break;
    default:
        $karma_contributions_list_title = "Not defined";
}
    
    ?>
  <li class="ripple" >
    <div class="PF PF-image circle" style="background-image: url('./pantallas/contributors/pantallas/my-contributions/icons/<?=$karma_contributions_list_action?>.svg');" ></div>
    <div class="data">
      <h1 t-dd><?=$karma_contributions_list_title?></h1>
      <p><?=$karma_contributions_list_date?></p>
    </div>
  </li>
<?}?>
</ul>