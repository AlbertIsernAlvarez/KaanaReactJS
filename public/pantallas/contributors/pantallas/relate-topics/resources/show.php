<?php

$topic_sql = mysqli_query($con,"SELECT * FROM topics WHERE LOWER(title) LIKE LOWER('$q%') ORDER BY demand DESC, id DESC, RAND() LIMIT 1");
while($row_topic_sql = mysqli_fetch_array($topic_sql))
{
	$topic_id = $row_topic_sql["id"];
	$topic_title = $row_topic_sql["title"];
	$topic_demand = $row_topic_sql["demand"];
	$topic_color = $row_topic_sql["color"];

	$json_image_topic = file_get_contents("https://" . substr($language, 0, -3) . ".wikipedia.org/w/api.php?action=query&titles=" . urlencode($topic_title) . "&prop=pageimages&format=json&pithumbsize=300");
	$json_image_topic = json_decode($json_image_topic,true);
	$topic_image = $json_image_topic['query']['pages'][key($json_image_topic['query']['pages'])]['thumbnail']['source'];
	
?>

<div class="textarea">
	<div class="PF PF-image circle ripple" style="background-image:url('<?=$topic_image?>'); background-color: rgba(var(--PF-color-primary), .2);"></div>
	<h1><?=$topic_title?></h1>
</div>

<script> $('#relatetopic-name').text('<?=$topic_title?>'); </script>

	
<?}?>