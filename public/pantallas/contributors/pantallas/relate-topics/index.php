<html>

<head>
	<style>
		.relatetopics_container {
			width: 100%;
			margin-bottom: 1em;
		}
		
		.relatetopics_container .translate_translated {
			width: 100%;
			min-height: 108px;
		}
		
		.relatetopics_container .translate_translated .box {
			flex: 1 auto;
			width: 100%;
			min-height: 5rem;
			display: flex;
			flex-direction: column;
			user-select: text;
			background-color: rgba(var(--PF-color-surface), .5);
			text-align: center;
		}
		
		.relatetopics_container .translate_translated .box .PF-image.circle {
			width: 10rem;
			height: 10rem;
			margin: auto;
			margin-bottom: .5em;
		}
		
		.relatetopics_container .translate_translated .box:first-child {
			border-right: 1px solid rgba(0, 0, 0, 0.12);
		}
		
		.relatetopics_container .translate_translated .box:last-child {
			color: rgba(var(--PF-color-primary));
			background: rgba(var(--PF-color-primary), .1);
		}
		
		.relatetopics_container .translate_translated .box .textarea {
			padding: 1rem;
			flex: 1;
		}
		
		.relatetopics_container .translate_translated .box .search_relatetopics {
			padding: .5em;
			border-radius: 1.5em;
			margin: .5em auto;
			margin-top: 1em;
			max-width: calc(100% - 1em);
			width: 15em;
			background-color: rgba(var(--PF-color-surface));
			border: none;
			font-size: 1.2rem;
			text-align: center;
		}
		
		.relatetopics_container .translate_translated .box #contribute-update-area {
			grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
		}
		
		.translate_zone {
			margin: auto;
			max-width: 80rem;
			border-radius: 1em;
			overflow: hidden;
			margin-top: .5em;
			position: relative;
		}
		
		.translate_zone .PF-buttons {
			padding: .5em;
			position: relative;
			z-index: 4;
			background: rgb(var(--PF-color-surface));
		}
		
		@media (max-width: 75em) {
			.relatetopics_container .translate_translated {
				flex-direction: column;
			}
			.relatetopics_container .translate_translated .box:first-child {
				border-right: 0;
				border-bottom: 1px solid rgba(0, 0, 0, 0.12);
			}
		}
		
		#nomorecontenttotranslate {
			display: none;
		}
	</style>
</head>

<body>

	<div class="relatetopics_container">

		<form class="PF shadow translate_zone" id="translate-contribute" method="post">
			<progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; z-index: 5; display: none;"></progress>
			<div class="translate_translated">
				<div class="PF box" id="contribute-show-area">
					<div class="textarea">
						<h1 t-dd>Loading</h1>
					</div>
				</div>
				<div class="box" id="contribute-selected-area-box" style="display:none;">
					<div class="PF PF-toolbar">
						<h1><span t-dd>Selected topics</span></h1>
					</div>
					<div class="PF PF-grid selector" id="contribute-selected-area"></div>
				</div>
				<div class="PF PF-guidenav shadow">
					<a class="link link-back PF ripple" onclick="window.history.back();">
						<i class="material-icons">arrow_back</i>
						<div class="text">
							<span t-dd>Go back</span>
							<p t-dd>Go to the previous screen</p>
						</div>
					</a>
					<a class="link link-forward PF ripple" onclick="submitcontribute();">
						<i class="material-icons">arrow_forward</i>
						<div class="text">
							<span t-dd>Show another topic to relate</span>
							<p t-dd>Submit and next</p>
						</div>
					</a>
				</div>
				<div class="box">
					<div class="PF PF-toolbar">
						<h1><span t-dd>Search and select topics related to</span> <span id="relatetopic-name"></span></h1>
					</div>
					<input class="PF search_relatetopics shadow" placeholder="Search topics" t-dd/>
					<div class="PF PF-grid selector" id="contribute-update-area"></div>
				</div>
			</div>
		</form>

	</div>

	<script>
		$('#contribute-show-area').load('./pantallas/contributors/pantallas/relate-topics/resources/show.php');
		$('#contribute-update-area').load('./pantallas/contributors/pantallas/relate-topics/resources/search.php');

		$(document).on("click", "#contribute-update-area .PF-card", function() {
			$('#contribute-update-area')
		});
		
	</script>

</body>

</html>