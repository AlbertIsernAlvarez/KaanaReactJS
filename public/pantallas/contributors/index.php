<html>

<head>
  <style>
    .contributors_container {
      width: 100%;
      margin: auto;
    }

    .contributors_container .contributors_top {
      width: 100%;
      background-position: 50% 10px;
      background-repeat: no-repeat;
      margin-top: 24px;
      margin-bottom: .5em;
    }

    .contributors_container .contributors_top h1 {
      font-size: 25px;
      padding: 0;
      font-weight: 500;
      text-align: center;
    }
    
    .contributors_container .contributors_top p {
      font-size: 20px;
      padding: 0;
      font-weight: 400;
      text-align: center;
    }

    .contributors_container .contributors_top .contributors_logo {
    width: fit-content;
    height: fit-content;
    margin: auto;
    margin-bottom: 1em;
    padding: .5em .8em;
    background-color: rgba(var(--PF-color-surface));
    border-radius: 28px;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .26);
    box-sizing: border-box;
    display: flex;
    }

    .contributors_container .content {
      max-width:60rem;
      width:100%;
      margin:auto;
    }
    
    .contributors_container .content .PF-card {
      flex-basis: calc(100%/3 - 1rem);
    }

    .contributors_container .content .PF-grid.selector .PF-card .PF-image {
      padding-bottom: 70%;
    }

    .contributors_container .content .PF-grid .PF-card .info p {
      -webkit-line-clamp: initial;
    }
    
  </style>
</head>

<body>

  <?php
    $contribute = $_GET['contribute'];
    switch ($contribute) {
      case "relate-topics":
        $contribute_title = "Relate topics";
        break;
      case "moderate-posts":
        $contribute_title = "Moderate";
        break;
      case "content-researcher":
        $contribute_title = "Content researcher";
        break;
      case "translate":
        $contribute_title = "Translate content";
        break;
      case "my-contributions":
        $contribute_title = "Your Contributions";
        break;
      default:
        $contribute_title = "Contributors platform";
        $contribute = null;
}
  ?>
  
  <div class="contributors_container">
    <div class="contributors_top">
      <div>
        <div class="PF contributors_logo ripple"><p><span id="contribute-karma"><?=$user_mismo_karma?></span> Karma</p></div>
        <h1 t-dd><?=$contribute_title?></h1>
      </div>
    </div>

    <div class="content" >
      <?php if(!$contribute){?>
      <div class="PF PF-grid selector" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); ">

      <div class="PF-card" opendd-href="?p=contributors&contribute=translate">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/contributors/translations.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Translations</h1>
          <p t-dd >Teach Kaana Translate how to speak your language better. It's easy and fun.</p>
        </div>
      </div>

      <!-----<div class="PF-card" opendd-href="?p=contributors&contribute=relate-topics" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/contributors/relate-topics.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Relate topics</h1>
          <p t-dd >Teach Kaana to learn the meaning of each topic.</p>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=new-post&action=article&ref=contributors" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/contributors/writter.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Writters</h1>
          <p t-dd >Write articles for the community. Ideas and perspectives we won't find anywhere else.</p>
        </div>
      </div>----->

      <div class="PF-card" opendd-href="?p=contributors&contribute=my-contributions" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/contributors/contentresearch.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >My contributions</h1>
          <p t-dd >See your last contributions</p>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=contributors&contribute=my-contributions" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/contributors/contentresearch.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Common funds</h1>
        </div>
      </div>

      <div class="PF-card" opendd-href="?p=donate" >
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/donate/donate.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Donate</h1>
          <p t-dd >Make a donation, it will go to electronic infrastructure, developers, etc ...</p>
        </div>
      </div>

    </div>
    <?} else {
      include_once(__DIR__.'/pantallas/'.$contribute."/index.php");
    }?>
    </div>

  </div>

<script>
  function updatecontributekarma(){
    $('#contribute-karma').load('./pantallas/contributors/resources/karma.php');
  }
</script>

</body>

</html>