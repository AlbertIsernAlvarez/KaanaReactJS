<div class="PF PF-grid selector center" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); ">

<?php
$help = mysqli_query($con,"SELECT * FROM help_topics WHERE language='$language' ORDER BY demand DESC, date DESC, id DESC LIMIT 10");
while($row_help_helps = mysqli_fetch_array($help))
  {
  $help_helps_id = $row_help_helps['id'];
  $help_helps_title = $row_help_helps['title'];
  $help_helps_description = $row_help_helps['description'];
  $help_helps_image = $row_help_helps['image'];
?>
<div class="PF-card" opendd-href="?p=help&topic=<?=$help_helps_id?>">
    <div class="PF-image ripple" style="background-image:url('./pantallas/help/images/<?=$help_helps_image?>.svg');background-size: contain;background-repeat: no-repeat;"></div>
    <div class="info">
      <h1><?=$help_helps_title?></h1>
    </div>
  </div>
<?}?>

  <div class="PF-card" opendd-href="?p=bugs-report">
    <div class="PF-image ripple" style="background-image:url('./pantallas/bugs-report/bugs-report.svg');background-size: contain;background-repeat: no-repeat;"></div>
    <div class="info">
      <h1>Errors, bugs and feedback</h1>
    </div>
  </div>

  <div class="PF-card" opendd-href="?p=donate">
    <div class="PF-image ripple" style="background-image:url('./pantallas/donate/donate.svg');background-size: contain;background-repeat: no-repeat;"></div>
    <div class="info">
      <h1>Donate</h1>
    </div>
  </div>

  <div class="PF-card" opendd-href="?p=team">
    <div class="PF-image ripple" style="background-image:url('./pantallas/team/team.png?w=500');background-size: contain;background-repeat: no-repeat;"></div>
    <div class="info">
      <h1>Kaana Team</h1>
    </div>
  </div>

</div>