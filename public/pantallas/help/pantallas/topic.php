<ul class="PF PF-collapsible">
  
<?php
$help = mysqli_query($con,"SELECT * FROM help WHERE id='$help_topic' AND text='' ORDER BY id DESC");
while($row_help_helps = mysqli_fetch_array($help))
  {
  $help_helps_id = $row_help_helps['id'];
  $help_helps_title = $row_help_helps['title'];
  $help_helps_text = $row_help_helps['text'];
  $help_helps_icon = $row_help_helps['icon'];
?>
  
  <li>
    <div class="collapsible-header">
        <div class="data" >
          <p><?=$help_helps_title?></p>
        </div>
    </div>
      <div class="collapsible-body" style="display: none;">
      <ul class="PF PF-collection">
        <?php $help_article = mysqli_query($con,"SELECT * FROM help WHERE topic='$help_helps_id' ORDER BY id DESC");
while($row_help_article = mysqli_fetch_array($help_article))
  {
  $help_article_id = $row_help_article['id'];
  $help_article_title = $row_help_article['title'];
  $help_article_text = $row_help_article['text'];
  $help_article_icon = $row_help_article['icon'];
  ?>
    <li class="collection-item ripple" opendd-href="?p=help&id=<?=$help_article_id?>">
      <?php if($help_article_icon){?><div class="PF PF-icon ripple"><i class="material-icons"><?=$help_article_icon?></i></div><?}?>
      <div class="data" >
        <p><?=$help_article_title?></p>
      </div>
    </li>
        <?}?>
  </ul>
      </div>
    </li>

<?}?>
  
  </ul>