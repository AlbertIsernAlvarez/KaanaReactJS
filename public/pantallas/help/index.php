<html>

<head>
  <style>
  
    .help_container {
    width: 100%;
    flex: 1 auto;
    background-image: url(//kaana.io/pantallas/help/cover.svg);
    background-position: center top 5em;
    background-repeat: no-repeat;
    background-size: auto 23vw;
    background-attachment: fixed;
    display: flex;
    flex-direction: column;
    transition: .2s;
    }

    @media (max-width: 45em) {
      .help_container {
    background-size: auto 33vw;
    }
    }

    .help_container .help_top {
      width: 100%;
      height: fit-content;
      padding-top: 5em;
      display: flex;
      flex-direction: column;
      padding-bottom: 4%;
      text-align: center;
    }

    .help_container .help_top h1 {
      font-size: 32px;
      line-height: 40px;
      margin: auto;
      /* margin-bottom: 26px; */
      padding: 0;
      color: rgb(var(--PF-color-on-surface));
    }

    .help_container .help_top h1 .help_logo {
      height: 56px;
      margin: 0 auto 18px auto;
      padding: 8px;
      width: 56px;
      background: #fff;
      border-radius: 28px;
      box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .26);
      box-sizing: border-box;
      display: flex;
      background-image: url('//img.kaana.io/logo/logo.svg');
      background-size: 3rem;
      background-position: center;
      background-repeat: no-repeat;
    }

    .help_container .help_top .search {
    background-color: rgb(var(--PF-color-surface));
    border-radius: 8px;
    box-shadow: 0px 1px 3px 1px rgba(60, 64, 67, .15);
    -webkit-appearance: none;
    border: none;
    box-sizing: border-box;
    color: rgba(var(--PF-color-on-surface), .8);
    display: flex;
    font-size: 16px;
    padding: .2em 0.5em;
    width: 520px;
    max-width: calc(100% - 1.5em);
    margin: auto;
    border-radius: 2em;
    margin-top: 1em;
    }

    .help_container .help_top .search .text {
      flex: 1 auto;
      border: none;
      font-size: 16px;
      background-color: transparent;
      color: rgba(var(--PF-color-on-surface), .9);
    }
    
    .help_container .content {
    max-width: 60rem;
    width: 100%;
    margin: .5em auto;
    /* flex: 1 auto; */
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    }
    
    .help_container .content>h1 {
      padding: 0 .5em;
    }
    
    .help_container .content .PF-collection {
    margin: auto;
    flex: 1 auto;
    }
    
    
    .center_logo.d-m {
      display:none;
    }
    
  </style>
</head>

<body>
  <div class="help_container">
    
    <?php
    $help_id = $_GET['id'];
    $help_topic = $_GET['topic'];
    
    if($help_topic){
      $help_page = "topic";
    } elseif($help_id){
      $help_page = "article";
    } else {
      $help_page = "start";
    }
    
    ?>
    
    <div class="help_top">
      <h1>
        <div class="PF help_logo ripple opendd" opendd-href="/"></div>
        <div t-dd>How can we help you?</div>
      </h1>
      <div class="search" >
        <div class="PF PF-icon ripple"><i class="material-icons">search</i></div>
        <input class="text" contenteditable="true" placeholder="Search" id="helpsearch" t-dd />
      </div>
    </div>

    <div class="content" >
      <div class="PF PF-card">
      <div class="info">
        <p t-dd>We have yet to write many helpful articles, you may experience a great lack of content.</p>
      </div>
    </div>
      <?php if($help_page != 'start'){?><button class="PF PF-button" onclick="window.history.back();" t-dd>Back</button><?}?>
      <?php include_once(__DIR__."/pantallas/$help_page.php"); ?>
    </div>

  </div>

<script>
  $('.header .buscador').addClass('hidded');

  $("#helpsearch").on("keyup paste", function(e) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13') {
        openddgo("?p=search&q=" + $(this).val() + "&section=help");
      }
      return false;
      e.preventDefault();
    });
</script>

</body>

</html>