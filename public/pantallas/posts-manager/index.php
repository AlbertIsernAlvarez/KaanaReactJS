<html>

<head>
  <style>
  
    
    .feed {
          width: 100%;
    max-width: 96rem;
    column-count: auto;
    column-width: 30rem;
      margin: auto;
    }
    
    .PF-content {
      height:15em;
      position: sticky;
      top:64px;
      left: 0;
      transition: 0s;
    }
    
    .PF-content, .PF-content * {
      transition: 0s!important;
    }
    
  </style>
</head>

<body>


<div>
  <div class="PF PF-content full noborder center">
    <div class="container">
      <div class="PF-image" style="background-image:url('./pantallas/<?=$pagina?>/<?=$pagina?>.svg');  background-size: contain; min-height: 10em; background-color: transparent;"></div>
      <h1 t-dd >Posts Manager</h1>
    </div>
  </div>
  </div>

  <div class="PF-tabs">
    <div class="PF PF-tabbar shadow">
      <div class="container">
        <ul>
          <li class="ripple active" data-for="postmanagertabs-1" data-taburl="./pantallas/<?=$pagina?>/pantallas/all.php" ><span t-dd >All</span></li>
          <li class="ripple" data-for="postmanagertabs-2" data-taburl="./pantallas/<?=$pagina?>/pantallas/drafts.php" ><span t-dd >Drafts</span></li>
          <li class="ripple" data-for="postmanagertabs-3" data-taburl="./pantallas/<?=$pagina?>/pantallas/private.php" ><span t-dd >Private</span></li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
    <div class="tabs">
      <div class="tab" data-name="postmanagertabs-1"></div>
      <div class="tab" data-name="postmanagertabs-2"></div>
      <div class="tab" data-name="postmanagertabs-3"></div>
    </div>
  </div>
  
  <script>
  
    $('.header .logo span#titlepage').text('Posts Manager').removeClass('translated');
    $('body').addClass('PFC-red');
    
  </script>

</body>

</html>