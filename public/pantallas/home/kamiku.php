<html>

<head>
  <style>
    .publicaciones_kamiku {
    width: 100%;
    height: auto;
    background: white;
    border-radius: 1em;
    }

    .publicaciones_kamiku .block {
      width: 100%;
      background: linear-gradient(to top, var(--PF-color-bg-first-default), var(--PF-color-default) 90%);
      border: 1px solid #e0e0e0;
      text-align: center;
      border-radius: 20px;
      margin-bottom: 1rem;
    }

    .publicaciones_kamiku .block .head {
      padding-top: 1rem;
      padding-bottom: 1rem;
    }

    .publicaciones_kamiku .block .head img {
      background: white;
      border-radius: 100px;
      padding: 0.5rem;
    }

    .publicaciones_kamiku .block .head h1 {
      font-size: 28px;
      line-height: 40px;
      padding-right: 0;
      padding-left: 0;
      padding-bottom: 4px;
      padding-top: 17px;
      vertical-align: top;
      color: white;
      font-weight: 300;
      font-family: Roboto, Arial, arial, sans-serif;
    }

    .publicaciones_kamiku .block .head p {
      font-size: 14px;
      line-height: 20px;
      padding-right: 0;
      padding-left: 3px;
      padding-bottom: 0px;
      padding-top: 0px;
      color: #eee;
      vertical-align: top;
      font-family: Roboto, Arial, arial, sans-serif;
      font-weight: 400;
      text-align: center;
    }

    .publicaciones_kamiku .block .bigimage {
      width: 100%;
      height: auto;
      background: #ddd;
    }

    /*CONVERSACION*/

    .publicaciones_kamiku .block .conversacion .mensaje {
      width: calc(100% - 2rem);
      padding: 0;
      padding-left: 1rem;
      padding-right: 1rem;
      position: relative;
      margin-top: 3rem;
    }

    .publicaciones_kamiku .block .conversacion .mensaje .kmk {
      position: absolute;
      top: -2rem;
      left: 1rem;
      padding: 1.5rem;
      background: url(http://user.kaana.io/avatares/518/kamikumini.png);
      background-repeat: no-repeat;
      background-size: 2.5rem;
      background-color: white;
      border-radius: 50px;
      background-position: center;
    }

    .publicaciones_kamiku .block .conversacion .mensaje.user {
      text-align: right;
    }

    .publicaciones_kamiku .block .conversacion .mensaje.kamiku_conversacion {
      text-align: left;
    }

    .publicaciones_kamiku .block .conversacion .mensaje .bubble {
      -webkit-text-size-adjust: none;
      background-color: #e0e0e0;
      border: 0;
      border-collapse: collapse;
      border-radius: 2px;
      border-spacing: 0;
      color: #212121;
      display: inline-block;
      font-size: 15px;
      font-weight: 400;
      margin: 0;
      mso-hide: all;
      text-align: left;
      text-decoration: none;
      border-radius: 15px;
      mso-line-height-rule: exactly;
      padding: 14px 20px;
      font-family: Product Sans, Roboto, Arial, arial, sans-serif;
      position: relative;
      margin-bottom: 0.7rem;
    }

    .publicaciones_kamiku .block .conversacion .mensaje.kamiku_conversacion .bubble {
      background-color: white;
      /*border:1.1px solid #e0e0e0;*/
      box-shadow: 0 1px 1.5px 0 rgba(0, 0, 0, .12), 0 1px 1px 0 rgba(0, 0, 0, .24);
    }

    .publicaciones_kamiku .block .conversacion .mensaje.kamiku_conversacion .bubble.first {
      margin-top: 2rem;
    }

    .publicaciones_kamiku .block .conversacion .mensaje.kamiku_conversacion .bubble.first:before {
      content: " ";
      position: absolute;
      top: -1.5rem;
      left: 1.5rem;
      width: 0;
      height: 0;
      border: .8rem solid transparent;
      border-color: transparent transparent white transparent;
    }

    .publicaciones_kamiku .block .conversacion .mensaje.kamiku_conversacion .bubble.image {
      padding: 0;
      border: none;
      background: transparent;
      border-radius: 0;
      box-shadow: none;
    }

    .publicaciones_kamiku .block .conversacion .mensaje.kamiku_conversacion .bubble.image img {
      width: auto;
      max-width: 100%;
      margin: 0;
    }
  </style>
</head>

<body>

  <div class="publicaciones_kamiku">

    <div class="block">

      <div class="head">
        <img src="http://user.kaana.io/avatares/518/kamikumini.png" alt="Kaana Assistant" width="52" style="width: 52px;  border:0">
        <h1>Find new ways to adventure</h1>
        <p>To get started, just touch the Kamiku button</p>
      </div>

      <!-------<img class="bigimage" src="http://payload206.cargocollective.com/1/4/137897/6428142/google_19-03.jpg" />------->

      <div class="conversacion">
        <div class="mensaje kamiku_conversacion">
          <div class="kmk"></div>
          <div class="bubble first">
            <p>Hi, how can I help!? </p>
          </div>
        </div>
        <div class="mensaje user">
          <div class="bubble" onclick="verkamikupreguntar('What is the weather in San Francisco?');">
            <p>What is the weather in San Francisco?</p>
          </div>
        </div>
        <div class="mensaje kamiku_conversacion">
          <div class="kmk"></div>
          <div class="bubble first">
            <p>This is the weather in San Francisco</p>
          </div>
          <div class="bubble image">
            <img src="https://hsto.org/storage2/213/e2f/ea2/213e2fea2046987cc8cbedf27e24840a.png">
          </div>
        </div>
      </div>

    </div>

    <div class="block">
      <div class="conversacion">
        <div class="mensaje kamiku_conversacion">
          <div class="kmk"></div>
          <div class="bubble first">
            <p>Hi, how can I help!? </p>
          </div>
        </div>
        <div class="mensaje user">
          <div class="bubble" onclick="verkamikupreguntar('Play some music');">
            <p>Play some music</p>
          </div>
        </div>
        <div class="mensaje kamiku_conversacion">
          <div class="kmk"></div>
          <div class="bubble first">
            <p>Ok, playing! </p>
          </div>
          <div class="bubble image">
            <img src="https://assistant.google.com/static/images/platforms/phones/entertainment.png">
          </div>
        </div>
      </div>
    </div>

    <div class="block">
      <div class="conversacion">
        <div class="mensaje kamiku_conversacion">
          <div class="kmk"></div>
          <div class="bubble first">
            <p>Hi, how can I help!? </p>
          </div>
        </div>
        <div class="mensaje user">
          <div class="bubble" onclick="verkamikupreguntar('Who invented sushi?');">
            <p>Who invented sushi?</p>
          </div>
        </div>
        <div class="mensaje kamiku_conversacion">
          <div class="kmk"></div>
          <div class="bubble first">
            <p>Sushi was invented by Hanaya Yohei</p>
          </div>
          <div class="bubble image">
            <img src="https://assistant.google.com/static/images/conversation/sushi-2.jpg">
          </div>
        </div>
      </div>
    </div>

  </div>

</body>

</html>