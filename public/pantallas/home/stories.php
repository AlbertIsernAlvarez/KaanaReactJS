<html><head>
<style>
	
	.stories.carousel{
		white-space:nowrap;
		overflow:auto;
		-webkit-overflow-scrolling:touch;
		overflow-scrolling:touch;
	}
	
	.stories.carousel::-webkit-scrollbar {
		display:none;
	}
	
	.stories.carousel .story {
		display:inline-block;
		width:4rem;
		max-width:90px;
		margin:5px 6px;
	}
	
	.stories.carousel .story:first-child{
		margin-left:0;
	}
	
	.stories.carousel .story:last-child{
		margin-right:0;
	}
	
	.stories.carousel .story > a {
		text-align:center;
		display:block;
	}
	
	.stories.carousel .story > a:active > .img {
		transform: scale(0.9);
	}
	
	.stories.carousel .story > a > .img {
    display: block;
    box-sizing: border-box;
    font-size: 0;
    width: 4rem;
    height: 4rem;
    overflow: hidden;
    transition: transform 0.2s;
	}
	
	.stories.carousel .story > a > .img > * {
		display:block;
		box-sizing:border-box;
		height:100%;
		width:100%;
		background-size:cover;
		background-position: center;
	}
	
	.stories.carousel .story > a > .info {
		display:inline-block;
		margin-top:3px;
		line-height:1.2em;
		width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
	}
	
	.stories.carousel .story > a > .info strong {
		font-weight:900;
		font-size:12px;
	}
	
	.stories.carousel .story > a > .info .time {
		display: none;
	}
	
	.stories.carousel .story > .items {
		display:none;
	}
	
	.stories.list{
		white-space:nowrap;
		overflow:auto;
	}
	
	.stories.list .story{
		display: block;
		width: auto;
		margin: 6px;
		padding-bottom:6px;
	}
	
	.stories.list .story > a {
		text-align:left;
		display:block;
	}
	
	.stories.list .story > a > .img {
		height:42px;
		width:42px;
		max-width:42px;
		margin-right: 12px;
		vertical-align: top;
		display:inline-block;
		box-sizing:border-box;
		font-size:0;
		overflow:hidden;
	}
	
	.stories.list .story > a > .img > * {
		display:block;
		box-sizing:border-box;
		height:100%;
		width:100%;
		background-size:cover;
		background-position: center;
	}
	
	.stories.list .story > a > .info {
		display:inline-block;
		line-height:1.6em;
		overflow:hidden;
		text-overflow:ellipsis;
		vertical-align: top;
	}
	
	.stories.list .story > a > .info strong {
		font-weight:500;
		display: block;
	}
	
	.stories.list .story > a > .info .time {
		display: inline-block;
	}
	
	.stories.list .story > .items {
		display:none;
	}
	
	.stories.snapgram .story > a {
		text-decoration:none;
		color:#333;
	}
	
	.stories.snapgram .story > a > .img {
		border-radius:50%;
		padding:2px;
		background:radial-gradient(ellipse at 70% 70%,#ee583f 8%,#d92d77 42%,#bd3381 58%);
	}
	
.stories.snapgram .story > a > .img > * {
	border-radius: 50%;
	border:3px solid #fff;
	}
	
	.stories.snapgram .story.seen{
		/*opacity:.75;*/
	}
	
	.stories.snapgram .story.seen > a > .img {
		background:#999;
	}
	
	.stories.snapgram .story.seen > a{
		color:#999!important;
	}
	
	.stories.vemdezap .story > a {
		text-decoration:none;
		color:#333;
	}
	
	.stories.vemdezap .story > a > .img {
		border-radius:50%;
		padding:2px;
		background:#00bfa5;
	}
	
	.stories.vemdezap .story > a > .img > * {
		border-radius: 50%;
		border:3px solid #fff;
	}
	
	.stories.list.vemdezap .story{
		border-bottom:1px solid #eee;
	}
	
	.stories.list.vemdezap .story:last-child{
		border-bottom:0;
	}
	
	.stories.list.vemdezap .story > a > .info .time {
		display: inline-block;
		color:#999;
	}
	
	.stories.vemdezap .story.seem > img {
		opacity:.75;
	}
	
	.stories.vemdezap .story.seem > a > .img {
		background:#999;
	}
	
	.stories.vemdezap .story.seem > a{
		color:#999!important;
	}
	
	.stories.facesnap .story > a {
		text-decoration:none;
		color:#333;
	}
	
	.stories.facesnap .story > a > .img {
		border-radius:50px;
		padding:2px;
		background:black;
		margin:auto;
	}
	
	.stories.facesnap .story > a > .img > * {
		border-radius: 50%;
		border:3px solid #fff;
	}
	
	.stories.facesnap .story.seen{
		opacity:.5;
	}
	
	.stories.facesnap .story.seen > a > .img {
		background:#999;
	}
	
	.stories.snapssenger.list .story > a {
		text-decoration:none;
		color:#333;
	}
	
	.stories.snapssenger.list .story > a > .img {
		border-radius:50%;
		padding:2px;
		background:#0084FF;
	}
	
	.stories.snapssenger.list .story > a > .img > * {
		border-radius: 50%;
		border:3px solid #fff;
	}
	
	.stories.snapssenger.list .story.seen{
		opacity:.75;
	}
	
	.stories.snapssenger.list .story.seen > a > span{
		background:#999;
		color:#999;
	}
	
	.stories.snapssenger.list .story.seen > a{
		color:#999!important
	}
	
	.stories.snapssenger.carousel .story {
		max-height: 100px;
		max-width: 100px;
		width: 25vw;
	}

	.stories.snapssenger.carousel .story > a {
		text-decoration:none;
		text-align: left; 
		color:#fff;
		position: relative;
		max-height: 100px;
	}
	
	.stories.snapssenger.carousel .story > a > .img {
		height: 18vh;
	}
	
	.stories.snapssenger.carousel .story > a > .img > * {
		border-radius: 5px;
		position: absolute;
	}
	
	.stories.snapssenger.carousel .story > a > .info {
		top: auto;
		height: auto;
		box-sizing: border-box;
		position: absolute;
		left:0;
		right:0;
		bottom:0;
		padding:0.5rem;
		font-weight: 700;
		font-size:12px;
		text-shadow:1px 1px 1px rgba(0,0,0,0.35), 1px 0 1px rgba(0,0,0,0.35);
	}
	
	.stories.snapssenger.carousel .story > a > .info strong {
		font-weight: 500;
	}
	
	.stories.snapssenger.carousel .story > a > .info .time {
		display: block;
		font-weight: 100;
		font-size:11px;
	}

	.stories.snapssenger .story.seen {
		opacity:0.5;
	}
	
	@-webkit-keyframes ddstoriesSlideTime {
		0%{max-width: 0;}
		100%{max-width:100%;}
	}
	
	@keyframes ddstoriesSlideTime {
		0%{max-width: 0;}
		100%{max-width:100%;}
	}
	
	@-webkit-keyframes ddstoriesLoading {
		0% {-webkit-transform: rotate(0deg);transform: rotate(0deg);}
		100% {-webkit-transform: rotate(360deg);transform: rotate(360deg);}
	}
	
	@keyframes ddstoriesLoading {
		0% {-webkit-transform: rotate(0deg);transform: rotate(0deg);}
		100% {-webkit-transform: rotate(360deg);transform: rotate(360deg);}
	}
	
	#ddstories-modal{
		outline: 0!important;
		overflow:hidden;
		position:fixed;
		background:rgba(0,0,0,0.75);
		z-index:100000;
		font-size:14px;
		font-family:inherit;
	}
	
	#ddstories-modal-content, #ddstories-modal-content .story-viewer, #ddstories-modal-content .story-viewer > .slides, #ddstories-modal-content .story-viewer > .slides > *{
		width:100vw;
		height:100%; 
		top: 0; 
		bottom: 0;
		position: absolute;
		overflow:hidden;
	}
	
	#ddstories-modal * {
		user-select:none;
		outline: 0;
	}
	
	#ddstories-modal.with-effects {
		transform: scale(0.01);
		transform-origin: center;
		transition: 0.25s;
		position: fixed;
		top:0;
		left:0;
		width:100vw;
		height:100%; 
		top: 0; 
		bottom: 0;
	}
	
	#ddstories-modal.with-effects.animated {
		transform:scale(1);
		border-radius: 0; 
		margin-top:0 !important;
		margin-left:0 !important;
	}
	
	#ddstories-modal.with-effects.closed {
		transform:translateY(100%);
	}
	
	#ddstories-modal .slider {
		width:300vw;
		height: 100%; 
		top: 0; 
		bottom: 0;
		left:-100vw;
		position: absolute;
	}
	
	#ddstories-modal .slider > * {
		width: 100vw;
		height: 100%;
		top: 0;
		bottom: 0;
		position: absolute;
	}
	
	#ddstories-modal .slider > .previous {
		left:0;
	}
	
	#ddstories-modal .slider > .viewing {
		left:100vw;
	}
	
	#ddstories-modal .slider > .next {
		left:200vw;
	}
	
	#ddstories-modal .slider.animated{
		-webkit-transition:-webkit-transform .25s linear;
		transition:-webkit-transform .25s linear;
		transition:transform .25s linear;
		transition:transform .25s linear, -webkit-transform .25s linear;
	}
	
	#ddstories-modal.with-cube #ddstories-modal-content {
		perspective: 1000vw;
		transform:scale(0.95);
		perspective-origin: 50% 50%;
		overflow: visible;
		transition:0.3s;
	}
	
	#ddstories-modal.with-cube .slider {
		transform-style: preserve-3d;
		transform: rotateY(0deg);
	}
	
	#ddstories-modal.with-cube .slider > .previous {
		backface-visibility: hidden;
		left: 100vw;
		transform: rotateY(270deg) translateX(-50%);
		transform-origin: center left;
	}
	
	#ddstories-modal.with-cube .slider > .viewing {
		backface-visibility: hidden;
		left: 100vw;
		transform: translateZ(50vw);
	}
	
	#ddstories-modal.with-cube .slider > .next {
		backface-visibility: hidden;
		left:100vw;
		transform: rotateY(-270deg) translateX(50%);
		transform-origin: top right;
	}

	#ddstories-modal-content .story-viewer.paused.longPress .head,#ddstories-modal-content .story-viewer.paused.longPress .slides-pointers,#ddstories-modal-content .story-viewer.paused.longPress .tip{
		opacity:0;
	}
#ddstories-modal-content .story-viewer.viewing:not(.paused):not(.stopped) .slides-pointers > * > .active > b{
	-webkit-animation-play-state:running;
	animation-play-state:running;
	}
	
	#ddstories-modal-content .story-viewer.next{
		z-index:10;
	}
	
	#ddstories-modal-content .story-viewer.viewing{
		z-index:5;
	}
	
	#ddstories-modal-content .story-viewer.previous{
		z-index:0;
	}
	
	#ddstories-modal-content .story-viewer.muted .tip.muted, #ddstories-modal-content .story-viewer.loading .head .loading {
		display: block;
	}
	
	#ddstories-modal-content .story-viewer.loading .head .right .time, #ddstories-modal-content .story-viewer.loading .head .right .close {
		display: none;
	}

	#ddstories-modal-content .story-viewer .slides-pointers{
    display: table;
    table-layout: fixed;
    border-spacing: 6px;
    border-collapse: separate;
    position: absolute;
    width: 100vw;
    top: 0;
    left: 0;
		right:0;
    z-index: 100020;
	  margin:0 auto;
	}
	
	#ddstories-modal-content .story-viewer .slides-pointers > *{
		display:table-row;
	}
	
	#ddstories-modal-content .story-viewer .slides-pointers > * > *{
		display:table-cell;
		background:rgba(255,255,255,0.5);
		border-radius:2px;
	}
	
	#ddstories-modal-content .story-viewer .slides-pointers > * > .seen{
		background:#fff;
	}
	
	#ddstories-modal-content .story-viewer .slides-pointers > * > * > b{
		background:#fff;
		width:auto;
		max-width:0;
		height:2px;
		display:block;
		-webkit-animation-fill-mode:forwards;
		animation-fill-mode:forwards;
		-webkit-animation-play-state:paused;
		animation-play-state:paused;
		border-radius:2px;
	}
	
	#ddstories-modal-content .story-viewer .slides-pointers > * > .active > b{
		-webkit-animation-name:ddstoriesSlideTime;
		animation-name:ddstoriesSlideTime;
		-webkit-animation-timing-function:linear;
		animation-timing-function:linear;
	}
	
	#ddstories-modal-content .story-viewer .head{
		position:absolute;
		display:inline-flex;
		align-items:center;
		left:0;
		width:calc(100% - 1rem);
		z-index:100010;
		color:#fff;
		font-size:14px;
		text-shadow:1px 1px 1px rgba(0,0,0,0.35), 1px 0 1px rgba(0,0,0,0.35);
		padding:0.5rem;
		margin-top:0.5rem;
	}
	
	#ddstories-modal-content .story-viewer .head .img{
		vertical-align: top;
		background-size:cover;
		width:42px;
		height:42px;
		display:inline-block;
		margin-right:12px;
		border-radius:50%;
		vertical-align:middle;
		background-repeat:no-repeat;
		background-position:center;
	}
	
	#ddstories-modal-content .story-viewer .head .img:before{
		content:'';
		display:block;
		width:100%;
		height:100%;
		border-radius:50%;
		border:1px solid #000;
		opacity:0.1;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=10)";
		box-sizing:border-box;
	}
	
	#ddstories-modal-content .story-viewer .head .time{
		opacity:0.75;
		font-weight:900;
		font-size:13px;
	}
	
	#ddstories-modal-content .story-viewer .head .left{
		line-height: 1 !important;
		display:inline-block;
		margin:0;
		width:100%;
	}
	
	#ddstories-modal-content .story-viewer .head .left > div{
		display:inline-block;
		max-width:30vw;
		vertical-align:middle;
	}
	
	#ddstories-modal-content .story-viewer .head .left > div > *{
		width:100%;
		display:inline-block;
		line-height:21px;
	}
	
	#ddstories-modal-content .story-viewer .head .left > div > strong {
		font-weight: 900;
	}
	
	#ddstories-modal-content .story-viewer .head .right{
	}
	
	#ddstories-modal-content .story-viewer .head .right .close, #ddstories-modal-content .story-viewer .head .back {
		font-size:2rem;
		padding:0.5rem 1.5rem;
		cursor: pointer;
		text-align:center;
	}
	
	#ddstories-modal-content .story-viewer .head .left > .back {
		display: none; 
		width:24px; 
		margin:-6px -6px 0 -6px;
	}
	
	#ddstories-modal-content .story-viewer .head .right .time {
		display:none;
	}
	
	#ddstories-modal-content .story-viewer .head .loading {
		display:none;
		border-radius:50%;
		width:30px;
		height:30px;
		border:4px solid rgba(255,255,255,0.2);
		box-sizing: border-box;
		border-top-color:#FFF;
		-webkit-animation:ddstoriesLoading 1s infinite linear;
		animation:ddstoriesLoading 1s infinite linear;
	}
	
	#ddstories-modal-content .story-viewer .head, #ddstories-modal-content .story-viewer .slides-pointers, #ddstories-modal-content .story-viewer .tip{
		-webkit-transition:opacity .5s;
		transition:opacity .5s;
	}
	
	#ddstories-modal-content .story-viewer .slides .item{
		display:none;
		overflow:hidden;
		background:#000;
	}
	
	#ddstories-modal-content .story-viewer .slides .item:before{
		z-index: 4;
		background: transparent;
		content:'';
		position: absolute;
		left:0;
		right:0;
		bottom:0;
		top:0;
	}
	
	#ddstories-modal-content .story-viewer .slides .item > .media {
		height:100%;
		position:absolute;
		left:50%;
		-webkit-transform:translateX(-50%);
		transform:translateX(-50%);
		margin:auto;
	}
	
	#ddstories-modal-content .story-viewer .slides .item.active, #ddstories-modal-content .story-viewer .slides .item.active .tip.link {
		display:block;
	}
	
	#ddstories-modal-content .story-viewer .tip{
		z-index: 5; 
		text-decoration: none;
		display: none;
		border-radius:24px;
		background:rgba(0,0,0,0.5);
		font-size:16px;
		position:absolute;
		bottom:24px;
		left:50%;
		transform:translateX(-50%);
		z-index:1000;color:#fff;
		text-align:center;
		text-transform:uppercase;
		font-weight:900;
		padding:12px 24px;
	}
	
	</style>
	</head>
	<body>
<div id="stories" class="stories user-icon carousel facesnap">
	</div>
	<script>

var ddstoriesJS = function () {
    var ddstoriesJS = false;

    /* Too much ddstories ddstories to maintain legacy */
    ddstoriesJS = window['ddstoriesitaDaGalera'] = window['ddstories'] = function (timeline, options) {
        var d = document;
        var ddstories = this;

        if (typeof timeline == 'string') {
            timeline = d.getElementById(timeline);
        }

        /* core functions */
        var query = function query(_query) {
            return d.querySelectorAll(_query)[0];
        };

        var get = function get(array, what) {
            if (array) {
                return array[what] || '';
            } else {
                return '';
            }
        };

        var each = function each(arr, func) {
            if (arr) {
                var total = arr.length;

                for (var i = 0; i < total; i++) {
                    func(i, arr[i]);
                }
            }
        };

        var setVendorVariable = function setVendorVariable(ref, variable, value) {
            var variables = [variable.toLowerCase(), 'webkit' + variable, 'MS' + variable, 'o' + variable];

            each(variables, function (i, val) {
                ref[val] = value;
            });
        };

        var addVendorEvents = function addVendorEvents(el, func, event) {
            var events = [event.toLowerCase(), 'webkit' + event, 'MS' + event, 'o' + event];
            var element = el;

            each(events, function (i, val) {
                el.addEventListener(val, func, false);
            });
        };

        var onAnimationEnd = function onAnimationEnd(el, func) {
            addVendorEvents(el, func, 'AnimationEnd');
        };

        var onTransitionEnd = function onTransitionEnd(el, func) {
            if (!el.transitionEndEvent) {
                el.transitionEndEvent = true;

                addVendorEvents(el, func, 'TransitionEnd');
            }
        };

        var prepend = function prepend(parent, child) {
            if (parent.firstChild) {
                parent.insertBefore(child, parent.firstChild);
            } else {
                parent.appendChild(child);
            }
        };

        var getElIndex = function getElIndex(el) {
            for (var i = 1; el = el.previousElementSibling; i++) {
                return i;
            }

            return 0;
        };

        var fullScreen = function fullScreen(elem, cancel) {
            var func = 'RequestFullScreen';
            var elFunc = 'requestFullScreen'; //crappy vendor prefixes.

            if (cancel) {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            } else {
                try {
                    if (elem[elFunc]) {
                        elem[elFunc]();
                    } else if (elem['ms' + func]) {
                        elem['ms' + func]();
                    } else if (elem['moz' + func]) {
                        elem['moz' + func]();
                    } else if (elem['webkit' + func]) {
                        elem['webkit' + func]();
                    }
                } catch (e) {}
            }
        };

        var translate = function translate(element, to, duration, ease) {
            var direction = to > 0 ? 1 : -1;
            var to3d = Math.abs(to) / query('#ddstories-modal').offsetWidth * 90 * direction;

            if (option('cubeEffect')) {
                var scaling = to3d == 0 ? 'scale(0.95)' : 'scale(0.930,0.930)';

                setVendorVariable(query('#ddstories-modal-content').style, 'Transform', scaling);

                if (to3d < -90 || to3d > 90) {
                    return false;
                }
            }

            var transform = !option('cubeEffect') ? 'translate3d(' + to + 'px, 0, 0)' : 'rotateY(' + to3d + 'deg)';

            if (element) {
                setVendorVariable(element.style, 'TransitionTimingFunction', ease);
                setVendorVariable(element.style, 'TransitionDuration', duration + 'ms');
                setVendorVariable(element.style, 'Transform', transform);
            }
        };

        var findPos = function findPos(obj, offsetY, offsetX, stop) {
            var curleft = 0;
            var curtop = 0;

            if (obj) {
                if (obj.offsetParent) {
                    do {
                        curleft += obj.offsetLeft;
                        curtop += obj.offsetTop;

                        if (obj == stop) {
                            break;
                        }
                    } while (obj = obj.offsetParent);
                }

                if (offsetY) {
                    curtop = curtop - offsetY;
                }

                if (offsetX) {
                    curleft = curleft - offsetX;
                }
            }

            return [curleft, curtop];
        };

        var timeAgo = function timeAgo(time) {
            time = Number(time) * 1000;

            var dateObj = new Date(time);
            var dateStr = dateObj.getTime();
            var seconds = (new Date().getTime() - dateStr) / 1000;

            var language = option('language', 'time');

            var formats = [[60, ' ' + language['seconds'], 1], // 60
            [120, '1 ' + language['minute'], ''], // 60*2
            [3600, ' ' + language['minutes'], 60], // 60*60, 60
            [7200, '1 ' + language['hour'], ''], // 60*60*2
            [86400, ' ' + language['hours'], 3600], // 60*60*24, 60*60
            [172800, ' ' + language['yesterday'], ''], // 60*60*24*2
            [604800, ' ' + language['days'], 86400]];

            var currentFormat = 1;
            if (seconds < 0) {
                seconds = Math.abs(seconds);

                currentFormat = 2;
            }

            var i = 0;
            var format = void 0;
            while (format = formats[i++]) {
                if (seconds < format[0]) {
                    if (typeof format[2] == 'string') {
                        return format[currentFormat];
                    } else {
                        return Math.floor(seconds / format[2]) + format[1];
                    }
                }
            }

            var day = dateObj.getDate();
            var month = dateObj.getMonth();
            var year = dateObj.getFullYear();

            return day + '/' + (month + 1) + '/' + year;
        };

        /* options */
        var id = timeline.id;

        var optionsDefault = {
            'skin': 'snapgram',
            'avatars': true,
            'stories': [],
            'backButton': true,
            'backNative': false,
            'previousTap': true,
            'autoFullScreen': false,
            'openEffect': true,
            'cubeEffect': false,
            'list': false,
            'localStorage': true,
            'callbacks': {
                'onOpen': function onOpen(storyId, callback) {

                    callback();
                },
                'onView': function onView(storyId) {},
                'onEnd': function onEnd(storyId, callback) {

                    callback();
                },
                'onClose': function onClose(storyId, callback) {
                    callback();
                },
                'onNextItem': function onNextItem(storyId, nextStoryId, callback) {
                    callback();
                },
                'onNavigateItem': function onNavigateItem(storyId, nextStoryId, callback) {
                    callback();
                }
            },
            'language': {
                'unmute': 'Touch to unmute',
                'keyboardTip': 'Press space to see next',
                'visitLink': 'Visit link',
                'time': {
                    'ago': 'ago',
                    'hour': 'hour ago',
                    'hours': 'hours ago',
                    'minute': 'minute ago',
                    'minutes': 'minutes ago',
                    'fromnow': 'from now',
                    'seconds': 'seconds ago',
                    'yesterday': 'yesterday',
                    'tomorrow': 'tomorrow',
                    'days': 'days ago'
                }
            }
        };

        var option = function option(name, prop) {
            var type = function type(what) {
                return typeof what !== 'undefined';
            };

            if (prop) {
                if (type(options[name])) {
                    return type(options[name][prop]) ? options[name][prop] : optionsDefault[name][prop];
                } else {
                    return optionsDefault[name][prop];
                }
            } else {
                return type(options[name]) ? options[name] : optionsDefault[name];
            }
        };

        /* modal */
        var ddstoriesModal = function ddstoriesModal() {
            var opened = false;
            var modalContainer = get('#ddstories-modal');

            if (!modalContainer) {
                modalContainer = d.createElement('div');
                modalContainer.id = 'ddstories-modal';

                if (option('cubeEffect')) {
                    modalContainer.className = 'with-cube';
                }

                modalContainer.innerHTML = '<div id="ddstories-modal-content"></div>';
                modalContainer.style.display = 'none';

                modalContainer.setAttribute('tabIndex', '1');
                modalContainer.onkeyup = function (e) {
                    var code = e.keyCode;

                    if (code == 27) {
                        modal.close();
                    } else if (code == 13 || code == 32) {
                        modal.next();
                    }
                };

                if (option('openEffect')) {
                    modalContainer.classList.add('with-effects');
                };

                onTransitionEnd(modalContainer, function () {
                    if (modalContainer.classList.contains('closed')) {
                        modalContent.innerHTML = '';
                        modalContainer.style.display = 'none';
                        modalContainer.classList.remove('closed');
                        modalContainer.classList.remove('animated');
                    }
                });

                d.body.appendChild(modalContainer);
            }

            var modalContent = query('#ddstories-modal-content');
            var moveStoryItem = function moveStoryItem(direction) {
                var target = '';
                var useless = '';
                var transform = '0';
                var modalSlider = query('#ddstories-modal-slider-' + id);

                var slideItems = {
                    'previous': query('#ddstories-modal .story-viewer.previous'),
                    'next': query('#ddstories-modal .story-viewer.next'),
                    'viewing': query('#ddstories-modal .story-viewer.viewing')
                };

                if (!slideItems['previous'] && !direction || !slideItems['next'] && direction) {
                    return false;
                }

                if (!direction) {
                    target = 'previous';
                    useless = 'next';
                } else {
                    target = 'next';
                    useless = 'previous';
                }

                var transitionTime = 600;
                if (option('cubeEffect')) {
                    if (target == 'previous') {
                        transform = modalContainer.slideWidth;
                    } else if (target == 'next') {
                        transform = modalContainer.slideWidth * -1;
                    }
                } else {
                    transform = findPos(slideItems[target]);
                    transform = transform[0] * -1;
                }

                translate(modalSlider, transform, transitionTime, null);

                setTimeout(function () {
                    if (target != '' && slideItems[target] && useless != '') {
                        var currentStory = slideItems[target].getAttribute('data-story-id');
                        ddstories.internalData['currentStory'] = currentStory;

                        var oldStory = query('#ddstories-modal .story-viewer.' + useless);
                        if (oldStory) {
                            oldStory.parentNode.removeChild(oldStory);
                        }

                        if (slideItems['viewing']) {
                            slideItems['viewing'].classList.add('stopped');
                            slideItems['viewing'].classList.add(useless);
                            slideItems['viewing'].classList.remove('viewing');
                        }

                        if (slideItems[target]) {
                            slideItems[target].classList.remove('stopped');
                            slideItems[target].classList.remove(target);
                            slideItems[target].classList.add('viewing');
                        }

                        var newStoryData = getStoryMorningGlory(target);
                        if (newStoryData) {
                            createStoryViewer(newStoryData, target);
                        }

                        var storyId = ddstories.internalData['currentStory'];
                        var items = query('#ddstories-modal [data-story-id="' + storyId + '"]');

                        if (items) {
                            items = items.querySelectorAll('[data-index].active');
                            var duration = items[0].firstElementChild;

                            ddstories.data[storyId]['currentItem'] = parseInt(items[0].getAttribute('data-index'), 10);

                            items[0].innerHTML = '<b style="' + duration.style.cssText + '"></b>';
                            onAnimationEnd(items[0].firstElementChild, function () {
                                ddstories.nextItem(false);
                            });
                        }

                        translate(modalSlider, '0', 0, null);

                        if (items) {
                            playVideoItem([items[0], items[1]], true);
                        }

                        option('callbacks', 'onView')(ddstories.internalData['currentStory']);
                    }
                }, transitionTime + 50);
            };

            var createStoryViewer = function createStoryViewer(storyData, className, forcePlay) {
                var modalSlider = query('#ddstories-modal-slider-' + id);

                var htmlItems = '';
                var pointerItems = '';
                var storyId = get(storyData, 'id');
                var slides = d.createElement('div');
                var currentItem = get(storyData, 'currentItem') || 0;
                var exists = query('#ddstories-modal .story-viewer[data-story-id="' + storyId + '"]');
                var currentItemTime = '';

                if (exists) {
                    return false;
                }

                slides.className = 'slides';
                each(get(storyData, 'items'), function (i, item) {
                    if (currentItem > i) {
                        storyData['items'][i]['seen'] = true;
                        item['seen'] = true;
                    }

                    var length = get(item, 'length');
                    var linkText = get(item, 'linkText');
                    var seenClass = get(item, 'seen') === true ? 'seen' : '';
                    var commonAttrs = 'data-index="' + i + '" data-item-id="' + get(item, 'id') + '"';

                    if (currentItem === i) {
                        currentItemTime = timeAgo(get(item, 'time'));
                    }

                    pointerItems += '<span ' + commonAttrs + ' class="' + (currentItem === i ? 'active' : '') + ' ' + seenClass + '"><b style="animation-duration:' + (length === '' ? '5' : length) + 's"></b></span>';
                    htmlItems += '<div data-time="' + get(item, 'time') + '" data-type="' + get(item, 'type') + '"' + commonAttrs + ' class="item ' + seenClass + ' ' + (currentItem === i ? 'active' : '') + '">' + (get(item, 'type') === 'video' ? '<video class="media" muted webkit-playsinline playsinline preload="auto" src="' + get(item, 'src') + '" ' + get(item, 'type') + '></video><b class="tip muted">' + option('language', 'unmute') + '</b>' : '<img class="media" src="' + get(item, 'src') + '" ' + get(item, 'type') + '>') + (get(item, 'link') ? '<a class="tip link" href="' + get(item, 'link') + '" rel="noopener" target="_blank">' + (linkText == '' || !linkText ? option('language', 'visitLink') : linkText) + '</a>' : '') + '</div>';
                });
                slides.innerHTML = htmlItems;

                var video = slides.querySelector('video');
                var addMuted = function addMuted(video) {
                    if (video.muted) {
                        storyViewer.classList.add('muted');
                    } else {
                        storyViewer.classList.remove('muted');
                    }
                };

                if (video) {
                    video.onwaiting = function (e) {
                        if (video.paused) {
                            storyViewer.classList.add('paused');
                            storyViewer.classList.add('loading');
                        }
                    };

                    video.onplay = function () {
                        addMuted(video);

                        storyViewer.classList.remove('stopped');
                        storyViewer.classList.remove('paused');
                        storyViewer.classList.remove('loading');
                    };

                    video.onready = video.onload = video.onplaying = video.oncanplay = function () {
                        addMuted(video);

                        storyViewer.classList.remove('loading');
                    };

                    video.onvolumechange = function () {
                        addMuted(video);
                    };
                }

                var storyViewer = d.createElement('div');
                storyViewer.className = 'story-viewer muted ' + className + ' ' + (!forcePlay ? 'stopped' : '') + ' ' + (option('backButton') ? 'with-back-button' : '');
                storyViewer.setAttribute('data-story-id', storyId);

                var html = '<div class="head"><div class="left">' + (option('backButton') ? '<a class="back">&lsaquo;</a>' : '') + '<u class="img" style="background-image:url(' + get(storyData, 'photo') + ');"></u><div><strong>' + get(storyData, 'name') + '</strong><span class="time">' + currentItemTime + '</span></div></div><div class="right"><span class="time">' + currentItemTime + '</span><span class="loading"></span><a class="close" tabIndex="2">&times;</a></div></div><div class="slides-pointers"><div>' + pointerItems + '</div></div>';
                storyViewer.innerHTML = html;

                each(storyViewer.querySelectorAll('.close, .back'), function (i, el) {
                    el.onclick = function (e) {
                        e.preventDefault();
                        modal.close();
                    };
                });

                storyViewer.appendChild(slides);

                if (className == 'viewing') {
                    playVideoItem(storyViewer.querySelectorAll('[data-index="' + currentItem + '"].active'), false);
                }

                each(storyViewer.querySelectorAll('.slides-pointers [data-index] > b'), function (i, el) {
                    onAnimationEnd(el, function () {
                        ddstories.nextItem(false);
                    });
                });

                if (className == 'previous') {
                    prepend(modalSlider, storyViewer);
                } else {
                    modalSlider.appendChild(storyViewer);
                }
            };

            var createStoryTouchEvents = function createStoryTouchEvents(modalSliderElement) {
                var enableMouseEvents = true;

                var modalSlider = modalSliderElement;
                var position = {};
                var touchOffset = void 0;
                var isScrolling = void 0;
                var delta = void 0;
                var timer = void 0;
                var nextTimer = void 0;

                var touchStart = function touchStart(event) {
                    var storyViewer = query('#ddstories-modal .viewing');

                    if (event.target.nodeName == 'A') {
                        return true;
                    } else {
                        event.preventDefault();
                    }

                    var touches = event.touches ? event.touches[0] : event;
                    var pos = findPos(query('#ddstories-modal .story-viewer.viewing'));

                    modalContainer.slideWidth = query('#ddstories-modal .story-viewer').offsetWidth;
                    position = {
                        x: pos[0],
                        y: pos[1]
                    };

                    var pageX = touches.pageX;
                    var pageY = touches.pageY;

                    touchOffset = {
                        x: pageX,
                        y: pageY,
                        time: Date.now()
                    };

                    isScrolling = undefined;
                    delta = {};

                    if (enableMouseEvents) {
                        modalSlider.addEventListener('mousemove', touchMove);
                        modalSlider.addEventListener('mouseup', touchEnd);
                        modalSlider.addEventListener('mouseleave', touchEnd);
                    }
                    modalSlider.addEventListener('touchmove', touchMove);
                    modalSlider.addEventListener('touchend', touchEnd);

                    if (storyViewer) {
                        storyViewer.classList.add('paused');
                    }
                    pauseVideoItem();

                    timer = setTimeout(function () {
                        storyViewer.classList.add('longPress');
                    }, 600);

                    nextTimer = setTimeout(function () {
                        clearInterval(nextTimer);
                        nextTimer = false;
                    }, 250);
                };

                var touchMove = function touchMove(event) {
                    var touches = event.touches ? event.touches[0] : event;
                    var pageX = touches.pageX;
                    var pageY = touches.pageY;

                    if (touchOffset) {
                        delta = {
                            x: pageX - touchOffset.x,
                            y: pageY - touchOffset.y
                        };

                        if (typeof isScrolling === 'undefined') {
                            isScrolling = !!(isScrolling || Math.abs(delta.x) < Math.abs(delta.y));
                        }

                        if (!isScrolling && touchOffset) {
                            event.preventDefault();
                            translate(modalSlider, position.x + delta.x, 0, null);
                        }
                    }
                };

                var touchEnd = function touchEnd(event) {
                    var storyViewer = query('#ddstories-modal .viewing');
                    var lastTouchOffset = touchOffset;

                    if (delta) {
                        var slidesLength = d.querySelectorAll('#ddstories-modal .story-viewer').length;
                        var duration = touchOffset ? Date.now() - touchOffset.time : undefined;
                        var isValid = Number(duration) < 300 && Math.abs(delta.x) > 25 || Math.abs(delta.x) > modalContainer.slideWidth / 3;
                        var direction = delta.x < 0;

                        var index = direction ? query('#ddstories-modal .story-viewer.next') : query('#ddstories-modal .story-viewer.previous');
                        var isOutOfBounds = direction && !index || !direction && !index;

                        if (!isScrolling) {
                            if (isValid && !isOutOfBounds) {
                                moveStoryItem(direction, true);
                            } else {
                                translate(modalSlider, position.x, 300);
                            }
                        }

                        touchOffset = undefined;

                        if (enableMouseEvents) {
                            modalSlider.removeEventListener('mousemove', touchMove);
                            modalSlider.removeEventListener('mouseup', touchEnd);
                            modalSlider.removeEventListener('mouseleave', touchEnd);
                        }
                        modalSlider.removeEventListener('touchmove', touchMove);
                        modalSlider.removeEventListener('touchend', touchEnd);
                    }

                    var video = ddstories.internalData['currentVideoElement'];
                    if (timer) {
                        clearInterval(timer);
                    }

                    if (storyViewer) {
                        storyViewer.classList.remove('longPress');
                        storyViewer.classList.remove('paused');
                    }

                    if (nextTimer) {
                        clearInterval(nextTimer);
                        nextTimer = false;

                        var navigateItem = function navigateItem() {
                            if (lastTouchOffset.x > window.screen.width / 3 || !option('previousTap')) {
                                ddstories.navigateItem('next', event);
                            } else {
                                ddstories.navigateItem('previous', event);
                            }
                        };

                        var _storyViewer = query('#ddstories-modal .viewing');
                        if (_storyViewer && video) {
                            if (_storyViewer.classList.contains('muted')) {
                                unmuteVideoItem(video, _storyViewer);
                            } else {
                                navigateItem();
                            }
                        } else {
                            navigateItem();

                            return false;
                        }
                    }
                };

                modalSlider.addEventListener('touchstart', touchStart);

                if (enableMouseEvents) {
                    modalSlider.addEventListener('mousedown', touchStart);
                }
            };

            return {
                'show': function show(storyId, page) {
                    var callback = function callback() {
                        modalContent.innerHTML = '<div id="ddstories-modal-slider-' + id + '" class="slider"></div>';

                        var storyData = ddstories.data[storyId];
                        var currentItem = storyData['currentItem'] || 0;
                        var modalSlider = query('#ddstories-modal-slider-' + id);

                        createStoryTouchEvents(modalSlider);

                        ddstories.internalData['currentStory'] = storyId;
                        storyData['currentItem'] = currentItem;

                        if (option('backNative')) {
                            location.hash = '#!' + id;
                        }

                        var previousItemData = getStoryMorningGlory('previous');
                        if (previousItemData) {
                            createStoryViewer(previousItemData, 'previous');
                        }

                        createStoryViewer(storyData, 'viewing', true);

                        var nextItemData = getStoryMorningGlory('next');
                        if (nextItemData) {
                            createStoryViewer(nextItemData, 'next');
                        }

                        if (option('autoFullScreen')) {
                            modalContainer.classList.add('fullscreen');
                        }

                        var tryFullScreen = function tryFullScreen() {
                            if (modalContainer.classList.contains('fullscreen') && option('autoFullScreen') && window.screen.width <= 1024) {
                                fullScreen(modalContainer);
                            }

                            modalContainer.focus();
                        };

                        if (option('openEffect')) {
                            var storyEl = query('#' + id + ' [data-id="' + storyId + '"] .img');
                            var pos = findPos(storyEl);

                            modalContainer.style.marginLeft = pos[0] + storyEl.offsetWidth / 2 + 'px';
                            modalContainer.style.marginTop = pos[1] + storyEl.offsetHeight / 2 + 'px';

                            modalContainer.style.display = 'block';

                            modalContainer.slideWidth = query('#ddstories-modal .story-viewer').offsetWidth;

                            setTimeout(function () {
                                modalContainer.classList.add('animated');
                            }, 10);

                            setTimeout(function () {
                                tryFullScreen();
                            }, 300); //because effects
                        } else {
                            modalContainer.style.display = 'block';
                            modalContainer.slideWidth = query('#ddstories-modal .story-viewer').offsetWidth;

                            tryFullScreen();
                        }

                        option('callbacks', 'onView')(storyId);
                    };

                    option('callbacks', 'onOpen')(storyId, callback);
                },
                'next': function next(unmute) {
                    var callback = function callback() {
                        var lastStory = ddstories.internalData['currentStory'];
                        var lastStoryTimelineElement = query('#' + id + ' [data-id="' + lastStory + '"]');

                        if (lastStoryTimelineElement) {
                            lastStoryTimelineElement.classList.add('seen');

                            ddstories.data[lastStory]['seen'] = true;
                            ddstories.internalData['seenItems'][lastStory] = true;

                            saveLocalData('seenItems', ddstories.internalData['seenItems']);
                            updateStoryseenPosition();
                        }

                        var stories = query('#ddstories-modal .story-viewer.next');
                        if (!stories) {
                            modal.close();
                        } else {
                            moveStoryItem(true);
                        }
                    };

                    option('callbacks', 'onEnd')(ddstories.internalData['currentStory'], callback);
                },
                'close': function close() {
                    var callback = function callback() {
                        if (option('backNative')) {
                            location.hash = '';
                        }

                        fullScreen(modalContainer, true);

                        if (option('openEffect')) {
                            modalContainer.classList.add('closed');
                        } else {
                            modalContent.innerHTML = '';
                            modalContainer.style.display = 'none';
                        }
                    };

                    option('callbacks', 'onClose')(ddstories.internalData['currentStory'], callback);
                }
            };
        };
        var modal = new ddstoriesModal();

        /* parse functions */
        var parseItems = function parseItems(story) {
            var storyId = story.getAttribute('data-id');
            var storyItems = d.querySelectorAll('#' + id + ' [data-id="' + storyId + '"] .items > li');
            var items = [];

            each(storyItems, function (i, el) {
                var a = el.firstElementChild;
                var img = a.firstElementChild;

                items.push({
                    'src': a.getAttribute('href'),
                    'length': a.getAttribute('data-length'),
                    'type': a.getAttribute('data-type'),
                    'time': a.getAttribute('data-time'),
                    'link': a.getAttribute('data-link'),
                    'linkText': a.getAttribute('data-linkText'),
                    'preview': img.getAttribute('src')
                });
            });

            ddstories.data[storyId].items = items;
        };

        var parseStory = function parseStory(story) {
            var storyId = story.getAttribute('data-id');
            var seen = false;

            if (ddstories.internalData['seenItems'][storyId]) {
                seen = true;
            }

            if (seen) {
                story.classList.add('seen');
            } else {
                story.classList.remove('seen');
            }

            try {
                ddstories.data[storyId] = {
                    'id': storyId, //story id
                    'photo': story.getAttribute('data-photo'), //story photo (or user photo)
                    'name': story.firstElementChild.lastElementChild.firstChild.innerText,
                    'link': story.firstElementChild.getAttribute('href'),
                    'lastUpdated': story.getAttribute('data-last-updated'),
                    'seen': seen,
                    'items': []
                };
            } catch (e) {
                ddstories.data[storyId] = {
                    'items': []
                };
            }

            story.onclick = function (e) {
                e.preventDefault();

                modal.show(storyId);
            };
        };

        var getStoryMorningGlory = function getStoryMorningGlory(what) {
            //my wife told me to stop singing Wonderwall. I SAID MAYBE.
            var currentStory = ddstories.internalData['currentStory'];
            var whatEl = what + 'ElementSibling';

            if (currentStory) {
                var foundStory = query('#' + id + ' [data-id="' + currentStory + '"]')[whatEl];

                if (foundStory) {
                    var storyId = foundStory.getAttribute('data-id');
                    var data = ddstories.data[storyId] || false;

                    return data; //(get(ddstories.data[storyId], 'seen')==true)?false:data;
                }
            }

            return false;
        };

        var updateStoryseenPosition = function updateStoryseenPosition() {
            each(d.querySelectorAll('#' + id + ' .story.seen'), function (i, el) {
                var newData = ddstories.data[el.getAttribute('data-id')];
                var timeline = el.parentNode;

                timeline.removeChild(el);
                ddstories.add(newData, true);
            });
        };

        var playVideoItem = function playVideoItem(elements, unmute) {
            var itemElement = elements[1];
            var itemPointer = elements[0];
            var storyViewer = itemPointer.parentNode.parentNode.parentNode;

            if (!itemElement || !itemPointer) {
                return false;
            }

            var cur = ddstories.internalData['currentVideoElement'];
            if (cur) {
                cur.pause();
            }

            if (itemElement.getAttribute('data-type') == 'video') {
                var video = itemElement.getElementsByTagName('video')[0];
                if (!video) {
                    ddstories.internalData['currentVideoElement'] = false;

                    return false;
                }

                var setDuration = function setDuration() {
                    if (video.duration) {
                        setVendorVariable(itemPointer.getElementsByTagName('b')[0].style, 'AnimationDuration', video.duration + 's');
                    }
                };

                setDuration();
                video.addEventListener('loadedmetadata', setDuration);
                ddstories.internalData['currentVideoElement'] = video;

                video.currentTime = 0;
                video.play();

                if (unmute.target) {
                    unmuteVideoItem(video, storyViewer);
                }
            } else {
                ddstories.internalData['currentVideoElement'] = false;
            }
        };

        var pauseVideoItem = function pauseVideoItem() {
            var video = ddstories.internalData['currentVideoElement'];
            if (video) {
                try {
                    video.pause();
                } catch (e) {}
            }
        };

        var unmuteVideoItem = function unmuteVideoItem(video, storyViewer) {
            video.muted = false;
            video.volume = 1.0;
            video.removeAttribute('muted');
            video.play();

            if (video.paused) {
                video.muted = true;
                video.play();
            }

            if (storyViewer) {
                storyViewer.classList.remove('paused');
            }
        };

        /* data functions */
        var saveLocalData = function saveLocalData(key, data) {
            try {
                if (option('localStorage')) {
                    var keyName = 'ddstories-' + id + '-' + key;

                    window.localStorage[keyName] = JSON.stringify(data);
                }
            } catch (e) {}
        };

        var getLocalData = function getLocalData(key) {
            if (option('localStorage')) {
                var keyName = 'ddstories-' + id + '-' + key;

                return window.localStorage[keyName] ? JSON.parse(window.localStorage[keyName]) : false;
            } else {
                return false;
            }
        };

        /* api */
        ddstories.data = {};
        ddstories.internalData = {};
        ddstories.internalData['seenItems'] = getLocalData('seenItems') || {};

        ddstories.add = ddstories.update = function (data, append) {
            var storyId = get(data, 'id');
            var storyEl = query('#' + id + ' [data-id="' + storyId + '"]');
            var html = '';
            var items = get(data, 'items');
            var story = false;

            ddstories.data[storyId] = {};

            if (!storyEl) {
                story = d.createElement('div');
                story.className = 'story';
            } else {
                story = storyEl;
            }

            if (data['seen'] === false) {
                ddstories.internalData['seenItems'][storyId] = false;
                saveLocalData('seenItems', ddstories.internalData['seenItems']);
            }

            story.setAttribute('data-id', storyId);
            story.setAttribute('data-photo', get(data, 'photo'));
            story.setAttribute('data-last-updated', get(data, 'lastUpdated'));

            var preview = false;
            if (items[0]) {
                preview = items[0]['preview'] || '';
            }

            html = '<a href="' + get(data, 'link') + '"><span class="img"><u style="background-image:url(' + (option('avatars') || !preview || preview == '' ? get(data, 'photo') : preview) + ')"></u></span><span class="info"><strong>' + get(data, 'name') + '</strong><span class="time">' + timeAgo(get(data, 'lastUpdated')) + '</span></span></a><ul class="items"></ul>';
            story.innerHTML = html;
            parseStory(story);

            if (!storyEl) {
                if (append) {
                    timeline.appendChild(story);
                } else {
                    prepend(timeline, story);
                }
            }

            each(items, function (i, item) {
                ddstories.addItem(storyId, item, append);
            });

            if (!append) {
                updateStoryseenPosition();
            }
        };
        ddstories.next = function () {
            modal.next();
        };
        ddstories.addItem = function (storyId, data, append) {
            var story = query('#' + id + ' > [data-id="' + storyId + '"]');
            var li = d.createElement('li');

            li.className = get(data, 'seen') ? 'seen' : '';
            li.setAttribute('data-id', get(data, 'id'));

            li.innerHTML = '<a href="' + get(data, 'src') + '" data-link="' + get(data, 'link') + '" data-linkText="' + get(data, 'linkText') + '" data-time="' + get(data, 'time') + '" data-type="' + get(data, 'type') + '" data-length="' + get(data, 'length') + '"><img src="' + get(data, 'preview') + '"></a>';

            var el = story.querySelectorAll('.items')[0];
            if (append) {
                el.appendChild(li);
            } else {
                prepend(el, li);
            }

            parseItems(story);
        };
        ddstories.removeItem = function (storyId, itemId) {
            var item = query('#' + id + ' > [data-id="' + storyId + '"] [data-id="' + itemId + '"]');

            timeline.parentNode.removeChild(item);
        };

        ddstories.navigateItem = ddstories.nextItem = function (direction, event) {
            var currentStory = ddstories.internalData['currentStory'];
            var currentItem = ddstories.data[currentStory]['currentItem'];
            var storyViewer = query('#ddstories-modal .story-viewer[data-story-id="' + currentStory + '"]');
            var directionNumber = direction == 'previous' ? -1 : 1;

            if (!storyViewer || storyViewer.touchMove == 1) {
                return false;
            }

            var currentItemElements = storyViewer.querySelectorAll('[data-index="' + currentItem + '"]');
            var currentPointer = currentItemElements[0];
            var currentItemElement = currentItemElements[1];

            var navigateItem = currentItem + directionNumber;
            var nextItems = storyViewer.querySelectorAll('[data-index="' + navigateItem + '"]');
            var nextPointer = nextItems[0];
            var nextItem = nextItems[1];

            if (storyViewer && nextPointer && nextItem) {
                var navigateItemCallback = function navigateItemCallback() {

                    if (direction == 'previous') {
                        currentPointer.classList.remove('seen');
                        currentItemElement.classList.remove('seen');
                    } else {
                        currentPointer.classList.add('seen');
                        currentItemElement.classList.add('seen');
                    }

                    currentPointer.classList.remove('active');
                    currentItemElement.classList.remove('active');

                    nextPointer.classList.remove('seen');
                    nextPointer.classList.add('active');

                    nextItem.classList.remove('seen');
                    nextItem.classList.add('active');

                    each(storyViewer.querySelectorAll('.time'), function (i, el) {
                        el.innerText = timeAgo(nextItem.getAttribute('data-time'));
                    });

                    ddstories.data[currentStory]['currentItem'] = ddstories.data[currentStory]['currentItem'] + directionNumber;

                    playVideoItem(nextItems, event);
                };

                var callback = option('callbacks', 'onNavigateItem');
                callback = !callback ? option('callbacks', 'onNextItem') : option('callbacks', 'onNavigateItem');

                callback(currentStory, nextItem.getAttribute('data-story-id'), navigateItemCallback);
            } else if (storyViewer) {
                if (direction != 'previous') {
                    modal.next(event);
                }
            }
        };

        /* of course, init! */
        var init = function init() {
            if (location.hash == '#!' + id) {
                location.hash = '';
            }

            if (query('#' + id + ' .story')) {
                each(timeline.querySelectorAll('.story'), function (i, story) {
                    parseStory(story, true);
                });
            }

            if (option('backNative')) {
                window.addEventListener('popstate', function (e) {
                    if (location.hash != '#!' + id) {
                        location.hash = '';
                    }
                }, false);
            }

            each(option('stories'), function (i, item) {
                ddstories.add(item, true);
            });

            updateStoryseenPosition();

            var avatars = option('avatars') ? 'user-icon' : 'story-preview';
            var list = option('list') ? 'list' : 'carousel';

            timeline.className = 'stories ' + avatars + ' ' + list + ' ' + (option('skin') + '').toLowerCase();

            return ddstories;
        };

        return init();
    };

    return ddstoriesJS;
}();
		
		
		function buildItem(id, type, length, src, preview, link, seen, time){
                return {
                            "id": id,
                            "type": type,
                            "length": length,
                            "src": src,
                            "preview": preview,
                            "link": link,
                            "seen": seen,
							"time": time
                        };
            }

			var initDemo = function(){
				var header = document.getElementById("header");
				var skin = location.href.split('skin=')[1];

				if(!skin) {
					skin = 'FaceSnap';
				}

				if(skin.indexOf('#')!==-1){
				   skin = skin.split('#')[0];
				}

				var skins = {
					'Snapgram': {
						'avatars': true,
						'list': false,
						'autoFullScreen': false,
						'cubeEffect': true
					},
          
          'VemDeZAP': {
						'avatars': false,
						'list': true,
						'autoFullScreen': false,
						'cubeEffect': false
					},

					'FaceSnap': {
						'avatars': true,
						'list': false,
						'autoFullScreen': true,
						'cubeEffect': false
					},

					'Snapssenger': {
						'avatars': false,
						'list': false,
						'autoFullScreen': false,
						'cubeEffect': false
					}
				};

				var stories = new ddstories('stories', {
					backNative: true,
					autoFullScreen: skins[skin]['autoFullScreen'],
					skin: skin,
					avatars: skins[skin]['avatars'],
					list: skins[skin]['list'],
					cubeEffect: skins[skin]['cubeEffect'],
					localStorage: false,
					stories: [
						{
							id: "ramon",
							photo: "https://avatars1.githubusercontent.com/u/2271175?v=3&s=460",
							name: "Ramon",
							link: "https://ramon.codes",
							lastUpdated: 1492665454,
							items: [
								buildItem("ramon-1", "photo", 3, "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", '', false, 1492665454),
								buildItem("ramon-1", "photo", 3, "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", '', false, 1492665454),
								buildItem("ramon-1", "photo", 3, "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", '', false, 1492665454),
								buildItem("ramon-1", "photo", 3, "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", '', false, 1492665454),
								buildItem("ramon-1", "photo", 3, "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", '', false, 1492665454),
								buildItem("ramon-1", "photo", 3, "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10810091_1527190460857578_541280638_n.jpg", '', false, 1492665454),
								buildItem("ramon-2", "photo", 3, "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10597412_455246124639813_1360162248_n.jpg", "https://scontent-gru2-2.cdninstagram.com/t51.2885-15/e15/10597412_455246124639813_1360162248_n.jpg", 'https://ramon.codes', false, '1492665454')
							]
						},
						{
							id: "gorillaz",
							photo: "https://lh3.googleusercontent.com/xYFz6B9FHMQq7fDOI_MA61gf0sNdzGBbdR7-mZ7i4rEVvE_N-kZEY_A4eP74Imcf8Sq3FYxAgd4eJA=w200",
							name: "Gorillaz",
							link: "",
							lastUpdated: 1492665454,
							items: [
								buildItem("gorillaz-1", "video", 0, "https://instagram.frao1-1.fna.fbcdn.net/t50.2886-16/17886251_1128605603951544_572796556789415936_n.mp4", "https://pbs.twimg.com/media/C8VgMQ8WAAE5i9M.jpg:small", '', false, 1492665454),
								buildItem("gorillaz-2", "photo", 3, "https://pbs.twimg.com/media/C8VgMQ8WAAE5i9M.jpg:large","https://pbs.twimg.com/media/C8VgMQ8WAAE5i9M.jpg:small", '', false, 1492665454),
							]
						},
						{
							id: "ladygaga",
							photo: "https://lh3.googleusercontent.com/VkANYSL1HOzINPSnzBJRM879b302ShsRwLoKD7mLezgTLvlpHPm_dIVop7mkAQfepze6O5N8rw8l4yM=w200",
							name: "Lady Gaga",
							link: "",
							lastUpdated: 1492665454,
							items: [
								buildItem("ladygaga-1", "photo", 5, "https://pbs.twimg.com/media/C8mtrEMXcAA9KKM.jpg:large", "https://pbs.twimg.com/media/C8mtrEMXcAA9KKM.jpg:small", '', false, 1492665454),
								buildItem("ladygaga-2", "photo", 3, "https://pbs.twimg.com/media/C4t_bxcXAAE3Hwb.jpg:large", "https://pbs.twimg.com/media/C4t_bxcXAAE3Hwb.jpg:small", 'http://ladygaga.com', false, 1492665454),
							]
						},
						{
							id: "starboy",
							photo: "https://lh3.googleusercontent.com/nMxfllzaAmaCCZJEMiKe2EARjyUNItQ-mdgAq6he-LWXwkIWbiiBIHyC3rGiqDu6fgyVK6NnjcgueA=w200",
							name: "The Weeknd",
							link: "",
							lastUpdated: 1492665454,
							items: [
								buildItem("starboy-1", "photo", 5, "https://pbs.twimg.com/media/C6f-dTqVQAAiy1K.jpg:large", "https://pbs.twimg.com/media/C6f-dTqVQAAiy1K.jpg:small", '', false, 1492665454)
							]
						},
						{
							id: "qotsa",
							photo: "https://lh3.googleusercontent.com/nE4grkY8s88P_1mFFA06mGCNshhqtIz4C4y2dV7AZbm0360zopRKDMCYtUHR0uQR2DAfYMRZdA=s180-p-e100-rwu-v1",
							name: "QOTSA",
							link: "",
							lastUpdated: 1492665454,
							items: [
								buildItem("qotsa-1", "photo", 10, "https://pbs.twimg.com/media/C8wTxgUVoAALPGA.jpg:large", "https://pbs.twimg.com/media/C8wTxgUVoAALPGA.jpg:small", '', false, 1492665454)
							]
						},
            {
							id: "qotsaaef",
							photo: "https://lh3.googleusercontent.com/nE4grkY8s88P_1mFFA06mGCNshhqtIz4C4y2dV7AZbm0360zopRKDMCYtUHR0uQR2DAfYMRZdA=s180-p-e100-rwu-v1",
							name: "QOTSA",
							link: "",
							lastUpdated: 1492665454,
							items: [
								buildItem("qotsa-1", "photo", 10, "https://pbs.twimg.com/media/C8wTxgUVoAALPGA.jpg:large", "https://pbs.twimg.com/media/C8wTxgUVoAALPGA.jpg:small", '', false, 1492665454)
							]
						}
					]
				});

                var el = document.querySelectorAll('#skin option');
                var total = el.length;
                for (var i = 0; i < total; i++) {if (window.CP.shouldStopExecution(1)){break;}
					var what = (skin==el[i].value)?true:false;

					if(what){
						el[i].setAttribute('selected', what);

						header.innerHTML = skin;
						header.className = skin;
					} else {
						el[i].removeAttribute('selected');
					}
                }
window.CP.exitedLoop(1);


				document.body.style.display = 'block';
			};

			initDemo();
</script>
	<div id="ddstories-modal" tabindex="1"><div id="ddstories-modal-content"></div></div>
</body></html>