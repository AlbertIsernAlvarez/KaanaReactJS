<html><head>
<style>
  .form_nuevap .window #gif-search-input {
        width: 100%;
    border: none;
    padding: 0.5rem;
    color: black;
    background: white;
    outline: none;
    font-size:1rem;
  }
  
  .form_nuevap .window .pic-container {
    display: inline-flex;
    align-items: center;
    overflow: auto;
    width: 100%;
  }
  
  .form_nuevap .window .pic-container img {
    height: 5rem;
    width: auto;
    margin: 0.2rem;
  }
</style></head><body>
  <div class="PF PF-input">
    <input class="input-item" type="text" id="gif-search-input" required="" id="input-field" onchange="giphyApiSearch($(this).val()); $(this).val('');">
    <label>Search for gifs</label>
    <span class="bar"></span>
  </div>
<div class="pic-container"></div>
<script src="./resources/js/stopExecutionOnTimeout.js"></script>
<script>function giphyApiSearch(search){
  var url;
  var xmlHttp = new XMLHttpRequest();
  
  search = search.split(' ').join('+');
  url = "https://api.giphy.com/v1/gifs/search?q=" + search + "&api_key=dc6zaTOxFJmzC&limit=12";
  
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      addPics(xmlHttp.responseText);
  }
  xmlHttp.open("GET", url, true);  
  xmlHttp.send(null);
}

function addPics(giffy) {
  document.querySelector(".pic-container").innerHTML = "";
  giffy = JSON.parse(giffy);
  if (giffy.data.length > 0){
    for (var x = 0; x < giffy.data.length; x++) {if (window.CP.shouldStopExecution(1)){break;}if (window.CP.shouldStopExecution(1)){break;}
      var el = document.createElement("img");
      el.setAttribute("src", giffy.data[x].images.original.url);
      document.querySelector(".pic-container").appendChild(el);
    }
window.CP.exitedLoop(1);

window.CP.exitedLoop(1);

  } else {
    /*document.querySelector(".error-container").innerHTML = "Sorry no gifs from that search.<br>Try another!";*/
  }
}
  
giphyApiSearch($('#typed').text());
</script>
</body></html>