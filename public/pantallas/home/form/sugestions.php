<?php 
include_once(__DIR__."/../../../resources/acceso_db.php");
include_once(__DIR__."/../../../resources/data-user.php");

if($app == 'music'){?>
  <div class="PF PF-chip sugestion ripple">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">library_music</i></div>
      <p>Share an album</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">music_note</i></div>
      <p>Have a party</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">queue_music</i></div>
      <p>Share a playlist</p>
    </div>
  </div>

<?} elseif($app == 'news'){?>

  <div class="PF PF-chip sugestion ripple">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">art_track</i></div>
      <p>Make an article</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">share</i></div>
      <p>Share a story</p>
    </div>
  </div>
  
<?} else {?>
  <div class="PF PF-chip sugestion ripple" data-tipo="article">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">art_track</i></div>
      <p>Make an article</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="photo">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">insert_photo</i></div>
      <p>Photo</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="video">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE63A;</i></div>
      <p>Video</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="ask">
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE8AF;</i></div>
      <p>Ask a question</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="code" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">code</i></div>
      <p>Share code</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="help" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE0C6;</i></div>
      <p>Ask for help</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="gif" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE908;</i></div>
      <p>GIF</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="ubicacion" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE0C8;</i></div>
      <p>Share your location</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="evento" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE878;</i></div>
      <p>Create an event</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="video-directo" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE639;</i></div>
      <p>Broadcast live</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="noticia" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE031;</i></div>
      <p>Share a story</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="donacion" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE227;</i></div>
      <p>Make a donation</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="evento" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE878;</i></div>
      <p>Have a party</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="alquiler" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE88A;</i></div>
      <p>Rent an apartment</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="poll" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE801;</i></div>
      <p>Take a survey</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="car" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE531;</i></div>
      <p>Share a car</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="shoppinglist" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">list_alt</i></div>
      <p>Share your shopping list</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="spoiler" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">&#xE63A;</i></div>
      <p>Do Spoiler</p>
    </div>
  </div>

<div class="PF PF-chip sugestion ripple" data-tipo="challenge" >
    <div class="inside">
      <div class="PF-icon ripple" ><i class="material-icons">loyalty</i></div>
      <p>Challenge</p>
    </div>
  </div>

<?}?>