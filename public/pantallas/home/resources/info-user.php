<div class="PF PF-card center transparent card_widget info-user">
  <div class="PF-image circle ripple ddbg-avatar" style="height: 10rem;
    padding: 0;
    width: 10rem;
    margin: auto;
    margin-bottom: 0.5rem;" onclick="windowdd('./resources/window/cambiar-avatar.php');" rgdd>
    <div class="PF PF-icon ripple"><i class="material-icons">cloud_upload</i></div>
  </div>
  <div class="info">
    <h1><?=$user_mismo_name?> <?=$user_mismo_lastname?></h1>
    <?php if($user_mismo_estado !='') {?><p><?=replaceemojis($user_mismo_estado)?></p><?}?>
  </div>
  <div class="statistics">
    <script> $('.contenedor-publicaciones .statistics').load('./pantallas/profile/resources/statistics.php?<?=$server_querystring;?>'); </script>
  </div>
</div>