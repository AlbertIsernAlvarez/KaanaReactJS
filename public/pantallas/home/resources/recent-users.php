<html>

<head>
  <style>
.recent-users {
    list-style: none;
    padding-top: 3px;
    margin-top: 0.5rem;
    width: fit-content;
    max-width: 100%;
}

.recent-users .recent-users--item {
  display: inline-block;
  width: 70px;
  height: auto;
  margin: 0 0 0 5px;
  position: relative;
  text-align: center;
  white-space: nowrap;
}

.recent-users .recent-users--item .recent-users--item_image {
  display: block;
  cursor: pointer;
  width: 60px;
  height: 60px;
  position: relative;
  margin: auto;
}
.recent-users .recent-users--item.seen .recent-users--item_text {
  color: #b3b3b3;
}


.recent-users .recent-users--item .recent-users--item_image .rece {
  border-radius: 100%;
  width: 100%;
  height: 100%;
  position: relative;
  border: 1px solid rgba(var(--PF-color-primary), .2);
  z-index: 2;
  transition: all 0.1s linear;
  margin: auto;
  background-size: cover;
  background-position: center;
}

.recent-users .recent-users--item .recent-users--item_text {
    font-size: 13px;
    margin: 0;
    padding: 5px;
    display: block;
    font-weight: 500;
    text-overflow: ellipsis;
    overflow: hidden;
}
  </style>
</head>

<body>
  <ul class="recent-users" id="recent-users">
    <?php
    $id_amigos_publicaciones_p = array();
    $amigos_publicaciones_p = mysqli_query($con,"SELECT * FROM user_follows WHERE type='user' AND user_id='$user_mismo_id'");
  while($row_amigos_publicaciones_p = mysqli_fetch_array($amigos_publicaciones_p))
  {
    $id_amigos_publicaciones_p[] = $row_amigos_publicaciones_p["id_seguir"];
  }
  $id_amigos_publicaciones_p = implode(',', $id_amigos_publicaciones_p);
$user_people = mysqli_query($con,"SELECT * FROM users WHERE user_id IN ($id_amigos_publicaciones_p) ORDER BY ISNULL(avatar), lastactivity DESC, signup_date DESC, RAND() LIMIT 8");
while($row_user_people = mysqli_fetch_array($user_people))
  {
    $user_username_people = $row_user_people['username'];
    $name_people = $row_user_people['name'];
    $lastname_people = $row_user_people['lastname'];
    $color_people = $row_user_people['color'];
    $avatar_people = $row_user_people['avatar'];
    $user_id_people = $row_user_people['user_id'];
    $lastactivity_people = $row_user_people['lastactivity'];
    if($name_people){$name_show_people = $name_people . " " . $lastname_people;} else {$name_show_people = $user_username_people;}
    $name_show_people = ucwords($name_show_people);
    ?>
    <li class="recent-users--item PFC-<?=$color_people?>" onclick="downmenudd('./pantallas/profile/preview/menu.php?id=<?=$user_id_people?>');">
      <div class="recent-users--item_image PF PF-avatar ripple" <?php if(round(abs(strtotime($datetime) - strtotime($lastactivity_people)) / 60) <= 5){?>online<?}?> style="background-image:url('//kaana.io/apps/photos/see.php?id=<?=$avatar_people?>');" rgdd>
      </div>
      <span class="recent-users--item_text"><?=$name_show_people?></span>
    </li>
    <?}?>
  </ul>
  
</body>

</html>