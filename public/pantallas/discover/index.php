<?php $q=$_GET['q'];?>
<html>

<head>
  <style>
    .explorar_container {}

    .explorar_container .header_explorar {
      width: 100%;
      padding: 4em 0;
      background: black;
      background: radial-gradient(at 0 0, #2196F3 0, rgba(255, 255, 255, 0) 70%, rgba(255, 255, 255, 0) 100%), radial-gradient(circle at 170% 0, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 0) 39%, #00BCD4 77%, #E91E63 92%, #F44336 100%), linear-gradient(135deg, #2196F3 0, #2196F3 24%, #9C27B0 100%);
      background-size: cover;
      overflow: hidden;
      margin-bottom: 1em;
      position: relative;
      display: inline-flex;
      align-items: center;
    }

    .explorar_container .header_explorar .bg {
      width: 100%;
      height: 100%;
      text-align: center;
      display: inline-flex;
      align-items: center;
      color: white;
      font-size: 2rem;
      background: rgb(var(--PF-color-surface));
      opacity: .5;
      position: absolute;
      top: 0;
      left: 0;
    }

    .explorar_container .header_explorar h1 {
      margin: auto;
      font-weight: 600;
      font-size: 4em;
      color: rgb(var(--PF-color-on-surface));
      z-index: 1;
    }

    .explorar_container .header_explorar .bg h1::first-letter {
      text-transform: uppercase;
    }

    .explorar_container .explorar_content {
      max-width: 60rem;
      margin: auto;
    }
  </style>
</head>

<body>

  <?php
  if($q != null){
    $title_discover = $q;
  } else {
    $title_discover = "Discover";
  }
  ?>
    <div class="explorar_container">
      <div class="header_explorar">
        <div class="bg"></div>
        <h1 t-dd ><?=$title_discover?></h1>
      </div>
      <div class="explorar_content circle">
        <?php include(__DIR__.'/../people/pantallas/explore.php'); ?>
      </div>
    </div>
</body>

</html>