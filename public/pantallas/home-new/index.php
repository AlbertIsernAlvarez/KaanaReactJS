<html>

<head>
  <style>
  .home_container.PF-tabs>.tabs>.tab {
  	overflow: visible;
  }
  </style>
</head>

<body>

<?php
  $idarticulo = $_GET['id'];
  if($idarticulo != '') {?>
  <script> windowdd('./apps/home/resources/articulo.php?id=<?=$idarticulo?>', 'fit'); </script>
  <?}?>


  <div class="PF-tabs home_container">
    <div class="tabs">
      <div class="tab" data-name="tabshome-home"></div>
      <div class="tab" data-name="tabshome-hub"></div>
      <div class="tab" data-name="tabshome-saved"></div>
      <div class="tab" data-name="tabshome-profile"></div>
    </div>

    <div class="PF PF-tabbar bottom shadow sh64">
      <div class="container center">
        <ul id="bottombar">
        	<!----------
          <li class="ripple" data-for="tabshome-home">
            <i class="material-icons">home</i>
            <span t-dd>Home</span>
          </li>
          --------->

          <li class="ripple active" data-for="tabshome-hub" data-taburl="./pantallas/hub/index.php?<?=$server_querystring?>">
            <i class="material-icons">assistant</i>
            <span t-dd>Hub</span>
          </li>

					<!----------
          <li class="ripple" data-for="tabshome-saved" data-taburl="./apps/<?=$app?>/pantallas/favorites.php?<?=$server_querystring?>">
            <i class="material-icons">bookmark</i>
            <span t-dd>Saved</span>
          </li>
          --------->

          <li class="ripple" data-for="tabshome-profile" data-taburl="./pantallas/profile/index.php?p=profile">
            <i class="material-icons">account_circle</i>
            <span t-dd>Profile</span>
          </li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
  </div>

</body>

</html>