<html>

<head>
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="recoverform" method="post" autocomplete="off" class="PF PF-form">
          <div class="overlay"></div>
          <div class="processSteps"></div>
          <div class="steps">
            <?php
            if($_GET['code'] and $_GET['user']){?>
            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="Kaana" alt="Kaana"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Account recovery</h1>
                <p t-dd>Please confirm your new password.</p>
              </div>
              <div class="block">
                <label class="PF-textfield filled">
                  <input type="password" placeholder=" " name="password" required>
                  <span t-dd>Password</span>
                </label>
                <br>
                <label class="PF-textfield filled">
                  <input type="password" placeholder=" " name="confirm" required>
                  <span t-dd>Confirm Password</span>
                </label>
              </div>
              <input name="user" type="text" value="<?=$_GET['user']?>" style="display:none;"/>
              <input name="code" type="text" value="<?=$_GET['code']?>" style="display:none;"/>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <!------<button type="button" class="PF-button text" t-dd>Try another way</button>------>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>
            <?} else {?>

            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="Kaana"></div>
              </div>
              <div class="presentation">
                <h1>We've sent you an email</h1>
                <p t-dd>Please check your email to recover your account.</p>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <div class="space"></div>
                  <button type="button" class="PF-button back-home" t-dd>Go back</button>
                </div>
              </div>
            </div>
            <?}?>

            <div class="content step auto">
              <div class="logo_container">
                <div class="logo" title="Kaana"></div>
              </div>
              <div class="presentation">
                <h1><span t-dd>Welcome</span> <span data-value="user_name"></span></h1>
                <p t-dd>Logging in... In a few moments you will be redirected.</p>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div style="display: none;">
    <input type="text" id="PreventChromeAutocomplete" name="PreventChromeAutocomplete" autocomplete="address-level4" />
  </div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>