<script>

$('#header').addClass('hidden');

  var timeoutkeypressformsteps = null;
  var formprocessSteps = $("#recoverform").children('.processSteps');
  var numberSteps = $('#recoverform .step').length;
  var currentStep = 0; // Current step is set to be the first step (0)
  var beforeStep = 0;
  processstep(); // Display the crurrent step

  $(document).on("click", "#recoverform .next", function(event) {
    clearTimeout(timeoutkeypressformsteps);
    timeoutkeypressformsteps = setTimeout(function() {
      processstep();
      event.preventDefault();
    }, 500);
  });

  $(document).on("click", "#recoverform .back", function(event) {
    clearTimeout(timeoutkeypressformsteps);
    timeoutkeypressformsteps = setTimeout(function() {
      processstep("back");
      event.preventDefault();
    }, 500);
  });

  $(document).on("click", "#recoverform .back-home", function(event) {
    clearTimeout(timeoutkeypressformsteps);
    timeoutkeypressformsteps = setTimeout(function() {
      document.getElementById('recoverform').reset();
      $('.steps .step').hide();
      $('.steps .step').first().show();
      currentStep = 0; // Current step is set to be the first step (0)
      beforeStep = 0;
      event.preventDefault();
    }, 500);
  });

  $(document).on("submit", "#recoverform", function(event) {
    processstep();
    event.preventDefault();
  });


  $(document).on("keypress", "#recoverform", function(e) {
    if (e.which == 13) {
      clearTimeout(timeoutkeypressformsteps);
      timeoutkeypressformsteps = setTimeout(function() {
        processstep();
        e.preventDefault();
      }, 500);
    }
  });

  var processtimeoutstep = null;
  var processtimeoutalert = null;
  
  function processstep(nextback) {
    var stepcontinue = true;
    $('.recover_container .PF-progress.linear.loading').show();
    $('#recoverform .overlay').addClass('show');
    clearTimeout(processtimeoutstep);
    processtimeoutstep = setTimeout(function() {
      if (currentStep < numberSteps) {
        if (nextback == "back") {
          currentStep = currentStep - 1;
          beforeStep = currentStep + 1;
        } else {
          currentStep = currentStep + 1;
          beforeStep = currentStep - 1;
        }
        var step = $(".step:nth-child(" + currentStep + ")");

        if ($(".step:nth-child(" + beforeStep + ")") && nextback != "back") {
          $(".step:nth-child(" + beforeStep + ") [required]").each(function() {
            if ($(this).val().trim() === "") {
              clearTimeout(processtimeoutalert);
              processtimeoutalert = setTimeout(function() {
                alertdd.show("The field is empty");
              }, 500);
              $(this).focus();
              stepcontinue = false;
            } else if(stepcontinue == true) {
              $.ajax({
                type: "POST",
                url: "./pantallas/recover/pages/password/validate.php",
                data: $('#recoverform').serialize() + "&step=" + beforeStep, // serializes the form's elements.
                async: false,
                success: function(data) {
                  eval(data);
                }
              });
            }
          });
        }

        if (stepcontinue === true) {
          $(".step:nth-child(" + beforeStep + ")").hide();
          step.show();

          if (!$(".step:nth-child(" + currentStep + ")").hasClass('auto')) {
            step.find('input').first().focus();
          } else {
            setTimeout(function() {
              processstep();
            }, Math.floor((Math.random() * 3000) + 2000));
          }
        } else {
          currentStep = currentStep - 1;
          $('.recover_container .PF-progress.linear.loading').hide();
          $('#recoverform .overlay').removeClass('show');
        }
      } else {

        $.ajax({
          type: "POST",
          url: $('#recoverform').attr('action'),
          data: $('#recoverform').serialize(), // serializes the form's elements.
          success: function(data) {
            eval(data); // show response from the final php script.
          }
        });
      }
      if (!$(".step:nth-child(" + currentStep + ")").hasClass('auto')) {
        $('.recover_container .PF-progress.linear.loading').hide();
      }
      $('#recoverform .overlay').removeClass('show');
    }, 500);

  }
</script>