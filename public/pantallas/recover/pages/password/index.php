<html>

<head>
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="recoverform" method="post" action="./pantallas/recover/recover.php" autocomplete="off" class="PF PF-form">
          <div class="overlay"></div>
          <div class="processSteps"></div>
          <div class="steps">
            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="Kaana" alt="Kaana"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Recovery</h1>
                <p t-dd>Please enter your Kaana ID to search for your account.</p>
              </div>
              <div class="block">
                <label class="PF-textfield filled">
                <input placeholder=" " type="text" name="username" required >
                <span t-dd >Kaana ID</span>
              </label>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <button type="button" class="PF-button text" onclick="window.history.back();" t-dd>Go back</button>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="Kaana"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>We've sent you an email</h1>
                <p t-dd>Please check your email to recover your account.</p>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <div class="space"></div>
                  <button type="button" class="PF-button back-home" t-dd>Go back</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <div style="display: none;">
 <input type="text" id="PreventChromeAutocomplete" 
  name="PreventChromeAutocomplete" autocomplete="address-level4" />
</div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>