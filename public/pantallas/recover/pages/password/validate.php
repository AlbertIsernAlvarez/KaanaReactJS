<?php
$step          = $_POST['step'];
$user_validate = mysqli_real_escape_string($con, $_POST['username']);

if ($step == '1') {
    
    if (!empty($user_validate)) {
        $sql = mysqli_query($con, "SELECT * FROM users WHERE LOWER(username)=LOWER('$user_validate') LIMIT 1 ");
        if ($row_validate = mysqli_fetch_array($sql)) {
            $validate_user_id     = $row_validate['user_id'];
            $validate_user_email    = $row_validate['email'];
            $validate_user_name     = $row_validate['username'];
            $validate_user_name     = $row_validate['name'];
            $validate_user_lastname = $row_validate['lastname'];

            $validate_code = substr(md5(uniqid(mt_rand(), true)) , 0, 91);
            
            if(!$validate_user_email){
                ?> stepcontinue = false; alertdd.show("There is no email associated with this account"); <?
            } else {


                mysqli_query($con, "INSERT INTO user_security_codes (user_id, code, type, date) VALUES('$validate_user_id', '$validate_code', 'recover', '$datetime') ");
                
                $subject         = translate("Recover your Kaana Account", "en-US", $language, false, true);
                $user_email_from = "noreply@kaana.io";
                $from            = "Kaana <" . $user_email_from . ">";
                
                // To send HTML mail, the Content-type header must be set
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
                
                // Create email headers
                $headers .= "From: " . $from . "\r\n" . "Reply-To: " . $from . "\r\n" . "X-Mailer: PHP/" . phpversion();
                
                // Compose a simple HTML email message
                $message = "Hi @$user_validate. <a href='https://kaana.io/?p=recover&sp=change-password&user=".$validate_user_id."&code=".$validate_code."'>Click here</a> to recover your account. ";
                
                // Sending email
                if(mail($validate_user_email, $subject, $message, $headers)) {
                } else {
                    ?> stepcontinue = false; alertdd.show("Unable to send email. Please try again."); <?
                }
            }
            
        } else {
            ?> stepcontinue = false; alertdd.show("The user don't exist"); <?
        }
    }
    
}

?>