<html>

<head>
  <style>
    .about-container {
      max-width: 35em;
      margin: auto;
      padding-top: 1em;
      padding-bottom: 1em;
    }
  </style>
</head>

<body>

  <div class="about-container">
    <div class="PF PF-content full noborder center">
      <div class="container">
        <div class="PF PF-image transparent" style="background-image:url('./pantallas/settings/update.svg');"></div>
        <h1 t-dd>About Kaana</h1>
      </div>
    </div>
    
    <?if($version['number'] < $last_version_number) {?>

    <div class="PF PF-card center">
      <div class="info">
        <h1 t-dd>An update is available</h1>
        <div style="display: flex;margin: auto;width: fit-content;">
          <?php if($pagina != 'lastest-updates'){?><button class="PF-button" opendd-href="?p=lastest-updates" t-dd>Lastest updates</button><?}?>
          <button class="PF-button" style=" color: rgb(var(--PF-color-surface)); background: rgb(var(--PF-color-primary)); " onclick="updateVersion();" t-dd>Update</button>
        </div>
        <p t-dd>Click update to get the last version (<?=$last_version_number?>)</p>
        <p t-dd>To update to the current version please click on the update button.</p>
      </div>
    </div>
    
    <?}?>
    
    <div class="PF PF-card">
      <div class="info">
        <?php if($version['number'] >= $last_version_number) {?><h1>Kaana is up to date</h1><?}?>
        <p><b t-dd>Version</b>:<br/><span t-dd><?=ucwords($version['channel'])?></span> <?=$version['name']?> (<?=$version['number']?>)</p>
        <p><b t-dd>Last time updated</b>:<br/><?=$version['date']?></p>
        <?php if($version['whatsnew']){?><p><b t-dd>What's New</b>:<br/><span t-dd><?=$version['whatsnew']?></span></p><?}?>
      </div>
    </div>

  </div>

  <ul class="PF PF-list">
<?php
$karma_contributions_list = mysqli_query($con,"SELECT * FROM internal_version WHERE number!='$last_version_number' AND public IS NOT NULL ORDER BY date DESC");
while($row_karma_contributions_list = mysqli_fetch_array($karma_contributions_list))
  {
    $karma_contributions_list_number = $row_karma_contributions_list['number'];
    $karma_contributions_list_whatsnew = $row_karma_contributions_list['whatsnew'];
    $karma_contributions_list_date = $row_karma_contributions_list['date'];
    
    ?>
  <li class="ripple" >
    <div class="data">
      <span><?=$karma_contributions_list_number?></span>
      <h1 style="font-size: 15px;" t-dd><?=$karma_contributions_list_whatsnew?></h1>
      <p><?=$karma_contributions_list_date?></p>
    </div>
  </li>
<?}?>
</ul>
  
  <script>
  function updateVersion(){
    $.get( "./resources/version/update.php", function( data ) {
      $("[data-for=about-settingstab]").removeClass('tabloaded');
      $("[data-for=about-settingstab]").click();
      alertdd.show('Kaana have been updated');
    });
  }
  </script>

</body>

</html>