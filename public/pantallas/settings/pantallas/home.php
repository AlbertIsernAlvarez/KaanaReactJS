<html>

<head>
  <style>
    .settings_container {
      width: 100%;
      margin: auto;
    }

    .settings_container .content {
      max-width:60rem;
      width:100%;
      margin:auto;
    }
    
    .settings_container .content .PF-card {
      flex-basis: calc(100%/3 - 1rem);
    }

    .settings_container .content .PF-grid.selector .PF-card .PF-image {
      padding-bottom: 70%;
    }

    .settings_container .content .PF-grid .PF-card .info p {
      -webkit-line-clamp: initial;
    }
    
  </style>
</head>

<body>

  <div class="settings_container">

    <div class="content" >
    	
      <div class="PF PF-grid selector" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); ">
        
        <div class="PF-card" onclick="$('[data-for=display-settingstab]').click();">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/settings/darkmode.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Display</h1>
        </div>
      </div>
      
      <div class="PF-card" opendd-href="?p=account">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/settings/account.svg');background-size: auto 7em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Account</h1>
        </div>
      </div>

      <div class="PF-card" onclick="$('[data-for=about-settingstab]').click();">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/settings/about.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >About</h1>
        </div>
      </div>

    </div>
    </div>

  </div>

</body>

</html>