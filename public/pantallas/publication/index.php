<?php 
$post=$_GET['post'];
?>
<html>

<head>
  <style>
    .contenedor-publication {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      margin: auto;
      width: 100%;
      max-width: 500px;
    }

    @media (max-width: 55rem) {
      .contenedor-publication .publication {
        margin: 0 auto;
        padding: 0;
      }
      .contenedor-publication {
        padding: 0;
        width: 100%;
      }
    }

    .feed {
      width: 100%;
    }
    <?php if($post){?>
    .seepost {
        display:none;
      }
<?}?>
  </style>
</head>

<body>
  <div class="contenedor-publication">
    <?php if($post != null){?>
    <div post data-id="<?=$post?>"></div>
    <?} else {?>
    
    <div class="PF-card">
      <div class="info" >
        <h1 t-dd >Looks like there's no publication with this id.</h1>
        <p t-dd >If you need help, don't forget that Kaana is at your disposal at the help center.</p>
      </div>
      <a class="PF PF-button transparent ripple" onclick="window.history.back();" t-dd>Go back</a>
    </div>
    
    <?}?>
  </div>
</body>

</html>