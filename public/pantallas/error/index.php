<html>

<head>
</head>

<body>
  
  
  <?php
  $error_motivo = "";
  $description_step2_error = "Redirecting";
  switch ($error) {
    case 400:
        $error_motivo = "Bad Request";
        break;
    case 401:
        $error_motivo = "Unauthorized";
        break;
    case 403:
        $error_motivo = "Forbbiden";
        break;
      case 404:
        $error_motivo = "Page Not Found";
        break;
      case 405:
        $error_motivo = "Method Not Allowed";
        break;
      case 406:
        $error_motivo = "Not Acceptable";
        break;
      case 407:
        $error_motivo = "Proxy Authentication Required";
        break;
      case 408:
        $error_motivo = "Request Timeout";
        break;
      case 409:
        $error_motivo = "Conflict";
        break;
      case 410:
        $error_motivo = "Gone";
        break;
      case 411:
        $error_motivo = "Leght Required";
        break;
      case 412:
        $error_motivo = "Precondition Failed";
        break;
      case 413:
        $error_motivo = "Payload Too Large";
        break;
      case 414:
        $error_motivo = "Request-URI Too Long";
        break;
      case 415:
        $error_motivo = "Unsupported Media Type";
        break;
      case 416:
        $error_motivo = "Requested Range Not Satisfiable";
        break;
      case 417:
        $error_motivo = "Expectation Failed";
        break;
      case 418:
        $error_motivo = "I'm a teapot";
        break;
      case 421:
        $error_motivo = "Misdirected Request";
        break;
      case 422:
        $error_motivo = "Unprocessable Entity";
        break;
      case 423:
        $error_motivo = "Locked";
        break;
      case 424:
        $error_motivo = "Failed Dependency";
        break;
      case 426:
        $error_motivo = "Upgrade Required";
        break;
      case 428:
        $error_motivo = "Precondition Required";
        break;
      case 429:
        $error_motivo = "Too Many Requests";
        break;
      case 431:
        $error_motivo = "Request Header Fields Too Large";
        break;
      case 444:
        $error_motivo = "Connection Closed Without Response";
        break;
      case 451:
        $error_motivo = "Unavailable For Legal Reasons";
        break;
      case 499:
        $error_motivo = "Client Closed Request";
        break;
      case 500:
        $error_motivo = "Internal Server Error";
        break;
      case 501:
        $error_motivo = "Not Implemented";
        break;
      case 502:
        $error_motivo = "Bad Gateway";
        break;
      case 503:
        $error_motivo = "Service Unavailable";
        break;
      case 504:
        $error_motivo = "Gateway Timeout";
        break;
      case 505:
        $error_motivo = "HTTP Version Not Supported";
        break;
      case 506:
        $error_motivo = "Variant Also Negotiates";
        break;
      case 507:
        $error_motivo = "Insufficient Storage";
        break;
      case 508:
        $error_motivo = "Loop Detected";
        break;
      case 510:
        $error_motivo = "Not Extended";
        break;
      case 511:
        $error_motivo = "Network Authentication Required";
        break;
      case 599:
        $error_motivo = "Network Connect Timeout Error";
        break;
}
        $title_error = "We have detected that the problem is that " . strtolower($error_motivo);
        $description_error = "We're analyzing what caused a $error error.";
        $title_step2_error = $error_motivo;
        
        switch ($error) {
          case 404:
            $seconds_2_error = "8";
            $title_step2_error = "Error $error is very common";
            $description_step2_error = "Possibly the link is misspelled, many users sometimes access urls that do not exist by mistake in a small character, do not worry.";
            
            $seconds_3_error = "5";
            $title_step3_error = "Check if you have entered the url correctly";
            $description_step3_error = "In a few moments you will be redirected.";
            $solved = true;
            break;
          case 403:
            $seconds_2_error = "8";
            $title_step2_error = "Error $error";
            $description_step2_error = "You don't have access to see this content.";
            
            $seconds_3_error = "5";
            $title_step3_error = "Check if you have entered the url correctly";
            $description_step3_error = "In a few moments you will be redirected.";
            $solved = true;
            break;
        }
  
  if($lasterrorstep == 2){
    $title_error = "Retrying to fix the error";
    $description_error = "The problem is that " . strtolower($error_motivo) . ", we are analyzing possible solutions.";
    $title_step2_error = "We haven't been able to solve the problem. 💁";
    $description_step2_error = "We have reported this error to the developers, now you will be redirected to the main screen.";
    $seconds_error = "8";
  }
  ?>
  
  
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="errorform" method="post" action="">
          
          <?php if($title_error){?>
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1 t-dd><?=$title_error?></h1>
              <?php if($description_error){?><p t-dd><?=$description_error?></p><?}?>
            </div>
          </div>
          <?}?>
          
          <?php if($title_step2_error){?>
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1 t-dd><?=$title_step2_error?></h1>
              <?php if($description_step2_error){?><p t-dd><?=$description_step2_error?></p><?}?>
            </div>
          </div>
          <?}?>
          
          <?php if($title_step3_error){?>
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1 t-dd><?=$title_step3_error?></h1>
              <?php if($description_step3_error){?><p t-dd><?=$description_step3_error?></p><?}?>
            </div>
          </div>
          <?}?>

        </form>
      </div>
    </div>
  </div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>