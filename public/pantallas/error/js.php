
<script>

$('#header').addClass('hidden');

  $(function() {
  var formprocessSteps = $('#errorform').append("<div></div>");
  var numberSteps = $('#errorform .step').length;
  var currentStep = 0; // Current step is set to be the first step (0)
  processstep(); // Display the crurrent step

    var secondssteps = Math.floor((Math.random() * 3000) + 2000);
    var secondssteps1 = null;
    var secondssteps2 = null;
    var secondssteps3 = null;
    
    <?php if($seconds_error != null){
  echo "secondssteps1 = " . $seconds_error . "000";
} elseif($seconds_2_error != null){
  echo "secondssteps2 = " . $seconds_2_error . "000";
} elseif($seconds_3_error != null){
  echo "secondssteps3 = " . $seconds_3_error . "000";
}?>
    
  var processtimeoutstep = null;

  function processstep(nextback) {
    $('.login_container .PF-progress.linear.loading').show();
    clearTimeout(processtimeoutstep);
    processtimeoutstep = setTimeout(function() {
      if (currentStep < numberSteps) {
        if (nextback == "back") {
          currentStep = currentStep - 1;
        } else {
          currentStep = currentStep + 1;
        }
        if(currentStep == 1 && secondssteps1){
          secondssteps = secondssteps1;
        } else if(currentStep == 2 && secondssteps2){
          secondssteps = secondssteps2;
        } else if(currentStep == 3 && secondssteps3){
          secondssteps = secondssteps3;
        }
        var step = $(".step:nth-child(" + currentStep + ")");
        $('.step').hide();
        step.show();

        if(!$(".step:nth-child(" + currentStep + ")").hasClass('auto')){
          step.find('input').first().focus();
        } else {
          setTimeout(function() {
            processstep();
          }, secondssteps);
        }
      } else {
        <?php if($continue){?>
        window.location.href = "<?=$continue?>";
        <?} else {?>
        window.location.href = "/";
        <?}?>
      }
      if(!$(".step:nth-child(" + currentStep + ")").hasClass('auto')){
      $('.login_container .PF-progress.linear.loading').hide();
      }
    }, 500);
    
  }
  });
</script>