<html>

<head>
  <style>
    .accounts-container {
      max-width: calc(50em + 2em);
      width: 100%;
      margin:auto;
    }
    
    .accounts-container .feed .post .top {
      display:none;
    }
    
  </style>
</head>

<body>

  <div class="accounts-container" >
    <div class="PF PF-content full noborder center transparent">
			<div class="container">
				<div class="PF-image PF-avatar ddbg-avatar circle" rgdd onclick="windowdd('./resources/window/cambiar-avatar.php');">
					<div class="PF PF-icon ripple"><i class="material-icons">cloud_upload</i></div>
				</div>
				<h1><?=$user_mismo_name?> <?=$user_mismo_lastname?></h1>
				<p t-dd>Select to change to another account</p>
				<div class="PF PF-button" style="margin: auto;" opendd-href="?p=logout&logoutall=true&<?php if($pagina or $app){?>&continue=<?=$server_url?><?}?>">Log out from all accounts</div>
			</div>
		</div>
    
    <div class="PF PF-grid circle center selector">


      <?php
      
      $user_people_ids = array_keys($_SESSION["users"]);
      $user_people_ids = implode("','", $user_people_ids);
      $user_people = mysqli_query($con,"SELECT * FROM users WHERE user_id IN('$user_people_ids') AND user_id NOT IN('$user_mismo_id') ORDER BY user_id DESC");
      while($row_user_people = mysqli_fetch_array($user_people))
      {
        $user_username_people = $row_user_people['username'];
        $name_people = $row_user_people['name'];
        $lastname_people = $row_user_people['lastname'];
        $color_people = $row_user_people['color'];
        $avatar_people = $row_user_people['avatar'];
        $user_id_people = $row_user_people['user_id'];
        if($name_people){$name_show_people = $name_people . " " . $lastname_people;} else {$name_show_people = $user_username_people;}
        $name_show_people = ucwords($name_show_people);
        if($avatar_people){
          $avatar_people = "//kaana.io/apps/photos/see.php?id=" . $avatar_people;
        } else {
          $avatar_people = "//user.kaana.io/user.svg";
        }
      ?>
      <a class="PF PF-card center" href="?p=accounts&account-switch=<?=$user_id_people?>">
        <div class="PF-image ripple" style=" background-image: url('<?=$avatar_people?>');" rgdd></div>
        <div class="info">
          <h1 t-dd><?=$name_show_people?></h1>
        </div>
      </a>
      <?}?>

      <!-----------------
      <?php
      if(!isset($_SESSION["users"]["10"])){?>
      <a class="PF PF-card center" href="?p=accounts&account-switch=10">
        <div class="PF-image ripple" style=" background-image: url('https://kaana.io/apps/photos/see.php?id=eQLcrA9hXiHhHxXFVjcmcGiKIA931LvnWrPEuxxHCXkoX7tN7AEE9OHggxMZyAxnEOdreukutLL4xVpmQPrrDgqgHnf');" rgdd></div>
        <div class="info">
          <h1 t-dd>Beta</h1>
        </div>
      </a>
      <?}?>

      <?php if(!isset($_SESSION["users"]["73"])){?>
      <a class="PF PF-card center" href="?p=accounts&account-switch=73">
        <div class="PF-image ripple" style="background-image: url('https://kaana.io/apps/photos/see.php?id=Kg1kilKei51MAfqtH5ARdmuiNFhHzYn6HCBTZxZyQptrsjnaMqPFMB5kEU3smSTXlWU7JUtUVNZvK5A1BCnHLZZRlVS');" rgdd></div>
        <div class="info">
          <h1 t-dd>Anonymous</h1>
        </div>
      </a>
      <?}?>
      ------------->
      
      <div class="PF PF-card center" opendd-href="?p=login">
					<div class="PF-image icon ripple"><i class="material-icons">add</i></div>
					<div class="info">
						<h1 t-dd>Add another account</h1>
					</div>
				</div>
    </div>

  </div>
  
</body>

</html>