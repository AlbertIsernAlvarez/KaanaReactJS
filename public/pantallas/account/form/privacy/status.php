<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Privacy</h1>
</div>
<form class="PF PF-form" id="formaccount-update-status" action="./pantallas/account/update/privacy/status.php" method="post" enctype="multipart/form-data">
	<div class="PF-select">
		<select id="PF-select" name="status">
			<?php if($user_mismo_status){?>
			<option value="0">Default</option>
			<option selected="selected" value="private">Private</option>
			<?} else {?>
			<option selected="selected" value="0">Default</option>
			<option value="private">Private</option>
			<?}?>
		</select>
		<label class="form-label" for="PF-select">Status</label>
	</div>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>
	$("#formaccount-update-status").ajaxForm({
		beforeSubmit: function(arr, $form, options) {
			$('.header .PF-progress.loading').show();
			alertdd.show('Saving profile privacy');
		},
		success: function(data) {
			$('.header .PF-progress.loading').hide();
			eval(data);
		}
	});
</script>