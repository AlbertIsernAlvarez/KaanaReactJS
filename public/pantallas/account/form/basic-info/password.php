<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Password</h1>
</div>
<form class="PF PF-form" id="formaccount-update-password" action="./pantallas/account/update/basic-info/password.php" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<input type="password" placeholder=" " name="currentpassword">
		<span t-dd>Current Password</span>
	</label>
	<br/>
	<label class="PF-textfield filled">
		<input type="password" placeholder=" " name="password">
		<span t-dd>Password</span>
	</label>
	<label class="PF-textfield filled">
		<input type="password" placeholder=" " name="confirmpassword">
		<span t-dd>Confirm Password</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>
	$("#formaccount-update-password").ajaxForm({
		beforeSubmit: function(arr, $form, options) {
			$('.header .PF-progress.loading').show();
			alertdd.show('Saving Password');
		},
		success: function(data) {
			$('.header .PF-progress.loading').hide();
			eval(data);
		}
	});
</script>