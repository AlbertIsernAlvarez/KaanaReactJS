<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Channel</h1>
</div>
<form class="PF PF-form" id="formaccount-update-channel" action="./pantallas/account/update/basic-info/channel.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<div class="PF-select">
  <i></i>
    <select name="channel">
      <option value="stable">Stable</option>
      <option value="test">Test</option>
      <option value="unstable">Unstable</option>
    </select>
    <label class="form-label" for="PF-select">Channel</label>
  </div>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

	$("select[name='channel'] option[value='<?=$user_mismo_channel?>']").attr("selected", true);

  $("#formaccount-update-channel").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Changing channel');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>