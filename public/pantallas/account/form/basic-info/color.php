<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Color</h1>
</div>
<form class="PF PF-form" id="formaccount-update-color" action="./pantallas/account/update/basic-info/color.php" method="post" enctype="multipart/form-data">
	<div class="PF-select">
		<select id="PF-select" name="color">
			<?php if($user_mismo_color){?>
			<option selected="selected" value="<?=$user_mismo_color?>"><?=ucfirst($user_mismo_color)?></option>
			<?}?>
			<option value="blue">Blue</option>
			<option value="red">Red</option>
			<option value="yellow">Yellow</option>
			<option value="orange">Orange</option>
			<option value="green">Green</option>
			<option value="greenwater">Green Water</option>
			<option value="pink">Pink</option>
			<option value="purple">Purple</option>
		</select>
		<label class="form-label" for="PF-select">Select color</label>
	</div>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>
	$("#formaccount-update-color").ajaxForm({
		beforeSubmit: function(arr, $form, options) {
			$('.header .PF-progress.loading').show();
		},
		success: function(data) {
			$('.header .PF-progress.loading').hide();
			eval(data);
		}
	});
</script>