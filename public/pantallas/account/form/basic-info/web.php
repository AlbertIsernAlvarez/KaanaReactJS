<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Website</h1>
</div>
<form class="PF PF-form" id="formaccount-update-web" action="./pantallas/account/update/basic-info/web.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<input type="web" placeholder=" " name="web" value="<?=$user_mismo_web?>">
		<span t-dd>Website</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

  $("#formaccount-update-web").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Saving web');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>