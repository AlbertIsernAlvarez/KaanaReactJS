<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Birth</h1>
</div>
<form class="PF PF-form" id="formaccount-update-birth" action="./pantallas/account/update/basic-info/birth.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<div class="PF-twocolumns">
		<label class="PF-textfield filled">
			<input type="text" placeholder=" " name="city" value="<?=$user_mismo_birth_city?>">
			<span t-dd>City of birth</span>
		</label>
		<label class="PF-textfield filled">
			<input type="date" placeholder=" " name="date" value="<?=$user_mismo_birth_day?>">
			<span t-dd>Date of birth</span>
		</label>
	</div>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

  $("#formaccount-update-birth").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Saving birth');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>