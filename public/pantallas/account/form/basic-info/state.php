<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>State</h1>
</div>
<form class="PF PF-form" id="formaccount-update-state" action="./pantallas/account/update/basic-info/state.php" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<textarea type="text" placeholder=" " name="state"><?=strip_tags($user_mismo_estado)?></textarea>
		<span t-dd>State</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

  $("#formaccount-update-state").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Saving state');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>