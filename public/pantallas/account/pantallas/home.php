<html>

<head>
  <style>
    .account_container {
      width: 100%;
      margin: auto;
    }

    .account_container .content {
      max-width:60rem;
      width:100%;
      margin:auto;
    }
    
    .account_container .content .PF-card {
      flex-basis: calc(100%/3 - 1rem);
    }

    .account_container .content .PF-grid.selector .PF-card .PF-image {
      padding-bottom: 70%;
    }

    .account_container .content .PF-grid .PF-card .info p {
      -webkit-line-clamp: initial;
    }
    
  </style>
</head>

<body>

  <div class="account_container">

    <div class="content" >
    	
      <div class="PF PF-grid selector" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); ">
        
        <div class="PF-card" onclick="$('[data-for=accounttab-personalinfo]').click();">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/account/imgs/personal.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Personal info</h1>
        </div>
      </div>

      <div class="PF-card" onclick="$('[data-for=accounttab-datapersonalization]').click();">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/account/imgs/personalization.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Data & Personalization</h1>
        </div>
      </div>

      <div class="PF-card" onclick="$('[data-for=accounttab-security]').click();">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/account/imgs/security.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Security</h1>
        </div>
      </div>

      <div class="PF-card" onclick="$('[data-for=accounttab-peoplesharing]').click();">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/account/imgs/people-sharing.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >People & Sharing</h1>
        </div>
      </div>

      <div class="PF-card" onclick="$('[data-for=accounttab-paymentssubscriptions]').click();">
        <div class="PF PF-image ripple" style="background-image:url('./pantallas/account/imgs/payments.svg');background-size: auto 5em; background-repeat: no-repeat;"></div>
        <div class="info">
          <h1 t-dd >Payments & Subscriptions</h1>
        </div>
      </div>

    </div>
    </div>

  </div>

</body>

</html>