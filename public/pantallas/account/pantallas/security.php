<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Account Security</h1>
			<p><span t-dd>Settings and recommendations to help you keep your account secure</span></p>
		</div>
	</header>
	<div class="grid-info">

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/password.php', 'fit');">
			<div class="left">
				<span t-dd>password</span>
			</div>
			<div class="right">
				<div class="double">
					<p>******</p>
					<span>Last changed <b>Undefined</b></span>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Privacy</h1>
		</div>
	</header>
	<div class="grid-info">

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/privacy/status.php', 'fit');">
			<div class="left">
				<span t-dd>Status</span>
			</div>
			<div class="right">
				<div class="double">
					<p user-data-status><?php if($user_mismo_status == 'private'){?>Private<?} elseif($user_mismo_status == null) {?>Default<?}?></p>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Activity</h1>
		</div>
	</header>
	<div class="content">
		<a class="PF PF-button transparent ripple" opendd-href="?p=activity">
              <div class="inside">
                <p t-dd>See your activity history</p>
              </div>
            </a>
	</div>
</div>