<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Profile</h1>
			<p><span t-dd>Some info may be visible to other people using Kaana services.</span></p>
		</div>
	</header>
	<div class="grid-info">

		<div class="item ripple" onclick="windowdd('./resources/window/cambiar-avatar.php');">
			<div class="left">
				<span t-dd>Avatar</span>
			</div>
			<div class="right">
				<div class="double">
					<p t-dd>An avatar helps personalize your account</p>
				</div>
				<div class="PF-avatar ddbg-avatar circle" rgdd></div>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/name.php', 'fit');">
			<div class="left">
				<span t-dd>Name</span>
			</div>
			<div class="right">
				<p user-data-fullname><?=$user_mismo_fullname?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/state.php', 'fit');">
			<div class="left">
				<span t-dd>State</span>
			</div>
			<div class="right">
				<p user-data-state><?=replaceemojis($user_mismo_estado)?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/birth.php', 'fit');">
			<div class="left">
				<span t-dd>Birth</span>
			</div>
			<div class="right">
				<p user-data-birth><?=$user_mismo_birth_date?> in <?=$user_mismo_birth_city?></p>
			</div>
		</div>
		
		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/gender.php', 'fit');">
			<div class="left">
				<span t-dd>Gender</span>
			</div>
			<div class="right">
				<p user-data-gender><?=$user_mismo_gender?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/color.php', 'fit');">
			<div class="left">
				<span t-dd>Color</span>
			</div>
			<div class="right">
				<p user-data-color><?=ucfirst($user_mismo_color)?></p>
			</div>
		</div>

	</div>
</div>


<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Contact info</h1>
		</div>
	</header>
	<div class="grid-info">


		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/email.php', 'fit');">
			<div class="left">
				<span t-dd>Email</span>
			</div>
			<div class="right">
				<p user-data-email><?=$user_mismo_email?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/phone.php', 'fit');">
			<div class="left">
				<span t-dd>Phone</span>
			</div>
			<div class="right">
				<p user-data-phone><?=$user_mismo_phone?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/web.php', 'fit');">
			<div class="left">
				<span t-dd>Website</span>
			</div>
			<div class="right">
				<p user-data-web><?=$user_mismo_web?></p>
			</div>
		</div>

	</div>
</div>

<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Kaana</h1>
		</div>
	</header>
	<div class="grid-info">

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/channel.php', 'fit');">
			<div class="left">
				<span t-dd>Channel</span>
			</div>
			<div class="right">
				<p user-data-channel><?=ucwords($user_mismo_channel)?></p>
			</div>
		</div>

	</div>
</div>

<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Kaana ID y Privacy</h1>
			<p t-dd>Kaana ID is designed to protect your information so you can choose what to share.</p>
			<br/>
			<p t-dd>We work hard to protect your privacy and collect only the data we need to improve your experience.</p>
			<br/>
			<p t-dd>When you sign in Kaana stores certain usage data for security, support, and reporting purposes, such as your IP address, time, security level, and log history.</p>
		</div>
	</header>
	<div class="grid-info"></div>
</div>