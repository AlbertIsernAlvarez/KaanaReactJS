<html>

<head>
	<style>

  html {
    height: 100%;
  }

  body {
    overflow: hidden;
  }

  #wrapper {
    overflow: auto;
  }
	
		.account-container {
			flex: 1;
		}
	</style>
</head>

<body>

	<div class="account-container">
		
		<div class="PF PF-content full noborder center between">
			<div class="container">
				<div class="PF-image PF-avatar ddbg-avatar circle" rgdd opendd-href="?p=kavatar"></div>
				<h1 user-data-fullname><?=$user_mismo_name?> <?=$user_mismo_lastname?></h1>
				<p t-dd>Manage your info, privacy, and security to make Kaana better for you</p>
			</div>
		</div>
		
		<div class="PF-tabs">
    <div class="PF PF-tabbar shadow">
      <div class="container">
        <ul>
        	<li class="ripple active" data-for="accounttab-home" data-taburl="./pantallas/account/pantallas/home.php?<?=$server_querystring?>"><span t-dd>Home</span></li>
          <li class="ripple" data-for="accounttab-personalinfo" data-taburl="./pantallas/account/pantallas/personal-info.php?<?=$server_querystring?>"><span t-dd>Personal Info</span></li>
          <li class="ripple" data-for="accounttab-datapersonalization" data-taburl="./pantallas/personalization/index.php?<?=$server_querystring?>"><span t-dd>Data & Personalization</span></li>
          <li class="ripple" data-for="accounttab-security" data-taburl="./pantallas/account/pantallas/security.php?<?=$server_querystring?>"><span t-dd>Security</span></li>
          <li class="ripple" data-for="accounttab-peoplesharing"><span t-dd>People & Sharing</span></li>
          <li class="ripple" data-for="accounttab-paymentssubscriptions" data-taburl="./pantallas/account/pantallas/payments-subscriptions.php?<?=$server_querystring?>"><span t-dd>Payments & Subscriptions</span></li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
    <div class="tabs">
    	<div class="tab" data-name="accounttab-home"></div>
      <div class="tab" data-name="accounttab-personalinfo"></div>
      <div class="tab" data-name="accounttab-datapersonalization"></div>
      <div class="tab" data-name="accounttab-security"></div>
      <div class="tab" data-name="accounttab-peoplesharing"></div>
      <div class="tab" data-name="accounttab-paymentssubscriptions"></div>
    </div>
  </div>
		
	</div>

	<script>
	  <?php if($user_mismo_color){?>$('body').addClass('PFC-<?=$user_mismo_color?>');<?}?>
	  <?php if($section){?> setTimeout(function(){ $("[data-for='accounttab-<?=$section?>']").click(); }, 600); <?}?>
	</script>

</body>

</html>