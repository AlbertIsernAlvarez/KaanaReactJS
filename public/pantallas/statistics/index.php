<html>

<head>
	<style>
		.statistics {
			width: 100%;
			height: auto;
			max-width: 80em;
			margin: auto;
			position: relative;
			display: flex;
		}
		
		.statistics>.column {
			display: flex;
			align-items: center;
			flex-direction: column;
			flex: 1;
		}
		
		.statistics>.column:first-child {
			max-width: 30em;
		}
		
		.statistics>.column>.block {
			margin: .5rem;
			width: calc(100% - 1rem);
			background-color: rgb(var(--PF-color-surface));
			border-radius: 1em;
			padding: .5em;
			margin: auto;
		}
		
		.statistics>.column>.block>.PF-grid {
			    grid-template-columns: repeat(auto-fill, minmax(70px, 1fr));
		}
		
		.statistics>.column>.block>h1,
		.statistics>.column>.block>p {
			padding: .5em;
			padding-bottom: 0;
		}
		
		.statistics>.column>.block>.total {
			padding: .5em;
			margin: .5em;
			width: calc(100% - 1em);
			border-radius: 1em;
			font-size: 2.2em;
			margin-top: 0;
		}
		
		.statistics>.column>.block>.signups {
			display: flex;
			align-items: center;
			font-size: 1.5em;
			padding: .5em;
		}
		
		.statistics>.column>.block>.signups>span {
			padding-right: .5em;
		}
		
		.statistics>.column>.block>.signups>p {
			padding-left: .5em;
		}
		
		@media (max-width: 75em) {
			.statistics {
				display: inline-block;
				padding-top: .5em;
				padding-bottom: .5em;
			}
			
			.statistics>.column:nth-child(2)>.block {
				margin-top: 1em;
			}
		}
	</style>
</head>

<body>
	<div class="statistics">
		<div class="column">
			<div class="block PF shadow">
				<h1><span id="statistics-total">0</span> <span t-dd>Total signups</span></h1>
				<div class="signups">
					<span id="statistics-12months">0</span>
					<p t-dd>Last 12 months</p>
				</div>
				<div class="signups">
					<span id="statistics-31days">0</span>
					<p t-dd>Last 31 days</p>
				</div>
				<div class="signups">
					<span id="statistics-7days">0</span>
					<p t-dd>Last 7 days</p>
				</div>
				<div class="signups">
					<span id="statistics-today">0</span>
					<p t-dd>Today</p>
				</div>
			</div>
			<div class="block PF shadow" style=" margin-top: 1em; ">
				<h1 t-dd>Newest faces</h1>
				<div class="PF-grid selector" id="statistics-lastusers"></div>
			</div>
		</div>
		<div class="column">
			<div class="block PF shadow">
				<div class="PF-grid selector" id="statistics-randusers"></div>
			</div>
		</div>
	</div>
	
	<script>
	$('.header').toggleClass('hidden');
	updatestatistics();
	setInterval(function(){
		updatestatistics();
	}, 8000);
	
	function updatestatistics() {
		$('#statistics-lastusers').load('./pantallas/statistics/resources/last-users.php');
		$('#statistics-randusers').load('./pantallas/statistics/resources/rand-users.php');
		$('#statistics-total').load('./pantallas/statistics/resources/numbers/total.php');
		$('#statistics-12months').load('./pantallas/statistics/resources/numbers/12months.php');
		$('#statistics-31days').load('./pantallas/statistics/resources/numbers/31days.php');
		$('#statistics-7days').load('./pantallas/statistics/resources/numbers/7days.php');
		$('#statistics-today').load('./pantallas/statistics/resources/numbers/today.php');
	}
	</script>
	
</body>

</html>