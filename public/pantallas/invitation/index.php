<html>

<head>
  <style>
    .invitation-container {
      max-width: 35em;
      margin: auto;
      padding-top: 1em;
      padding-bottom: 1em;
    }
  </style>
</head>

<body>

  <div class="invitation-container">
    <div class="PF PF-content full noborder center">
      <div class="container">
        <div class="PF PF-image transparent" style="background-image:url('./pantallas/invitation/invitation.svg');"></div>
        <h1 t-dd>You received an invitation to join Kaana</h1>
        <p t-dd>The first free integrated Social Platform with many services and applications, developed with collaborative feedback.</p>
      </div>
    </div>
    

  </div>

</body>

</html>