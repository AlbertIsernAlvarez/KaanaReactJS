<div class="PF PF-grid selector" >
<?php

$topic_sql = mysqli_query($con,"SELECT * FROM topics WHERE language IN('$language') ORDER BY demand DESC LIMIT 30");
while($row_topic_sql = mysqli_fetch_array($topic_sql))
{
	$topic_id = $row_topic_sql["id"];
	$topic_title = $row_topic_sql["title"];
	$topic_demand = $row_topic_sql["demand"];
	$topic_color = $row_topic_sql["color"];
	$topic_image = $row_topic_sql["image"];
	
?>

<div class="PF PF-card PFC-<?=$topic_color?>" opendd-href="?p=topics&topic=<?=$topic_title?>">
    <div class="PF PF-image ripple <?=$space_avatar_contain_spaces?>" style="background-image:url('<?=$topic_image?>'); background-color: rgba(var(--PF-color-primary), .2);" >
  <div follow-button data-id="<?=$topic_title?>" data-type="topic" data-classes="minimized absolute top right" ></div>
  </div>
    <div class="info">
    <h1><?=$topic_title?></h1>
    </div>
  </div>
	
<?}?>

</div>