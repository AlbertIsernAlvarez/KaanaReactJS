<?php
if($topic_id){
?>

<div class="topic-container PFC-<?=$topic_color?>">
<div class="PF PF-content full noborder center between">
	<div class="container">
		<?php if($topic_image){?><img kzoom="" data-kzoom-src="<?=$topic_image?>" class="PF-image PF-avatar circle ripple" style="background-image:url('<?=$topic_image?>');"/><?}?>
		<h1><?=$topic_title?></h1>
		<div follow-button data-id="<?=$topic_title?>" data-type="topic" ></div>
	</div>
</div>

<div class="PF-tabs">
	<div class="PF PF-tabbar shadow">
		<div class="container">
			<ul>
				<li class="ripple active" data-for="home-topictab" data-taburl="./pantallas/topics/pantallas/feed.php?<?=$server_querystring;?>"><span t-dd>Feed</span></li>
				<li class="ripple" data-for="about-topictab" data-taburl="./pantallas/topics/pantallas/about.php?<?=$server_querystring;?>" preload><span t-dd>About</span></li>
			</ul>
			<div class="slider"></div>
		</div>
	</div>
	<div class="tabs">
		<div class="tab" data-name="home-topictab"></div>
		<div class="tab" data-name="about-topictab"></div>
	</div>
</div>

</div>

<?} else {?>
<script> 
/*OPENDD WITHOUT GO BECAUSE DONT SAVE TO THE HISTORY*/
opendd('?p=search&q=<?=$topic?>&section=topics'); </script>
<?}?>