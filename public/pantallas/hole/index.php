<html>

<head>
  <style>
    
    .header {
      background-color: transparent;
    }
    
    .PF-emptypage.hole {
      background:transparent;
      max-width: 100%;
      flex: 1;
    }
    
    .PF-emptypage.hole * {
      text-align: left;
    }
    
    .PF-emptypage.hole .container .PF-image {
          height: 13em;
    width: 20em;
    }
    
    .PF-emptypage.hole .container>h1 {
      margin-bottom: .5em;
    font-size: 2.8em;
    font-weight: 900;
    }
    
    .blackhole {
      display: none;
    }
  
    .PF-dark .blackhole {
      display: inherit;
    }
  
    .PF-dark .whitehole {
      display: none;
    }
    
    
    
    
    @keyframes blue-circle-anim-x {
      50% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateX(44px);
      }
    }

    @keyframes blue-circle-anim-y {
      50% {
        animation-timing-function: cubic-bezier(0.55, 0, 0.2, 1);
        transform: translateY(17px);
      }
    }

    @keyframes green-rectangle-anim {
      100% {
        transform: rotate(360deg);
      }
    }

    @keyframes red-triangle-anim {
      50% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateY(25px) rotate(-53deg);
      }
    }

    @keyframes yellow-semicircle-anim {
      40% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateY(40px) rotate(-1deg);
      }
    }

    @keyframes grey-rounded-rectangle-anim {
      65% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateY(-48px) rotate(-75deg);
      }
    }

    .hole #bghole {
      height: 100%;
      left: 50%;
      position: absolute;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 100%;
      z-index: -1;
    }

    .hole #bghole img,
    .hole #bghole span {
      position: absolute;
    }

    .hole #bghole #blue-circle-container {
      animation: blue-circle-anim-x 9s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      left: calc(13% - 50px);
      top: calc(18% - 26px);
    }

    .hole #bghole #blue-circle-container::after {
      animation: blue-circle-anim-y 9s cubic-bezier(0.25, 0, 0.2, 1) infinite;
      content: url('/pantallas/hole/images/blue_circle.svg');
      position: absolute;
    }

    .hole #bghole #yellow-dots {
      left: 13%;
      top: 18%;
    }

    .hole #bghole #grey-rounded-rectangle {
      animation: grey-rounded-rectangle-anim 10s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      left: -42px;
      top: 45%;
    }

    .hole #bghole #red-triangle {
      animation: red-triangle-anim 9.6s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      bottom: 15%;
      left: 12%;
    }

    .hole #bghole #yellow-semicircle {
      animation: yellow-semicircle-anim 10s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      right: 28.5%;
      top: -50px;
      transform: rotate(-7deg);
    }

    .hole #bghole #green-rectangle {
      animation: green-rectangle-anim 40s infinite linear;
      bottom: 8%;
      right: -255px;
    }

    .hole #bghole #grey-oval {
      bottom: calc(8% + 24px);
      mix-blend-mode: multiply;
      right: 48px;
    }
    
  </style>
</head>

<body>

  <div class="PF PF-emptypage hole">
    <div class="container">
      <div class="PF-image transparent contain" style="background-image:url('//img.kaana.io/otros/emptypage.png');"></div>
      <h1 class="whitehole" t-dd >It seems that you're lost in a perpetual white hole.</h1>
      <h1 class="blackhole" t-dd >It seems that you're lost in a perpetual black hole.</h1>
      <h2 t-dd>Let us help guide you out and get you back home.</h2>
      <div class="PF PF-chips">
        <div class="PF PF-chip ripple" opendd-href="?p=applications">
          <div class="inside" t-dd>See applications list</div>
        </div>
        <div class="PF PF-chip ripple" opendd-href="?p=">
          <div class="inside" t-dd>Go home</div>
        </div>
        <div class="PF PF-chip ripple" opendd-href="?p=help&url=<?=$url_actual?>">
          <div class="inside" t-dd >Ask for help</div>
        </div>
      </div>
    </div>
    
    <div id="bghole">
      <span id="blue-circle-container"></span>
      <img id="green-rectangle" alt="" src="/pantallas/hole/images/green_rectangle.svg">
      <img id="grey-oval" alt="" src="/pantallas/hole/images/grey_oval.svg">
      <img id="grey-rounded-rectangle" alt="" src="/pantallas/hole/images/grey_rounded_rectangle.svg">
      <img id="red-triangle" alt="" src="/pantallas/hole/images/red_triangle.svg">
      <img id="yellow-dots" alt="" src="/pantallas/hole/images/yellow_dots.svg">
      <img id="yellow-semicircle" alt="" src="/pantallas/hole/images/yellow_semicircle.svg">
    </div>
    
  </div>

<?php if($user_mismo_color){?> <script> $('#body').addClass('PFC-<?=$user_mismo_color?>'); </script> <?}?>

</body>

</html>