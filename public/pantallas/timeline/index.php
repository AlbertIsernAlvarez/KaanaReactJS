<html>

<head>
  <style>
    .actividad_contenedor {
    margin: 0 auto;
    max-width: 45rem;
    padding: 0.5rem;
      width:100%;
    }

    .actividad_contenedor .card {
width: calc(100% - 1rem);
    background: var(--PF-color-bg-second-default);
    box-shadow: 0 1px 1.5px 0 rgba(0, 0, 0, .12), 0 1px 1px 0 rgba(0, 0, 0, .24);
    overflow: hidden;
    border-radius: 1em;
    margin: 0.5rem;
    display: inline-block;
    color: var(--PF-color-original-default);
    }
    
    .actividad_contenedor .card h1 {
      font-size:1rem;
      padding:0.5rem;
    }

    .actividad_contenedor .card .headercard {
    width: 100%;
    background: var(--PF-color-bg-first-default);
    position: relative;
    padding: 1rem;
    font-size: 16px;
    color: var(--PF-color-original-default);
    }

    .actividad_contenedor .card .contenidocard {
    width: 100%;
    padding: 0.5rem;
    font-size: 1rem;
    color: var(--PF-color-text-first-default);
    border-bottom: 1px solid var(--PF-color-bg-first-default);
    display: inline-flex;
    align-items: center;
    }

    .actividad_contenedor .card .avatar {
      width: 2rem;
      height: 2rem;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      margin-right: 1rem;
      border-radius: 100%;
    }
  </style>
</head>

<body>
  <div class="actividad_contenedor" id="actividadfeed">
      <?php $hoysi = "si"; include('card.php'); ?>
  </div>
  
  <script>
    $( document ).ready(function() {
      var timeoutmoreactividad;
      $(window).scroll(function() {
        if ($(window).scrollTop() == 0){
          //cargarmasactividadtop();
        } else if($(window).scrollTop() + window.innerHeight == $(document).height()) {
          cargarmasactividad();
        }
      });
    });
    
    function cargarmasactividad(){
      var ultimafecha = $("#actividadfeed .card").last().children('.contenidocard').last().children('.fechaactividad').text();
      $.get("./pantallas/<?=$pagina?>/card.php?ultimafecha=" + ultimafecha + '&<?=$_SERVER["QUERY_STRING"];?>', function (data) {
        $('#actividadfeed').append(data);
      });
    }
    
    function cargarmasactividadtop(){
      var ultimafecha = $("#actividadfeed .card").first().children('.contenidocard').first().children('.fechaactividad').text();
      $.get("./pantallas/<?=$pagina?>/card.php?updatefecha=" + ultimafecha + '&<?=$_SERVER["QUERY_STRING"];?>', function (data) {
        $('#actividadfeed').prepend(data);
      });
    }
      
    </script>

</body>

</html>