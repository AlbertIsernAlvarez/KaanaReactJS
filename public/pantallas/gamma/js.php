<script>
  var timeoutkeypressformsteps = null;
  var formprocessSteps = $("#gammaform").children('.processSteps');
  var numberSteps = $('#gammaform .step').length;
  var currentStep = 0; // Current step is set to be the first step (0)
  var beforeStep = 0;
  processstep(); // Display the crurrent step

  $(document).on("click", "#gammaform .next", function(event) {
    clearTimeout(timeoutkeypressformsteps);
    timeoutkeypressformsteps = setTimeout(function() {
      processstep();
      event.preventDefault();
    }, 500);
  });

  $(document).on("click", "#gammaform .back", function(event) {
    clearTimeout(timeoutkeypressformsteps);
    timeoutkeypressformsteps = setTimeout(function() {
      processstep("back");
      event.preventDefault();
    }, 500);
  });

  $(document).on("submit", "#gammaform", function(event) {
    processstep();
    event.preventDefault();
  });


  $(document).on("keypress", "#gammaform", function(e) {
    if (e.which == 13) {
      clearTimeout(timeoutkeypressformsteps);
      timeoutkeypressformsteps = setTimeout(function() {
        processstep();
        e.preventDefault();
      }, 500);
    }
  });

  var processtimeoutstep = null;
  var processtimeoutalert = null;
  
  function processstep(nextback) {
    var stepcontinue = true;
    $('.gamma_container .PF-progress.linear.loading').show();
    $('#gammaform .overlay').addClass('show');
    clearTimeout(processtimeoutstep);
    processtimeoutstep = setTimeout(function() {
      if (currentStep < numberSteps) {
        if (nextback == "back") {
          currentStep = currentStep - 1;
          beforeStep = currentStep + 1;
        } else {
          currentStep = currentStep + 1;
          beforeStep = currentStep - 1;
        }
        var step = $(".step:nth-child(" + currentStep + ")");

        if ($(".step:nth-child(" + beforeStep + ")") && nextback != "back") {
          $(".step:nth-child(" + beforeStep + ") [required]").each(function() {
            if ($(this).val().trim() === "") {
              clearTimeout(processtimeoutalert);
              processtimeoutalert = setTimeout(function() {
                alertdd.show("The field is empty");
              }, 500);
              $(this).focus();
              stepcontinue = false;
            } else if(stepcontinue == true) {
              $.ajax({
                type: "POST",
                url: "./pantallas/gamma/validate.php",
                data: $('#gammaform').serialize() + "&step=" + beforeStep, // serializes the form's elements.
                async: false,
                success: function(data) {
                  eval(data);
                }
              });
            }
          });
        }

        if (stepcontinue === true) {
          $(".step:nth-child(" + beforeStep + ")").hide();
          step.show();

          if (!$(".step:nth-child(" + currentStep + ")").hasClass('auto')) {
            step.find('input').first().focus();
          } else {
            setTimeout(function() {
              processstep();
            }, Math.floor((Math.random() * 3000) + 2000));
          }
        } else {
          currentStep = currentStep - 1;
          $('.gamma_container .PF-progress.linear.loading').hide();
          $('#gammaform .overlay').removeClass('show');
        }
      } else {

        $.ajax({
          type: "POST",
          url: $('#gammaform').attr('action'),
          data: $('#gammaform').serialize(), // serializes the form's elements.
          success: function(data) {
            eval(data); // show response from the final php script.
          }
        });
      }
      if (!$(".step:nth-child(" + currentStep + ")").hasClass('auto')) {
        $('.gamma_container .PF-progress.linear.loading').hide();
      }
      $('#gammaform .overlay').removeClass('show');
    }, 500);

  }

  /*
var r=255,g=0,b=0;

setInterval(function(){
  if(r > 0 && b == 0){
    r--;
    g++;
  }
  if(g > 0 && r == 0){
    g--;
    b++;
  }
  if(b > 0 && g == 0){
    r++;
    b--;
  }
  
  if($('body').hasClass('PF-dark')){
    document.body.style.setProperty('--PF-color-primary', r+','+g+','+b);
  } else {
    document.body.style.setProperty('--PF-color-primary', '');
  }
  
},50);
  */
</script>