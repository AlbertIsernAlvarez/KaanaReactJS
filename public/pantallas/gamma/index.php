<html>

<head>
  <?php $cache_version = file_get_contents(__DIR__.'/../../version.txt'); ?>
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/dumdarac-internal.css?version=<?=$cache_version?>">
  <style>
  
  .dd_screen {
    background-image: url(https://kaana.io/pantallas/gamma/exclusive.svg);
    background-size: 55vw;
    background-repeat: no-repeat;
    background-position: center right;
    transition: .2s;
  }
  
  .internal_container {
        background-color: transparent;
  }
    
    .box_container {
      background: rgba(var(--PF-color-surface));
    border-radius: 1em;
    padding: 1em;
    margin-bottom: 0;
    border-radius: 1em 1em 0 0;
    }
    
    
    @media (max-width: 40em) {
      .dd_screen {
        background-position: top 5vw center;
    background-size: 55vw;
      }
    }
    
  </style>
</head>

<body>
  <div class="internal_container">
    <div class="box_container PF shadow">
      <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
      <i class="material-icons" style="position: absolute; right: 0; top: 0; margin: .5em; cursor: pointer;" opendd-href="?p=languages">language</i>
      <div class="box">
        <form id="gammaform" method="post" action="./pantallas/gamma/insert.php" autocomplete="off" class="PF PF-form">
          <div class="overlay"></div>
          <div class="processSteps"></div>
          <div class="steps">
            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="Kaana" alt="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Kaana arrives soon</h1>
                <p t-dd>Subscribe to be notified when the Gamma version becomes public.</p>
              </div>
              <div class="block">
                <div class="PF-twocolumns" >
                  
                  <label class="PF-textfield filled">
                <input placeholder=" " type="text" name="name" >
                <span t-dd >First name</span>
              </label>
              
              <label class="PF-textfield filled">
                <input placeholder=" " type="text" name="lastname" >
                <span t-dd >Last name</span>
              </label>
                  
                </div>
                <label class="PF-textfield filled">
                <input placeholder=" " type="email" name="email" pattern=".{3,60}" required >
                <span t-dd >Email</span>
              </label>
              </div>
              <div class="block">
                <p><span t-dd>Building Kaana for everyone, everywhere.<br/>
                Want make us a donation? </span> <a opendd-href="?p=donate" t-dd>Learn more</a></p>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <button type="button" class="PF PF-button text" opendd-href="?p=about" t-dd>What is it?</button>
                  <div class="space"></div>
                  <button type="button" class="PF PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <div class="content step auto">
              <div class="logo_container">
                <div class="logo" title="Kaana"></div>
              </div>
              <div class="presentation">
                <h1 id="titlesubscription" t-dd ></h1>
                <p id="descriptionsubscription" t-dd>Thank you very much, also if you want to help the project you can make a donation. 😉</p>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <button type="button" class="PF PF-button" opendd-href="?p=about" t-dd>What is it?</button>
                  <button type="button" class="PF PF-button" opendd-href="?p=donate" t-dd>Donate</button>
                  <button type="button" class="PF PF-button" opendd-href="?p=help" t-dd>Get help</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <div style="display: none;">
 <input type="text" id="PreventChromeAutocomplete" 
  name="PreventChromeAutocomplete" autocomplete="address-level4" />
</div>

  <?php include_once(__DIR__."/js.php"); ?>

</body>

</html>