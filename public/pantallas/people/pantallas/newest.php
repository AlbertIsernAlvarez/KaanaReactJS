<h1 t-dd>Newest</h1>
<div class="people PF-grid circle" >
<?php
$user_people_select[] = " user_id!='$user_mismo_id' ";
if($qlanguage){
  $user_people_select[] = " language='$qlanguage' ";
}
$user_people_select = implode(" AND ", $user_people_select);
$user_people = mysqli_query($con,"SELECT * FROM users WHERE $user_people_select ORDER BY user_id DESC LIMIT 30");
while($row_user_people = mysqli_fetch_array($user_people))
  {
    $user_username_people = $row_user_people['username'];
    $name_people = $row_user_people['name'];
    $lastname_people = $row_user_people['lastname'];
    $color_people = $row_user_people['color'];
    $avatar_people = $row_user_people['avatar'];
    $user_id_people = $row_user_people['user_id'];
    if($name_people){$name_show_people = $name_people . " " . $lastname_people;} else {$name_show_people = $user_username_people;}
    $name_show_people = ucwords($name_show_people);
    if($avatar_people){
      $avatar_people = "//kaana.io/apps/photos/see.php?id=" . $avatar_people;
    } else {
      $avatar_people = "//user.kaana.io/user.svg";
    }
    ?>

    <div class="PF PF-card circle center <?php if($color_people){?>PFC-<?=$color_people?><?}?>" data-userid="<?=$user_id_people?>" opendd-href="?p=profile&u=<?=$user_username_people?>">
      <div class="PF-image ripple" style="background-image:url('<?=$avatar_people?>');" rgdd ></div>
      <div follow-button data-id="<?=$user_id_people?>" data-type="user" data-classes="minimized absolute top right" ></div>
      <div class="info">
        <?php if($name_show_people){?><h1><?=$name_show_people?></h1><?}?>
      </div>
    </div>
  <?}?>
</div>
