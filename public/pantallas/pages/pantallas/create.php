<html>

<head>
  <style>
  </style>
</head>

<body>
  <div class="PF PF-content full center transparent">
    <div class="container">
      <div class="image" style="background-image:url('./pantallas/pages/create.svg');"></div>
      <h1 t-dd>Create new page</h1>
      <p t-dd >Connect with more people, grow your audience and showcase your products with a free business Page.</p>
    </div>
  </div>
  <div class="content">
    <form class="PF PF-form" id="newpageform" action="./pantallas/pages/resources/create.php" method="post" enctype="multipart/form-data">
      
      <label class="PF-textfield filled">
            <input placeholder=" " type="text" required name="name" validate="name">
            <span t-dd >Page Name</span>
        </label>
      
      <label class="PF-textfield filled">
            <textarea placeholder=" " type="text" required="" id="input-field"></textarea>
            <span t-dd >Description</span>
        </label>


      <div class="PF PF-buttons full">
        <button class="PF PF-button" type="submit"><div class="inside" ><p t-dd >Create</p></div></button>
      </div>

    </form>
  </div>

  <script>
    $(document).ready(function() {
      $("#newpageform").ajaxForm(function(idpage) {
        openddgo("?p=page&page=" + idpage);
      });
    });
  </script>

</body>

</html>