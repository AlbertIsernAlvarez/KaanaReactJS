<html>

<head>
	<style>
		.related_topics {
    margin: auto;
    width: 100%;
    max-width: 50em;
    column-gap: 1em;
		}
		
		.related_topics>.topic {
			padding: .5em;
			border: solid 2px black;
			margin-bottom: 1em;
		}

		.related_topics>.topic>.related {
			margin-top: .5em;
			padding: 1em;
			border: solid 2px red;
		}
	</style>
</head>

<body>

	<div class="related_topics PF PF-grid">


		<?php
$topic_sql = mysqli_query($con,"SELECT * FROM topics WHERE language IN('$language') ORDER BY demand DESC, id DESC LIMIT 20");
while($row_topic_sql = mysqli_fetch_array($topic_sql))
{
	$topic_id = $row_topic_sql["id"];
	$topic_title = $row_topic_sql["title"];
	$topic_demand = $row_topic_sql["demand"];
	$topic_language = $row_topic_sql["language"];
	$topic_color = $row_topic_sql["color"];
?>
		<div class="topic">
			<div><?=$topic_title?></div>

			<?php
$topic_related_sql = mysqli_query($con,"SELECT * FROM topics t1 WHERE title IN(SELECT related FROM topics_related WHERE title IN('$topic_title') AND language IN('$topic_language') ORDER BY id DESC) ORDER BY id DESC LIMIT 20");
while($row_topic_related_sql = mysqli_fetch_array($topic_related_sql))
{
	$topic_related_id = $row_topic_related_sql["id"];
	$topic_related_title = $row_topic_related_sql["title"];
	$topic_related_demand = $row_topic_related_sql["demand"];
	$topic_related_color = $row_topic_related_sql["color"];
?>
			<div class="related">
				<?=$topic_related_title?>
			</div>
			<?}?>
		</div>

<?}?>
	</div>

</body>

</html>