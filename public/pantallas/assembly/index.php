<html>

<head>
  <style>
    
    .feed {
          width: 100%;
    max-width: 96rem;
    column-count: auto;
    column-width: 30rem;
      margin: auto;
    }
    
  </style>
</head>

<body>


<div>
  <div class="PF PF-content full noborder center" style="max-height:15rem;">
    <div class="container">
      <div class="PF-image" style="background-image:url('./pantallas/<?=$pagina?>/<?=$pagina?>.svg'); max-height: 3em;"></div>
      <h1 t-dd >Assembly</h1>
    </div>
  </div>
  </div>

  <div class="PF-tabs">
    <div class="PF PF-tabbar shadow">
      <div class="container">
        <ul>
          <li class="ripple active" data-for="assemblytabs-1" data-taburl="/dominio/hey/pantallas/conversation.php?id=1" ><span t-dd >Conversation</span></li>
          <li class="ripple" data-for="assemblytabs-2" data-taburl="" ><span t-dd >People</span></li>
          <li class="ripple" data-for="assemblytabs-3" data-taburl="" ><span t-dd >Media</span></li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
    <div class="tabs">
      <div class="tab" data-name="assemblytabs-1"></div>
      <div class="tab" data-name="assemblytabs-2"></div>
      <div class="tab" data-name="assemblytabs-3"></div>
    </div>
  </div>
  
  <script>
  
    $('body').addClass('PFC-red');
    
  </script>

</body>

</html>