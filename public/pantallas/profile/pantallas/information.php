<div <?php if($user_color){?>class="PFC-<?=$user_color?>"<?}?> style="background-image: url(https://img.kaana.io/backgrounds/information.png); background-size: 45em auto; background-attachment: fixed; background-position: top center; background-repeat: no-repeat;" >

<div>
  <div class="PF PF-toolbar">
  <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  <h1 t-dd>Information</h1>
</div>

<div class="PF PF-content full noborder center" style="background-color: transparent;">
      <div class="container">
        <img src="//img.kaana.io/1px.png?not" KZoom data-KZoom-src="<?=$user_avatar?>&w=900" class="PF-image PF-avatar circle ripple" style="background-image: url('<?=$user_avatar?>');" rgdd />
        <h1><?=$user_name?> <?=$user_lastname?></h1>
        <?php if($user_mismo_id === $user_id){?>
        <button class="PF PF-button ripple" style="margin: auto;" opendd-href="?p=account" t-dd >Edit information</button>
        <?}?>
      </div>
  <div follow-button data-id="<?=$user_id?>" data-type="user" data-size="" ></div>
    </div>
  
</div>

<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Basic</h1>
  <?php if($user_name != null){?><p><b t-dd >Name:</b> <?=$user_name?></p><?}?>
  <?php if($user_lastname != null){?><p><b t-dd >Surnames:</b> <?=$user_lastname?></p><?}?>
  <?php if($user_nacimiento != null){?><p><b t-dd >Birthdate:</b> <?=$user_nacimiento?></p><?}?>
  <?php if($user_ciudad != null){?><p><b t-dd >City:</b> <?=$user_ciudad?></p><?}?>
  <?php if($user_language != null){?><p><b t-dd >Speak:</b> <?=$user_language?></p><?}?>
  <?php if($user_karma != null){?><p><b t-dd >Karma:</b> <?=$user_karma?></p><?}?>
  <?php if($user_gender){?><p><b t-dd >Gender:</b> <span t-dd><?=$user_gender?></span></p><?}?>
  <?php if($user_estado){?><p><b t-dd >Status:</b> <?=replaceemojis($user_estado)?></p><?}?>
  <?php if($user_lastactivity){?><p><b t-dd>Last activity:</b> <span <?php if($user_lastactivity != 'online'){?>prettydate<?}?>><?=$user_lastactivity?></span></p><?}?>
  </div>
</div>

<?php if($user_biography != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Biography</h1>
    <p><?=$user_biography?></p>
  </div>
</div>
<?}?>

<?php if($user_trabajo != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Work</h1>
    <p><?=$user_trabajo?></p>
  </div>
</div>
<?}?>

<?php if($user_escuela != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Education</h1>
    <p><b t-dd >School:</b> <?=$user_escuela?></p>
  </div>
</div>
<?}?>

<?php if($user_mismo_id and $user_mismo_id != $user_id && $user_name != null){

$followbutton_me = mysqli_query($con,"SELECT * FROM user_follows WHERE type='user' AND id_seguir='$user_id' AND user_id='$user_mismo_id' LIMIT 1");
while($followbutton_me_row = mysqli_fetch_array($followbutton_me))
{ 
  $id_followbutton_me = $followbutton_me_row['id'];
  $date_followbutton_me = $followbutton_me_row['date'];
}
  
$followbutton_he = mysqli_query($con,"SELECT * FROM user_follows WHERE type='user' AND id_seguir='$user_mismo_id' AND user_id='$user_id' LIMIT 1");
while($followbutton_he_row = mysqli_fetch_array($followbutton_he))
{ 
  $id_followbutton_he = $followbutton_he_row['id'];
  $date_followbutton_he = $followbutton_he_row['date'];
}

?>
<div class="PF-card" >
  <div class="info">
    <h1><?=$user_name?> <span t-dd>& You</span></h1>
  <p>
  <?php if($id_followbutton_me){?><span t-dd >You followed</span> <?=$user_name?> <span prettydate ><?=$date_followbutton_me?></span> <?} else {?> <span t-dd >You do not follow</span> <?=$user_name?> <?}?> and <?php if($id_followbutton_he){?>  <?=$user_name?> <span t-dd >followed you</span> <span prettydate ><?=$date_followbutton_he?></span> <?} else {?> <?=$user_name?> <span t-dd >do not follow you</span><?}?>.</p>
  
  <!-------<p><span t-dd>Affinity</span> 0%</p>------>
  </div>
</div>
<?}?>

<?php if($user_signup_date != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Other information</h1>
  <p><span t-dd >Joined Kaana</span> <span prettydate ><?=$user_signup_date?></span> 😃</p>
  </div>
</div>
<?}?>
    
</div>