<?php if($pagina == 'profile' or !$pagina){?>
<div class="PF PF-toolbar">
  <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  <h1 t-dd>Following</h1>
</div>
<?}?>

<ul class="PF PF-list loadfollowers">
  
  <?php
    $id_amigos_publicaciones_p = array();
    $amigos_publicaciones_p = mysqli_query($con,"SELECT * FROM user_follows WHERE type='user' AND user_id='$user_id'");
  while($row_amigos_publicaciones_p = mysqli_fetch_array($amigos_publicaciones_p))
  {
    $id_amigos_publicaciones_p[] = $row_amigos_publicaciones_p["id_seguir"];
  }
  $id_amigos_publicaciones_p = implode(',', $id_amigos_publicaciones_p);
  $user = mysqli_query($con,"SELECT * FROM users WHERE user_id IN ($id_amigos_publicaciones_p) ORDER BY lastactivity DESC, RAND()");
  while($row_user_followers = mysqli_fetch_array($user))
  {
    $user_id_followers = $row_user_followers['user_id'];
$user_username_followers = $row_user_followers['username'];
$user_username_name_followers = $row_user_followers['name'];
$user_lastname_followers = $row_user_followers['lastname'];
$user_ciudad_followers = $row_user_followers['ciudad'];
$user_trabajo_followers = $row_user_followers['trabajo'];
$user_email_followers = $row_user_followers['email'];
$descripcion_followers = $row_user_followers['descripcion'];
$estado_followers = $row_user_followers['estado'];
$color_followers = $row_user_followers['color'];
$user_avatar_followers = $row_user_followers['avatar'];
$user_id_followers = $row_user_followers['user_id'];
$user_id_followers_id = $row_user_followers['user_id'];
if($user_username_name_followers != ''){$user_username_mostrar_followers = $user_username_name_followers;} else {$user_username_mostrar_followers = $user_username_followers;}
if($user_avatar_followers == null) {
  $user_id_followers_id = '0';
  $user_avatar_followers = "user.svg";
}
  ?>
<li class="ripple opendd closewindowdd follower <?php if($color_followers != null){?>PFC-<?=$color_followers?><?}?>" data-userid="<?=$user_id_followers?>" opendd-href="?p=profile&id=<?=$user_id_followers?>" >
    <div class="PF PF-avatar" style="background-image:url('//kaana.io/apps/photos/see.php?id=<?=$user_avatar_followers?>');" rgdd ></div>
    <div class="data">
      <?php if($user_username_mostrar_followers != ''){?><h1><?=$user_username_mostrar_followers?> <?=$user_lastname_followers?></h1><?}?>
      <?php if($descripcion_y_ciudad != ''){?><p><?=$descripcion_y_ciudad?></p><?}?>
    </div>
  <div follow-button data-id="<?=$user_id_followers?>" data-type="user" data-classes="minimized" ></div>
  </li>
<?}?>
    
    </ul>

<?php if($user_id_followers == null){?>
  <div class="PF PF-emptypage">
    <div class="container">
      <div class="image">
        <i class="material-icons">supervisor_account</i>
      </div>
      <h1 t-dd>Not following anyone yet</h1>
      <p t-dd>Here will be shown the users that follow.</p>
    </div>
  </div>
<?}?>

  <script>
        $(".header .loading_bar_container").hide();
  </script>