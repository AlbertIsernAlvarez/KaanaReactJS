
  <div class="PF PF-content full noborder center transparent">
			<div class="container">
				<div class="PF-image" style="background-image:url('./pantallas/profile/resources/privatelist/people.svg');background-size: contain;background-color: transparent;height: 2em;"></div>
				<h1 t-dd>Private list</h1>
				<label class="PF-textfield filled" style=" text-align: left; ">
			<input class="privatelist_addusers" type="text" placeholder=" " name="name" autocomplete="off">
			<span t-dd>Add user</span>
		</label>
			</div>
		</div>

  <ul class="PF PF-list privatelist"></ul>

  <script>
  $('.privatelist').load("./pantallas/profile/resources/privatelist/list.php");
  $(".privatelist_addusers").on("keyup paste", function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    $('.privatelist').load("./pantallas/profile/resources/privatelist/search.php?q=" + $('.privatelist_addusers').val());
  });
  </script>