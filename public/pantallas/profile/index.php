<html>

<head>
  <style>
    .profile_container {
      max-width: 50rem;
      width: 100%;
      margin: 0.5rem auto;
      display: flex;
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      flex-direction: column;
    }

    .profile_container>.top {
      display: flex;
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      flex-direction: column;
      max-width: calc(500px + 3em);
      margin: auto;
      width: 100%;
      padding: .5em;
    }

    .profile_container>.top>.bar {
      display: flex;
      flex-direction: row;
    }

    .profile_container>.top>.bar>.PF-avatar {
      width: 5em;
      height: 5em;
      margin-right: auto;
    }

    .profile_container>.top>.data {
      display: flex;
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      flex-direction: column;
      padding: 0.5rem;
    }

    .profile_container>.top>.data>h1 {
      font-size: 2.5em;
      font-weight: 600;
    }

    .profile_container>.top>.data>p {
      font-size: 1em;
      font-weight: 500;
      margin-top: .5em;
      white-space: pre-wrap;
    }

    .profile_container>.top>.statistics {
      padding: .5em;
      margin: auto;
      width: 100%;
      align-items: center;
      display: grid;
      grid-template-columns: 1fr 1fr 1fr 1fr;
      column-gap: .5em;
      row-gap: .5em;
      grid-auto-rows: 1fr;
    }

    .profile_container>.top>.statistics>.stat {
      text-align: center;
      border: solid 1px;
      border-color: rgba(var(--PF-color-on-surface), .1);
      border-radius: 1em;
      padding: 0.5em;
      height: 100%;
      align-items: center;
      vertical-align: middle;
      display: flex;
      flex-direction: column;
      cursor: pointer;
      background-color: rgb(var(--PF-color-surface));
    }

    :root .PF-dark .profile_container>.top>.statistics>.stat {
      border-color: rgb(var(--PF-color-surface));
      box-shadow: 0px 1px 3px 1px rgba(0,0,0, 0.1);
    }

    .profile_container>.top>.statistics>.stat>div>h1 {
      font-size: 2em;
      font-weight: 600;
    }

    .profile_container>.top>.statistics>.stat>div>p {
      font-size: 1em;
      font-weight: 500;
    }

    @media (max-width: 45em) {
      .profile_container>.top>.statistics>.stat>div>h1 {
        font-size: 1.8em;
        font-weight: 500;
      }
      .profile_container>.top>.statistics>.stat>div>p {
        font-size: 0.8em;
        font-weight: 500;
      }
    }
  </style>
</head>

<body>
<?php if($user_id){?>
  <div class="profile_container">
    <div class="top">
      <div class="bar">
        <img src="//img.kaana.io/1px.png?not" <?php if($user_id !== $user_id_mismo) {?>KZoom data-KZoom-src="<?=$user_avatar?>&w=900"<?}?> class="PF PF-avatar ripple <?php if($user_id === $user_id_mismo) {?>ddbg-avatar<?}?>" <?php if($user_id === $user_id_mismo) {?>opendd-href="?p=kavatar"<?}?> <?php if($user_id != $user_id_mismo) {?>style="background-image: url('<?=$user_avatar?>');"<?}?> rgdd />
        <?php if($user_status == 'private' and $user_id === $user_id_mismo){?><div class="PF PF-icon ripple" onclick="downmenudd('./pantallas/profile/resources/privatelist/edit.php?<?=$server_querystring?>');"><i class="material-icons">visibility</i></div><?}?>
        <div class="PF PF-icon ripple" onclick="downmenudd('./pantallas/profile/preview/menu.php?<?=$server_querystring?>');"><i class="material-icons">more_horiz</i></div>
        <div follow-button data-id="<?=$user_id?>" data-type="user" data-size=""></div>
      </div>
      <div class="data">
        <h1><?=$user_name?> <?=$user_lastname?></h1>
        <?php if($user_estado !='') {?>
          <p><?=replaceemojis($user_estado)?></p>
        <?}?>
      </div>
      <div class="statistics"></div>
    </div>
    
    <div class="content">
      <?php if(($user_status == 'private' and !in_array($user_id_mismo, $user_privatelist)) and $user_id !== $user_id_mismo){?>
      <div class="PF PF-emptypage">
        <div class="container">
          <div class="image"><i class="material-icons">security</i></div>
          <h1 t-dd>This account is private</h1>
          <p t-dd>You can submit a follow request and if it accepts you will be able to view his profile.</p>
        </div>
      </div>
      <?} else {?>
        <div feed data-type="all" data-classes="shadow" data-currenturl="?<?=$server_querystring?>"></div>
      <?}?>
    </div>

  </div>

  <script>
    
    <?php if($user_color != null){?>$('body').addClass('PFC-<?=$user_color?>');<?}?>
    $('.profile_container .top .statistics').load('./pantallas/profile/resources/statistics.php?<?=$server_querystring?>');

    $(document).on("click", ".stat.posts", function() {
      $('html, body').animate({scrollTop: $(".feed-multisource").offset().top}, 1000);
    });
    <?php if($function === 'scrollfeed'){?>setTimeout(function(){ $('html, body').animate({scrollTop: $(".feed-multisource").offset().top}, 1000); }, 1000);<?}?>
  </script>
<?} else {?>
<div class="PF PF-emptypage">
        <div class="container">
          <div class="image"><i class="material-icons">person_outline</i></div>
          <h1 t-dd>You are not logged in</h1>
          <p t-dd>Login and try again.</p>
        </div>
      </div>
<?}?>
</body>

</html>