<?php if($user_id != $user_mismo_id) {?>
<div class="PF PF-toolbar">
  <div class="PF PF-avatar ripple" style="background-image: url('<?=$user_avatar?>');" rgdd></div>
  <div class="dbl" >
    <h1><?=$user_name?> <?=$user_lastname?></h1>
    <?php if($user_estado !='') {?><p><?=$user_estado?></p><?}?>
  </div>
  <div follow-button data-id="<?=$user_id?>" data-type="user" data-size=""></div>
</div>
<?}?>

  <ul>
    <?php if($pagina != 'profile'){?>
    <a opendd-href="?p=profile&id=<?=$user_id?>" ><li><i class="material-icons">person_outline</i><span t-dd >See profile</span></li></a>
    <?}
    if(in_array($user_id, array_keys($_SESSION["users"])) and $user_id !== $user_mismo_id or (in_array($account_switch, $public_accounts_switch))){?>
    <a href="?account-switch=<?=$user_id?>&<?=$server_querystring?>" ><li><i class="material-icons">supervised_user_circle</i><span t-dd >Switch user</span></li></a>
    <?}?>
    <li onclick="windowdd('./pantallas/profile/pantallas/information.php?id=<?=$user_id?>&<?=$server_querystring?>', 'fit');" ><i class="material-icons">info</i><span t-dd >Information</span></li>
    <?php if($user_id == $user_mismo_id){?>
    <li opendd-href="?p=account" ><i class="material-icons">settings</i><span t-dd >Edit profile</span></li>
    <?} elseif($user_mismo_id) {?>
    <hr>
    <li opendda-href="?app=phone&id=<?=$user_id?>" onclick="alertdd.show('The call could not be made');" ><i class="material-icons">call</i><span t-dd >Call</span></li>
    <li opendd-href="?app=duo&videocall=<?=$user_id?>" ><i class="material-icons">video_call</i><span t-dd >Video Call</span></li>
    <li opendda-href="?p=hey&id=<?=$user_id?>" onclick="alertdd.show('The chat is not working right now');"  ><i class="material-icons">chat</i><span t-dd >Send a message</span></li>
    <li onclick="windowdd('./apps/pay/resources/transfer.php?app=pay&id=<?=$user_id?>', 'fit');" ><i class="material-icons">money</i><span t-dd >Send money</span></li>
    <hr>
    <?}?>
    <li onclick="downmenudd('./pantallas/share/modal.php?share=https://kaana.io/u/@<?=$user_username?>&<?=$server_querystring?>');" ><i class="material-icons">share</i><span t-dd >Share this profile</span></li>
  </ul>