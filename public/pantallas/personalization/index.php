<html>

<head>
	<style>
		.personalization_container {
			margin: auto;
			width: 100%;
			max-width: 50em;
		}
		
	</style>
</head>

<body>

	<div class="personalization_container">

		<?php if($pagina === 'personalization'){?>
		<div class="PF PF-content full noborder center transparent">
			<div class="container">
				<div class="PF-image" style="background-image:url('./pantallas/personalization/personalization.svg'); background-size: contain; min-height: 6em; background-color: transparent;"></div>
				<h1 t-dd>Get better recomendations</h1>
				<p><span t-dd>See more of what you're interested in, less of what you're not.</span> <br/> <span t-dd>Add interests & preferences.</span></p>
			</div>
		</div>
		<?} else {?>
		
		<div class="PF PF-card">
	<header>
		<h1 t-dd>Dark mode</h1>
	</header>
<div class="PF PF-form" style="padding: 1rem;">
	<label class="PF-switch" style="width:100%;">
		<input type="checkbox" darkmode-checkbox>
		<span t-dd> Enable</span>
	</label>
</div>
</div>

<script>

	if($(body).hasClass('PF-dark')){
		$('[darkmode-checkbox]').click();
	}

	$(document).on('change', '[darkmode-checkbox]', function() {
		if($('[darkmode-checkbox]').is(':checked')){
			$('body').removeClass('PF-light');
			$('body').addClass('PF-dark');
			document.cookie = 'darkmode=1';
		} else {
			$('body').removeClass('PF-dark');
			$('body').addClass('PF-light');
			document.cookie = 'darkmode=0';
		}
	});
</script>
		<?}?>

		<div class="PF PF-card">
			<header class="PF PF-toolbar">
				<div class="dbl">
					<h1 t-dd>Interested</h1>
					<p t-dd>See more of these topics</p>
				</div>
				<div class="PF PF-button" onclick="windowdd('./pantallas/personalization/pages/add/more.php');">Add</div>
			</header>
			<div class="PF PF-grid selector center" id="interestsMore" ></div>
		</div>

		<div class="PF PF-card">
			<header class="PF PF-toolbar">
				<div class="dbl">
					<h1 t-dd>Less Interested</h1>
					<p t-dd>See less of these topics</p>
				</div>
				<div class="PF PF-button" onclick="windowdd('./pantallas/personalization/pages/add/less.php');">Add</div>
			</header>
			<div class="PF PF-grid selector center" id="interestsLess" ></div>
		</div>
		
	</div>

	<script>
		$('#interestsMore').load("./pantallas/personalization/resources/more.php");
		$('#interestsLess').load("./pantallas/personalization/resources/less.php");
	</script>

</body>

</html>