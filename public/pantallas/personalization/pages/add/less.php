<div class="pay_container">

  <div class="PF PF-content full noborder center transparent">
			<div class="container">
				<div class="PF-image" style="background-image:url('./apps/pay/search-friends.svg'); background-size: contain; min-height: 6em; background-color: transparent;"></div>
				<h1 t-dd>Search for topics of you are interested in</h1>
				<input class="search_friends PF shadow" type="text" placeholder="Search people" />
			</div>
		</div>

  <ul class="PF PF-grid search_friends_results"></ul>

  <script>
    $(".search_friends").on("keyup paste", function(event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      $('.search_friends_results').load("./pantallas/personalization/resources/less-search.php?q=" + $('.search_friends').val());
    });

    function lessInterest(title){
    	$.post("./resources/more-less/update-data.php", {
    		moreless: "less",
    		content: title
    	}, function(data){
    		$('#interestsLess').load("./pantallas/personalization/resources/less.php");
    	});
    }
  </script>

</div>