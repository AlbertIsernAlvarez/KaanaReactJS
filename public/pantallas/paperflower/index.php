<html>

<head>


  <style>
    
    .style-guide {
      display: flex;
      justify-content: center;
      padding: 10px;
    }
    
    .style-guide .section {
      padding: 10px;
      margin: 10px;
      border-top:solid 1px #ddd;
    }

    .style-guide .block {
      max-width: 450px;
      flex:1 auto;
    }

    .style-guide .colors .color {
      display: flex;
      align-items: center;
      margin: 15px 0;
      position: relative;
    }

    .style-guide .colors .color .name {
      font-weight: bold;
      color: #314956;
    }

    .style-guide .colors .color .swatch {
      width: 35px;
      height: 35px;
      border-radius: 50%;
      margin-right: 10px;
    }

    .style-guide .colors .color .swatch.cherry {
      background-color: #e0393d;
    }

    .style-guide .colors .color .swatch.grey {
      background-color: #e7e7e7;
    }

    .style-guide .colors .color .hex {
      font-size: 13px;
      position: absolute;
      right: 20px;
    }

    .style-guide .typography h1,
    .style-guide .typography h2,
    .style-guide .typography h3,
    .style-guide .typography h4,
    .style-guide .typography h5,
    .style-guide .typography p:not(.description) {
    margin-bottom: 0;
    padding: 0.5rem;
    }

    .style-guide .typography h2,
    .style-guide .typography h3,
    .style-guide .typography h4,
    .style-guide .typography h5 {
      
    }

    .style-guide .typography .description {
      font-size: 12px;
      margin: 0;
      margin-left:0.5rem;
      color: #A0A4A7;
    }

    .style-guide .label {
    color: #314956;
    font-weight: bold;
    font-size: 2em;
    border-bottom: solid 20px #1f76e9;
    line-height: 0.4em;
    margin-bottom: 0.5em;
    text-shadow: 3px 3px var(--PF-color-bg-first-default);
    padding-left: 0.1em;
      width:fit-content;
    }
    
    .style-guide .PF-dark .label {
      color:#FFEB3B;
    }

    .style-guide .reverse {
      background: #fff;
      border-color: #e0393d;
      color: #e0393d;
    }

    .style-guide .reverse:hover {
      background: #e0393d;
      color: #fff;
    }
  </style>
</head>

<body>
  
  <div class="style-guide">
    <div class="block">
      <section class="colors">
        <div class="label">Color Palette</div>
        <div class="color">
          <div class="swatch cherry"></div>
          <div class="name">Cherry Red</div>
          <div class="hex">#e0393d</div>
        </div>
        <div class="color">
          <div class="swatch grey"></div>
          <div class="name">Cool Mountain Grey</div>
          <div class="hex">#e7e7e7</div>
        </div>
      </section>
      <section class="buttons">
        <div class="label">Buttons</div>
        <div class="PF PF-buttons">
          <button class="PF PF-button ripple">Send<div class="PF-badge" >16</div></button>
          <button class="PF PF-button ripple" disabled>Send</button>
          <button class="PF PF-button ripple transparent">Send</button>
          <button class="PF PF-button ripple border">Send</button>
        </div>
      </section>
    </div>
    <div class="block">
      <section class="typography">
        <div class="label">Typography</div>
        <h1>Header 1</h1>
        <p class="description">Font: Montserrat Bold / Color: #000</p>

        <h2>Header 2</h2>
        <p class="description">Font: Montserrat Bold / Color: #000</p>

        <h3>Header 3</h3>
        <p class="description">Font: Montserrat Bold / Color: #000</p>

        <h4>Header 4</h4>
        <p class="description">Font: Montserrat Bold / Color: #000</p>

        <h5>Header 5</h5>
        <p class="description">Font: Montserrat Bold / Color: #000</p>
        <p> This is Body text Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus iure sequi commodi, corrupti aperiam consequatur et nemo, magni suscipit autem, modi nihil cum assumenda tenetur ipsa. Modi excepturi nemo eos.</p>
        <p class="description">Font: Monserrat / Color: #000</p>
      </section>

    </div>
  </div>

</body>

</html>