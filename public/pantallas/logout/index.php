<html>

<head>
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="logoutform" method="post" action="">
          
          <?php
          if($user_mismo_id){
            $fecha_actividad = date("Y-m-d H:i:s");
            $id_actividad = $_GET['id'];
            $url_actual_actividad = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            mysqli_query($con, "INSERT INTO activity (user_id, type, url, id_actual, date, function) VALUES('$user_mismo_id', 'pagina', '?p=logout', '$id_actividad', '$fecha_actividad', 'logout' ) ") or die(mysqli_error());
            
            
            unset($_SESSION["users"]["$user_mismo_id"]);
            unset($_SESSION["users"]["current"]);
            
            $_SESSION["users"]["current"] = key($_SESSION["users"]);
            
            if($_GET['logoutall'] or !key($_SESSION["users"])){
              session_unset();
              session_destroy();
            }
            
            $title_logout = "Session closed correctly";
            $description_logout = "We hope to see you again soon 😃";
          } else {
            $title_logout = "There is no open session";
            $description_logout = "If you think it is due to an error, you can contact us at the help button. 💁‍♀️";
          }
          ?>
          
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1><span t-dd >See you soon</span><?php if($_GET['logoutall']){?>, closing all sessions<?} elseif($user_mismo_name){?>, <?=$user_mismo_name?><?}?></h1>
              <p t-dd >Leaving the session... In a few moments you will be redirected.</pt>
            </div>
          </div>
          
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1 t-dd><?=$title_logout?></h1>
              <p t-dd><?=$description_logout?></p>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>