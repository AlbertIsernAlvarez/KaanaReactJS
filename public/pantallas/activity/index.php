<html>

<head>
  <style></style>
</head>

<body>
  <ul class="PF PF-collection">
    <li class="collection-item ripple">
      <div class="PF PF-avatar ripple"></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
    <li class="collection-item ripple">
      <div class="PF PF-icon ripple"><i class="material-icons">folder</i></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
    <li class="collection-item ripple">
      <div class="PF PF-icon ripple"><i class="material-icons">insert_chart</i></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
    <li class="collection-item ripple">
      <div class="PF PF-icon ripple"><i class="material-icons">play_arrow</i></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
  </ul>
</body>

</html>