<?php
$id_page_get=$_GET['page'];
$page=mysqli_query($con, "SELECT * FROM pages WHERE id='$id_page_get' LIMIT 1");
while($row_page_page=mysqli_fetch_array($page)) {
  $id_page=$row_page_page['id'];
  $titulo_page=$row_page_page['name'];
  $date_page=$row_page_page['date'];
  $user_id_page=$row_page_page['user_id'];
  $description_page=$row_page_page['description'];
}

?>
<html>
<style>
  
  .profile-nav .follow-button-container .follow-button {
        border: solid 1px #ddd;
    box-shadow: none;
  }
  
  .profile-nav .follow-button-container {
    margin-right:0;
  }
  
  .contenedor {
    text-align: center;
  }

  .cover-photo {
    width: 100%;
    max-width: 980px;
    min-height: 250px;
    height: calc(100% - 20rem);
    margin: 0 auto;
    background-color: #f5f5f5;
    position: relative;
    background: white;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    border-radius: 0px;
    display: none;
    background-image: radial-gradient(at 0 0, #2196F3 0, rgba(255, 255, 255, 0) 70%, rgba(255, 255, 255, 0) 100%), radial-gradient(circle at 170% 0, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 0) 39%, #00BCD4 77%, #E91E63 92%, #F44336 100%), linear-gradient(135deg, #2196F3 0, #2196F3 24%, #9C27B0 100%);
  }

  .bodypage .center-col .cover-photo {
    display: block;
  }

  .cover-photo .menu-info-page {
    width: 100%;
    padding: 0.5rem;
    background: white;
    background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0.60), rgba(255, 255, 255, 0));
    position: absolute;
    top: 0;
    left: 0;
    overflow: hidden;
    text-align: center;
  }

  .cover-photo .menu-info-page ul {
    display: inline-flex;
  }

  .cover-photo .menu-info-page ul li {
    padding: 0rem 1rem;
    color: white;
  }

  .cover-photo .menu-info-page ul li span {
    color: white;
  }

  .bodypage {
    max-width: 980px;
    margin: 0 auto;
    position: relative;
    vertical-align: top;
    display: inline-flex;
    text-align: left;
    color: #263238;
    width: 100%;
    margin-bottom: 0.5rem;
  }

  .left-col {
    display: inline-table;
    margin: 0.5rem;
    position: sticky;
    top: 5rem;
    width: 100%;
    max-width: 180px;
    padding: 0.5rem;
  }

  .left-col a {
    color: <?=$color_page?>;
  }

  .center-col {
    border-radius: 1em;
    width: 100%;
    margin-top: 1rem;
    overflow: hidden;
    height:fit-content;
  }

  .right-col {
    background-color: #eee;
  }

  /* LEFT COL */

  .user-info h1 {
    font-size: 20px;
    font-weight: 900;
  }

  .user-info h2 {
    color: #666;
    font-size: 16px;
  }

  .user-info p.estado {
    font-size: 0.8rem;
  }

  .user-info .meta {
    padding: 8px 0;
    font-size: 14px;
  }

  .user-info .meta li {
    line-height: 1.6;
    display: inline-flex;
    align-items: center;
    width: 100%;
    word-break: break-word;
  }

  .user-info .meta p,
  .user-info .meta p span {
    word-break: break-word;
  }
  
  .user-info .meta i {
    padding-right: 0.5rem;
    font-size: 1.2em;
    color: <?=$color_page?>;
  }

  .user-info .meta a {
    color: <?=$color_page?>;
  }

  .profile-avatar {
    position: relative;
    width: 100%;
    margin-bottom: 0.5rem;
  }

  .profile-avatar .inner {
    width: 100%;
    padding-bottom: 100%;
    background-color: white;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    background-image: url(https://scontent-mxp1-1.xx.fbcdn.net/v/t1.0-1/c59.0.200.200/p200x200/399548…285987789_1102888142_n.png?oh=b0caf2b…&oe=5AE164C6);
    margin: auto;
    border: solid 1px #ddd;
    display: inline-block;
    border-radius: 1em;
  }

  /* CENTER */

  .posts {
    padding-top: 1rem;
    margin: auto;
  }

  .image-grid {
    width: 100%;
  }

  .image-grid li {
    background-color: #EFEFEF;
  }

  .image-grid.col-3 li {
    width: 32%;
    margin: 0;
    height: 0;
    padding-bottom: 30%;
    background-image: url(http://lorempixel.com/200/200/);
    -webkit-background-size: 100%;
    background-size: 100%;
    display: inline-block;
  }

  .image-grid.col-3 li:nth-child(3n) {
    margin-right: 0;
  }

  .profile-nav {
    background-color: white;
    padding: 0;
    width: 100%;
    display: inline-flex;
    align-items: center;
    border: 1px solid #ddd;
    border-top: none;
    border-radius: 0 0 1em 1em;
  }

  .profile-nav button {
    background: #DB4437;
    padding: 0.5rem 1rem;
    border-radius: 1em;
    color: white;
    display: inline-flex;
    align-items: center;
    font-weight: bold;
    font-size: 12px;
    cursor: pointer;
    text-align: center;
    margin: 0.5rem;
  }

  .profile-nav ul {
    padding: 0rem;
    text-align: left;
    display: inline-flex;
    align-items: center;
    width: 100%;
  }

  .profile-nav .button {
    background: <?=$color_page?>;
    padding: 0.5rem 1rem;
    border-radius: 1em;
    color: white;
    display: inline-flex;
    align-items: center;
    font-weight: bold;
    font-size: 12px;
    cursor: pointer;
    text-align: center;
    margin: 0.5rem;
  }

  .profile-nav ul li {
    display: inline-flex;
    margin: 0.5rem;
  }

  .profile-nav ul li a {
    padding: 0.5em 10px;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0;
    text-transform: capitalize;
    border: 1px solid var(--PF-color-original-default);
    border-right: 0;
    -webkit-transition: all .2s ease;
    -moz-transition: all .2s ease;
    transition: all .2s ease;
    font-weight: bold;
    font-size: 12px;
    cursor: pointer;
    display: inline-flex;
    align-items: center;
    text-align: center;
    margin: 0;
    color: var(--PF-color-original-default);
    fill: var(--PF-color-original-default);
    background-color: var(--PF-color-semitransparent-default);
  }

  .profile-nav ul li a:first-child {
    border: 1px solid #ddd;
    -webkit-border-radius: 1.5em 0 0 1.5em;
    -moz-border-radius: 1.5em 0 0 1.5em;
    border-right: 0;
  }

  .profile-nav ul li a:last-child {
    border: 1px solid #ddd;
    -webkit-border-radius: 0 1.5em 1.5em 0;
    -moz-border-radius: 0 1.5em 1.5em 0;
  }

  .profile-nav ul li a span {
    font-size: 16px;
    font-weight: 500;
    margin: 0 5px;
  }

  .profile-nav ul li a i {
    font-size: 18px;
    margin: 0 2px;
  }

  .cover-photo .PF.PF-button {
    position: absolute;
    top: 0;
    right: 0;
    display: none;
  }

  .cover-photo:hover .PF.PF-button {
    display: block;
  }

  .formulario_nuevap_contenedor {
    border: 1px solid #eee;
    box-shadow: none!important;
  }

  .formulario_nuevap_negab .formulario_nuevap_contenedor {
    border: none!important;
    box-shadow: 0 1px 1.5px 0 rgba(0, 0, 0, .12), 0 1px 1px 0 rgba(0, 0, 0, .24)!important;
  }

  .card-publication {
    margin: 0.5rem auto;
  }

  .posts .post.placeholder .shimmer {
    background-image: -webkit-linear-gradient(left, <?=$color_page?>, #ffffff, <?=$color_page?>)!important;
  }

  @media screen and (max-width: 50rem) {
    .center-col {
      border-radius: 1em;
    }
    .bodypage {
      display: inline-block;
    }
    .bodypage .cover-photo {
      display: block;
    }
    .bodypage .center-col .cover-photo {
      display: none;
    }
    .user-info {
      max-width: 100%;
      text-align: center;
      background: white;
      border-radius: 0;
      margin: 0;
    }
    .profile-avatar {
      margin: auto;
      max-width: 10rem;
    }
    .profile-avatar .inner {
      margin-top: -7rem;
      margin-bottom: 1rem;
    }
    .section.center-col.content {
      text-align: center;
    }
    .ocultartab {
      display: none;
    }
  }
</style>
</head>

<body>
  <div class="bodypage">
    <div class="cover-photo">
      <div class="cover-photo">
        <?php if($user_id_page==$user_id_mismo) {?>
        <a class="PF PF-button">
          <?=CAMBIARPORTADA?>
        </a>
        <?}?>
      </div>
    </div>
    <section class="left-col user-info">
      <div class="profile-avatar">
        <div class="inner PF shadow"></div>
      </div>
      <h1><?=$titulo_page?></h1>
      <p class="estado"><?=$description_page?></p>
      <ul class="meta">
        <li><i class="material-icons">people</i><span t-dd >10K Followers</span></li>
        <li><i class="material-icons">info</i><div><span t-dd >Created </span> <span prettydate t-dd ><?=$date_page?></span></div></li>
        <li><i class="material-icons">&#xE157;</i><a href="?app=navegador&url=<?=$user_page_web?>" target="_blank">www.kaana.io</a></li>
      </ul>
    </section>
    <section class="section center-col content PF shadow">
      <div class="cover-photo">
        <?php if($user_id_page==$user_id_mismo) {?>
        <a class="PF PF-button"><?=CAMBIARPORTADA?></a><?}?>
      </div>
      <!-- Nav -->
      <nav class="profile-nav">
        <ul>
          <div follow-button data-id="<?=$id_page?>" data-type="page" data-size="" ></div>
          <li class="menupageac active PF">
            <a><i class="material-icons">&#xE876;</i><span t-dd >Notifications</span></a>
            <a><i class="material-icons">chat</i><span t-dd >Message</span></a>
          </li>
          
          <li class="menupageac active PF">
            <a><i class="material-icons" style="transform: rotateY(180deg);">&#xE15E;</i><span t-dd >Share</span></a>
            <a><i class="material-icons">&#xE5D3;</i><span t-dd >More</span></a>
          </li>
        </ul>
        <div class="button"><?=INFORMACION?></div>
      </nav>
    </section>
  </div>
  <script>
    $(document).ready(function() {
        $('#posts').load('./resources/posts/posts.php?<?=$_SERVER["QUERY_STRING"];?>');
        $(window).scroll(function() {
          if ($(document).scrollTop() > 475) {
            $("#menu-info-page").addClass('menu-info-page-fijo');
          } else if ($('#menu-info-page').hasClass('menu-info-page-fijo')) {
            $("#menu-info-page").removeClass('menu-info-page-fijo');
          }
          $(".menu-info-page").css("top", "-" + $(window).scrollTop() / 2 + "px");
        });
        document.getElementById("namesubapp").innerHTML = "<?=$user_name?> <?=$user_lastname?>";
      }

    );

    function editarportada() {
      verventana();
      setTimeout(function() {
          $("#contenidoventana").load("./resources/ventanas/cambiar-portada.php?tipo=portada&user_id=<?=$user_id?>");
        }, 1000);
    }

    function editaravatar() {
      verventana();
      setTimeout(function() {
          $("#contenidoventana").load("./resources/ventanas/cambiar-avatar.php?tipo=avatar&user_id=<?=$user_id?>");
        }, 1000);
    }
  </script>
</body>
</html>