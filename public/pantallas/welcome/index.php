<html>

<head>

  <style>
    .welcome {
      max-width: 500px;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      margin: auto;
      width: calc(100% - 1rem);
    }
    
    .welcome .logo {
      width: 96px;
      height: 96px;
      position: relative;
    }
    
    .welcome .logo:before {
      content: '';
      display: block;
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-image: url(//img.kaana.io/logo/logo.svg);
      background-size: contain;
      background-position: center;
      background-repeat: no-repeat;
      z-index: 1;
    }
    
    .welcome .h1 {
      font-size: calc( (12vmin + 4rem) / 3);
      text-align: center;
      margin: 0.5em 0 0.2em 0;
      color: rgb(var(--PF-color-on-surface));
      font-weight: 600;
    }
    
    .welcome .h2 {
      font-size: 1.5rem;
      text-align: center;
      color: #939393;
      font-weight: 500;
    }
    
    .welcome .signin {
      margin: 3em 0 0 0;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      font-size: calc( (2vmin + 2em) / 3);
    }
    
    .welcome .yes {
      font-size: 1.2em;
      white-space: nowrap;
      background-color: #1759e9;
      flex-shrink: 0;
      font-weight: 500;
      margin: 0;
      padding: .5em .8em;
      text-decoration: none;
      text-transform: none;
      color: white;
      align-items: center;
      display: inline-grid;
      text-align: center;
      border-radius: 1.5em;
    }
    
    .welcome .no {
      margin: 1em 0 0 0;
      color: #5d5d5d;
      cursor: pointer;
      font-weight: 500;
      font-size: 1em;
    }
    
    .welcome a {
      color: inherit;
      text-decoration: inherit;
    }
    
    @keyframes blue-circle-anim-x {
      50% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateX(44px);
      }
    }
    
    @keyframes blue-circle-anim-y {
      50% {
        animation-timing-function: cubic-bezier(0.55, 0, 0.2, 1);
        transform: translateY(17px);
      }
    }
    
    @keyframes green-rectangle-anim {
      100% {
        transform: rotate(360deg);
      }
    }
    
    @keyframes red-triangle-anim {
      50% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateY(25px) rotate(-53deg);
      }
    }
    
    @keyframes yellow-semicircle-anim {
      40% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateY(40px) rotate(-1deg);
      }
    }
    
    @keyframes grey-rounded-rectangle-anim {
      65% {
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transform: translateY(-48px) rotate(-75deg);
      }
    }
    
    .welcome #bgwelcome {
      height: 100%;
      left: 50%;
      position: absolute;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 100%;
      z-index: -1;
      overflow: hidden;
    }
    
    .welcome #bgwelcome img,
    .welcome #bgwelcome span {
      position: absolute;
    }
    
    .welcome #bgwelcome #blue-circle-container {
      animation: blue-circle-anim-x 9s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      left: calc(13% - 50px);
      top: calc(18% - 26px);
    }
    
    .welcome #bgwelcome #blue-circle-container::after {
      animation: blue-circle-anim-y 9s cubic-bezier(0.25, 0, 0.2, 1) infinite;
      content: url('/pantallas/welcome/images/blue_circle.svg');
      position: absolute;
    }
    
    .welcome #bgwelcome #yellow-dots {
      left: 13%;
      top: 18%;
    }
    
    .welcome #bgwelcome #grey-rounded-rectangle {
      animation: grey-rounded-rectangle-anim 10s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      left: -42px;
      top: 45%;
    }
    
    .welcome #bgwelcome #red-triangle {
      animation: red-triangle-anim 9.6s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      bottom: 15%;
      left: 12%;
    }
    
    .welcome #bgwelcome #yellow-semicircle {
      animation: yellow-semicircle-anim 10s cubic-bezier(0.4, 0, 0.2, 1) infinite;
      right: 28.5%;
      top: -50px;
      transform: rotate(-7deg);
    }
    
    .welcome #bgwelcome #green-rectangle {
      animation: green-rectangle-anim 40s infinite linear;
      bottom: 8%;
      right: -255px;
    }
    
    .welcome #bgwelcome #grey-oval {
      bottom: calc(8% + 24px);
      mix-blend-mode: multiply;
      right: 48px;
    }
  </style>
</head>

<body>
  <div class="welcome">
    <div class="logo"></div>
    <div class="h1" t-dd>Welcome to Kaana</div>
    <div class="h2" t-dd>Click start to get started enjoying this wonderful community.</div>
    <div class="signin">
      <div class="yes PF shadow" opendd-href="?welcome=true" t-dd>Get Started</div>
      <div class="no" opendd-href="" t-dd>Skip</div>
    </div>

    <div id="bgwelcome">
      <span id="blue-circle-container"></span>
      <img id="green-rectangle" src="/pantallas/welcome/images/green_rectangle.svg">
      <img id="grey-oval" src="/pantallas/welcome/images/grey_oval.svg">
      <img id="grey-rounded-rectangle" src="/pantallas/welcome/images/grey_rounded_rectangle.svg">
      <img id="red-triangle" src="/pantallas/welcome/images/red_triangle.svg">
      <img id="yellow-dots" src="/pantallas/welcome/images/yellow_dots.svg">
      <img id="yellow-semicircle" src="/pantallas/welcome/images/yellow_semicircle.svg">
    </div>
  </div>

  <script>
  $('.header').addClass('hidden');
  </script>

</body>

</html>