<script>

<?php if($pagina === 'login'){?>
$('#header').addClass('hidden');
$(document).on("keydown", function(e) {
  	if (e.keyCode == 9) {
  		e.preventDefault();
  		e.stopPropagation();
  		$("#loginform").submit();
  	}
  });
<?}?>

  var timeoutkeypressformsteps = null;
  var formprocessSteps = $("#loginform").children('.processSteps');
  var numberSteps = $('#loginform .step').length;
  var currentStep = 0; // Current step is set to be the first step (0)
  var beforeStep = 0;
  processstep(); // Display the crurrent step

  $(document).on("click", "#loginform .next", function(event) {
    clearTimeout(timeoutkeypressformsteps);
    timeoutkeypressformsteps = setTimeout(function() {
      processstep();
      event.preventDefault();
    }, 500);
  });

  $(document).on("click", "#loginform .back", function(event) {
    clearTimeout(timeoutkeypressformsteps);
    timeoutkeypressformsteps = setTimeout(function() {
      processstep("back");
      event.preventDefault();
    }, 500);
  });

  $(document).on("submit", "#loginform", function(event) {
    processstep();
    event.preventDefault();
  });


  $(document).on("keypress", "#loginform", function(e) {
    if (e.which == 13) {
      clearTimeout(timeoutkeypressformsteps);
      timeoutkeypressformsteps = setTimeout(function() {
        processstep();
        e.preventDefault();
      }, 500);
    }
  });

  var processtimeoutstep = null;
  var processtimeoutalert = null;
  
  function processstep(nextback) {
    var stepcontinue = true;
    $('.login_container .PF-progress.linear.loading').show();
    $('#loginform .overlay').addClass('show');
    clearTimeout(processtimeoutstep);
    processtimeoutstep = setTimeout(function() {
      if (currentStep < numberSteps) {
        if (nextback == "back") {
          currentStep = currentStep - 1;
          beforeStep = currentStep + 1;
        } else {
          currentStep = currentStep + 1;
          beforeStep = currentStep - 1;
        }
        var step = $(".step:nth-child(" + currentStep + ")");

        if ($(".step:nth-child(" + beforeStep + ")") && nextback != "back") {
          $(".step:nth-child(" + beforeStep + ") [required]").each(function() {
            if ($(this).val().trim() === "") {
              clearTimeout(processtimeoutalert);
              processtimeoutalert = setTimeout(function() {
                alertdd.show("The field is empty");
              }, 500);
              $(this).focus();
              stepcontinue = false;
            } else if(stepcontinue == true) {
              $.ajax({
                type: "POST",
                url: "./pantallas/login/validate.php",
                data: $('#loginform').serialize() + "&step=" + beforeStep + "&" + location.search.slice(1), // serializes the form's elements.
                async: false,
                success: function(data) {
                  eval(data);
                }
              });
            }
          });
        }

        if (stepcontinue === true) {
          $(".step:nth-child(" + beforeStep + ")").hide();
          step.show();

          if (!$(".step:nth-child(" + currentStep + ")").hasClass('auto')) {
            step.find('input').first().focus();
          } else {
            setTimeout(function() {
              processstep();
            }, Math.floor((Math.random() * 3000) + 2000));
          }
        } else {
          currentStep = currentStep - 1;
          $('.login_container .PF-progress.linear.loading').hide();
          $('#loginform .overlay').removeClass('show');
        }
      } else {

        $.ajax({
          type: "POST",
          url: $('#loginform').attr('action'),
          data: $('#loginform').serialize() + "&" + location.search.slice(1), // serializes the form's elements.
          success: function(data) {
            eval(data); // show response from the final php script.

            //DONT SHOW ABOUT PAGE AGAIN BY DEFAULT
            document.cookie = 'about=disabled';
          }
        });
      }
      if (!$(".step:nth-child(" + currentStep + ")").hasClass('auto')) {
        $('.login_container .PF-progress.linear.loading').hide();
      }
      $('#loginform .overlay').removeClass('show');
    }, 500);

  }

  /*
var r=255,g=0,b=0;

setInterval(function(){
  if(r > 0 && b == 0){
    r--;
    g++;
  }
  if(g > 0 && r == 0){
    g--;
    b++;
  }
  if(b > 0 && g == 0){
    r++;
    b--;
  }
  
  if($('body').hasClass('PF-dark')){
    document.body.style.setProperty('--PF-color-primary', r+','+g+','+b);
  } else {
    document.body.style.setProperty('--PF-color-primary', '');
  }
  
},50);
  */
</script>