<html>

<head>
	<style>
		.comunicado_container {
			padding-top: 20px;
			font-size: 20px;
			color: rgb(var(--PF-color-on-surface));
			width: calc(100% - 1rem);
			margin: 0.5rem;
			max-width: 60rem;
			padding: 1em;
			margin: auto;
		}
		
		.comunicado_container * {
			display: inline-block;
		}
		
		.comunicado_container p {
			padding: 0.5rem;
			margin: 0px;
			text-align: justify;
			color: var(--PF-color-text-first-default);
			line-height: 1.5;
		}
		
		.comunicado_container h1 {
			font-size: 45px;
			color: rgb(var(--PF-color-on-surface));
			background-color: rgba(var(--PF-color-primary), .2);
			display: inline-block;
			padding: 0.5rem;
			margin: 0.5rem;
			font-weight: 600;
		}
		
		.comunicado_container h2 {
			display: inline-block;
			padding: 0.5rem;
			margin-top: 0.5rem;
			font-weight: 600;
		}
		
		.comunicado_container h3 {
			display: inline-block;
			padding: 0.5rem;
			margin-top: 0.5rem;
			font-weight: 600;
		}
		
		.comunicado_container span {
			display: block;
			margin: 0.5rem;
			margin-top: 20px;
			font-weight: 600;
			font-size: 20px;
			color: var(--PF-color-text-first-default);
		}
		
		.comunicado_container blockquote {
			font-size: 21px;
			background-color: #fdc689;
			padding: 1em;
			margin: 0.5rem;
		}
	</style>
</head>

<body>
	<div class="comunicado_container">
		<h1>Terms & Conditions</h1><br/>
		<h2>Welcome to Kaana</h2><br/>
		<p>These terms and conditions outline the rules and regulations for the use of Kaana's Website & App.</p>
		<br />
		<p>Kaana offices are at: Marni 52, Athens, 104 37, Greece.</p>
		<br />
		<p>By accessing this website we assume you accept these terms and conditions in full. Do not continue to use Kaana's website & App if you do not accept all of the terms and conditions stated on this page.</p>
		<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", "You" and "Your" refers to you, the person accessing this website and accepting the Company's terms and conditions.
			"The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment
			necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client's needs in respect of provision of
			the Company's stated services/products, in accordance with and subject to, prevailing law of Greece. Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore
			as referring to same.</p>
		<h2>Cookies</h2>
		<p>We employ the use of cookies. By using Kaana's website you consent to the use of cookies in accordance with DumDarac's privacy policy.</p>
		<p>Most of the modern day interactive web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our
			affiliate / advertising partners may also use cookies.</p>
		<h2>License</h2>
		<br/>
		<p>In Kaana all content is free.</p>
		<br/>
		<ol start="2">
			<li>These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products
				or services; and (c) fits within the context of the linking party's site.
			</li>
			<li>We may consider and approve in our sole discretion other link requests from the following types of organizations:
				<ol>
					<li>commonly-known consumer and/or business information sources such as Chambers of Commerce, American Automobile Association, AARP and Consumers Union;</li>
					<li>dot.com community sites;</li>
					<li>associations or other groups representing charities, including charity giving sites,</li>
					<li>online directory distributors;</li>
					<li>internet portals;</li>
					<li>accounting, law and consulting firms whose primary clients are businesses; and</li>
					<li>educational institutions and trade associations.</li>
				</ol>
			</li>
		</ol>
		<p>We will approve link requests from these organizations if we determine that: (a) the link would not reflect unfavorably on us or our accredited businesses (for example, trade associations or other organizations representing inherently suspect types
			of business, such as work-at-home opportunities, shall not be allowed to link); (b)the organization does not have an unsatisfactory record with us; (c) the benefit to us from the visibility associated with the hyperlink outweighs the absence of
			<?=$companyName?>; and (d) where the link is in the context of general resource information or is otherwise consistent with editorial content in a newsletter or similar product furthering the mission of the organization.</p>

		<p>These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and it products
			or services; and (c) fits within the context of the linking party's site.
		</p>

		<p>If you are among the organizations listed in paragraph 2 above and are interested in linking to our website, you must notify us by sending an e-mail to <a href="mailto:contact@kaana.io" title="send an email to contact@kaana.io">contact@kaana.io</a>.
			Please include your name, your organization name, contact information (such as a phone number and/or e-mail address) as well as the URL of your site, a list of any URLs from which you intend to link to our Web site, and a list of the URL(s) on our
			site to which you would like to link. Allow 2-3 weeks for a response.</p>

		<p>Approved organizations may hyperlink to our Web site as follows:</p>

		<ol>
			<li>By use of our corporate name; or</li>
			<li>By use of the uniform resource locator (Web address) being linked to; or</li>
			<li>By use of any other description of our Web site or material being linked to that makes sense within the context and format of content on the linking party's site.</li>
		</ol>
		<p>No use of Kaana's logo or other artwork will be allowed for linking absent a trademark license agreement.
		</p>
		<h2>Iframes</h2>
		<p>Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site.</p>
		<h2>Reservation of Rights</h2>
		<p>We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. We also reserve the right to amend
			these terms and conditions and its linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions.</p>
		<h2>Removal of links from our website</h2>
		<p>If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.</p>
		<p>Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.</p>
		<h2>Disclaimer</h2>
		<p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory
			quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:</p>
		<ol>
			<li>limit or exclude our or your liability for death or personal injury resulting from negligence;</li>
			<li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
			<li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
			<li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
		</ol>
		<p>The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this
			disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>
		<p>To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>
	</div>


</body>

</html>