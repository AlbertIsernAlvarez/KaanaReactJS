<html>

<head>
	<style>

@media (min-width: 50em) {
body:not([scrolling='true']) .header .flex-container .flex-center,
body:not([scrolling='true']) .header .flex-container .flex-right .icons{
    opacity: 0;
    pointer-events: none;
    }
}
	
		.dd_screen h1 {
			text-align: center;
			margin: 1rem 0;
			font-size: 3rem;
			line-height: 3rem;
			margin: auto;
		}
		
		.dd_screen section {
			padding: 3rem 1rem;
		}
		
		.dd_screen section .section-title {
			text-align: center;
			margin: 0 0 2rem;
			font-size: 1.5rem;
			line-height: 2rem;
		}
		/* Landing Page */
		
		.dd_screen .landing-page {
    text-align: center;
    font-size: 1.25rem;
    line-height: 1.5rem;
    padding: 4rem 0;
    min-height: 50vh;
    display: flex;
		}
		/* categories */
		
		.dd_screen .categories-container {
			display: flex;
			justify-content: center;
			flex-wrap: wrap;
			margin: 0 -.25rem;
		}
		
		@media (min-width: 1000px) {
			.dd_screen .categories-container {
				max-width: 1000px;
				margin: 0 auto;
			}
		}
		
		.dd_screen .categories-container .category {
			background: rgb(var(--PF-color-surface));
    padding: .5em 1em;
    margin: .5em;
    white-space: nowrap;
    cursor: pointer;
    position: relative;
    border-radius: 4px;
    flex-grow: 1;
    text-align: center;
    width: 40%;
    box-sizing: border-box;
    border-radius: 1.5em;
		}
		
		@media (min-width: 600px) {
			.dd_screen .categories-container .category {
				flex-grow: 0;
				width: auto;
			}
		}
		
		.dd_screen .categories-container .category.like {
			background: rgba(var(--PF-color-primary), .2);
			color: rgb(var(--PF-color-primary));
		}
		/* Titles */
		
		.dd_screen .topics-container {
    display: flex;
    justify-content: center;
			margin: 0 -.5rem;
		}
		
		@media (min-width: 1000px) {
			.dd_screen .topics-container {
				max-width: 1000px;
				margin: 0 auto;
			}
		}
		
		
		.dd_screen .topics-container .topic.like {
			opacity: .25;
		}
		/* Providers */
		
		.dd_screen .providers-container {
			display: flex;
			justify-content: center;
			flex-wrap: wrap;
			margin: 0 -.25rem;
		}
		
		@media (min-width: 1000px) {
			.dd_screen .providers-container {
				max-width: 1000px;
				margin: 0 auto;
			}
		}
		
		.dd_screen .providers-container .provider {
			background-color: rgb(var(--PF-color-surface));
			padding: .75rem 2rem .75rem .75rem;
			margin: 4px;
			white-space: nowrap;
			cursor: pointer;
			position: relative;
			border-radius: 4px;
			display: flex;
			align-items: center;
			width: 90%;
			flex-grow: 1;
		}
		
		@media (min-width: 600px) {
			.dd_screen .providers-container .provider {
				flex-grow: 0;
				width: auto;
			}
		}
		
		.dd_screen .providers-container .provider-icon {
			width: 32px;
			margin-right: 16px;
			border-radius: 4px;
		}
		
		.dd_screen .providers-container .provider.selected {
			background: rgba(var(--PF-color-primary), .2);
			color: rgb(var(--PF-color-primary));
		}
		
		.dd_screen .providers-container .provider.selected .provider-icon {
			opacity: .5;
		}
		
		/* Statusbar */
		
		.dd_screen .statusbar {
			background-color: rgba(var(--PF-color-surface));
			position: sticky;
			width: 100%;
			padding: 1rem;
			right: 0;
			bottom: 0;
			left: 0;
			text-align: center;
			z-index: 100;
			display: flex;
			justify-content: center;
			align-items: center;
			transition: transform .1s ease-out;
			flex-wrap: wrap;
			box-sizing: border-box;
		}
		
		.dd_screen .statusbar.hide {
			opacity: 0;
			pointer-events: none;
		}
		
		.dd_screen .quality-score {
			width: 100%;
			display: flex;
			align-items: center;
			justify-content: center;
			margin-bottom: 1rem;
		}
		
		@media (min-width: 700px) {
			.dd_screen .quality-score {
				width: auto;
				margin-bottom: 0;
				margin-right: 2rem;
			}
		}
		
		.dd_screen .quality-score--hidden {
			display: none;
		}
		
		.dd_screen .quality-score__label {
			white-space: no-wrap;
		}
		
		.dd_screen .quality-score__bar {
			background: rgb(var(--PF-color-surface));
			height: 16px;
			width: 20%;
			position: relative;
			border-radius: 4px;
			overflow: hidden;
			min-width: 80px;
			margin-left: 1rem;
		}
		
		@media (min-width: 600px) {
			.dd_screen .quality-score__bar {
				min-width: 200px;
			}
		}
		
		.dd_screen .quality-score__bar__active {
			background: linear-gradient(to right, #465183 0%, #a4ace5 100%);
			position: absolute;
			left: 0;
			height: 100%;
			border-radius: 1.5em;
			text-align: right;
			padding-right: .5rem;
			box-sizing: border-box;
			font-weight: bold;
			font-size: 12px;
			line-height: 16px;
		}
		
		.dd_screen a.button {
			display: block;
			background: rgb(var(--PF-color-primary));
			color: white;
			text-decoration: none;
			border-radius: 1.5em;
			padding: .5rem 1.5rem;
			font-weight: bold;
		}
		
		.dd_screen a.button.disabled {
			opacity: .5;
		}
	</style>

</head>

<body>

	<section class="landing-page">
		<h1>Customize your Kaana account</h1>
	</section>
	<section class="categories">
		<h2 class="section-title" t-dd>Which topics do you like most?</h2>
		<div class="categories-container">
			<?php
			$moreless_more = mysqli_query($con,"SELECT title FROM topics WHERE language='$language' ORDER BY demand DESC LIMIT 20");
			while($row_moreless_more = mysqli_fetch_array($moreless_more)){?>
			<div class="category PF shadow"><?=$row_moreless_more['title']?></div>
			<?}?>
		</div>
	</section>
	<section class="titles">
		<h2 class="section-title">Select a few titles you like.</h2>
		<div class="PF PF-grid topics-container" id="topics-container">
			<div class="topic PF shadow"><img src="https://images.justwatch.com/topic/8583458/s332/game-of-thrones"></div>
		</div>
	</section>
	<section class="providers" id="providers">
		<h2 class="section-title">Select your streaming services.</h2>
		<div class="providers-container">
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/430997/s100" class="provider-icon">Netflix</div>
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/52449539/s100" class="provider-icon">Amazon Prime Video</div>
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/120096335/s100" class="provider-icon">Sky Ticket</div>
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/120096503/s100" class="provider-icon">Sky Go</div>
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/430988/s100" class="provider-icon">Maxdome</div>
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/614491/s100" class="provider-icon">Netzkino</div>
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/85100561/s100" class="provider-icon">Das Erste Mediathek</div>
			<div class="provider PF shadow"><img src="https://images.justwatch.com/icon/94568507/s100" class="provider-icon">Arte</div>
		</div>
	</section>

	<div class="statusbar hide PF shadow">
		<div class="quality-score">
			<div class="quality-score__label">Recommendations quality</div>
			<div class="quality-score__bar">
				<div class="quality-score__bar__active"></div>
			</div>
		</div>
		<a class="button disabled">Check out your Recommendations</a>
	</div>


	<script>

	$('#topics-container').load('./pantallas/customize/resources/topics.php');
	$('#categories-container').load('./pantallas/customize/resources/categories.php');

		var titlesCounter = 0;
		var titlesCounterShow = 5;
		var titlesCounterMilestone = 10;
		var titlesCounterGoal = 15;
		var providersCounter = 0;
		var LastActionTitleClick = false;

		$(".dd_screen .category").click(function(e) {
			lastActionTitleClick = false;
			$(this).toggleClass("like");
			// inserttopics();
		});

		$(".dd_screen .provider").click(function(e) {
			lastActionTitleClick = false;
			$(this).toggleClass("selected");
			providersCounter = $(".dd_screen .provider.selected").length;
			// update button
			updateButton();
		});

		$(document).on("click", ".dd_screen .topic", function() {
			lastActionTitleClick = true;
			$(this).toggleClass("like");
			titlesCounter = $(".dd_screen .topic.like").length;
			// insert topics
			var index = $(".dd_screen .topic").index(this);
			inserttopics(index);
			// show statusbar
			if (titlesCounter == titlesCounterShow) $(".statusbar").removeClass("hide");
			// calculate and set fake score
			// var score = Math.min( 98, Math.round( Math.log10( titlesCounter ) * 65 ) ) + "%";
			var score = Math.round(Math.tanh(0.09 * titlesCounter) * 98) + "%";
			$(".dd_screen .quality-score__bar__active").css("width", score).text(score);;
			// update button
			updateButton();
		});

		$(".dd_screen .button").click(function(e) {
			lastActionTitleClick = false;
			if (titlesCounter >= titlesCounterMilestone && providersCounter > 0) {
				// quit onboarding
			} else if (titlesCounter >= titlesCounterMilestone) {
				// scroll to provider selection
				$("body,html").animate({
					scrollTop: $("#providers").offset().top
				}, 200);
				$(".dd_screen .quality-score").addClass("quality-score--hidden");
				updateButton();
			} else if ($(this).hasClass('disabled')) {
				e.preventDefault();
				return;
			}
		});

		function inserttopics(index = 0, number = 5) {
			// insert 5 topics at position index plus 3
			index += 3;
			var topics = $(".dd_screen .topic").slice(-number);
			index === 0 ?
				$(".dd_screen .topics-container").prepend(topics) :
				$(".dd_screen .topic:nth-child(" + index + ")").after(topics);
		}

		function updateButton() {
			if (titlesCounter < titlesCounterMilestone) {
				$(".dd_screen .button").addClass("disabled");
				$(".dd_screen .button").text("Select " + (titlesCounterMilestone - titlesCounter) + " more titles to proceed");
			} else {
				if (providersCounter > 0) {
					$(".dd_screen .button").removeClass("disabled");
					$(".dd_screen .button").text("Check out your recommmendations");
				} else if (lastActionTitleClick) {
					$(".dd_screen .button").removeClass("disabled");
					$(".dd_screen .button").text("Proceed to next step");
				} else {
					$(".dd_screen .button").addClass("disabled");
					$(".dd_screen .button").text("Select at least 1 service");
				}
			}
		}
		
	</script>


</body>

</html>