<?php $color_app_dd = "#d2e3fc"; ?>
<html>

<head>
  <style>
    body {
      background-color: var(--PF-color-bg-first-default);
    }

    .clk {
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 30vh;
    height: fit-content;
    margin: auto;
    z-index: -1;
    text-align: center;
    }
    
    .clk canvas {
      border-radius:100%;
    }
  </style>
</head>

<body>
  <div class="clk">
    <canvas id="analog" width="280" height="280"></canvas>
    <!----<h1 id="timeclock" ></h1>---->
  </div>
  <script>
    var chc = "<?=$color_app_dd?>";
    $("#analog").css('background', chc);

    var canvas = document.getElementById("analog");
    var ctx = canvas.getContext("2d");
    var radius = canvas.height / 2;
    ctx.translate(radius, radius);
    radius = radius * 0.90
    drawClock();

    function drawClock() {
      drawFace(ctx, radius, chc);
      drawTime(ctx, radius, "#2196f3", "#E34688"); //Hours'& Minutes'Hands Color, Seconds'Hand Color
      setTimeout(drawClock, 999);
    }

    function drawFace(ctx, radius, col) {
      ctx.beginPath();
      ctx.arc(0, 0, radius + 5, 0, 2 * Math.PI);
      ctx.fillStyle = "#2196f3";
      ctx.fill();
      ctx.beginPath();
      ctx.arc(0, 0, radius, 0, 2 * Math.PI);
      ctx.fillStyle = col;
      ctx.fill();
    }

    function drawTime(ctx, radius, colb, cols) {
      var now = new Date();
      var hour = now.getHours() % 12;
      var minute = now.getMinutes();
      var second = now.getSeconds();
      hour = (hour * Math.PI / 6) + (minute * Math.PI / (6 * 60)) + (second * Math.PI / (6 * 60 * 60));
      drawHand(ctx, hour, radius * 0.5, radius * 0.03, colb, "round");
      minute = (minute * Math.PI / 30) + (second * Math.PI / (30 * 60));
      drawHand(ctx, minute, radius * 0.8, radius * 0.03, colb, "round");
      second = (second * Math.PI / 30);
      drawHand(ctx, second, radius * 0.9, radius * 0.015, cols, "round");
    }

    function drawHand(ctx, pos, length, width, color, end) {
      ctx.beginPath();
      ctx.lineWidth = width;
      ctx.lineCap = end;
      ctx.moveTo(0, 0);
      ctx.rotate(pos);
      ctx.lineTo(0, -length);
      ctx.strokeStyle = color;
      ctx.stroke();
      ctx.rotate(-pos);
      ctx.beginPath();
      ctx.lineWidth = width;
      ctx.lineCap = end;
      ctx.moveTo(0, 0);
      ctx.rotate(pos);
      ctx.lineTo(0, length / 4);
      ctx.strokeStyle = color;
      ctx.stroke();
      ctx.rotate(-pos);
    }
  </script>


  <?php include('horas_paises.php'); ?>

</body>

</html>