<html>

<head>
  <style>
    .flex,
    .flex-wrap,
    .flex-left,
    .flex-right,
    .flex-center,
    .flex-between {
      display: flex;
      align-items: center;
      width: 100%;
    }

    .flex-wrap {
      flex-wrap: wrap;
      text-align: left;
      position: relative;
      z-index: 1;
    }

    .flex-left {
      justify-content: flex-start;
    }

    .flex-right {
      justify-content: flex-end;
    }

    .flex-inline {
      display: inline-flex;
    }

    .flex-center {
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .flex-between {
      justify-content: space-between;
    }

    .align-center {
      align-items: center;
    }

    .background {
      width: 100%;
      height: 56.25vw;
      background-position: center top;
      background-size: cover;
      box-shadow: inset 0px -20vw 20vw 1vw black;
      position: absolute;
      top: 0;
      z-index: 1;
      background-attachment: fixed;
    }

    .lo {
      padding: 31vw 56px 56px 56px;
    }

    @media (min-width: 1200px) {
      .lo {
        padding: 31vw 164px 1rem 164px;
      }
    }

    @media (max-width: 500px) {
      .lo {
        padding: 31vw 20px 20px 20px;
      }
    }

    .album-info {
      width: 220px;
      flex-wrap: wrap;
      align-self: flex-start;
      margin: auto;
      margin-top: -5rem;
    }

    .album-info img {
      width: 220px;
      margin-bottom: 20px;
      border-radius: 1em;
      box-shadow: 0 0 10px rgba(0, 0, 0, .3);
    }

    .album-info .btn.price,
    .album-info .price.btn-round {
      width: 100%;
      padding: 0.5rem 1rem;
      background: #E91E63;
      color: #fff;
      text-transform: uppercase;
      border-radius: 3px;
      margin: 0.5rem;
      font-size: 1rem;
      font-family: Roboto;
    }

    .album-info .btn.save,
    .album-info .save.btn-round {
      color: #9E9E9E;
      padding: 0.7rem;
      border-radius: 2px;
      margin: 0.5rem;
      font-size: 1rem;
    }

    .album-details {
      flex: 1;
      padding: 1rem 0 0 40px;
      color: white;
    }

    .album-details .band {
      align-items: center;
    }

    .album-details .avatar_autor {
      width: 4rem;
      height: 4rem;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      border-radius: 7em;
      background-color: #E91E63;
      margin: 0.5rem;
    }

    .album-details h2 {
      margin: 0;
      font-size: 14px;
      font-weight: 900;
      display: inline-flex;
      align-items: center;
      padding: 0.5rem;
    }

    .album-details h1 {
      margin: 20px 0;
      font-size: 1.5rem;
      font-weight: 900;
      color: white;
    }

    .album-details h3 {
      font-size: 14px;
      font-weight: 900;
      color: white;
      margin-bottom: 1rem;
    }

    .album-details h3 span {
      padding-right: 10px;
    }

    .more {
      overflow: auto;
      flex-wrap: wrap;
      width: 100%;
      margin-right: 17px;
    }

    @media (max-width: 600px) {
      .more {
        width: 100%;
        left: 0;
      }
    }

    .more .up-next {
      width: 100%;
      background: rgba(255, 255, 255, 0.1);
    }

    .more .up-next .left {
      width: 320px;
    }

    .more .up-next .right {
      margin-left: auto;
    }

    .more .up-next .right .length {
      opacity: .8;
      padding-right: 10vw;
    }

    .more .up-next .right .rating {
      padding-right: calc(10vw - 22px);
    }

    .more .up-next .right .rating .rate {
      width: 10px;
      height: 10px;
      background: #949494;
      border-radius: 50%;
      margin: 2.5px;
      background: #D9D9D9;
    }

    .more .up-next .right .rating .rate.fill {
      background: #949494;
    }

    .more .up-next .info {
      margin-left: 16px;
      max-width: calc(80vw - 325px);
    }

    .more .up-next .info p {
      padding: 2px;
      margin: 0;
      width: 100%;
      black-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .more .up-next .info p:first-child {
      font-size: 18px;
    }

    .more .up-next .info p:last-child {
      font-size: 14px;
      opacity: .8;
    }

    .up-next {
      height: 56px;
      color: white;
    }

    .album-infos .logo {
      border-radius: 100%;
      margin-right: 14px;
      background-color: #E91E63;
      padding: 0.5rem;
    }

    .album-infos {
      width: 100%;
      color: white;
      padding: 0.5rem;
      margin: 0.5rem;
      margin-top: 1rem;
    }

    .album-infos span:first-child {
      font-weight: 900;
    }

    .album-infos span:last-child {
      font-size: 13px;
      color: #9E9E9E;
    }

    .tracks {
      width: 100%;
      margin-top: 48px;
    }

    .tracks ol {
      margin: 0;
      padding: 1rem 0;
    }

    .tracks li {
      color: white;
      list-style: none;
      height: 56px;
      align-content: center;
      justify-content: space-between;
      padding: 0.5rem 2rem;
      border-top: solid 1px #eee;
    }

    .tracks li span {
      align-self: center;
      width: 100%;
    }

    .tracks li span:nth-child(1) {
      color: white;
    }

    .tracks li span:nth-child(1) span {
      color: white;
      width: 34px;
    }

    .tracks li span.price {
      height: auto;
      width: auto;
      padding: 0.5rem 1rem;
      background: #faa800;
      color: #fff;
      border-radius: 2px;
    }

    .section-container {
      background: black;
    }

    @media screen and (max-width: 450px) {
      .album-details {
        padding: 0;
      }
      .background {
        min-height: 70vh;
      }
      .abc {
        display: inline-block;
      }
      .album-infos.flex-between {
        display: inline-block;
      }
      .album-infos.flex-between .flex {
        margin: 1rem;
      }
      .no .abc {
        width: 60px;
        text-align: center;
      }
      .no span {
        font-size: 20px;
        font-weight: 900;
      }
      .no svg {
        transform: scale(1.3);
        width: 0.7rem;
      }
      .cg.flex-between {
        display: inline-block;
      }
      .cg.flex-between .btn {
        background: black;
        color: black;
        border-radius: 2px 2px 0 0;
      }
      .lo.flex-wrap .rating.flex {
        border-radius: 0px 0px 2px 2px;
      }
      .single {
        width: 100%;
      }
      .single .rv {
        padding: 0.5rem;
      }
    }
  </style>
</head>

<body>
  <div class="background" id="background"></div>
  <?php include('../../../acceso_db.php');
$id_album_ver=$_GET['id'];
$movies_play=mysqli_query($con, "SELECT * FROM movies_albumes WHERE id='$id_album_ver' LIMIT 1");
while($row_album_ver=mysqli_fetch_array($movies_play)) {
  $name_album_ver=$row_album_ver["titulo"];
  $name_search_album_ver=urlencode($row_album_ver["titulo"]);
  $id_album_ver=$row_album_ver["id"];
  $fecha_album_ver=$row_album_ver["fecha"];
  $genero_album_ver=$row_album_ver["genero"];
  $precio_album_ver=$row_album_ver["precio"];
  $compositor_album_ver=$row_album_ver["compositor"];
  $compositor_search_album_ver=urlencode($row_album_ver["compositor"]);
  $productora_album_ver=$row_album_ver["productora"];
  $descripcion_album_ver=$row_album_ver["descripcion"];
  $compositor_album_ver_bg=explode(" &", $compositor_album_ver, 2);
  $compositor_album_ver_bg=$compositor_album_ver_bg[0];
  $artista_album_ver=mysqli_query($con, "SELECT * FROM artistas WHERE artist_name LIKE '$compositor_album_ver' OR artist_name LIKE '$compositor_album_ver_bg' ORDER BY id DESC LIMIT 1");
  while($row_artista_album_ver=mysqli_fetch_array($artista_album_ver)) {
    $name_artista_album_ver=$row_artista_album_ver["name"];
    $id_artista_album_ver=$row_artista_album_ver["id"];
  }?>
  <div class="lo flex-wrap">
    <div class="abc flex">
      <div class="album-info flex-between"><img src="//media.kaana.io/movies/albumes/caratulas/<?=$id_album_ver?>.jpg">
        <div style="display:inline-flex; align-items:center; width:100%;">
          <div class="btn flex-center price">
            <?=SEGUIR?>
          </div>
          <!----<div class="btn save"><i class="material-icons">&#xE867;
  </i></div>-------></div>
      </div>
      <div class="album-details">
        <div class="band flex">
          <div class="avatar_autor" style="background-image:url('//media.kaana.io/artistas/<?=$id_artista_album_ver?>.jpg');"></div>
          <h2 onclick="buscardd('<?=$compositor_album_ver?>');">
            <?=$compositor_album_ver?>
          </h2>
        </div>
        <h1>
          <?=$name_album_ver?>
        </h1>
        <h3><span onclick="buscardd('<?=$genero_album_ver?>'); "><?=$genero_album_ver?></span><span><?=$productora_album_ver?></span></h3>
        <div class="tracks">
          <ol>
            <?php $resultado_numero_canciones_album_ver=0;
  $album_canciones_album_ver=mysqli_query($con, "SELECT * FROM movies_canciones WHERE album LIKE '%$name_album_ver%' ORDER BY trackNumber DESC");
  while($row_canciones_album_ver=mysqli_fetch_array($album_canciones_album_ver)) {
    $titulo_canciones_album_ver=$row_canciones_album_ver["titulo"];
    $titulo_search_canciones_album_ver=urlencode($row_canciones_album_ver["titulo"]);
    $duracion_canciones_album_ver=$row_canciones_album_ver["duracion"];
    $id_canciones_album_ver=$row_canciones_album_ver["id"];
    $resultado_numero_canciones_album_ver=$resultado_numero_canciones_album_ver+1;
    ?>
            <li class="flex"><span class="flex"><span><?=$resultado_numero_canciones_album_ver?></span>
              <?=$titulo_canciones_album_ver?>
                </span>
                <!-------<span><?=$duracion_canciones_album_ver?></span>--------><i class="material-icons" onclick="reproducir('<?=$compositor_search_album_ver?>%20<?=$titulo_search_canciones_album_ver?>', '', '<?=$id_canciones_album_ver?>');">&#xE039;
    </i></li>
            <?}?>
          </ol>
        </div>
      </div>
    </div>
    <div class="album-infos flex-between">
      <div class="flex">
        <div class="logo"><i class="material-icons">&#xE226;
  </i></div>
        <div><span>Album Size</span><br><span>34.4 MB</span></div>
      </div>
      <div class="flex">
        <div class="logo"><i class="material-icons">&#xE037;
  </i></div>
        <div><span>Total Duration</span><br><span>48:48</span></div>
      </div>
      <div class="flex" style="max-width:20rem;">
        <div class="logo"><i class="material-icons">&#xE7EE;
  </i></div>
        <div><span>Label</span><br><span><?=$productora_album_ver?></span></div>
      </div>
    </div>
  </div>
  <?
}

?>
</body>

</html>