<html>
  <head>
    <style>
      .musicfeed {
        margin:1rem auto;
        width:100%;
      }
    </style>
  </head>
  <body>
    
    <div class="musicfeed" id="musicfeed" >
      
    </div>
    
    <script>
    $('#musicfeed').empty().load('./resources/feed/feeds.php?<?=$_SERVER["QUERY_STRING"];?>');
    </script>
    
  </body>
</html>