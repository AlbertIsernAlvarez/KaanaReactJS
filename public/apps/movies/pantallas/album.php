<html>

<head>
  <style>
    .flex,
    .flex-wrap,
    .flex-left,
    .flex-right,
    .flex-center,
    .flex-between {
      display: flex;
      align-items: center;
      width: 100%;
    }

    .flex-wrap {
      flex-wrap: wrap;
      text-align: left;
      position: relative;
      padding:0.5rem;
    }

    .flex-left {
      justify-content: flex-start;
    }

    .flex-right {
      justify-content: flex-end;
    }

    .flex-inline {
      display: inline-flex;
    }

    .flex-center {
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .flex-between {
      justify-content: space-between;
    }

    .align-center {
      align-items: center;
    }

    .album-info {
    max-width: 13rem;
    flex-wrap: wrap;
    align-self: flex-start;
    margin: 0 auto;
    padding: 0.5rem;
    }

    .album-info img {
      width: 100%;
      margin-bottom: 0.5rem;
      border-radius: 1em;
      box-shadow: 0px 1px 3px 1px rgba(60, 64, 67, .15);
    }
    .album-details {
      flex: 1;
      padding: 0.5rem;
    }

    .album-details .band {
      align-items: center;
    }
    
    .album-details .band h2 {
      font-size:1rem;
    }

    .album-details .avatar_autor {
    width: 2rem;
    height: 2rem;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    border-radius: 7em;
    margin: 0.5rem;
      flex:none;
    }

    .album-details > h2 {
      margin: 0;
      font-size: 14px;
      display: inline-flex;
      align-items: center;
      padding: 0.5rem;
    }

    .album-details > h1 {
      padding:0.5rem;
      font-size: 2rem;
      font-weight: 500;
    }

    .album-details > h3 {
    font-size: 14px;
    font-weight: 500;
    padding: 0.5rem;
    display: flex;
    flex-direction: column;
    }

    .album-details > h3 span {
      padding:0.5rem;
    }

    .more {
      overflow: auto;
      flex-wrap: wrap;
      width: 100%;
      margin-right: 17px;
    }

    @media (max-width: 600px) {
      .more {
        width: 100%;
        left: 0;
      }
    }

    .more .up-next {
      width: 100%;
      background: rgba(255, 255, 255, 0.1);
    }

    .more .up-next .left {
      width: 320px;
    }

    .more .up-next .right {
      margin-left: auto;
    }

    .more .up-next .right .length {
      opacity: .8;
      padding-right: 10vw;
    }

    .more .up-next .right .rating {
      padding-right: calc(10vw - 22px);
    }

    .more .up-next .right .rating .rate {
      width: 10px;
      height: 10px;
      background: #949494;
      border-radius: 50%;
      margin: 2.5px;
      background: #D9D9D9;
    }

    .more .up-next .right .rating .rate.fill {
      background: #949494;
    }

    .more .up-next .info {
      margin-left: 16px;
      max-width: calc(80vw - 325px);
    }

    .more .up-next .info p {
      padding: 2px;
      margin: 0;
      width: 100%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .more .up-next .info p:first-child {
      font-size: 18px;
    }

    .more .up-next .info p:last-child {
      font-size: 14px;
      opacity: .8;
    }

    .tracks {
      width: 100%;
      margin-top: 48px;
    }
    
    .tracks .PF-list li .data {
      width:fit-content;
    }

    .section-container {
      background: black;
    }

    @media screen and (max-width: 450px) {
      .album-details {
        padding: 0;
      }
      .abc {
        display: inline-block;
      }
      .no .abc {
        width: 60px;
        text-align: center;
      }
      .no span {
        font-size: 20px;
        font-weight: 500;
      }
      .single {
        width: 100%;
      }
      .single .rv {
        padding: 0.5rem;
      }
    }
  </style>
</head>

<body>
  
  <?php include_once(__DIR__."/../../../acceso_db.php");
$id_album_ver=$_GET['id'];
$music_play=mysqli_query($con, "SELECT * FROM music_albumes WHERE id='$id_album_ver' LIMIT 1");
while($row_album_ver=mysqli_fetch_array($music_play)) {
  $name_album_ver=$row_album_ver["titulo"];
  $name_search_album_ver=urlencode($row_album_ver["titulo"]);
  $id_album_ver=$row_album_ver["id"];
  $fecha_album_ver=$row_album_ver["fecha"];
  $genero_album_ver=$row_album_ver["genero"];
  $precio_album_ver=$row_album_ver["precio"];
  $compositor_album_ver=$row_album_ver["compositor"];
  $compositor_search_album_ver=urlencode($row_album_ver["compositor"]);
  $productora_album_ver=$row_album_ver["productora"];
  $descripcion_album_ver=$row_album_ver["descripcion"];
  $compositor_album_ver_bg=explode(" &", $compositor_album_ver, 2);
  $compositor_album_ver_bg=$compositor_album_ver_bg[0];
  $artista_album_ver=mysqli_query($con, "SELECT * FROM artistas WHERE artist_name LIKE '$compositor_album_ver' OR artist_name LIKE '$compositor_album_ver_bg' ORDER BY id DESC LIMIT 1");
  while($row_artista_album_ver=mysqli_fetch_array($artista_album_ver)) {
    $name_artista_album_ver=$row_artista_album_ver["name"];
    $id_artista_album_ver=$row_artista_album_ver["id"];
  }?>
  
  <div class="PF PF-toolbar">
  <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  <h1 t-dd><?=$name_album_ver?></h1>
</div>
  
  <div class="lo flex-wrap">
    <div class="abc flex">
      <div class="album-info flex-between"><img class="PF PF-image" src="//media.kaana.io/music/albumes/caratulas/<?=$id_album_ver?>.jpg">
        <div style="display:inline-flex; align-items:center; width:100%;">
          <button class="PF PF-button flex ripple">
    <div class="inside">
      <p>Play album</p>
    </div>
  </button>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>
      <div class="album-details">
        <div class="band flex">
          <div class="PF PF-image avatar_autor" style="background-image:url('//media.kaana.io/artistas/<?=$id_artista_album_ver?>.jpg');"></div>
          <h2 onclick="searchdd('<?=$compositor_album_ver?>');">
            <?=$compositor_album_ver?>
          </h2>
        </div>
        <h1>
          <?=$name_album_ver?>
        </h1>
        <h3><span onclick="searchdd('<?=$genero_album_ver?>'); "><?=$genero_album_ver?></span><span><?=$productora_album_ver?></span></h3>
        <div class="tracks">
          <ul class="PF PF-list normal">
      <?php $resultado_numero_canciones_album_ver=0;
  $album_canciones_album_ver=mysqli_query($con, "SELECT * FROM music_canciones WHERE album LIKE '$name_album_ver' ORDER BY trackNumber DESC");
  while($row_canciones_album_ver=mysqli_fetch_array($album_canciones_album_ver)) {
    $titulo_canciones_album_ver=$row_canciones_album_ver["titulo"];
    $titulo_search_canciones_album_ver=urlencode($row_canciones_album_ver["titulo"]);
    $duracion_canciones_album_ver=$row_canciones_album_ver["duracion"];
    $id_canciones_album_ver=$row_canciones_album_ver["id"];
    $resultado_numero_canciones_album_ver=$resultado_numero_canciones_album_ver+1;
    ?>
      <li class="ripple" onclick="reproducir('', '', '<?=$id_canciones_album_ver?>');">
        <div class="data" >
          <span><?=$resultado_numero_canciones_album_ver?></span>
        </div>
        <div class="data" >
        <span><?=$titulo_canciones_album_ver?></span>
        </div>
      </li>
      <?}?>
    </ul>
        </div>
      </div>
    </div>
  </div>
  <?}?>
</body>

</html>