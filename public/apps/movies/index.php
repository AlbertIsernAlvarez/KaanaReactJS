<html>

<head>
  <style>
    
    .movies_container {
      display: flex;
      flex-wrap: wrap;
      width: 100%;
      top: 0;
      right: 0;
      will-change: width;
      transition: .3s;
      max-width: 980px;
      margin: auto;
      margin-top: 1rem;
      min-height: 10rem;
      padding: 0.5rem;
      width: 90vw;
      color:black;
    }
    
    .movies_container .PF-grid .PF-card {
      max-width:20rem;
    }
    
    .movies_container .PF-grid.movies .PF-card .image {
    padding-bottom: 155%;
}
    
    .movies_container .PF-card .PF-icon {
      top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    position: absolute;
    margin: auto;
    background: var(--PF-color-bg-second-default);
      opacity:0;
    }
    
    .movies_container .PF-card:hover .PF-icon {
      opacity:1;
    }
    
    .movies_container .PF-card .PF-icon i {
      font-size:2rem;
    }
    
    .movies_container .head {
      padding: 0.5rem;
      width: 100%;
    }

    .movies_container .head h2 {
      text-align: center;
      font-size: 36px;
      line-height: 44px;
      font-weight: 600;
      text-transform: none;
      margin: 0;
      position:relative;
      font-weight:900;
      display:inline-block;
      color:rgb(var(--PF-color-on-surface));
    }

    .movies_container .head  h2:before {
      content: "";
      position: absolute;
      left: 0;
      right: 0;
      top: 0.70em;
      bottom: 0.15em;
      -webkit-transform: rotateZ(-2deg);
      -ms-transform: rotate(-2deg);
      transform: rotateZ(-2deg);
      background: rgb(var(--PF-color-primary));
      z-index: -1;
    }

    .movies_container .content_main {
      width: 100%;
      display: flex;
      flex-wrap: wrap;
    }
  </style>
</head>

<body>
  <div id="movies_content" class="movies_container" ></div>
  <script>
    $( document ).ready(function() {
      $('body').addClass('PFC-red');
        $('.PF.PF-tabbar .container').addClass('center');
        pantallamovies('start', '', 'first');
      
      <?php 
      $section_movies = $_GET['sc'];
      if($section_movies != ''){?>
      sectionmovies('<?=$section_movies?>');
      <?}?>
      
    });

    function veralbum(id) {
      $('#veralbum').load('./apps/<?=$app?>/resources/ver-album.php?id=' + id);
    }

    function sectionmovies(pantalla, id, first) {
      $('#movies_content').load('./apps/<?=$app?>/pantallas/section.php?section=' + pantalla + '&<?=$_SERVER["QUERY_STRING"];?>');
    }
    
    function pantallamovies(pantalla, id, first) {
      $('#movies_content').load('./apps/<?=$app?>/pantallas/' + pantalla + '.php?id=' + id + '&<?=$_SERVER["QUERY_STRING"];?>', function() {});
    }
  </script>
 
</body>

</html>