<html>

<head>
  <style>
    
    body {
      overflow: hidden;
    }
    
    #camera {
flex: 1 auto;
    display: flex;
    flex-direction: column;
    }
    
    #camera--sensor {
      object-fit: cover;
      display:none;
      width:100%;
      height:100%;
      position:absolute;
      top:0;
      left:0;
    }

    #camera--view,
    #camera--sensor,
    #camera--output {
      transform: scaleX(-1);
      filter: FlipH;
    }
    
    #camera--view {
    flex: 1;
    border-radius: 2em 2em 0 0;
    background-color: black;
    margin: 0;
    overflow: hidden;
    max-width: 100%;
    max-height: 100%;
    position: relative;
    }
    
    #camera--view video {
    min-width:100%;
      min-height:100%;
      position: absolute;
    top: -9999px;
    bottom: -9999px;
    left: -9999px;
    right: -9999px;
    margin: auto;
    }

    #camera--trigger {
    width: 3em;
    height: 3em;
    background-color: #2196F3;
    color: white;
    font-size: 16px;
    border: 3px solid white;
    border-radius: 30px;
    padding: 15px 20px;
    text-align: center;
    margin: 0.5rem auto;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    position: absolute;
    left: 0;
    bottom: 0;
    right: 0;
    margin: 1em auto;
      cursor: pointer;
    }

    .taken {
      transition: all 0.5s ease-in;
      border: solid 3px white;
      box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.2);
    position: absolute;
    bottom: 0;
    right: 0;
    margin: 1em;
    border-radius: 10em;
          height: 5em;
    width: 5em;
    }
  </style>
</head>

<body>



  <main id="camera">
    <canvas id="camera--sensor"></canvas>
    <div id="camera--view" >
      <video autoplay="" playsinline=""></video>
    </div>
    <img src="//:0" alt="" id="camera--output">
    <button id="camera--trigger"></button>
  </main>


  <script>
    
    
    var constraints = {
      video: {
        facingMode: "environment"
      },
      audio: false
    };
    var track = null;
    const cameraView = document.querySelector("#camera--view video"),
      cameraOutput = document.querySelector("#camera--output"),
      cameraSensor = document.querySelector("#camera--sensor"),
      cameraTrigger = document.querySelector("#camera--trigger");

    function cameraStart() {
      navigator.mediaDevices.
      getUserMedia(constraints).
      then(function(stream) {
        track = stream.getTracks()[0];
        cameraView.srcObject = stream;
      }).
      catch(function(error) {
        console.error("Oops. Something is broken.", error);
      });
    }
    cameraTrigger.onclick = function() {
      cameraSensor.width = cameraView.videoWidth;
      cameraSensor.height = cameraView.videoHeight;
      cameraSensor.getContext("2d").drawImage(cameraView, 0, 0);
      cameraOutput.src = cameraSensor.toDataURL("image/webp");
      cameraOutput.classList.add("taken");
    };
    window.addEventListener("load", cameraStart, false);
    
    cameraStart();
  </script>
</body>

</html>