<html>

<head>
  <style>

    .container {
    }

    .material-color-picker {
    display: flex;
    width: 100%;
    margin: 0 auto;
    }

    .material-color-picker__left-panel {
      z-index: 1;
    }

    .material-color-picker__right-panel {
      position: relative;
      flex-grow: 1;
      overflow: hidden;
    }

    .color-selector {
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;
    flex-direction: column;
    padding: 1em 0;
    border-right: solid 1px #ddd;
    }

    .color-selector input[type='radio'] {
      display: none;
    }

    .color-selector label {
      position: relative;
      display: inline-block;
      padding: 0.5em 1.5em;
      cursor: pointer;
    }

    .color-selector label:before {
      content: '';
      display: inline-block;
      vertical-align: middle;
      padding: 0.75em;
      background-color: currentColor;
      border-radius: 50%;
    }

    .color-selector label:after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
      padding: 0.5em;
      border: 0.25em solid;
      border-radius: 50%;
      transition: padding 250ms;
    }

    .color-selector input[type='radio']:checked+label:after {
      padding: 1em;
    }

    .color-palette-wrapper {
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      -webkit-transform: translateX(-100%);
      transform: translateX(-100%);
      display: flex;
      flex-direction: column;
      padding: 1.5em;
    }

    .color-palette-wrapper.js-active {
      -webkit-transform: translateX(0);
      transform: translateX(0);
    }

    .color-palette-header {
      display: flex;
      justify-content: space-between;
      margin: 0;
      margin-bottom: 1em;
      color: #757575;
    }

    .color-palette {
      margin: 0;
      padding: 0;
      list-style: none;
      display: flex;
      flex-direction: column;
      flex-grow: 1;
    }

    .color-palette__item {
      position: relative;
      display: flex;
      align-items: center;
      justify-content: space-between;
      flex-grow: 1;
      margin: 0.25em 0;
      padding: 0 1em;
      border-radius: 0.25em;
      font-family: "Roboto Mono", monospace;
      transition: -webkit-transform 250ms;
      transition: transform 250ms;
      transition: transform 250ms, -webkit-transform 250ms;
      cursor: pointer;
    }

    .color-palette__item:hover {
      -webkit-transform: scale(1.01);
      transform: scale(1.01);
    }

    .copied-indicator {
      position: absolute;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, 0);
      transform: translate(-50%, 0);
      opacity: 0;
      transition: all 250ms;
    }

    .copied-indicator.js-copied {
      -webkit-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
      opacity: 0.75;
    }
  </style>
</head>

<body>
  <div class="container">
    <div class="material-color-picker">
      <div class="material-color-picker__left-panel">
        <ol class="color-selector" data-bind="foreach: materialColors">
          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor0" value="Red">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor0" title="Red" style="color: rgb(239, 83, 80);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor1" value="Pink">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor1" title="Pink" style="color: rgb(236, 64, 122);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor2" value="Purple">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor2" title="Purple" style="color: rgb(171, 71, 188);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor3" value="Deep Purple">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor3" title="Deep Purple" style="color: rgb(126, 87, 194);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor4" value="Indigo">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor4" title="Indigo" style="color: rgb(92, 107, 192);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor5" value="Blue">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor5" title="Blue" style="color: rgb(66, 165, 245);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor6" value="Light Blue">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor6" title="Light Blue" style="color: rgb(41, 182, 246);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor7" value="Cyan">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor7" title="Cyan" style="color: rgb(38, 198, 218);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor8" value="Teal">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor8" title="Teal" style="color: rgb(38, 166, 154);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor9" value="Green">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor9" title="Green" style="color: rgb(102, 187, 106);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor10" value="Light Green">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor10" title="Light Green" style="color: rgb(156, 204, 101);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor11" value="Lime">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor11" title="Lime" style="color: rgb(212, 225, 87);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor12" value="Yellow">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor12" title="Yellow" style="color: rgb(255, 238, 88);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor13" value="Amber">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor13" title="Amber" style="color: rgb(255, 202, 40);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor14" value="Orange">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor14" title="Orange" style="color: rgb(255, 167, 38);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor15" value="Deep Orange">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor15" title="Deep Orange" style="color: rgb(255, 112, 67);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor16" value="Brown">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor16" title="Brown" style="color: rgb(141, 110, 99);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor17" value="Grey">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor17" title="Grey" style="color: rgb(189, 189, 189);"></label>
          </li>

          <li>
            <input name="material-color" type="radio" data-bind="attr: { id: 'materialColor' + $index() }, checked: selectedColor, value: color" id="materialColor18" value="Blue Grey">
            <label data-bind="attr: { for: 'materialColor' + $index(), title: color }, style: { 'color': $data.variations[4].hex }" for="materialColor18" title="Blue Grey" style="color: rgb(120, 144, 156);"></label>
          </li>
        </ol>
      </div>
      <div class="material-color-picker__right-panel" data-bind="foreach: materialColors">
        <div class="color-palette-wrapper js-active" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Red</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFEBEE" style="background-color: rgb(255, 235, 238);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#FFEBEE</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFCDD2" style="background-color: rgb(255, 205, 210);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#FFCDD2</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#EF9A9A" style="background-color: rgb(239, 154, 154);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#EF9A9A</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E57373" style="background-color: rgb(229, 115, 115);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#E57373</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#EF5350" style="background-color: rgb(239, 83, 80);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#EF5350</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F44336" style="background-color: rgb(244, 67, 54);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#F44336</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E53935" style="background-color: rgb(229, 57, 53);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#E53935</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#D32F2F" style="background-color: rgb(211, 47, 47);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#D32F2F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#C62828" style="background-color: rgb(198, 40, 40);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#C62828</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#B71C1C" style="background-color: rgb(183, 28, 28);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#B71C1C</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Pink</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FCE4EC" style="background-color: rgb(252, 228, 236);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#FCE4EC</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F8BBD0" style="background-color: rgb(248, 187, 208);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#F8BBD0</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F48FB1" style="background-color: rgb(244, 143, 177);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#F48FB1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F06292" style="background-color: rgb(240, 98, 146);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#F06292</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#EC407A" style="background-color: rgb(236, 64, 122);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#EC407A</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E91E63" style="background-color: rgb(233, 30, 99);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#E91E63</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#D81B60" style="background-color: rgb(216, 27, 96);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#D81B60</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#C2185B" style="background-color: rgb(194, 24, 91);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#C2185B</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#AD1457" style="background-color: rgb(173, 20, 87);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#AD1457</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#880E4F" style="background-color: rgb(136, 14, 79);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#880E4F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Purple</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F3E5F5" style="background-color: rgb(243, 229, 245);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#F3E5F5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E1BEE7" style="background-color: rgb(225, 190, 231);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#E1BEE7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#CE93D8" style="background-color: rgb(206, 147, 216);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#CE93D8</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#BA68C8" style="background-color: rgb(186, 104, 200);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#BA68C8</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#AB47BC" style="background-color: rgb(171, 71, 188);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#AB47BC</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#9C27B0" style="background-color: rgb(156, 39, 176);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#9C27B0</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#8E24AA" style="background-color: rgb(142, 36, 170);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#8E24AA</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#7B1FA2" style="background-color: rgb(123, 31, 162);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#7B1FA2</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#6A1B9A" style="background-color: rgb(106, 27, 154);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#6A1B9A</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#4A148C" style="background-color: rgb(74, 20, 140);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#4A148C</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Deep Purple</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#EDE7F6" style="background-color: rgb(237, 231, 246);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#EDE7F6</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#D1C4E9" style="background-color: rgb(209, 196, 233);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#D1C4E9</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#B39DDB" style="background-color: rgb(179, 157, 219);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#B39DDB</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#9575CD" style="background-color: rgb(149, 117, 205);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#9575CD</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#7E57C2" style="background-color: rgb(126, 87, 194);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#7E57C2</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#673AB7" style="background-color: rgb(103, 58, 183);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#673AB7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#5E35B1" style="background-color: rgb(94, 53, 177);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#5E35B1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#512DA8" style="background-color: rgb(81, 45, 168);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#512DA8</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#4527A0" style="background-color: rgb(69, 39, 160);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#4527A0</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#311B92" style="background-color: rgb(49, 27, 146);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#311B92</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Indigo</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E8EAF6" style="background-color: rgb(232, 234, 246);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#E8EAF6</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#C5CAE9" style="background-color: rgb(197, 202, 233);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#C5CAE9</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#9FA8DA" style="background-color: rgb(159, 168, 218);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#9FA8DA</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#7986CB" style="background-color: rgb(121, 134, 203);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#7986CB</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#5C6BC0" style="background-color: rgb(92, 107, 192);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#5C6BC0</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#3F51B5" style="background-color: rgb(63, 81, 181);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#3F51B5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#3949AB" style="background-color: rgb(57, 73, 171);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#3949AB</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#303F9F" style="background-color: rgb(48, 63, 159);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#303F9F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#283593" style="background-color: rgb(40, 53, 147);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#283593</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#1A237E" style="background-color: rgb(26, 35, 126);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#1A237E</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Blue</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E3F2FD" style="background-color: rgb(227, 242, 253);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#E3F2FD</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#BBDEFB" style="background-color: rgb(187, 222, 251);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#BBDEFB</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#90CAF9" style="background-color: rgb(144, 202, 249);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#90CAF9</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#64B5F6" style="background-color: rgb(100, 181, 246);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#64B5F6</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#42A5F5" style="background-color: rgb(66, 165, 245);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#42A5F5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#2196F3" style="background-color: rgb(33, 150, 243);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#2196F3</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#1E88E5" style="background-color: rgb(30, 136, 229);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#1E88E5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#1976D2" style="background-color: rgb(25, 118, 210);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#1976D2</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#1565C0" style="background-color: rgb(21, 101, 192);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#1565C0</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#0D47A1" style="background-color: rgb(13, 71, 161);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#0D47A1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Light Blue</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E1F5FE" style="background-color: rgb(225, 245, 254);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#E1F5FE</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#B3E5FC" style="background-color: rgb(179, 229, 252);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#B3E5FC</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#81D4FA" style="background-color: rgb(129, 212, 250);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#81D4FA</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#4FC3F7" style="background-color: rgb(79, 195, 247);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#4FC3F7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#29B6F6" style="background-color: rgb(41, 182, 246);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#29B6F6</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#03A9F4" style="background-color: rgb(3, 169, 244);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#03A9F4</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#039BE5" style="background-color: rgb(3, 155, 229);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#039BE5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#0288D1" style="background-color: rgb(2, 136, 209);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#0288D1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#0277BD" style="background-color: rgb(2, 119, 189);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#0277BD</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#01579B" style="background-color: rgb(1, 87, 155);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#01579B</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Cyan</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E0F7FA" style="background-color: rgb(224, 247, 250);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#E0F7FA</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#B2EBF2" style="background-color: rgb(178, 235, 242);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#B2EBF2</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#80DEEA" style="background-color: rgb(128, 222, 234);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#80DEEA</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#4DD0E1" style="background-color: rgb(77, 208, 225);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#4DD0E1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#26C6DA" style="background-color: rgb(38, 198, 218);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#26C6DA</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#00BCD4" style="background-color: rgb(0, 188, 212);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#00BCD4</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#00ACC1" style="background-color: rgb(0, 172, 193);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#00ACC1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#0097A7" style="background-color: rgb(0, 151, 167);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#0097A7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#00838F" style="background-color: rgb(0, 131, 143);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#00838F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#006064" style="background-color: rgb(0, 96, 100);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#006064</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Teal</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E0F2F1" style="background-color: rgb(224, 242, 241);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#E0F2F1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#B2DFDB" style="background-color: rgb(178, 223, 219);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#B2DFDB</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#80CBC4" style="background-color: rgb(128, 203, 196);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#80CBC4</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#4DB6AC" style="background-color: rgb(77, 182, 172);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#4DB6AC</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#26A69A" style="background-color: rgb(38, 166, 154);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#26A69A</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#009688" style="background-color: rgb(0, 150, 136);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#009688</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#00897B" style="background-color: rgb(0, 137, 123);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#00897B</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#00796B" style="background-color: rgb(0, 121, 107);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#00796B</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#00695C" style="background-color: rgb(0, 105, 92);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#00695C</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#004D40" style="background-color: rgb(0, 77, 64);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#004D40</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Green</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E8F5E9" style="background-color: rgb(232, 245, 233);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#E8F5E9</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#C8E6C9" style="background-color: rgb(200, 230, 201);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#C8E6C9</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#A5D6A7" style="background-color: rgb(165, 214, 167);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#A5D6A7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#81C784" style="background-color: rgb(129, 199, 132);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#81C784</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#66BB6A" style="background-color: rgb(102, 187, 106);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#66BB6A</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#4CAF50" style="background-color: rgb(76, 175, 80);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#4CAF50</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#43A047" style="background-color: rgb(67, 160, 71);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#43A047</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#388E3C" style="background-color: rgb(56, 142, 60);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#388E3C</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#2E7D32" style="background-color: rgb(46, 125, 50);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#2E7D32</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#1B5E20" style="background-color: rgb(27, 94, 32);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#1B5E20</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Light Green</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F1F8E9" style="background-color: rgb(241, 248, 233);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#F1F8E9</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#DCEDC8" style="background-color: rgb(220, 237, 200);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#DCEDC8</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#C5E1A5" style="background-color: rgb(197, 225, 165);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#C5E1A5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#AED581" style="background-color: rgb(174, 213, 129);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#AED581</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#9CCC65" style="background-color: rgb(156, 204, 101);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#9CCC65</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#8BC34A" style="background-color: rgb(139, 195, 74);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#8BC34A</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#7CB342" style="background-color: rgb(124, 179, 66);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#7CB342</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#689F38" style="background-color: rgb(104, 159, 56);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#689F38</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#558B2F" style="background-color: rgb(85, 139, 47);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#558B2F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#33691E" style="background-color: rgb(51, 105, 30);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#33691E</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Lime</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F9FBE7" style="background-color: rgb(249, 251, 231);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#F9FBE7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F0F4C3" style="background-color: rgb(240, 244, 195);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#F0F4C3</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E6EE9C" style="background-color: rgb(230, 238, 156);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#E6EE9C</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#DCE775" style="background-color: rgb(220, 231, 117);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#DCE775</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#D4E157" style="background-color: rgb(212, 225, 87);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#D4E157</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#CDDC39" style="background-color: rgb(205, 220, 57);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#CDDC39</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#C0CA33" style="background-color: rgb(192, 202, 51);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#C0CA33</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#AFB42B" style="background-color: rgb(175, 180, 43);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#AFB42B</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#9E9D24" style="background-color: rgb(158, 157, 36);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#9E9D24</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#827717" style="background-color: rgb(130, 119, 23);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#827717</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Yellow</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFFDE7" style="background-color: rgb(255, 253, 231);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#FFFDE7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFF9C4" style="background-color: rgb(255, 249, 196);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#FFF9C4</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFF59D" style="background-color: rgb(255, 245, 157);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#FFF59D</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFF176" style="background-color: rgb(255, 241, 118);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#FFF176</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFEE58" style="background-color: rgb(255, 238, 88);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#FFEE58</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFEB3B" style="background-color: rgb(255, 235, 59);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#FFEB3B</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FDD835" style="background-color: rgb(253, 216, 53);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#FDD835</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FBC02D" style="background-color: rgb(251, 192, 45);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#FBC02D</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F9A825" style="background-color: rgb(249, 168, 37);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#F9A825</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F57F17" style="background-color: rgb(245, 127, 23);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#F57F17</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Amber</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFF8E1" style="background-color: rgb(255, 248, 225);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#FFF8E1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFECB3" style="background-color: rgb(255, 236, 179);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#FFECB3</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFE082" style="background-color: rgb(255, 224, 130);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#FFE082</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFD54F" style="background-color: rgb(255, 213, 79);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#FFD54F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFCA28" style="background-color: rgb(255, 202, 40);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#FFCA28</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFC107" style="background-color: rgb(255, 193, 7);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#FFC107</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFB300" style="background-color: rgb(255, 179, 0);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#FFB300</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFA000" style="background-color: rgb(255, 160, 0);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#FFA000</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FF8F00" style="background-color: rgb(255, 143, 0);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#FF8F00</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FF6F00" style="background-color: rgb(255, 111, 0);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#FF6F00</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Orange</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFF3E0" style="background-color: rgb(255, 243, 224);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#FFF3E0</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFE0B2" style="background-color: rgb(255, 224, 178);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#FFE0B2</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFCC80" style="background-color: rgb(255, 204, 128);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#FFCC80</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFB74D" style="background-color: rgb(255, 183, 77);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#FFB74D</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFA726" style="background-color: rgb(255, 167, 38);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#FFA726</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FF9800" style="background-color: rgb(255, 152, 0);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#FF9800</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FB8C00" style="background-color: rgb(251, 140, 0);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#FB8C00</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F57C00" style="background-color: rgb(245, 124, 0);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#F57C00</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#EF6C00" style="background-color: rgb(239, 108, 0);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#EF6C00</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E65100" style="background-color: rgb(230, 81, 0);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#E65100</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Deep Orange</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FBE9E7" style="background-color: rgb(251, 233, 231);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#FBE9E7</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFCCBC" style="background-color: rgb(255, 204, 188);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#FFCCBC</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FFAB91" style="background-color: rgb(255, 171, 145);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#FFAB91</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FF8A65" style="background-color: rgb(255, 138, 101);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#FF8A65</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FF7043" style="background-color: rgb(255, 112, 67);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#FF7043</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FF5722" style="background-color: rgb(255, 87, 34);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#FF5722</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F4511E" style="background-color: rgb(244, 81, 30);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#F4511E</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E64A19" style="background-color: rgb(230, 74, 25);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#E64A19</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#D84315" style="background-color: rgb(216, 67, 21);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#D84315</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#BF360C" style="background-color: rgb(191, 54, 12);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#BF360C</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Brown</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#EFEBE9" style="background-color: rgb(239, 235, 233);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#EFEBE9</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#D7CCC8" style="background-color: rgb(215, 204, 200);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#D7CCC8</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#BCAAA4" style="background-color: rgb(188, 170, 164);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#BCAAA4</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#A1887F" style="background-color: rgb(161, 136, 127);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#A1887F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#8D6E63" style="background-color: rgb(141, 110, 99);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#8D6E63</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#795548" style="background-color: rgb(121, 85, 72);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#795548</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#6D4C41" style="background-color: rgb(109, 76, 65);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#6D4C41</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#5D4037" style="background-color: rgb(93, 64, 55);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#5D4037</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#4E342E" style="background-color: rgb(78, 52, 46);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#4E342E</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#3E2723" style="background-color: rgb(62, 39, 35);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#3E2723</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Grey</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#FAFAFA" style="background-color: rgb(250, 250, 250);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#FAFAFA</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#F5F5F5" style="background-color: rgb(245, 245, 245);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#F5F5F5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#EEEEEE" style="background-color: rgb(238, 238, 238);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#EEEEEE</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#E0E0E0" style="background-color: rgb(224, 224, 224);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#E0E0E0</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#BDBDBD" style="background-color: rgb(189, 189, 189);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#BDBDBD</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#9E9E9E" style="background-color: rgb(158, 158, 158);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#9E9E9E</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#757575" style="background-color: rgb(117, 117, 117);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#757575</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#616161" style="background-color: rgb(97, 97, 97);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#616161</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#424242" style="background-color: rgb(66, 66, 66);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#424242</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#212121" style="background-color: rgb(33, 33, 33);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#212121</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>

        <div class="color-palette-wrapper" data-bind="css: { 'js-active': selectedColor() === color }">
          <h2 class="color-palette-header" data-bind="text: color">Blue Grey</h2>
          <ol class="color-palette" data-bind="foreach: variations">
            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#ECEFF1" style="background-color: rgb(236, 239, 241);">
              <span data-bind="text: weight">50</span>
              <span data-bind="text: hex">#ECEFF1</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#CFD8DC" style="background-color: rgb(207, 216, 220);">
              <span data-bind="text: weight">100</span>
              <span data-bind="text: hex">#CFD8DC</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#B0BEC5" style="background-color: rgb(176, 190, 197);">
              <span data-bind="text: weight">200</span>
              <span data-bind="text: hex">#B0BEC5</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#90A4AE" style="background-color: rgb(144, 164, 174);">
              <span data-bind="text: weight">300</span>
              <span data-bind="text: hex">#90A4AE</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#78909C" style="background-color: rgb(120, 144, 156);">
              <span data-bind="text: weight">400</span>
              <span data-bind="text: hex">#78909C</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#607D8B" style="background-color: rgb(96, 125, 139);">
              <span data-bind="text: weight">500</span>
              <span data-bind="text: hex">#607D8B</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#546E7A" style="background-color: rgb(84, 110, 122);">
              <span data-bind="text: weight">600</span>
              <span data-bind="text: hex">#546E7A</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#455A64" style="background-color: rgb(69, 90, 100);">
              <span data-bind="text: weight">700</span>
              <span data-bind="text: hex">#455A64</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#37474F" style="background-color: rgb(55, 71, 79);">
              <span data-bind="text: weight">800</span>
              <span data-bind="text: hex">#37474F</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>

            <li id="clipboardItem" class="color-palette__item" data-bind="attr: { 'data-clipboard-text': hex }, style: { 'background-color': hex }" data-clipboard-text="#263238" style="background-color: rgb(38, 50, 56);">
              <span data-bind="text: weight">900</span>
              <span data-bind="text: hex">#263238</span>
              <span class="copied-indicator" data-bind="css: { 'js-copied': copiedHex() === hex }">Color copied!</span>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</body>

</html>