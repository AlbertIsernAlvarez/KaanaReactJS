<div class="pay_container">

  <div class="PF PF-content full noborder center transparent">
			<div class="container">
				<div class="PF-image" style="background-image:url('./apps/pay/search-friends.svg'); background-size: contain; min-height: 6em; background-color: transparent;"></div>
				<h1 t-dd>Send or request money</h1>
				<input class="search_friends PF shadow" type="text" placeholder="Search people" />
			</div>
		</div>

  <ul class="PF PF-list search_friends_results"></ul>

  <script>
  $('.search_friends_results').load("./apps/pay/resources/following.php");
    $(".search_friends").on("keyup paste", function(event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      $('.search_friends_results').load("./apps/pay/resources/search-friends.php?q=" + $('.search_friends').val());
    });
  </script>

</div>