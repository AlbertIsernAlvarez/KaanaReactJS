<html>

<head>
  <style>
    .pay_container {
      max-width: 40rem;
      margin: auto;
      width: 100%;
    }
    
    .pay_container .search_friends {
    padding: .5em;
    border-radius: 1em;
    border: none;
    font-size: 1em;
    text-align: center;
    width: 20em;
    margin: auto;
    background-color: rgb(var(--PF-color-surface));
    }
    
    .pay_container .credit {
    width: fit-content;
    padding: .5em 0.7em;
    font-size: 3em;
    margin: auto;
    margin-top: -.5em;
    white-space: nowrap;
    display: flex;
    align-items: center;
    }
    
    .paybalance {
      text-align: center;
      font-size: 1.2rem;
      display: flex;
      flex-direction: column;
    }
    
    .paybalance .balance-group {
      font-size: 3rem;
      white-space: nowrap;
    display: flex;
    align-items: center;
    margin: auto;
    }
  </style>
</head>

<body>

  <div class="pay_container">

    <div class="PF PF-content center transparent">
      <div class="container">
        <div class="image" style="background-image:url('./apps/<?=$app?>/<?=$app?>.svg');"></div>
        <div class="credit">
          <div class="paybalance">Balance <div class="balance-group"><span pay-current-balance><?=$pay_total?></span><i class="ddcoin-symbol"></i></div></div>
        </div>
      </div>
    </div>

    <div class="PF PF-card">
      <div class="info">
        <h1 t-dd>Kaana Pay takes 0.10 fee for the community when sending or receiving money</h1>
        <p t-dd>Easily send or receive money using your Kaana Coins</p>
      </div>
    </div>

    <div class="PF PF-card">
      <div class="info">
        <h1 t-dd>New! Still in test.</h1>
        <p t-dd>Kaana Pay is a new payment system inside the platform, still is in development.</p>
      </div>
    </div>

    <div class="PF PF-card">
      <div class="info">
        <h1 t-dd>No reminders set</h1>
        <p t-dd>Get a reminder when it's time to send or request money, so you'll never miss a payment again</p>
      </div>
    </div>

    <div class="PF PF-card">
      <div class="info">
        <h1 t-dd>Recent activity</h1>
      </div>
      <ul class="PF PF-collection" id="pay-recentactivity">
        <?php include(__DIR__.'/../resources/activity.php'); ?>
      </ul>
    </div>

  </div>

</body>

</html>