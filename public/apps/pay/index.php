<html>

<head>
  <style>
  .header .buscador  {
    opacity: 0;
    pointer-events: none;
  }
  </style>
</head>

<body>


      <div class="PF-tabs">
        <div class="tabs">
          <div class="tab" data-name="tabspay-1"></div>
          <div class="tab" data-name="tabspay-2"></div>
          <div class="tab" data-name="tabspay-3"><div class="pay_container"><?php include(__DIR__.'/../../apps/coupons/index.php'); ?></div></div>
          <div class="tab" data-name="tabspay-4"></div>
        </div>

        <div class="PF PF-tabbar bottom shadow">
          <div class="container center">
            <ul id="bottombar">
              <li class="ripple active" data-for="tabspay-1" data-taburl="./apps/<?=$app?>/pantallas/home.php?<?=$server_querystring?>" preload>
                <i class="material-icons bd">home</i>
                <span t-dd>Home</span>
              </li>

              <li class="ripple" data-for="tabspay-2" data-taburl="./apps/<?=$app?>/pantallas/payment.php?<?=$server_querystring?>" preload>
                <i class="material-icons">credit_card</i>
                <span t-dd>Payment</span>
              </li>

              <li class="ripple" data-for="tabspay-3" preload>
                <i class="material-icons">local_activity</i>
                <span t-dd>Coupons</span>
              </li>

              <li class="ripple" data-for="tabspay-4" data-taburl="./apps/<?=$app?>/pantallas/send.php?<?=$server_querystring?>" preload>
                <i class="material-icons bd">send</i>
                <span t-dd>Send</span>
              </li>
            </ul>
            <div class="slider"></div>
          </div>
        </div>
      </div>

      <script>

        <?php if($action === 'send' and $_GET['send']){?>
          windowdd('./apps/pay/resources/transfer.php?app=pay&id=<?=$_GET['send']?>', 'fit');
        <?}?>
      
        function updatebalance(){
          $.get("./apps/pay/resources/current-balance.php?<?=$server_querystring?>", function(data, status){
            $('[pay-current-balance]').text(data);
          });
        }

        setInterval(function(){ updatebalance(); }, 2000);
      </script>

</body>

</html>