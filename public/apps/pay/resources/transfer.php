<html>

<head>
	<style>
		.paybalance {
			text-align: center;
			font-size: 1.2rem;
			display: flex;
			flex-direction: column;
		}
		
		.paybalance .balance-group {
			font-size: 3rem;
			white-space: nowrap;
			display: flex;
			align-items: center;
			margin: auto;
		}
	</style>
</head>

<body>
	<div class="PF PF-toolbar transparent">
		<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
		<h1><span t-dd>Send or request money to</span> <?=$user_name?></h1>
	</div>
	<div class="paybalance">Balance
		<div class="balance-group"><span pay-current-balance><?=$pay_total?></span><i class="ddcoin-symbol"></i></div>
	</div>
	<div class="PF-tabs" style="overflow:hidden;">
		<div class="PF PF-tabbar shadow">
			<div class="container">
				<ul>
					<li class="ripple active" preload data-for="tabspay-send" data-taburl="./apps/pay/resources/send.php?<?=$server_querystring;?>"><span t-dd>Send money</span></li>
					<li class="ripple" preload data-for="tabspay-request" data-taburl="./apps/pay/resources/request.php?<?=$server_querystring;?>"><span t-dd>Request Money</span></li>
				</ul>
				<div class="slider"></div>
			</div>
		</div>
		<div class="tabs">
			<div class="tab" data-name="tabspay-send"></div>
			<div class="tab" data-name="tabspay-request"></div>
		</div>
	</div>
</body>

</html>