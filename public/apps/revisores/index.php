<?php 
$transporteelegido = $_GET['transporte'];
?>
<html>
  <head>
    <style>
      .transporte_contenedor {
        text-align:center;
      }
      
      .transporte_contenedor .transportes {
    width: auto;
    display: inline-flex;
    align-items: center;
    color: #e34923;
    padding: 0rem;
    background: white;
    border-radius: 2px;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    text-align: center;
      }
			
			.transporte_contenedor .transportes a {
				margin:auto;
			}
			
			.transporte_contenedor .transporte {
				margin:auto;
			}
      
      .transporte_contenedor .transporte i {
    padding: 0.5rem;
    margin: auto;
        transition:0.5s;
        margin: 0.5rem;
        cursor:pointer;
      }
      
      .transporte_contenedor .transporte:hover i {
        border-radius:100%;
        color:white;
        background:#e34923;
      }
      
      .transporte_contenedor .cards_contenedor {
    max-width:22rem;
    text-align: center;
    padding: 0.5rem;
    width: calc(100% - 1rem);
        margin:auto;  
      }
      
      .transporte_contenedor .cards_contenedor .card {
    width: calc(100% - 2rem);
    background: #e34923;
    border-radius: 2px;
    padding: 0.5rem;
    color: white;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    text-align: left;
    display: inline-block;
    margin: 0.5rem;
	  transition:0.5s;
    user-select: none;
	  cursor:pointer;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .grupo_informacion {
        padding:0rem;
        display:inline-flex;
        width:100%;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .L {
    border-radius: 25px;
    background: rgba(255, 255, 255, 0.25);
    color: white;
    padding: 0.5rem 1rem;
    margin: 0.5rem;
    display: inline-block;
    font-size: 1.2rem;
    border: solid rgba(255, 255, 255, 0.3) 0.5rem;
    border-top: none;
    border-bottom: none;
      }
      
      .transporte_contenedor .card .informacion_rapida .parada_actual {
    margin: 0.5rem;
    display: inline-block;
        background: transparent;
    color: white;
    font-size: 1rem;
    padding: 0;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .proximo {
        padding:0.5rem;
        text-align:right;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .proximo .actual {
        font-size:2rem;
        display:block!important;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias {
    max-width: calc(100% - 1rem);
    background: rgb(255, 255, 255);
    border-radius: 30px;
    display: inline-flex;
    padding: 0;
    align-items:center;
    color:black;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias .transbordo {
    padding: 0.5rem;
    border-radius: 30px;
    background: #e34923;
    color: white;
    font-size: 1rem;
    margin: 0.2rem;
    margin-right:0.5rem;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias .correspondencia {
    border-radius: 30px;
    background: #e34923;
    color: white;
    padding: 0.5rem 0.8rem;
    margin: 0rem 0.5rem;
    display: inline-block;
    font-size: 0.8rem;
    border: solid rgba(0, 0, 0, 0.15) 0.5rem;
    border-top: none;
    border-bottom: none;
    margin-left:0;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias span {
        margin-left:0.5rem;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias .correspondencia.LI {
        padding: 0.3rem 0.5rem;
      }
			
			.transporte_contenedor .cards_contenedor .card .info {
				display:none;
			}
			
			.transporte_contenedor .cards_contenedor .card.visible .info {
				display:block;
			}
			
			.transporte_contenedor .cards_contenedor .card.visible .info p {
		padding: 1rem 0.5rem;
    padding-bottom: 0.5rem;
			}
      
      
      .alertarrevisor {
    width: 100%;
    background: white;
    margin: auto;
    display: none;
    transition: 0.5s;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    border-radius: 0.5em;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    max-height: 25rem;
    max-width: 35rem;
				text-align:left;
      }
			
			.alertarrevisor h1 {
				width: calc(100% - 1rem);
    border: none;
    color: black;
    padding: 0.5rem;
    text-align: left;
    font-size: 1rem;
    font-family: Roboto;
			}
			
			@media screen and (max-width: 450px) { 
				.transporte_contenedor .transportes {
					width:100%;
				}
				
				.transporte_contenedor .cards_contenedor {
					max-width:100%;
				}
				
			}
    </style>
  </head>
  <body>
    <div class="transporte_contenedor" >
      
      <div class="alertarrevisor" >
        <h1>Alertar de un revisor</h1>
        <form action="/resources/apps/revisores/alertar.php" name="form_alertarevisor" id="nuevaalerta" method="post" enctype="multipart/form-data" >
					<select name="ciudad" form="nuevaalerta" required >
						<option selected disabled >Ciudad</option>
						<option value="<?=strtolower($ciudadactual)?>" ><?=$ciudadactual?></option>
						<option value="barcelona">Barcelona</option>
						<option value="new york" >New York</option>
						<option value="milano" >Milano</option>
						<option value="madrid">Madrid</option>
						<option value="athens">Athens</option>
						<option value="paris">Paris</option>
						
          </select>
					<input type='time' value='now' name="hora" style="display:none;"/>
					
					<div class="PF PF-input">
      <select class="input-item">
        <option disabled="" selected=""></option>
						<option value="bus">Bus</option>
						<option value="metro">Metro</option>
						<option value="tren">Tren</option>
						<option value="tramvia">Tramvia</option>
      </select>
      <label>Transporte</label>
      <span class="bar"></span>
    </div>
					
					
					<div class="PF PF-input">
      <input class="input-item" type="text" required="" id="input-field1">
      <label>name parada</label>
      <span class="bar"></span>
    </div>
					
					
					<div class="PF PF-input">
      <select class="input-item">
        <option disabled="" selected=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
      </select>
      <label>Cuantos son</label>
      <span class="bar"></span>
    </div>
					
					<div class="PF PF-input">
      <input class="input-item" type="text" required="" id="input-field1">
      <label>Descripcion</label>
      <span class="bar"></span>
    </div>
					
          
					<div class="PF PF-buttons" >
          <button class="PF PF-button" ><?=ENVIAR?></button>
        </div>
					
        </form>
      </div>
      
      <div class="cards_contenedor" >
        
        
        <?php
        $linea_transporte = mysqli_query($con,"SELECT * FROM revisores WHERE ciudad LIKE '%$ciudadactual%' AND transporte LIKE '%$transporteelegido%' ORDER BY id DESC LIMIT 100");
	        while($row_linea_transporte = mysqli_fetch_array($linea_transporte))
          { 
	            $id_linea_transporte = $row_linea_transporte["id"];
              $color_linea_transporte = $row_linea_transporte["transporte"];
              $hora_linea_transporte = $row_linea_transporte["hora"];
              $paradas_2_linea_transporte = $row_linea_transporte["donde"];
              $cuantos_linea_transporte = $row_linea_transporte["cuantos"];
						$descripcion_linea_transporte = $row_linea_transporte["descripcion"];
						$ciudad_linea_transporte = $row_linea_transporte["ciudad"];
						
						$color_aleatorio_card_recordatorios_colores = array("#f44336", "#3f51b5", "#ff5722", "#607d8b", "#4caf50");
						$color_aleatorio_card_recordatorios = array_rand($color_aleatorio_card_recordatorios_colores, 1);
						$color_aleatorio_card_recordatorios = $color_aleatorio_card_recordatorios_colores[$color_aleatorio_card_recordatorios];
    ?>
        
      <div class="card" style="background:<?=$color_aleatorio_card_recordatorios?>;" onclick="$(this).toggleClass('visible');" >
        <div class="informacion_rapida" >
          <div class="grupo_informacion" >
            <div style=" width: 100%;" >
              
              <span class="L" ><?=$paradas_2_linea_transporte?></span>
              <span class="parada_actual" ><?=$descripcion_linea_transporte?></span>
            </div>
            <ul class="proximo" >
              <li class="actual" ><?=$hora_linea_transporte?></li>
							<li><b><?=ucwords($ciudad_linea_transporte)?></b></li>
            </ul>
          </div>
          <div class="correspondencias" >
            <i class="material-icons material-icons transbordo">&#xE85E;</i>
            <div class="correspondencia LI" style="background:<?=$color_aleatorio_card_recordatorios?>;" ><?=$cuantos_linea_transporte?> Revisores</div>
          </div>
        </div>
        <div class="info" >
          <p>
						
					</p>
        </div>
      </div>
        
        
        <?}?>
        
        
        
        
    </div>
    </div>
		
		
		<script>

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        alert("Geolocation is not supported by this browser.");
    }

function showPosition(position) {
    alert("Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude);
}
</script>
    
    <script>
    function alertarrevisor(){
      $('.alertarrevisor').toggle();
    }
			
			$(function(){  
  $('input[type="time"][value="now"]').each(function(){    
    var d = new Date(),        
        h = d.getHours(),
        m = d.getMinutes();
    if(h < 10) h = '0' + h; 
    if(m < 10) m = '0' + m; 
    $(this).attr({
      'value': h + ':' + m
    });
  });
});
			
    </script>
    
  </body>
</html>