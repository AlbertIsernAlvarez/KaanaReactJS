<html>
  <head>
    <style>
    
      .topbar, .topbar .topbar_container {
        margin: auto;
        width: auto;
      }
      
      .posts {
        padding-top:1rem;
      }
      
    </style>
  </head>
  <body>
    
    <div class="posts" >
    <?php include('./pantallas/publicaciones/form.php'); ?>
    <div id="posts" ></div>
    </div>
    
    
    <script>
      
      $(document).ready(function(){
        $('#posts').load('./resources/posts/posts.php?<?=$_SERVER["QUERY_STRING"];?>');
      });
      
      function startask(){
        $('#posts').load('./resources/posts/posts.php?<?=$_SERVER["QUERY_STRING"];?>');
      }
      
      function pantallaask(pantalla){
        $('#posts').load('./resources/posts/posts.php?<?=$_SERVER["QUERY_STRING"];?>&tipo=' + pantalla);
      }
      
    </script>
    
  </body>
</html>