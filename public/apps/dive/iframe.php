<html><head>
<style>
  
  @font-face {
  font-family: 'Material Icons';
  font-style: normal;
  font-weight: 400;
  src: url(/resources/fuentes/MaterialIcons/MaterialIcons-Regular.eot); /* For IE6-8 */
  src: local('Material Icons'),
       local('MaterialIcons-Regular'),
       url(/resources/fuentes/MaterialIcons/MaterialIcons-Regular.woff2) format('woff2'),
       url(/resources/fuentes/MaterialIcons/MaterialIcons-Regular.woff) format('woff'),
       url(/resources/fuentes/MaterialIcons/MaterialIcons-Regular.ttf) format('truetype');
}

.material-icons {
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;  /* Preferred icon size */
  display: inline-block;
  line-height: 1;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: normal;
  white-space: nowrap;
  direction: ltr;

  /* Support for all WebKit browsers. */
  -webkit-font-smoothing: antialiased;
  /* Support for Safari and Chrome. */
  text-rendering: optimizeLegibility;

  /* Support for Firefox. */
  -moz-osx-font-smoothing: grayscale;

  /* Support for IE. */
  font-feature-settings: 'liga';
}
  
  body {
    margin:0;
  }
  
.chrome-button {
  background: none;
  border: none;
  width: 24px;
  height: 24px;
  box-sizing: border-box;
  padding: none;
  position: relative;
  font-size: 20px;
  color: rgba(0, 0, 0, 0.7);
}
.chrome-button i {
  font-style: 10px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
}
.chrome-button .highlight,
.chrome-button .ripple {
  background: rgba(0, 0, 0, 0.1);
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 2px;
}
.chrome-button .highlight {
  opacity: 0;
  -webkit-transition: opacity 0.2s;
  transition: opacity 0.2s;
}
.chrome-button:hover:not(.down) .highlight {
  opacity: 1;
}
.chrome-button:active .highlight {
  opacity: 0;
}
.chrome-button:focus {
  outline: none;
}
.chrome-button .ripple {
  opacity: 1;
  border-radius: 50%;
  top: -10%;
  left: -10%;
  width: 120%;
  height: 120%;
  -webkit-transition: opacity 0.2s, -webkit-transform 0.2s;
  transition: opacity 0.2s, -webkit-transform 0.2s;
  transition: opacity 0.2s, transform 0.2s;
  transition: opacity 0.2s, transform 0.2s, -webkit-transform 0.2s;
  -webkit-transform: scale(0);
          transform: scale(0);
}
.chrome-button.square .ripple {
  border-radius: 2px;
}
.tab-header-bar {
  background: #f2f2f2;
  padding: 5px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-negative: 0;
      flex-shrink: 0;
}
.tab-header-bar > .chrome-button {
  margin: 0 2px;
  margin-top: 2px;
  -ms-flex-negative: 0;
      flex-shrink: 0;
}
.tab-header-bar .omnibox {
  margin: 0 4px;
  width: 100%;
  border-radius: 3px;
  background: #fff;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 28px;
  box-sizing: border-box;
  border: 1px solid rgba(0, 0, 0, 0.3);
}
.tab-header-bar .omnibox .omnibox-input {
  width: 100%;
  line-height: 26px;
}
.tab-header-bar .omnibox .omnibox-input .security {
  margin-left: 6px;
  color: #0d7f42;
  padding-right: 5px;
  margin-right: 1px;
  border-right: 1px solid rgba(0, 0, 0, 0.1);
}
.tab-header-bar .omnibox .omnibox-input .security i {
  font-family: 'Material Icons';
  font-style: normal;
  line-height: 0;
  vertical-align: -0.1em;
  font-size: 12px;
}
.tab-header-bar .omnibox .omnibox-input .protocol {
  color: #0d7f42;
}
.tab-header-bar .omnibox .omnibox-input .slashes,
.tab-header-bar .omnibox .omnibox-input .path {
  opacity: 0.6;
}
.tab-header-bar .omnibox .bookmark {
  -ms-flex-negative: 0;
      flex-shrink: 0;
  margin: 1px;
}
.tab-header-bar .omnibox .bookmark.bookmarked {
  color: #559df3;
}
.content-frame {
  width:100%;
  height: calc(100% - 38px);
  border: none;
}
</style></head><body>
<div class="tab-header-bar">
  <button class="chrome-button"><i class="material-icons">&#xE5C4;</i><div class="highlight"></div><div class="ripple" style=""></div></button>
  <button class="chrome-button"><i class="material-icons">&#xE5C8;</i><div class="highlight"></div><div class="ripple" style=""></div></button>
  <button class="chrome-button refresh"><i class="material-icons">&#xE5D5;</i><div class="highlight"></div><div class="ripple"></div></button>
  <div class="omnibox">
    <div class="omnibox-input">
      <span class="security"><i>https</i> Secure</span>
      <span class="protocol">https</span><span class="slashes">://</span><span class="domain">codepen.io</span><span class="path">/</span>
    </div>
    <button class="chrome-button bookmark"><i class="material-icons">&#xE83A;</i><div class="highlight"></div><div class="ripple"></div></button>
  </div>
  <button class="chrome-button square"><i class="material-icons">&#xE5D4;</i><div class="highlight"></div><div class="ripple"></div></button>
</div>
<iframe class="content-frame" src="<?=$_GET['url']?>"></iframe>
  
  <script src="/resources/js/jquery-1.11.3.js"></script>
  <script src="/resources/js/jquery.min.js"></script>
<script>let initButton = button => {
  let buttonDown = false
  let downTime = 0
  let highlight = document.createElement('div')
  let ripple = document.createElement('div')
  highlight.classList.add('highlight')
  ripple.classList.add('ripple')
  button.appendChild(highlight)
  button.appendChild(ripple)
  button.addEventListener('mousedown', e => {
    buttonDown = true
    downTime = Date.now()
    ripple.style.transform = 'scale(1)'

    button.classList.add('down')
  })
  button.addEventListener('mouseup', e => {
    buttonDown = false
    ripple.style.transform = 'scale(1.3)'
    ripple.style.opacity = 0
  })
  window.addEventListener('mouseup', e => {
    if (buttonDown) {
      buttonDown = false
      ripple.style.opacity = 0
      ripple.style.transform = 'scale(0.8)'
    }
  })
  ripple.addEventListener('transitionend', e => {
    if (!buttonDown) {
      button.classList.remove('down')
      ripple.style.transition = 'none'
      ripple.style.opacity = ''
      ripple.style.transform = ''
      setTimeout(() => {
        ripple.style.transition = ''
      })
    }
  })
}

let buttons = Array.from(document.querySelectorAll('.chrome-button'))
for (let button of buttons) {if (window.CP.shouldStopExecution(1)){break;}initButton(button)
window.CP.exitedLoop(1);
}

// specific button stuff
let refreshButton = document.querySelector('.refresh')
refreshButton.addEventListener('click', e => {
  refreshButton.firstChild.textContent = 'close'
  setTimeout(() => {
    refreshButton.firstChild.textContent = 'refresh'
  }, 600)
})
let bookmarkButton = document.querySelector('.bookmark')
bookmarkButton.addEventListener('click', e => {
  if (bookmarkButton.classList.contains('bookmarked')) {
    bookmarkButton.firstChild.textContent = 'star_border'
    bookmarkButton.classList.remove('bookmarked')
  } else {
    bookmarkButton.firstChild.textContent = 'star'
    bookmarkButton.classList.add('bookmarked')
  }
});
</script>
</body></html>