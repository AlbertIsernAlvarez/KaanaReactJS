<html><head>
  
<style>
  
  .header {
  height: 60px;
  background: #333;
  padding: 10px 0px 10px 0px;
  width: 100%;
  z-index: 1;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
}
.header .search {
  max-width: 600px;
  margin: 0px auto;
  height: 100%;
  background: rgba(0, 0, 0, 0.25);
  padding: 0px 10px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}
.header .search i {
  line-height: 40px;
  color: #666;
  -webkit-box-ordinal-group: 0;
      -ms-flex-order: -1;
          order: -1;
  /* Hacker man */
}
.header .search input {
  width: 100%;
  background: transparent;
  border: none;
  border-radius: 2px;
  padding: 0px 10px;
  color: #FFF;
  outline: none;
}
.header .search input:focus + i {
  color: #FFF;
  -webkit-transition: 0.25s;
  transition: 0.25s;
}

.container {
  margin: 0px auto;
  font-size: 14px;
}
.container .groups-container {
  width: 100%;
}
.container .groups-container .group {
  margin: auto;
  margin-top: 30px;
}
.container .groups-container .group .title {
  max-width: 600px;
  margin: auto;
  padding-bottom: 0.5rem;
}
.container .groups-container .group .options {
  width: 100%;
}
.container .groups-container .group .options .option {
  box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);

  background: white;
  position: relative;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  cursor: pointer;
  max-width: 600px;
  -webkit-transition: all 0.25s;
  transition: all 0.25s;
  margin: auto;
}
.container .groups-container .group .options .option .title {
  padding: 20px;
  margin: 0px;
  width: calc(100% - 40px);
  max-width: 100%;
  -webkit-transition: all 0.25s;
  transition: all 0.25s;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}
.container .groups-container .group .options .option .settings {
  -webkit-transition: all 0.25s;
  transition: all 0.25s;
  max-height: 0px;
  opacity:0;
}
.container .groups-container .group .options .option .settings .setting {
  padding: 0px 20px;
  opacity: 0;
  -webkit-transition: all 0.25s;
  transition: all 0.25s;
}
.container .groups-container .group .options .option.open {
  max-width: 650px;
  margin: 5px auto;
  box-shadow: 0 10px 10px 0 rgba(0,0,0,.19), 0 6px 3px 0 rgba(0,0,0,.23);
  z-index:999;
}
.container .groups-container .group .options .option.open .title {
    background: #263238;
    color: white;
}
.container .groups-container .group .options .option.open .settings .setting {
  height: 60px;
  line-height: 60px;
  opacity: 1;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  overflow:hidden;
}
.container .groups-container .group .options .option.open:after {
  opacity: 0;
}
  .container .groups-container .group .options .option.open .settings {
    height:auto;
    min-height:100%;
    max-height:100%;
    opacity:9;
  }
.container .groups-container .group .options .option .info {
  color: rgba(255, 255, 255, 0.25);
  padding-top: 0.5rem;
  display:none;
}
.container .groups-container .group .options .option.open .info {
    display:block;
  }
.container .groups-container .group .options .option:after {
  content: '';
  -webkit-transition: all 0.25s;
  transition: all 0.25s;
  border: 5px solid transparent;
  border-left: 5px solid #FFF;
  position: absolute;
  right: 20px;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
}
</style></head><body>

  <div class="header">
  <div class="search">
    <input type="text" placeholder="Search settings"><i class="material-icons">search</i>
  </div>
</div>
<div class="container">
  <div class="groups-container">
            <div class="group">
               <div class="title">Basic</div>
               <div class="options">
            <div class="option" data-group="Basic" data-option="Search Engine">
               <div class="title">Search Engine</div>
               <div class="settings" >
            <div class="setting">
               Engine: <option>Google</option><option>Bing</option><option>Yahoo</option><select><option>Google</option><option>Bing</option><option>Yahoo</option></select>
            </div>
         
            <div class="setting">
               Save Results: <input type="checkbox">
            </div>
         
            <div class="setting">
               Show Search Box: <input type="checkbox">
            </div>
         </div>
            </div>
         
            <div class="option" data-group="Basic" data-option="Appearance">
               <div class="title">Appearance</div>
               <div class="settings" >
            <div class="setting">
               Theme: <option>Light</option><option>Dark</option><select><option>Light</option><option>Dark</option></select>
            </div>
         
            <div class="setting">
               Show home button: <input type="checkbox">
            </div>
         
            <div class="setting">
               Show bookmarks bar: <input type="checkbox">
            </div>
         
            <div class="setting">
               Font Size: <option>10px</option><option>12px</option><option>14px</option><select><option>10px</option><option>12px</option><option>14px</option></select>
            </div>
         </div>
            </div>
         </div>
            </div>
         
            <div class="group">
               <div class="title">Advanced</div>
               <div class="options">
            <div class="option" data-group="Advanced" data-option="Privacy and security">
               <div class="title">Privacy and security<div class="info">Choose more data to send us not less</div></div>
               <div class="settings" >
            <div class="setting">
               Allow sites to save and read cookie data: <input type="checkbox">
            </div>
         
            <div class="setting">
               Keep local data until you quit your browser: <input type="checkbox">
            </div>
         
            <div class="setting">
               Block third-party cookies: <input type="checkbox">
            </div>
         </div>
            </div>
         
            <div class="option" data-group="Advanced" data-option="Cookies">
               <div class="title">Cookies<div class="info">Nom nom nom</div></div>
               <div class="settings" >
            <div class="setting">
               Allow sites to save and read cookie data: <input type="checkbox">
            </div>
         
            <div class="setting">
               Keep local data until you quit your browser: <input type="checkbox">
            </div>
         
            <div class="setting">
               Block third-party cookies: <input type="checkbox">
            </div>
         </div>
            </div>
         
            <div class="option" data-group="Advanced" data-option="System">
               <div class="title">System</div>
               <div class="settings" >
            <div class="setting">
               Continue running apps when Google Chrome is closed: <input type="checkbox">
            </div>
         
            <div class="setting">
               Use hardware acceleration when available: <input type="checkbox">
            </div>
         
            <div class="setting">
               Save location: <option>Downloads</option><option>Desktop</option><option>Documents</option><select><option>Downloads</option><option>Desktop</option><option>Documents</option></select>
            </div>
         </div>
            </div>
         </div>
            </div>
         </div>
</div>
<script>
  
    $(document).ready(function() {
        $('.options .option').click(function() {
            $(this).toggleClass('open');
            scrollToElement(this, 600);
        });
    });
  
  var scrollToElement = function(el, ms){
    var speed = (ms) ? ms : 600;
    $('html,body').animate({
        scrollTop: $(el).offset().top
    }, speed);
}
  
</script>
</body></html>