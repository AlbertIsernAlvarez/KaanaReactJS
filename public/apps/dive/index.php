<html>

<head>
  <style>

  .header, #frame .PF-tabbar {
    background-color: rgb(var(--PF-color-on-surface), 0.05);
  }
    
    .PF-tabbar .container ul li span {
      padding:0;
    }
    
    :root {
      --r: 9px;
    }

    #frame {
    width: 100%;
    flex: 1 auto;
    display: inline-flex;
    flex-direction: column;
    margin: 0;
    box-sizing: border-box;
    }
    
    #frame .PF-tabbar {
    padding-top: .5em;
    }
    
    #frame #navbar {
      background-color: rgb(var(--PF-color-surface));
      display: flex;
      align-items: center;
      width: 100%;
      box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.1);
    }

    #frame #navbar>.PF-icon {
      margin: .2em 0;
    }

    #frame #navbar>.PF-icon:first-child {
      margin-left: .5em;
    }

    #frame #navbar>.PF-icon:last-child {
      margin-right: .5em;
    }

    #frame #navbar>.PF-icon>i {
      font-size: 20px;
    }

    #frame #navbar .omni {
    background-color: rgb(var(--PF-color-on-surface), .04);
    height: 32px;
    line-height: 32px;
    padding: 0px 28px;
    margin: 0 5px;
    border-radius: 1.5em;
    flex: 1 1 100%;
    }

    #frame .PF-tabbar .container {
      flex: 1;
    }

    #frame .PF-tabbar .container ul li {
    display: flex;
    position: relative;
    margin: 0px;
    min-width: 60px;
    border-radius: var(--r) var(--r) 0px 0px;
    box-sizing: border-box;
    font-size: 13px;
    padding: 0.6rem;
    color: rgb(var(--PF-color-on-surface));
    cursor: pointer;
    overflow: visible;
    flex: 1;
    max-width: 19em;
    font-weight: 500;
    text-align: left;
    }

#frame .PF-tabbar .container ul li:first-child {
  margin-left: .8em;
}

#frame .PF-tabbar .container ul li:last-child {
  margin-right: .8em;
}
    
/*
    .PF-tabbar .container ul li:hover {
      background-color: var(--PF-color-bg-second-default);
    }

    .PF-tabbar .container ul li:hover:before {
      background-color: var(--PF-color-bg-second-default);
    }

    .PF-tabbar .container ul li:hover:after {
      background-color: rgba(255, 255, 255, 0);
    }*/

    #frame .PF-tabbar .container ul li:not(.active):before {
      left: 0px;
      background-color: rgba(0, 0, 0, 0.2);
    }
    
    #frame .PF-tabbar .container ul li:first-child:before {
      background-color: transparent;
    }
    
    #frame .PF-tabbar .container ul li.active + li:before, .PF-tabbar .container ul li:hover + li:before {
      left: 0px;
      background-color: transparent;
    }

    #frame .PF-tabbar .container ul li:not(.active):before,
    li:after {
      content: '';
      position: absolute;
      width: 1px;
      height: calc(40px - var(--r)*2);
      z-index: 1;
      bottom: 8px;
    }

    #frame .PF-tabbar .container ul li.active {
      color: rgb(var(--PF-color-on-surface));
    background-color: rgb(var(--PF-color-surface));
    z-index: 1;
    }

    #frame .PF-tabbar .container ul li.active + li {
      border-left: none;
    }
    
    #frame li.active:hover:before,
    #frame li.active:hover:after {
      background-color: transparent;
    }

    #frame li.active:before {
      left: calc(var(--r)*-1);
      background-image: radial-gradient(circle at top left, transparent, transparent 67.7%, rgb(var(--PF-color-surface)) 0%);
    }

    #frame li.active:after {
      right: calc(var(--r)*-1);
      background-image: radial-gradient(circle at top right, transparent, transparent 67.7%, rgb(var(--PF-color-surface)) 0%);
    }

    #frame li.active:before,
    #frame li.active:after {
      content: '';
      position: absolute;
      width: calc(var(--r));
      height: calc(var(--r));
      bottom: 0px;
      background-color: transparent;
    }
    
    #frame iframe {
          border: 0;
    width: 100%;
    flex: 1 auto;
    }
    
  </style>
</head>

<body>

<div id="frame">


  <div class="PF-tabs">
    <div class="PF PF-tabbar">
      <div class="container">
        <ul>
          <li class="" data-for="tabs-1" data-taburl="./apps/navegador/resources/new-tab.php" ><span t-dd>🤔 Lorem ipsum</span></li>
          <li class="active" data-for="tabs-2" data-taburl="./apps/navegador/resources/welcome.php" ><span t-dd>🔥 Dolor sit ame</span></li>
          <li class="" data-for="tabs-3"><span t-dd>🏹 Consectetur</span></li>
          <li class="" data-for="tabs-4"><span t-dd>📆 Adipiscing elit</span></li>
        </ul>
      </div>
    </div>
    <div id="navbar" class="PF shadow">
      <div class="PF PF-icon ripple"><i class="material-icons">arrow_back</i></div>
      <div class="PF PF-icon ripple"><i class="material-icons">arrow_forward</i></div>
      <div class="PF PF-icon ripple"><i class="material-icons">refresh</i></div>
      <div class="omni" contenteditable="true"> </div>
      <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
    </div>
    <div class="tabs">
      <div class="tab" data-name="tabs-1"></div>
      <div class="tab" data-name="tabs-2"></div>
      <div class="tab" data-name="tabs-3"></div>
      <div class="tab" data-name="tabs-4"></div>
    </div>
  </div>
  
  </div>

</body>

</html>