<html>

<head>
  <style>
    .search-box * {
      font-family: Calibre;
    }

    .search-box {
      width: 100%;
      flex: 1 auto;
      background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/66955/river-scene.svg"), #cbdeef;
      background-repeat: no-repeat;
      background-position: bottom;
      background-size: cover;
    }

    .search-box .search-wrap--home {
      padding: 1.3em .8em 1em;
      max-width: 590px;
      margin-left: auto;
      margin-right: auto;
      display: block;
      float: none;
      width: 70%;
    }

    .search-box .search {
      box-sizing: border-box;
      border-radius: 6px;
      display: block;
      position: relative;
      height: 50px;
      background-color: #fff;
      border: 1px solid rgba(0, 0, 0, 0.15);
      box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16);
      padding-left: .75em;
      padding-right: 6.5em;
    }

    .search-box .search__input {
      appearance: none;
      font-size: 1.1em;
      font-family: "Proxima Nova", "Helvetica Neue", "Helvetica", "Segoe UI", "Nimbus Sans L", "Liberation Sans", "Open Sans", FreeSans, Arial, sans-serif;
      font-weight: normal;
      color: #333;
      display: block;
      width: 100%;
      background: none;
      outline: none;
      border: none;
      padding: 0;
      height: 2.54545em;
      z-index: 1;
      position: relative;
      top: -1px;
    }

    .search-box .search__button {
      border-radius: 0 2px 2px 0;
      min-width: 26px;
      color: #999;
      font-size: 1.25em;
      padding: 0 .64em;
      height: auto;
      min-height: 1.8em;
      margin-top: -1px;
      margin-bottom: -1px;
      margin-right: -3px;
      line-height: 1.5;
      background-color: transparent;
      background-position: 50% 50%;
      background-repeat: no-repeat;
      -webkit-appearance: none;
      -moz-appearance: none;
      -ms-appearance: none;
      -o-appearance: none;
      appearance: none;
      -moz-box-sizing: content-box;
      -webkit-box-sizing: content-box;
      box-sizing: content-box;
      font-family: 'ddg-serp-icons' !important;
      speak: none;
      font-style: normal;
      font-weight: normal !important;
      font-variant: normal;
      text-transform: none;
      text-decoration: none !important;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      -webkit-tap-highlight-color: transparent;
      width: 1em;
      display: block;
      cursor: pointer;
      background: transparent;
      text-align: center;
      border: none;
      height: 2.45em;
      line-height: 2.45em;
      position: absolute;
      top: 0;
      bottom: 0;
      right: 2px;
      left: auto;
      margin: auto;
      z-index: 2;
      outline: none;
    }

    .search-box .search__button--html {
      background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/66955/loupe.svg");
      background-position: 50% 50%;
      background-repeat: no-repeat;
    }

    .search-box .logo {
      width: 6em;
      height: 6em;
    }

    .search-box .logo-container {
      text-align: center;
    }

    .search-box {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
  </style>
</head>

<body>
  <section class="search-box">
    <div class="logo-container">
      <img src="//img.kaana.io/logo/logo.svg" class="logo">
    </div>

    <div class="search-wrap--home">
      <form name="x" id="search_form_homepage" class="search" action="https://kaana.io/dominio/xyz" method="get">

        <input name="q" autocomplete="off" id="search_form_input_homepage" class="search__input" type="text">

        <input name="b" id="search_button_homepage" class="search__button search__button--html" value="" title="Search" alt="Search" type="submit">

      </form>
    </div>
  </section>
</body>

</html>