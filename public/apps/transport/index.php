<html>
  <head>
    <meta charset="UTF-8">
    <style>
      .transporte_contenedor {
        text-align:center;
      }
      
      .transporte_contenedor .transportes {
    width: auto;
    display: inline-flex;
    align-items: center;
    color: #e34923;
    padding: 0rem;
    background: white;
    border-radius: 2px;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    text-align: center;
      }
			
			.transporte_contenedor .transporte {
				margin:auto;
			}
      
      .transporte_contenedor .transporte i {
    padding: 0.5rem;
    margin: auto;
        transition:0.5s;
        margin: 0.5rem;
        cursor:pointer;
      }
      
      .transporte_contenedor .transporte:hover i {
        border-radius:100%;
        color:white;
        background:#e34923;
      }
      
      .transporte_contenedor .cards_contenedor {
    max-width:22rem;
    text-align: center;
    padding: 0.5rem;
    width: calc(100% - 1rem);
        margin:auto;  
      }
      
      .transporte_contenedor .cards_contenedor .card {
    width: calc(100% - 2rem);
    background: #e34923;
    border-radius: 2px;
    padding: 0.5rem;
    color: white;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    text-align: left;
    display: inline-block;
    margin: 0.5rem;
	  transition:0.5s;
    user-select: none;
	  cursor:pointer;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .grupo_informacion {
        padding:0rem;
        display:inline-flex;
        width:100%;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .L {
    border-radius: 25px;
    background: rgba(255, 255, 255, 0.25);
    color: white;
    padding: 0.5rem 1rem;
    margin: 0.5rem;
    display: inline-block;
    font-size: 1.5rem;
    border: solid rgba(255, 255, 255, 0.3) 0.5rem;
    border-top: none;
    border-bottom: none;
      }
      
      .transporte_contenedor .card .informacion_rapida .parada_actual {
    margin: 0.5rem;
    display: inline-block;
        background: transparent;
    color: white;
    font-size: 1rem;
    padding: 0;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .proximo {
        padding:0.5rem;
        text-align:right;
        width:100%;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .proximo .actual {
        font-size:2rem;
        display:block!important;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias {
    max-width: calc(100% - 1rem);
    background: rgb(255, 255, 255);
    border-radius: 30px;
    display: inline-flex;
    padding: 0;
    align-items:center;
    color:black;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias .transbordo {
    padding: 0.5rem;
    border-radius: 30px;
    background: #e34923;
    color: white;
    font-size: 1rem;
    margin: 0.2rem;
    margin-right:0.5rem;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias .correspondencia {
    border-radius: 30px;
    background: #e34923;
    color: white;
    padding: 0.5rem 0.8rem;
    margin: 0rem 0.5rem;
    display: inline-block;
    font-size: 0.8rem;
    border: solid rgba(0, 0, 0, 0.15) 0.5rem;
    border-top: none;
    border-bottom: none;
    margin-left:0;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias span {
        margin-left:0.5rem;
      }
      
      .transporte_contenedor .cards_contenedor .card .informacion_rapida .correspondencias .correspondencia.LI {
        padding: 0.3rem 0.5rem;
      }
			
			.transporte_contenedor .cards_contenedor .card .info {
				display:none;
			}
			
			.transporte_contenedor .cards_contenedor .card.visible .info {
				display:block;
			}
      
      .transporte_contenedor .cards_contenedor .card .paradas {
        padding:0.5rem;
      }
      
      .transporte_contenedor .cards_contenedor .card .paradas li {
        padding:0.5rem;
        font-size:1rem;
        transition:0.5s;
        width:100%;
        display:inline-flex;
        width:calc(100% - 1rem);
        border-radius: 30px;
      }
      
      .transporte_contenedor .cards_contenedor .card .paradas li:hover {
        box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background: white;
    color: #9c27b0;
    border-radius: 30px;
    padding-left: 1rem;
        width:calc(100% - 2.5rem;);
      }
			
			@media screen and (max-width: 450px) { 
				.transporte_contenedor .transportes {
					width:100%;
				}
				
				.transporte_contenedor .cards_contenedor {
					max-width:100%;
				}
				
			}
    </style>
  </head>
  <body>
    <div class="transporte_contenedor" >
      <div class="transportes" >
        <div class="transporte" ><i class="material-icons">&#xE533;</i></div>
        <div class="transporte" ><i class="material-icons">&#xE56F;</i></div>
        <div class="transporte" ><i class="material-icons">&#xE530;</i></div>
        <div class="transporte" ><i class="material-icons">&#xE559;</i></div>
        <div class="transporte" ><i class="material-icons">&#xE532;</i></div>
        <div class="transporte" ><i class="material-icons">&#xE539;</i></div>
      </div>
      
      <div class="cards_contenedor" >
        
        
        <?php
        $linea_transporte = mysqli_query($con,"SELECT * FROM lineas_transporte WHERE ciudad LIKE '%Barcelona%' ORDER BY id ASC");
	        while($row_linea_transporte = mysqli_fetch_array($linea_transporte))
          { 
	            $id_linea_transporte = $row_linea_transporte["id"];
              $color_linea_transporte = $row_linea_transporte["color"];
              $paradas_1_linea_transporte = $row_linea_transporte["paradas_1"];
              $paradas_2_linea_transporte = $row_linea_transporte["paradas_2"];
              $name_linea_transporte = $row_linea_transporte["name"];
    ?>
        
      <div class="card" style="background:<?=$color_linea_transporte?>;" onclick="$(this).toggleClass('visible');" >
        <div class="informacion_rapida" >
          <div class="grupo_informacion" >
            <div>
              
              <span class="L" ><?=$name_linea_transporte?></span>
              <span class="parada_actual" >Sant Andreu</span>
            </div>
            <ul class="proximo" >
              <li class="actual" >1 min</li>
              <li>8 min</li>
              <li>16 min</li>
            </ul>
          </div>
          <div class="correspondencias" >
            <i class="material-icons transbordo" style="background:<?=$color_linea_transporte?>;" >&#xE572;</i>
            <?php 
              $correspondencias_linea_transporte = mysqli_query($con,"SELECT * FROM lineas_transporte WHERE paradas_1 LIKE '%Passeig de Grácia%' ORDER BY id ASC");
	        while($row_correspondencias_linea_transporte = mysqli_fetch_array($correspondencias_linea_transporte))
          { 
	            $id_correspondencias_linea_transporte = $row_correspondencias_linea_transporte["id"];
              $color_correspondencias_linea_transporte = $row_correspondencias_linea_transporte["color"];
              $paradas_1_correspondencias_linea_transporte = $row_correspondencias_linea_transporte["paradas_1"];
              $paradas_2_correspondencias_linea_transporte = $row_correspondencias_linea_transporte["paradas_2"];
              $name_correspondencias_linea_transporte = $row_correspondencias_linea_transporte["name"];
            if($id_correspondencias_linea_transporte != $id_linea_transporte){
            ?>
            <div class="correspondencia LI" style="background:<?=$color_correspondencias_linea_transporte?>;" ><?=$name_correspondencias_linea_transporte?></div>
            <?} }?>
          </div>
        </div>
        <div class="info" >
          <ul class="paradas" >
            <?php
              $pieces = explode(', ', $paradas_1_linea_transporte);
              foreach($pieces as $element){
              echo '<li>' . $element . '</li>';
              }?>
          </ul>
        </div>
      </div>
        
        
        <?}?>
        
        
        
        
    </div>
    </div>
  </body>
</html>