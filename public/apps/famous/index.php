<html>

<head>
	<style>
	
		.famous-container {
			max-width: calc(500px + 2rem);
			margin: auto;
		}
		
	</style>
</head>

<body>

	<div class="famous-container">
		<div class="PF PF-content full noborder center">
			<div class="container">
				<div class="PF PF-image transparent" style="background-image:url('./apps/famous/famous.svg'); background-size: contain; min-height: 6em;"></div>
				<h1 t-dd>Search for a known person</h1>
				<p t-dd>Search for an actor, philosopher, writer, singer, etc to see more</p>
			</div>
		</div>

	</div>

</body>

</html>