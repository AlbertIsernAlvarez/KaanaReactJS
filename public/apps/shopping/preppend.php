<?php
$section = $_GET['section'];
$department = $_GET['department'];
$product = $_GET['product'];
$store = $_GET['store'];
$tags = $_GET['tags'];

if($product){
	$product_select = mysqli_query($con,"SELECT * FROM shopping_products WHERE id='$product' ORDER BY id DESC LIMIT 1");
	while($row_product_products = mysqli_fetch_array($product_select))
	{
		$product_id = $row_product_products['id'];
		$product_user_id = $row_product_products['user_id'];
		$product_name = ucwords($row_product_products['name']);
		$product_description = $row_product_products['description'];
		$store = $row_product_products['store'];
		$department = ucwords($row_product_products['department']);
		$product_topic = $row_product_products['topic'];
		$product_image = $row_product_products['image'];
		if($product_image){
			$product_image = "//kaana.io/apps/photos/see.php?id=" . $product_image_products;
			
		} else {
			$product_image = "//kaana.io/apps/shopping/imgs/product.svg";
			$product_image_contain_products = "contain";
		}
	}
}

if($store){
	$store_select = mysqli_query($con,"SELECT * FROM shopping_stores WHERE id='$store' ORDER BY id ASC");
	while($row_store = mysqli_fetch_array($store_select))
  {
  	$store_id = $row_store['id'];
  	$store_name = ucwords($row_store['name']);
  	$store_slogan = $row_store['slogan'];
  	$store_color = $row_store['color'];
  	$store_owners = $row_store['owners'];
  	if($store_avatar){
			$store_avatar = "//kaana.io/apps/photos/see.php?id=" . $store_avatar_stores;
			
		} else {
			$store_avatar = "//kaana.io/apps/shopping/imgs/store.svg";
			$store_avatar_contain_stores = "contain";
		}
  }
}

if($department){
	$department_select = mysqli_query($con,"SELECT * FROM shopping_departments WHERE id='$department' ORDER BY id ASC");
	while($row_department = mysqli_fetch_array($department_select))
  {
  	$department_id = $row_department['id'];
  	$department_name = $row_department['name'];
  	$department_name = ucwords($department_name);
  	$department_descripcion = $row_department['description'];
  	$department_color = $row_department['color'];
  }
}

?>