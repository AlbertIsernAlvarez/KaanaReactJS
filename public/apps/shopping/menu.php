<li opendd-href="?app=shopping"><i class="material-icons">home</i><span t-dd>Home</span></li>
<li opendd-href="?app=shopping&section=departments"><i class="material-icons">border_all</i><span t-dd>Departments</span></li>
<li opendd-href="?app=shopping&section=stores"><i class="material-icons">store</i><span t-dd>Stores</span></li>
<li opendd-href="?app=shopping&section=saved"><i class="material-icons">collections_bookmark</i><span t-dd>Saved items</span></li>
<li opendd-href="?app=shopping&section=orders"><i class="material-icons" opendd-href="?app=shoppingsection=orders">inbox</i><span t-dd>Orders</span></li>
<li opendd-href="?app=shopping&section=cart"><i class="material-icons">shopping_cart</i><span t-dd>Cart</span></li>