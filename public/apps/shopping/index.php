<html>

<head>
  <style>
  .header .buscador  {
    opacity: 0;
    pointer-events: none;
  }

  .shopping_container {
			width: 100%;
		}
		
		.shopping_container .shopping_top {
			width: 100%;
			background-position: 50% 10px;
			background-repeat: no-repeat;
			margin-top: 5em;
			margin-bottom: 3.5em;
		}
		
		.shopping_container .shopping_top h1 {
			font-size: 2.8rem;
			padding: 0;
			font-weight: 500;
			text-align: center;
		}
		
		.shopping_container .shopping_top p {
			font-size: 20px;
			padding: 0;
			font-weight: 400;
			text-align: center;
		}
		
		.shopping_container .shopping_top .shopping_logo {
			width: fit-content;
			height: fit-content;
			margin: auto;
			margin-bottom: 1em;
			padding: .5em .8em;
			background-color: rgba(var(--PF-color-surface));
			border-radius: 28px;
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .26);
			box-sizing: border-box;
			display: flex;
		}
		
		.shopping_container .content {
			max-width: 60rem;
			width: 100%;
			margin: auto;
		}
		
		.shopping_container .content .PF-card {
			flex-basis: calc(100%/3 - 1rem);
		}
		
		.shopping_container .content .PF-grid.selector .PF-card .PF-image {
			padding-bottom: 70%;
		}
		
		.shopping_container .content .PF-grid .PF-card .info p {
			-webkit-line-clamp: initial;
		}

		.PF-grid.selector.departments .PF-card .info {
			padding-top: 40%;
		}

		.shoppingAddCircle {
    background: white;
    border-radius: 10em;
    position: absolute;
    top: -1.5em;
    left: 0;
    right: 0;
    margin: auto;
    box-shadow: 0px 1px 6px 1px rgba(0, 0, 0, 0.1);
    text-align: center;
    display: flex;
    align-items: center;
    width: fit-content;
    height: fit-content;
    padding: .7em;
    font-weight: 900;
    cursor: pointer;
		}
  </style>
</head>

<body>


      <div class="PF-tabs shopping_container">
        <div class="tabs">
          <div class="tab" data-name="tabsshopping-home"></div>
          <div class="tab" data-name="tabsshopping-categories"></div>
          <div class="tab" data-name="tabsshopping-add"></div>
          <div class="tab" data-name="tabsshopping-saved"></div>
          <div class="tab" data-name="tabsshopping-cart"></div>
        </div>

        <div class="PF PF-tabbar bottom shadow">
          <div class="container center">
            <ul id="bottombar">
              <li class="ripple active" data-for="tabsshopping-home" data-taburl="./apps/<?=$app?>/pages/home/index.php?<?=$server_querystring?>" preload>
                <i class="material-icons">home</i>
                <span t-dd>Home</span>
              </li>

              <li class="ripple" data-for="tabsshopping-categories" data-taburl="./apps/<?=$app?>/pages/categories/index.php?<?=$server_querystring?>" preload>
                <i class="material-icons">category</i>
                <span t-dd>Categories</span>
              </li>

              <li class="ripple" data-for="tabsshopping-add" data-taburl="./apps/<?=$app?>/pages/add-product/index.php?<?=$server_querystring?>">
                <i class="material-icons">add</i>
                <span t-dd>Add Product</span>
              </li>

              <li class="ripple" data-for="tabsshopping-saved" data-taburl="./pantallas/saved/index.php?<?=$server_querystring?>">
                <i class="material-icons">collections_bookmark</i>
                <span t-dd>Saved</span>
              </li>

              <li class="ripple" data-for="tabsshopping-cart" data-taburl="./apps/<?=$app?>/pages/cart/index.php?<?=$server_querystring?>">
                <i class="material-icons">shopping_cart</i>
                <span t-dd>Cart</span>
              </li>
            </ul>
            <div class="slider"></div>
          </div>
        </div>
      </div>

      <script>
      	<?php if($section){?> setTimeout(function(){ $("[data-for='tabsshopping-<?=$section?>']").click(); }, 600); <?}?>
      </script>

</body>

</html>