<div class="content">
	<div class="shopping_top">
		<div>
			<h1><span t-dd>Let's go shopping</span><?php if($user_mismo_name){?>, <?=$user_mismo_name?><?}?></h1>
		</div>
	</div>
	<div class="PF PF-grid selector departments" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); ">
		<?php include(__DIR__.'/../../resources/departments.php'); ?>
	</div>
</div>