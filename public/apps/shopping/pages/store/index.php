<html>
	<head>
		<style>
			.store_page .store-info {
			width: 100%;
			padding: .5em;
			text-align: center;
			background-color: rgb(var(--PF-color-background));
			z-index: 5;
			position: relative;
		}
		
		.store_page .store-info .PF-image {
			width: 5em;
			height: 5em;
			margin: auto;
		}

		.store_page .products {
			max-width: 60em;
    margin: 0 auto;
		}
		</style>
	</head>
<body>
<?php
if($store_id){?>
<div class="store_page" data-storeid="<?=$store_id?>">
<div class="PF store-info">
  <div class="container">
    <div class="PF-image circle ripple <?=$store_avatar?>" style="background-image:url('<?=$store_avatar?>');" rgdd></div>
    <?php if($store_name){?><h1 data-store="name"><?=$store_name?></h1><?}?>
    <?php if($store_description){?><p data-store="description"><?=$store_description?></p><?}?>
  </div>
</div>


<div class="PF-tabs">
			<div class="PF PF-tabbar shadow">
				<div class="container">
					<ul>
						<li class="ripple active" data-for="storetabs-popular" data-taburl="./apps/shopping/pages/store/popular-products.php?<?=$server_querystring?>"><span t-dd>Popular Products</span></li>
						<li class="ripple" data-for="storetabs-about" data-taburl="./apps/shopping/pages/store/reviews.php?<?=$server_querystring?>"><span t-dd>Reviews</span></li>
						<?php if($store_user_id === $user_mismo_id){?><li class="ripple" data-for="storetabs-edit" data-taburl="./apps/shopping/pantallas/store/edit.php?<?=$server_querystring?>"><span t-dd>Edit</span></li><?}?>
					</ul>
					<div class="slider"></div>
				</div>
			</div>
			<div class="tabs">
				<div class="tab" data-name="storetabs-popular"></div>
				<div class="tab" data-name="storetabs-reviews"></div>
				<div class="tab" data-name="storetabs-about"></div>
				<?php if($store_user_id === $user_mismo_id){?><div class="tab" data-name="storetabs-edit"></div><?}?>
			</div>
		</div>
</div>
<?}?>
</body></html>