<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Edit product</h1>
			<p><span t-dd>Information to edit your product.</span></p>
		</div>
	</header>
	<div class="grid-info">

		<div class="item ripple" onclick="windowdd('./apps/shopping/pantallas/product/edit/avatar.php?<?=$server_querystring?>');">
			<div class="left">
				<span t-dd>Photo</span>
			</div>
			<div class="right">
				<div class="double">
					<p t-dd>A photo helps personalize your product</p>
				</div>
				<div class="PF-avatar circle" rgdd></div>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./apps/shopping/pantallas/product/edit/name.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Product name</span>
			</div>
			<div class="right">
				<p data-product="name" ><?=replaceemojis($product_name)?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./apps/shopping/pantallas/product/edit/description.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Description</span>
			</div>
			<div class="right">
				<p data-product="description"><?=replaceemojis($product_description)?></p>
			</div>
		</div>
		
		<div class="item ripple" onclick="windowdd('./apps/shopping/pantallas/product/edit/location.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Location</span>
			</div>
			<div class="right">
				<p data-product="location"><?=$product_location?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./apps/shopping/pantallas/product/edit/department.php?<?=$server_querystring?>', 'fit');">
			<div class="left">
				<span t-dd>Department</span>
			</div>
			<div class="right">
				<p data-product="topic"><?=$product_department?></p>
			</div>
		</div>

	</div>
</div>