<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Edit name</h1>
</div>
<form class="PF PF-form" id="formaccount-update-name" action="./apps/shopping/pantallas/product/edit/save/name.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<input type="text" placeholder=" " name="name" value="<?=$product_name?>">
		<span t-dd>name</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>
	$("#formaccount-update-name").ajaxForm({
		beforeSubmit: function(arr, $form, options) {
			$('.header .PF-progress.loading').show();
			alertdd.show('Saving name');
		},
		success: function(data) {
			$('.header .PF-progress.loading').hide();
			eval(data);
		}
	});
</script>