<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Edit description</h1>
</div>
<form class="PF PF-form" id="formaccount-update-description" action="./apps/shopping/pantallas/product/edit/save/description.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<input type="text" placeholder=" " name="description" value="<?=$product_description?>">
		<span t-dd>Description</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>
	$("#formaccount-update-description").ajaxForm({
		beforeSubmit: function(arr, $form, options) {
			$('.header .PF-progress.loading').show();
			alertdd.show('Saving description');
		},
		success: function(data) {
			$('.header .PF-progress.loading').hide();
			eval(data);
		}
	});
</script>