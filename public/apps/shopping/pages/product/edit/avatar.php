<html>

<head>
  <style>
    .PF-window .container.productimagebgcontainer {
      position: relative;
      overflow: hidden;
      background-color: var(--PF-color-bg-first-default);
    }

    .productimage-upload {
      position: relative;
      max-width: 10rem;
      margin: auto;
      text-align: center;
    }

    .productimage-upload .productimage-edit {
    position: absolute;
    right: 0.5rem;
    z-index: 1;
    bottom: 0.5rem;
    border-radius: 2em;
    background: rgb(var(--PF-color-primary));
    color: rgb(var(--PF-color-surface));
    padding: 0.5rem;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 5px 0 rgba(0, 0, 0, .23);
    transition: 0.5s;
    cursor: pointer;
    }

    .productimage-upload .productimage-edit i {
      cursor: pointer;
    }

    .productimage-upload .productimage-edit:hover {
      box-shadow: 0 10px 10px 0 rgba(0, 0, 0, .19), 0 6px 3px 0 rgba(0, 0, 0, .23);
    }

    .productimage-upload .productimage-edit input {
      display: none;
    }

    .productimage-upload .productimage-edit input+label {
      display: inline-block;
      width: 2rem;
      height: 2rem;
      margin-bottom: 0;
      border-radius: 100%;
      cursor: pointer;
      transition: 0.5s;
    }

    .productimage-upload .productimage-edit input+label {
      text-align: center;
      margin: auto;
      display: inline-flex;
      align-items: center;
      text-align: center;
    }

    .productimage-upload .productimage-edit input+label i {
      margin: auto;
    }

    .productimage-upload .productimage-preview {
    width: 10rem;
    height: 10rem;
    position: relative;
    border-radius: 5em;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    overflow: hidden;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }
  </style>
</head>

<body>
  <div class="PF PF-toolbar">
    <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
    <h1 t-dd>Change product image</h1>
  </div>

  <div class="content">
    <div class="container full middle productimagebgcontainer">
      <div class="productimage-upload">
        <div class="productimage-edit">
          <form id="subirproductimage" method="post" enctype="multipart/form-data" action="./resources/actualizar/productimage.php">
            <input name="files[]" accept="image/*;capture=camera" type="file" id="imageUpload">
          </form>
          <label for="imageUpload"><i class="material-icons">&#xE2C3;</i></label>
        </div>
        <div class="productimage-preview ddbg-productimage imagePreview" id="imagePreview"></div>
      </div>
    </div>
    <div class="container">
      <div class="PF PF-buttons full">
        <button class="PF PF-button" onclick="$('#subirproductimage').submit();" t-dd>
          <div class="inside">
            <p t-dd>Upload</p>
          </div>
        </button>
      </div>
    </div>
  </div>

  <script>
    
    
    $("#subirproductimage").ajaxForm(function(data) {
      $('#ddwindow').removeClass('open');
      $('.closewindowdd').removeClass('show');
      $('body').removeClass('overflowhidden');
      if(data){
        $("#ddbg-productimage").load('./resources/ddbg-productimage.php');
        $(".ddbg-productimage").css("background-image", "");
        $(".ddbg-productimage").attr("data-bg", "");
        alertdd.show('product image updated correctly');
      } else {
        alertdd.show('The product image cannot be updated');
      }
    });
    
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.imagePreview').css('background-image', 'url(' + e.target.result + ')');
          $('.imagePreview').hide();
          $('.imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imageUpload").change(function() {
        readURL(this);
      }

    );
  </script>
</body>

</html>