<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Add product</h1>
			<p><span t-dd>Information to edit your product.</span></p>
		</div>
	</header>
	<div class="content">
		<form class="PF PF-form">

			<label class="PF-textfield filled">
				<input placeholder=" " type="text">
				<span>Title</span>
			</label>

			<label class="PF-textfield filled">
				<textarea type="text" placeholder=" " name="state"></textarea>
				<span t-dd>Description</span>
			</label>

			<label class="PF-textfield filled">
				<input placeholder=" " type="text">
				<span>Brand</span>
			</label>

			<label class="PF-textfield filled">
				<input placeholder=" " type="number">
				<span>Price</span>
			</label>

			<label class="PF-textfield filled">
				<input placeholder=" " type="number">
				<span>Stock</span>
			</label>

			<label class="PF-textfield filled">
				<input placeholder=" " type="text">
				<span>Tags</span>
			</label>


			<button class="PF PF-button ripple">Create product</button>

		</form>
	</div>
</div>