<?php

$select_products = "id IS NOT NULL";

if($department){
  $select_products .= " AND department='$department' ";
}

if($store){
  $select_products .= " AND store='$store' ";
}

if($tags){
  $select_products .= " AND (LOWER(tags) LIKE LOWER('$tags%') OR LOWER(name) LIKE LOWER('$tags%')) ";
}

$product = mysqli_query($con,"SELECT * FROM shopping_products WHERE $select_products ORDER BY id DESC");
while($row_product_products = mysqli_fetch_array($product))
  {
$id_products = $row_product_products['id'];
$product_name_products = $row_product_products['name'];
$product_name_products = ucwords($product_name_products);
$descripcion_products = $row_product_products['description'];
$product_image_products = $row_product_products['image'];
if($product_image_products){
	$product_image_products = "//kaana.io/apps/photos/see.php?id=" . $product_image_products;
} else {
	$product_image_products = "//kaana.io/apps/shopping/imgs/product.svg";
	$product_image_contain_products = "contain";
}
?>

<div class="PF PF-card product" data-productid="<?=$id_products?>" opendd-href="?app=shopping&sp=product&product=<?=$id_products?>">
    <div class="PF PF-image ripple <?=$product_image_contain_products?>" style="background-image:url('<?=$product_image_products?>');" rgdd ></div>
    <div class="info">
      <?php if($product_name_products != ''){?><h1><?=$product_name_products?></h1><?}?>
      <?php if($store_name != ''){?><p><?=$store_name?></p><?}?>
    </div>
  </div>

<?}?>