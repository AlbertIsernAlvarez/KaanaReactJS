<html>

<head>
  <style>
    .homepage-container {
      max-width: 35em;
      margin: auto;
      padding-top: 1em;
      padding-bottom: 1em;
    }

    .homepage-container>.add {
      position: fixed;
      left: 0;
      right: 0;
      bottom: 2.5em;
      background: white;
      border-radius: 10em;
      padding: 1em;
      margin: auto;
      z-index: 5;
    }
  </style>
</head>

<body>

  <div class="homepage-container">
    <div class="PF PF-content full noborder center">
      <div class="container">
        <div class="PF PF-image transparent" style="background-image:url('./apps/home/assets/images/start.svg');"></div>
        <h1 t-dd>Home</h1>
        <p t-dd>Your home is empty, start by linking your smart devices to your Kaana account.</p>
      </div>
    </div>

    <div class="add PF PF-icon shadow sh64 ripple"><i class="material-icons">add</i></div>

  </div>

</body>

</html>