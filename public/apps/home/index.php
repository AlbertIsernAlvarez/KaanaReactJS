<html>

<head>
  <style>
  .home_container.PF-tabs>.tabs>.tab {
  	overflow: visible;
  }
  </style>
</head>

<body>
	
  <div class="PF-tabs home_container">
    <div class="tabs">
      <div class="tab" data-name="tabshome-home"></div>
      <div class="tab" data-name="tabshome-hub"></div>
    </div>

    <div class="PF PF-tabbar bottom shadow sh64">
      <div class="container center">
        <ul id="bottombar">
          <li class="ripple active" data-for="tabshome-home" data-taburl="./apps/home/pages/home/index.php?<?=$server_querystring?>">
            <i class="material-icons">home</i>
            <span t-dd>Home</span>
          </li>

          <li class="ripple" data-for="tabshome-hub" data-taburl="./apps/home/pages/hub/index.php?<?=$server_querystring?>">
            <i class="material-icons">assistant</i>
            <span t-dd>Hub</span>
          </li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
  </div>

</body>

</html>