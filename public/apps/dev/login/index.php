<html>

<head>
  <style>
    body {
      overflow: auto;
      background-color:rgb(var(--PF-color-surface));
      background-image: linear-gradient(180deg, hsla(0, 0%, 100%, 0) 60%, rgb(var(--PF-color-surface))), linear-gradient(70deg, rgb(var(--PF-color-primary), .2) 32%, rgb(var(--PF-color-surface)));
      /* background-image: url(./dumdarac/bg.svg); */
      background-size: cover;
      background-position: center;
      background-attachment: fixed;
    }

    .header {
      display: none;
    }

    .login_container {
    background: rgb(var(--PF-color-surface));
    max-width: 80%;
    margin: auto;
    margin-right: 0;
    width: 100%;
    min-height: fit-content;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
      position:relative;
    }
    
    .login_container .PF-progress.linear.loading {
      display:none;
    }

    .box_container {
      max-width: 450px;
      margin: auto;
      position: relative;
      display: flex;
      flex-direction: column;
      margin-left: 1rem;
      overflow:hidden;
    }

    .box_container .box {
    width: 100%;
    -webkit-flex-shrink: 0;
    flex-shrink: 0;
    display: block;
    margin: 0 auto;
    min-height: 500px;
    flex: 1 auto;
    text-align: left;
    }

    .box_container .box .content {
      height: auto;
      padding: 48px 40px 0.5rem;
      transition: 0.5s;
      display:none;
    }

    .box_container .box .content.nopadding {
      margin: 0 -40px;
    }

    .box_container .box .content.contentleft {
      transform: translateX(-100%);
    }

    .box_container .box .content.contentright {
      transform: translateX(100%);
    }

    .box_container .box .content.hided {
      display: none;
    }

    .box_container .box .content .logo_container {
      width: 100%;
    }

    .box_container .box .content .logo_container .logo {
      margin: 0;
      overflow: visible;
      position: relative;
      width: 50px;
      height: 50px;
      background-repeat: no-repeat;
      background-size: contain;
      background-position: center;
      background-image: url(//kaana.io/imgs/logo/logo.svg);
    }

    .box_container .box .content .presentation {}

    .box_container .box .content .presentation h1 {
      padding-bottom: 0;
      padding-top: 16px;
      font-size: 30px;
      line-height: 1.3333;
      margin-bottom: 0;
      margin-top: 0;
    }

    .box_container .box .content .presentation>p {
      font-size: 16px;
      line-height: 1.5;
      padding-bottom: 0;
      padding-top: 8px;
      font-weight: 500;
      opacity: 0.7;
    }

    .box_container .box .content .block {
      display: inline-block;
      font-size: 14px;
      padding: 24px 0 0;
      -webkit-transblock: translateZ(0);
      transblock: translateZ(0);
      vertical-align: top;
      white-space: normal;
      width: 100%;
    }

    .box_container .box .content .block .PF-textfield {
      width: 100%;
    }

    .box_container .box .content .block>p {
      color: #5f6368;
      font-size: 14px;
      line-height: 1.4286;
      padding-bottom: 3px;
      padding-top: 9px;
    }

    .box_container .box .content .block a {
      -webkit-border-radius: 4px;
      border-radius: 4px;
      color: var(--PF-color-original-default);
      display: inline-block;
      font-weight: 500;
      letter-spacing: .25px;
      background-color: text;
      border: 0;
      cursor: pointer;
      font-size: inherit;
      outline: 0;
      padding: 0;
      text-align: left;
      padding-bottom: 3px;
      padding-top: 9px;
      color: rgb(var(--PF-color-primary));
    }

    @media (max-width: 30em) {
      .login_container {
        max-width: 100%;
        width: 100%;
      }
      .box_container {
        border-radius: 0;
      }
      .box_container .box {
        border-radius: 0;
        box-shadow: none;
      }
    }
  </style>
</head>

<body>
  <div class="login_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="testform" method="post">
          <div class= "content step">
            <div class="logo_container">
              <div class="logo" title="Kaana" alt="DumDarac"></div>
            </div>
            <div class="presentation">
              <h1 t-dd >Hello</h1>
              <p t-dd >with your Kaana Account</p>
            </div>
            <div class="block">
              <label class="PF-textfield filled">
                <input placeholder=" " type="text" name="">
                <span>Kaana ID or Email</span>
              </label>
              <a opendd-href="?p=recover&type=email" >Forgot email?</a>
            </div>
            <div class="block">
              <p>Not your computer? Use Guest mode to sign in privately. <a>Learn more</a></p>
            </div>
            <div class="block">
              <div class="PF PF-buttons flex full">
                <div class="PF-button text" opendd-href="?p=register" t-dd>Create account</div>
                <div class="space"></div>
                <button type="button" class="PF-button next" t-dd>Next</button>
              </div>
            </div>
          </div>

          <div class= "content step">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1>Hi Albert</h1>
              <p>with your Kaana Account</p>
            </div>
            <div class="block">

              <label class="PF-textfield filled">
                <input placeholder=" " type="password">
                <span>Password</span>
              </label>
            </div>
            <div class="block">
              <p>Not your computer? Use Guest mode to sign in privately. <a>Learn more</a></p>
            </div>
            <div class="block">
              <div class="PF PF-buttons flex full">
                <div class="PF-button text" opendd-href="?p=recover&type=password"  t-dd>Forgot password?</div>
                <div class="space" ></div>
                <button type="button" class="PF-button next" t-dd>Next</button>
              </div>
            </div>
          </div>

          <div class= "content step">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1>Welcome <b></b></h1>
              <p>with your Kaana Account</p>
            </div>
            <div class="block">
              <label class="PF-textfield filled">
                <input placeholder=" " type="text">
                <span>Kaana ID or Email</span>
              </label>
              <a>Forgot email?</a>
            </div>
            <div class="block">
              <p>Not your computer? Use Guest mode to sign in privately. <a>Learn more</a></p>
            </div>
            <div class="block">
              <div class="PF PF-buttons flex full">
                <button type="button" class="PF-button text back" t-dd>Previous</button>
                <div class="space" ></div>
                <button type="submit" class="PF-button submit" t-dd>Create account</button>
              </div>
            </div>
          </div>
          
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="Kaana"></div>
            </div>
            <div class="presentation">
              <h1>Hi Albert</h1>
              <p>Logging in... In a few moments you will be redirected.</p>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>