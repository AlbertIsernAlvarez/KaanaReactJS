<html>

<head>
  <style>
    
    body {
      background:#eeeeee;
    }
    
    .headerbajo {display:none;}
    
    .section {
      display: none;
    }
    
    .document * {
      transition:0.5s;
    }

    .document #card {
    text-align: left;
    display: flex;
    flex-direction: column;
    width: 100%;
    height: calc(100% - 64px);
    }

    .document #card .documentbar {
    display: inline-flex;
    align-items: center;
    width: calc(100% - 2rem);
    background: white;
    padding: 0.5rem 1rem;
      box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
      position:relative;

    }
    
    .document #card .documentbar .selector {
    display: inline-block;
    background: white;
    transition: 0.5s;
    position: relative;
    width: 3.5rem;
    height: 3.5rem;
    }
    
    .document #card .documentbar .selector-content {
    position: absolute;
    }
    
    .document #card .documentbar .selector-content:hover {
      box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);

      border-radius: 0.5em;
      background: white;
    }
    
    .document #card .documentbar .selector-content i {
      display:none;
    }
    
    .document #card .documentbar .selector-content:hover i {
      display:block;
    }
    
    .document #card .documentbar .selector-content i:first-child {
      display:block;
    }

    .document hr {
      border: none;
      height: 1px;
      background-color: #E1E1E1;
      margin-left: -25px;
      margin-right: -25px;
      margin-top: 20px;
      margin-bottom: 20px;
    }

    .document p {
      line-height: 1.75;
    }

    .document .doctitle {
      width: 100%;
      border:none;
      font-size:1.2rem;
      max-width:20rem;
      padding:0.5rem 1rem;
      border:solid 2px transparent;
      margin-right:0.5rem;
      
    }
    
    .document .doctitle:hover, .document .doctitle:focus {
      border:solid 2px #eee;
      border-radius:2em;
    }

    .document #icons {
    position: relative;
    display: inline-flex;
    white-space: nowrap;
    align-items: center;
    }

    .document i {
    padding: 0.5rem;
    border-radius: 50%;
    -webkit-transition: background-color 0.25s ease-in-out;
    transition: background-color 0.25s ease-in-out;
    cursor: pointer;
    margin: 0.5rem;
    }

    .document i:hover {
      background-color: rgba(33, 33, 33, 0.125);
    }

    .document #card a {
      color: #737373;
    }
    
    .document .document-container {
      flex:1;
    width: 100%;
    height: 100%;
    overflow: auto;
      padding-top:0.5rem;
      padding-bottom:0.5rem;
    }
    
    .document .document-container .document-content {
    max-width: 42.8125rem;
    width: 100%;
    background: white;
    min-height: 59.75rem;
    margin: auto;
    border-radius: 0.5em;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
      margin-bottom:0.5rem;
      overflow:hidden;
      padding:2.5cm;
    }
  </style>
</head>

<body>
  <div class="document">

    <div id="card">
      <div class="documentbar">
        <input class="doctitle" value="Title" maxlength="40" placeholder="Titulo" />
        <div id="icons">
          <div class="selector" >
            <div class="selector-content" >
              <i class="material-icons">&#xE236;</i>
              <i class="material-icons">&#xE234;</i>
              <i class="material-icons">&#xE237;</i>
              <i class="material-icons">&#xE235;</i>
            </div>
          </div>
          <i class="material-icons">&#xE245;</i>
          <i class="material-icons" id="color">color_lens</i>
          <i class="material-icons" id="pin">vertical_align_top</i>
          <i class="material-icons" id="invert">invert_colors</i>
          <i class="material-icons" id="font">font_download</i>
          <i class="material-icons">more_vert</i>
        </div>
      </div>
      <div class="document-container" >
        <div class="document-content" contenteditable="true" ></div>
      </div>
    </div>
  </div>
  
  <script>
    
    $('.header').addClass('headerblanco');
    
    theme = "light";
$("#invert").click(function(){
	if (theme == "light") {
		$("body").css("background-color", "#212121");
		$("#card, #card .documentbar, .header").css("background-color", "#2C2C2C");
		$("#card, input, #input i, a, .header *").css("color", "#F5F5F5");
		$("hr").css("background-color", "#424242");
		theme = "dark";
	} else {
		$("body").css("background-color", "#eeeeee");
		$("#card, .documentbar, .header").css("background-color", "white");
		$("#card, input, #input i, a, .header *").css("color", "rgba(0, 0, 0, 0.6)");
		$("hr").css("background-color", "#E1E1E1");
		theme = "light";
	}
});

$("#font").click(function(){
	$("#input").toggle();
	$("input").focus();
});

$("#input i").click(function(){
	var response = $.ajax({
url: "https://fonts.googleapis.com/css?family=" + $("input").val().replace(" ","+"),
		type: "HEAD",
		async: false
	}).status;
	if (response == 200) {
		$("head").append("<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=" + $("input").val().replace(" ","+") + "'>");
    	$("body, input").css("font-family", $("input").val());
		$("#input").hide();
		$("input").val("");
	} else {
		alert("Something went wrong. You should probably check the font name you entered. Or your internet connection.");
	}
});

$("#input").on("keyup",function(e){
    if (e.keyCode == 13) {
        $("#input i").click();
    }
});

$(".headerbajo").click(function(e){
	if (e.target == this) {
		$(".headerbajo").css("background-color", randomColor());
	}
});

$("#pin").click(function(){
	if ($(".headerbajo").css("position") == "relative") {
		$(".headerbajo").css("position", "fixed");
	} else {
		$(".headerbajo").css("position", "relative");
	}
});
</script>

</body>

</html>