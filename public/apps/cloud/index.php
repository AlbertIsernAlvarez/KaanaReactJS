<html>
  <head>
    <style>
      
      .headerbajo {
        display:block;
            height: 70%;
    min-height: 28rem;
        max-height: 30rem;
      }
      
      .cloud_container {
        text-align:center;
        padding-bottom:4.5rem;
      }
      
      .cloud_container .menu_cloud {
    display: inline-flex;
    align-items: center;
    background: #000000;
    width: calc(100% - 3rem);
    max-width: 50rem;
    padding: 0.5rem;
    position: fixed;
    bottom: 1rem;
    left: 0;
    right: 0;
    margin: auto;
    z-index: 10;
    border-radius:2px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    text-align:right;
      }
      
      .cloud_container .menu_cloud .conjunto {
    display: inline-flex;
    align-items: center;
    max-width: 100%;
    text-align:right;
      }
      
      .cloud_container .menu_cloud .conjunto .boton {
    display: inline-flex;
    align-items: center;
    font-size: 1rem;
    padding: 0.5rem;
    border-radius: 2px;
    background: #eee;
    margin-left:0.5rem;
      }
      
      .cloud_container .section {
        width:100%;
        max-width:45rem;
        margin:auto;
        text-align:left;
      }
      
      .cloud_container .section h1 {
    font-size: 1.2rem;
    padding: 1rem;
    color:white;
      }
      
      .cloud_container .section.carrousel .files {
    display: inline-flex;
    align-items: center;
    overflow-x: auto;
    width: 100%;
    color:black;
      }
      
      .cloud_container .section.carrousel .files * {
        color:black;
      }
      
      .cloud_container .section.carrousel .files::-webkit-scrollbar {
    display:none;
    }
      
      .cloud_container .section.carrousel .files.block .file:first-child {
        margin-left:1.5rem;
      }
      
      .cloud_container .section.carrousel .files.block .file {
    width: 10rem;
    background: white;
    margin: 0.5rem;
    margin-top: 0;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    border-radius: 2px;
    overflow: hidden;
    min-width: 10rem;
      }
      
      .cloud_container .section .files.block .file .icon {
    width: 100%;
    height: 7rem;
    background: #eee;
    text-align: center;
    display: inline-flex;
    align-items: center;
    position: relative;
      }
      
      .cloud_container .section.carrousel .files.block .file .icon i {
    font-size: 2rem;
    color: blue;
    margin: auto;
      }
      
      .cloud_container .section.carrousel .files.block .file .icon .btn {
    padding: 0.5rem;
    background: white;
    border-radius: 20px;
    position: absolute;
    bottom: -1rem;
    right: 1rem;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
      }
      
      .cloud_container .section.carrousel .files.block .file .icon .btn i {
    font-size: 1rem;
    color:black;
      }
      
      .cloud_container .section.carrousel .files.block .file .info {
        padding:0.5rem;
      }
      
      .cloud_container .section.carrousel .files.block .file .info h1 {
        font-size:1rem;
        padding:0.5rem;
        padding-bottom:0rem;
      }
      
      .cloud_container .section.carrousel .files.block .file .info p {
        font-size:0.8rem;
        padding:0.5rem;
      }
      
      .cloud_container .section .files.list {
        background:white;
        box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
        border-radius:2px;
        margin-bottom:1rem;
      }
      
      .cloud_container .section .files.list .file {
        width:100%;
        display:inline-flex;
        align-items:center;
        border-bottom: solid 1px #eee;
      }
      
      .cloud_container .section .files.list .file:first-child {
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23)!important;
      }
      
      .cloud_container .section .files.list .file:hover {
        box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
        cursor:pointer;
      }
      
      .cloud_container .section .files.list .icon {
        padding:0.5rem;
      }
      
      .cloud_container .section .files.list .icon i {
        color:blue;
        padding:0.5rem;
      }
      
      .cloud_container .section .files.list .info {
    display: inline-flex;
    align-items: center;
    width: 100%;
    padding: 0.5rem;
      }
      
      .cloud_container .section .files.list .info h1 {
        font-size:1rem;
        white-space: nowrap;
        width:100%;
        padding:0.5rem;
        color:black;
      }
      
      .cloud_container .section .files.list .info p {
    font-size: 1rem;
    color: #737373;
    white-space: nowrap;
    padding:0.5rem;
    width:100%;
    max-width:10rem;
      }
      
    </style>
  </head>
  <body>
    
    <div class="cloud_container" >
      
      <div class="menu_cloud" >
        <div class="conjunto" >
          <div class="boton" ><p>Editar</p><i class="material-icons">&#xE254;</i></div>
          <div class="boton" ><p>Eliminar</p><i class="material-icons">&#xE872;</i></div>
        </div>
      </div>
      
      <div class="section carrousel" >
        
        <h1>Ultimos archivos</h1>
        
        <div class="files block" >
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE8D2;</i>
          <div class="btn" ><i class="material-icons">&#xE3C9;</i></div>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>You started this week</p>
          </div>
        </div>
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE8D2;</i>
          <div class="btn" ><i class="material-icons">&#xE3C9;</i></div>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>You started this week</p>
          </div>
        </div>
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE8D2;</i>
          <div class="btn" ><i class="material-icons">&#xE3C9;</i></div>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>You started this week</p>
          </div>
        </div>
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE8D2;</i>
          <div class="btn" ><i class="material-icons">&#xE3C9;</i></div>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>You started this week</p>
          </div>
        </div>
          
        </div>
        
      </div>
      
      <div class="section list" >
        
        <h1>Archivos</h1>
        
        <div class="files list" >
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE313;</i>
          </div>
          <div class="info" >
            <h1>name</h1>
            <p>Ultima vez</p>
          </div>
        </div>
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE8D2;</i>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>Feb 12, 2015</p>
          </div>
        </div>
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE2C8;</i>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>Feb 12, 2015</p>
          </div>
        </div>
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE8D2;</i>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>Feb 12, 2015</p>
          </div>
        </div>
          
          <div class="file" >
          <div class="icon" >
            <i class="material-icons">&#xE8D2;</i>
          </div>
          <div class="info" >
            <h1>Files</h1>
            <p>Feb 12, 2015</p>
          </div>
        </div>
          
        </div>
        
      </div>
      
    </div>
    
  </body>
</html>