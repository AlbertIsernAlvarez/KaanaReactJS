<html>

<head>
  <style>
    .calendar-contain {
      position: relative;
      left: 0;
      right: 0;
      border-radius: 0;
      width: 100%;
      overflow: hidden;
      margin: 0;
      display: inline-flex;
    }

    .title-bar__month {
      position: relative;
      float: left;
      font-size: 1rem;
      width: 22%;
      padding: 0 0.5rem;
      text-align: left;
    }

    @media screen and (min-width: 55em) {
      .title-bar__month {
        width: 12%;
      }
    }

    .title-bar__month:after {
      content: "";
      display: inline;
      position: absolute;
      width: 10px;
      height: 10px;
      right: 0;
      top: 5px;
      margin: auto;
      border-top: 1px solid black;
      border-right: 1px solid black;
      -webkit-transform: rotate(135deg);
      transform: rotate(135deg);
    }

    .calendar__sidebar {
      width: 100%;
      margin: 0 auto;
      padding-bottom: 0rem;
      overflow: auto;
      border-right: solid 1px #ddd;
    }

    .calendar__sidebar {
      height: calc(100% - 64px);
      width: 100%;
      margin-bottom: 0;
      max-width: 22rem;
    }

    .calendar__sidebar .content {
      padding: 2rem 1.5rem 2rem 4rem;
    }

    .sidebar__nav {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: start;
      -ms-flex-pack: start;
      justify-content: flex-start;
      padding: 0.7rem 1rem;
      background-color: <?=$color_app_dd?>;
    }

    .sidebar__nav-item {
      display: inline-block;
      width: 22px;
      margin-right: 23px;
      padding: 0;
      color: white;
    }

    .sidebar__list {
      list-style: none;
      margin: 0;
      padding-left: 1rem;
      padding-right: 1rem;
    }

    .sidebar__list-item {
      margin: 0;
      font-weight: 900;
      font-size: 1rem;
      width: 100%;
      padding: 0.5rem;
      display: inline-flex;
      align-items: center;
    }

    .list-item__time {
      display: inline-block;
      width: 4rem;
      padding: 0.5rem;
    }

    .list-item__text {
      width: 100%;
      padding: 0.5rem;
    }


    .sidebar__list-item--complete {
    }
    .sidebar__heading {
      font-size: 2.2rem;
      font-weight: 500;
      padding-left: 1rem;
      padding-right: 1rem;
      padding-top: 1rem;
      padding-bottom: 1.5rem;
      background: var(--PF-color-bg-first-default);
      color: var(--PF-color-original-default);
      border-radius: 0.5em;
      margin: 0.5rem;
    }

    .sidebar__heading span {
      float: right;
      font-weight: 900;
    }

    .calendar__heading-highlight {
      color: #2d444a;
      font-weight: 500;
    }

    .calendar__days {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-flow: column wrap;
      flex-flow: column wrap;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      width: 100%;
      padding: 0.5rem;
      border: solid 1px #ddd;
      border-radius: 1em;
      margin: 0.5rem;
    }

    .calendar__days {
      width: 100%;
    }

    .calendar__top-bar {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
    }

    .top-bar__days {
      width: 100%;
      padding: 0 5px;
      font-weight: 400;
      -webkit-font-smoothing: subpixel-antialiased;
      font-size: 1rem;
    }

    .calendar__week {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-flex: 1;
      -ms-flex: 1 1 0;
      flex: 1 1 0;
    }

    .calendar__day {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-flow: column wrap;
      flex-flow: column wrap;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      width: 100%;
      padding: 0.5rem;
      border-radius:1em;
      margin:1px;
    }

    .calendar__date {
      font-size: 1rem;
      font-weight: 600;
      font-weight: 500;
    }

    @media screen and (min-width: 55em) {
      .calendar__date {
        font-size: 1.5rem;
      }
    }

    .calendar__week .inactive .calendar__date,
    .calendar__week .inactive .task-count {
      color: #c6c6c6;
    }
    .calendar__week .today {
      background-color:var(--PF-color-bg-first-default);
    }

    .calendar__week .today .calendar__date {
      font-weight:900;
      color: var(--PF-color-original-default);
    }

    .calendar__task {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      font-size: 0.8rem;
    }

    @media screen and (min-width: 55em) {
      .calendar__task {
        font-size: 13px;
      }
    }

    .calendar__task.calendar__task--today {
      color: var(--PF-color-original-default);
    }

    @media screen and (max-width: 450px) {
      .calendar-contain {
        display: inline-block;
      }
      .calendar__sidebar {
        max-width: 100%;
      }
    }
  </style>
</head>

<body>
  <main class="calendar-contain">
    <aside class="calendar__sidebar">
      <div class="sidebar__nav">
        <!-- Icons by Icons8 --><span class="sidebar__nav-item"><i class="material-icons">&#xE145;
</i></span><span class="sidebar__nav-item"><i class="material-icons">&#xE5D8;
</i></span><span class="sidebar__nav-item"><i class="material-icons">&#xE5DB;
</i></span></div>
      <h2 class="sidebar__heading">Wednesday<br>April 6</h2>
      <ul class="sidebar__list">
        <li class="sidebar__list-item sidebar__list-item--complete"><span class="list-item__time">8.30</span><span class="list-item__text">Team Meeting</span></li>
        <li class="sidebar__list-item sidebar__list-item--complete"><span class="list-item__time">10.00</span><span class="list-item__text">Lunch with Sasha</span></li>
        <li class="sidebar__list-item"><span class="list-item__time">2.30</span><span class="list-item__text">Design Review</span></li>
        <li class="sidebar__list-item"><span class="list-item__time">4.00</span><span class="list-item__text">Get Groceries</span></li>
      </ul>
    </aside>
    <section class="calendar__days">
      <section class="calendar__top-bar"><span class="top-bar__days">Mon</span><span class="top-bar__days">Tue</span><span class="top-bar__days">Wed</span><span class="top-bar__days">Thu</span><span class="top-bar__days">Fri</span><span class="top-bar__days">Sat</span><span class="top-bar__days">Sun</span></section>
      <section
        class="calendar__week">
        <div class="calendar__day inactive"><span class="calendar__date">30</span></div>
        <div class="calendar__day inactive"><span class="calendar__date">31</span></div>
        <div class="calendar__day"><span class="calendar__date">1</span></div>
        <div class="calendar__day"><span class="calendar__date">2</span></div>
        <div class="calendar__day"><span class="calendar__date">3</span></div>
        <div class="calendar__day"><span class="calendar__date">4</span></div>
        <div class="calendar__day"><span class="calendar__date">5</span></div>
    </section>
    <section class="calendar__week">
      <div class="calendar__day"><span class="calendar__date">6</span></div>
      <div class="calendar__day"><span class="calendar__date">7</span></div>
      <div class="calendar__day"><span class="calendar__date">8</span></div>
      <div class="calendar__day"><span class="calendar__date">9</span></div>
      <div class="calendar__day"><span class="calendar__date">10</span></div>
      <div class="calendar__day"><span class="calendar__date">11</span></div>
      <div class="calendar__day"><span class="calendar__date">12</span></div>
    </section>
    <section class="calendar__week">
      <div class="calendar__day"><span class="calendar__date">13</span></div>
      <div class="calendar__day"><span class="calendar__date">14</span></div>
      <div class="calendar__day today"><span class="calendar__date">15</span><span class="calendar__task calendar__task--today">4 items</span></div>
      <div class="calendar__day"><span class="calendar__date">16</span></div>
      <div class="calendar__day"><span class="calendar__date">17</span></div>
      <div class="calendar__day"><span class="calendar__date">18</span></div>
      <div class="calendar__day"><span class="calendar__date">19</span></div>
    </section>
    <section class="calendar__week">
      <div class="calendar__day"><span class="calendar__date">20</span></div>
      <div class="calendar__day"><span class="calendar__date">21</span></div>
      <div class="calendar__day"><span class="calendar__date">22</span></div>
      <div class="calendar__day"><span class="calendar__date">23</span></div>
      <div class="calendar__day"><span class="calendar__date">24</span></div>
      <div class="calendar__day"><span class="calendar__date">25</span></div>
      <div class="calendar__day"><span class="calendar__date">26</span></div>
    </section>
    <section class="calendar__week">
      <div class="calendar__day"><span class="calendar__date">27</span></div>
      <div class="calendar__day"><span class="calendar__date">28</span></div>
      <div class="calendar__day inactive"><span class="calendar__date">1</span></div>
      <div class="calendar__day inactive"><span class="calendar__date">2</span></div>
      <div class="calendar__day inactive"><span class="calendar__date">3</span></div>
      <div class="calendar__day inactive"><span class="calendar__date">4</span></div>
      <div class="calendar__day inactive"><span class="calendar__date">5</span></div>
    </section>
    </section>
  </main>
</body>

</html>