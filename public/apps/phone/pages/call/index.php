<html lang="en">

<head>
	<style>
		#my-video {
			margin-top: 15px;
			width: 280px;
			height: auto;
			background-color: #eee;
		}
		
		#their-video {
			width: 100%;
			height: 75%;
			max-height: 480px;
			background-color: #eee;
		}
		
		#video-container {
			padding: 25px;
			text-align: center;
		}
		
		#step1-error,
		#step2,
		#step3 {
			display: none;
		}
		
		#my-id {
			font-weight: bold;
		}
	</style>
</head>

<body translate="no">

	<script src="https://kaana.io/resources/js/peerjs.min.js" type="text/javascript"></script>

	<div class="pure-u-2-3" id="video-container">
		<video id="their-video" autoplay=""></video>
		<video id="my-video" muted="true" autoplay=""></video>
	</div>

	<div class="pure-u-1-3">
		<h2>PeerJS Video Chat</h2>

		<div style="display: none;" id="step1">
			<p>Please click `allow` on the top of the screen so we can access your webcam and microphone for calls.</p>
			<div id="step1-error">
				<p>Failed to access the webcam and microphone. Make sure to run this demo on an http server and click allow when asked for permission by the browser.</p>
				<a href="#" class="pure-button pure-button-error" id="step1-retry">Try again</a>
			</div>
		</div>

		<div style="display: block;" id="step2">
			<p>Your id: <span id="my-id">6hrn8hx31pv00000</span></p>
			<p>Share this id with others so they can call you.</p>
			<h3>Make a call</h3>
			<div class="pure-form">
				<input placeholder="Call user id..." id="callto-id" type="text">
				<a class="pure-button pure-button-success" id="make-call">Call</a>
			</div>
		</div>

		<div style="display: none;" id="step3">
			<p>Currently in call with <span id="their-id">...</span></p>
			<p><a class="pure-button pure-button-error" id="end-call">End call</a></p>
		</div>
	</div>
	<script>
		// Compatibility shim
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

		// PeerJS object
		var peer = new Peer({
			debug: 3,
			config: {
				'iceServers': [{
						url: 'stun:stun.l.google.com:19302'
					} // Pass in optional STUN and TURN server for maximum network compatibility
				]
			}
		});

		peer.on('open', function() {
			$('#my-id').text(peer.id);
		});

		// Receiving a call
		peer.on('call', function(call) {
			// Answer the call automatically (instead of prompting user) for demo purposes
			call.answer(window.localStream);
			step3(call);
		});
		peer.on('error', function(err) {
			alert(err.message);
			// Return to step 2 if error occurs
			step2();
		});

		// Click handlers setup
		$(function() {
			$('#make-call').click(function() {
				// Initiate a call!
				var call = peer.call($('#callto-id').val(), window.localStream);

				step3(call);
			});

			$('#end-call').click(function() {
				window.existingCall.close();
				step2();
			});

			// Retry if getUserMedia fails
			$('#step1-retry').click(function() {
				$('#step1-error').hide();
				step1();
			});

			// Get things started
			step1();
		});

		function step1() {
			// Get audio/video stream
			navigator.getUserMedia({
				audio: true,
				video: true
			}, function(stream) {
				// Set your video displays
				var video = document.querySelector('#my-video');
				video.srcObject = stream;
				video.onloadedmetadata = function(e) {
					video.play();
				};

				window.localStream = stream;
				step2();
			}, function() {
				$('#step1-error').show();
			});
		}

		function step2() {
			$('#step1, #step3').hide();
			$('#step2').show();
		}

		function step3(call) {
			// Hang up on an existing call if present
			if (window.existingCall) {
				window.existingCall.close();
			}

			// Wait for stream on the call, then set peer video display
			call.on('stream', function(stream) {
				var video = document.querySelector('#their-video');
				video.srcObject = stream;
				video.onloadedmetadata = function(e) {
					video.play();
				};
			});

			// UI stuff
			window.existingCall = call;
			$('#their-id').text(call.peer);
			call.on('close', step2);
			$('#step1, #step2').hide();
			$('#step3').show();
		}
	</script>


</body>

</html>