<html>

<head>

  <style>
    #icon-mic {
      border-radius: 50%;
      background-color: white;
      height: 163px;
      width: 163px;
      position: relative;
      margin: auto;
      background-size: cover;
      background-position: center;
      z-index: 1;
    }
    
    #icon-mic::before {
      content: ' ';
      position: absolute;
      left: -3px;
      top: 0px;
      border: 1px solid #ddd;
      box-shadow: 0 0 1px #ddd;
      border-radius: 50%;
      height: 163px;
      width: 163px;
      opacity: 0;
      z-index: -1;
      animation: ping 1s ease-out;
      /* weird number to appear more random */
      animation-iteration-count: infinite;
    }
    
    @keyframes ping {
      0% {
        transform: scale(0.1, 0.1);
        opacity: 0.0;
      }
      50% {
        opacity: .65;
      }
      100% {
        transform: scale(2.5, 2.5);
        opacity: 0.0;
      }
    }
  </style>
</head>

<body>

  <script src="https://kaana.io/resources/js/peerjs.min.js"></script>
  <div id="icon-mic" class="PF shadow" rgdd style="background-image: url('<?=$user_avatar?>');"></div>

  <button id="start-call">start call</button>
    <audio controls></audio>

  <script>
var peer = new Peer({key: 'lwjd5qra8257b9'}, 3);  
var conn = peer.connect('dest-peer-id');  
conn.on('open', function()  
{  
    // Receive messages  
    conn.on('data', function(data)  
    {  
        console.log('Received', data);  
    });  
  
    // Send messages  
    conn.send('Hello !');  
});  
  </script>

</body>

</html>