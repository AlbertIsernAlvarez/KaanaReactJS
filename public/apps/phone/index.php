<html>

<head>

  <style>
    #icon-mic {
border-radius: 50%;
    background-color: white;
    height: 163px;
    width: 163px;
    position: relative;
    margin: auto;
    background-size: cover;
    background-position: center;
      z-index:1;
    }

    #icon-mic::before {
      content: ' ';
      position: absolute;
      left: -3px;
      top: 0px;
      border: 1px solid #ddd;
      box-shadow: 0 0 1px #ddd;
      border-radius: 50%;
      height: 163px;
      width: 163px;
      opacity: 0;
      z-index: -1;
      animation: ping 1s ease-out;
      /* weird number to appear more random */
      animation-iteration-count: infinite;
    }

    @keyframes ping {
      0% {
        transform: scale(0.1, 0.1);
        opacity: 0.0;
      }
      50% {
        opacity: .65;
      }
      100% {
        transform: scale(2.5, 2.5);
        opacity: 0.0;
      }
    }
  </style>
</head>

<body>
  <div id="icon-mic" class="PF shadow" rgdd style="background-image: url('<?=$user_avatar?>');"></div>

  <script>
  
    alertdd.show('The call could not be made');
    setTimeout(function(){ window.history.back(); }, 500);
  
  </script>
  
</body>

</html>