<html>

<head>
  <style>
    .photos_container {
      margin:auto;
      flex: 1 auto;
      width: 100%;
    }
    
    .photos_container .photos {
      padding:4px;
    }
    
    .photos_container .pig-figure {
      background-color: rgba(var(--PF-color-primary), 0.1);
    }
    
  </style>
</head>

<body>

  <div class="photos_container" ></div>
  
  <script>
  $('.photos_container').load('./apps/photos/pantallas/photos.php');
  </script>
  
</body>

</html>