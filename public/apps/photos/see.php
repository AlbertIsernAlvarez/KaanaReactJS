<?php

$dir_img = __DIR__."/../../";

$filesmanager = mysqli_query($con,"SELECT * FROM files WHERE fileid='$id_get' LIMIT 1");
if($row_filesmanager = mysqli_fetch_array($filesmanager))
{
  $id_filesmanager = $row_filesmanager['id'];
  $userid_filesmanager = $row_filesmanager['user_id'];
  $mime_filesmanager = $row_filesmanager['mime'];
  $mime_filesmanager = str_replace("image/", "", $mime_filesmanager);
  
  $img = "user/" . $userid_filesmanager . "/image/".$id_get.'.'.$mime_filesmanager;
  
  if (!file_exists($dir_img.$img)) {
    //transparent/grey - In bd but not exist the file
    $img = "user/logo-noimage.png";
  }

} else {
  //Yellow - No in BD
  $img = "user/logo-noimage-2.png";
}

if ($id_get == 'user.svg') {
    //Default avatar
    $img = "user/user.svg";
    $img_mime = "image/svg";
}

if(!$user_mismo_id){
  $checkifafatar = mysqli_query($con,"SELECT * FROM users WHERE avatar='$id_get' LIMIT 1");
  if(!mysqli_fetch_array($checkifafatar) and $id_get != 'user.svg'){
    //Green Water - Client is not logged in and is not an avatar
    $img = "user/logo-noimage-3.png";
  }
}

if($img){
  
  $img_mime = mime_content_type($dir_img.$img);
  if($img_mime === 'image/svg'){ $img_mime = "image/svg+xml"; }
  if($img_mime === 'image/svg+xml'){
    header('Content-Type: '.$img_mime);
    readfile($dir_img.$img);
  } else {
    $requested_uri = "/".$img;
    include_once($dir_img."resources/adaptive-images/adaptive-images.php");
  }
  
}
?>