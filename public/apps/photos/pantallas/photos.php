<div class="PF PF-content full noborder center">
  <div class="container">
    <div class="PF-image" style="background-image:url('./apps/photos/photos.svg');min-height: 5em;background-size: contain;"></div>
    <h1 t-dd>Photos</h1>
  </div>
</div>

<div class="PF PF-card shadow" style="position: fixed;left: 0;bottom: 0;width: 20rem;max-width: calc(100% - 1rem); z-index: 1; border:none;">
  <div class="info">
    <h1 t-dd>Ready to add some photos?</h1>
    <p t-dd>Click the button to start uploading photos</p>
  </div>
  <form class="PF PF-form" id="uploadphotoform" action="./pantallas/files/manager/upload.php" method="post" enctype="multipart/form-data">
    <input type="file" name="files[]" multiple style="display: none;" id="imageUpload" required onchange="$('#uploadphotoform').submit();">
    <div class="PF PF-buttons full">
      <label for="imageUpload" class="PF PF-button" type="submit" t-dd>Upload</label>
    </div>
  </form>
</div>
<?php include(__DIR__."/../resources/photos.php"); ?>

<script>

  $("#uploadphotoform").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Uploading photos');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
      if(data){
        alertdd.show('Photo uploaded correctly');
        opendd('?app=photos');
      } else {
        alertdd.show('The photo cannot be uploaded');
      }
    }
  });
  
</script>