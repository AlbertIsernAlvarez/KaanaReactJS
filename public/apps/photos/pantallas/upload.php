<html>

<head>
  <style>
  </style>
</head>

<body>

  <div class="PF PF-content full noborder center">
    <div class="container">
      <div class="PF-image" style="background-image:url('./apps/photos/photos.svg');"></div>
      <h1 t-dd>Upload an image</h1>
    </div>
  </div>

  <form class="PF PF-form" id="newpageform" action="./resources/photo-manager/upload.php" method="post" enctype="multipart/form-data">

    <input type="file" name="files[]" multiple>
    
    <div class="PF PF-buttons full">
      <button class="PF PF-button" type="submit"><div class="inside"><p t-dd="">Upload</p></div></button>
    </div>

  </form>

  <script>
    /*
    $(document).ready(function() {
      $("#newpageform").ajaxForm(function(data) {
        $('#newpageform').replaceWith(data);
      });
    });
    */
  </script>

</body>

</html>