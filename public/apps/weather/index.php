<html>

<head>
  <style>

  .weather_container {
  	display: flex;
  	flex-direction: column;
  	flex: 1 auto;
  }

  .weather_container>.PF-tabs>.tabs>.tab>.PF-button {
  	margin: .5em auto;
  }

.weather_container .PF-grid {
	margin: auto;
	max-width: 80rem;
	justify-items: center;
	grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
}

.weather_container .PF-grid>.weather-card {
  max-width: 400px;
  width: 100%;
    -webkit-border-radius: 1em;
    border-radius: 1em;
}

.weather_container .PF-grid>.weather-card>.title {
  width: 100%;
}

.weather_container .PF-grid>.weather-card>.title>h1 {
    font-size: 17px;
    font-weight: 500;
    margin: .5rem;
    border-bottom: 1px solid #eee;
    padding: .5em;
}

.weather_container .PF-grid>.weather-card>.content {
    -webkit-box-sizing: inherit;
    box-sizing: inherit;
}

.weather_container .PF-grid>.weather-card>.content>.top {
    display: -webkit-box;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: row;
    flex-direction: row;
    -webkit-align-items: center;
    align-items: center;
    padding: 8px 12px 12px 16px;
}

.weather_container .PF-grid>.weather-card>.content>.top>.data {
    -webkit-box-flex: 1;
    box-flex: 1;
    -webkit-flex-grow: 1;
    flex-grow: 1;
    -webkit-flex-shrink: 1;
    flex-shrink: 1;
}

.weather_container .PF-grid>.weather-card>.content>.top>.data>span {
    font-size: 1rem;
    
    font-weight: 500;
}

.weather_container .PF-grid>.weather-card>.content>.top>.data>h1 {
    font-size: 2rem;
    
    font-weight: 400;
}

.weather_container .PF-grid>.weather-card>.content>.top>.data>h1>span {
    font-size: 1.4rem;
    font-weight: 500;
    margin-left: 2px;
}

.weather_container .PF-grid>.weather-card>.content>.top>.icon {
    -webkit-box-flex: 0;
    box-flex: 0;
    -webkit-flex-grow: 0;
    flex-grow: 0;
    -webkit-flex-shrink: 0;
    flex-shrink: 0;
    text-align: right;
    width: 84px;
    height: 84px;
    font-size: 0;
    background-size: contain;
    background-position: center;
}

.weather_container .PF-grid>.weather-card>.content>.down {
    padding: 0 .8rem;
    padding-bottom: 1rem;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container {
    display: -webkit-box;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: row;
    flex-direction: row;
    -webkit-align-items: center;
    align-items: center;
    -webkit-flex: 1 1 auto;
    flex: 1 1 auto;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container>.block {
    -webkit-flex-shrink: 1;
    flex-shrink: 1;
    -webkit-box-flex: 1;
    box-flex: 1;
    -webkit-flex-grow: 1;
    flex-grow: 1;
    overflow: hidden;
    text-align: center;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container>.block>div>span {
    display: block;
    font-size: 1rem;
    
    font-weight: 500;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container>.block>div>.icon {
    width: 52px;
    max-width: 100%;
    height: 52px;
    margin: 0 auto;
    background-size: contain;
    background-position: center;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container>.block>div>.maxmin {
font-size: 0.8rem;
    text-align: center;
    
    display: flex;
    flex-direction: column;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container>.block>div>.maxmin>.max {
    font-weight: 500;
    padding-bottom: .2rem;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container>.block>div>.maxmin>.min {
    opacity: .9;
}

.weather_container .PF-grid>.weather-card>.content>.down>.container>.block>div>.maxmin>*>span {
    text-align: center;
    
    font-weight: 500;
}
  
  </style>
</head>

<body>

	<div class="weather_container">

  <div class="PF-tabs">
    <div class="tabs">
      <div class="tab" data-name="tabsweather-1"></div>
      <div class="tab" data-name="tabsweather-2"></div>
    </div>

    <div class="PF PF-tabbar bottom shadow">
      <div class="container center">
        <ul id="bottombar">
          <li class="ripple active" data-for="tabsweather-1" data-taburl="./apps/<?=$app?>/pantallas/all.php?<?=$server_querystring?>">
            <i class="material-icons bd">local_library</i>
            <span t-dd>All</span>
          </li>

          <li class="ripple" data-for="tabsweather-2" data-taburl="./apps/<?=$app?>/pantallas/reminders.php?<?=$server_querystring?>" onclick="addweather();">
            <i class="material-icons">language</i>
            <span t-dd>Add</span>
          </li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
  </div>
  </div>

  <script>
  $('body').addClass('PFC-green');
  </script>

</body>

</html>