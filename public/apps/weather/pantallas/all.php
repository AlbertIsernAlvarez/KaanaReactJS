<div glance data-currenturl="?<?=$server_querystring?>"></div>
<div class="PF PF-grid" id="weather-cards">

	<?php
$user_mismo_weather = mysqli_query($con,"SELECT * FROM weather WHERE user_id='$user_mismo_id'");
while($row_user_mismo_weather = mysqli_fetch_array($user_mismo_weather))
  {

    $user_mismo_weather_array = json_decode($row_user_mismo_weather['currently'], true);
    $user_mismo_weather_temperature = $user_mismo_weather_array['temperature'];
    $user_mismo_weather_temperature = substr($user_mismo_weather_temperature, 0, strpos($user_mismo_weather_temperature, "."));
    $user_mismo_weather_icon = $user_mismo_weather_array['icon'];
    $user_mismo_weather_icon_extension = ".png";
    if (in_array($user_mismo_weather_icon, array("clear-day", "partly-cloudy-day", "rain", "moon", "thunderstorms", "clear-night"))) {
      $user_mismo_weather_icon_extension = ".svg";
    }
?>
	<div class="weather-card PF shadow">
		<div class="title">
			<h1>Athens</h1>
		</div>
		<div class="content">
			<div class="top">
				<div class="data">
					<span><?=$user_mismo_weather_array['summary']?></span>
					<h1><?=$user_mismo_weather_temperature?>º<span>C</span></h1>
				</div>
				<div class="icon" style="background-image: url('//img.kaana.io/weather/<?=$user_mismo_weather_icon.$user_mismo_weather_icon_extension?>');"></div>
			</div>
			<div class="down">
				<div class="container">
					<?php
					$weather_blocks = array(1,2,3,4,5,6,7);
					$weather_block = 1;
					foreach ($weather_blocks as $block) {
					$weather_block_number = $weather_block++;
					$weather_block_data = json_decode($row_user_mismo_weather['day'.$weather_block_number], true);
					$weather_block_icon = $weather_block_data['icon'];
					$weather_block_icon_extension = ".png";
					if (in_array($weather_block_icon, array("clear-day", "partly-cloudy-day", "rain", "moon", "thunderstorms", "clear-night"))) {
						$weather_block_icon_extension = ".svg";
					}

					if(strpos($weather_block_data['temperatureHigh'], '.') !== false){ $weather_block_data_high = strstr($weather_block_data['temperatureHigh'], '.', true); } else { $weather_block_data_high = $weather_block_data['temperatureHigh']; }
					if(strpos($weather_block_data['temperatureLow'], '.') !== false){ $weather_block_data_low = strstr($weather_block_data['temperatureLow'], '.', true); } else { $weather_block_data_low = $weather_block_data['temperatureLow']; }
					?>
					<div class="block">
						<div>
							<span><?=date('D', strtotime("Sunday +{$weather_block_number} days"))?></span>
							<div class="icon" style="background-image: url('//img.kaana.io/weather/<?=$weather_block_icon.$weather_block_icon_extension?>');"></div>
							<div class="maxmin">
								<span class="max"><?=$weather_block_data_high?>°<span>C</span></span>
								<span class="min"><?=$weather_block_data_low?>°<span>C</span></span>
							</div>
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
<?}?>
</div>