<html><head>
  <style>

.breathing_container {
  margin: auto;
}

#meditate-container h1 {
  font-size: 13px;
  font-weight: 100;
  margin: 0;
  color: rgb(var(--PF-color-on-surface));
}

#meditate-container {
  color: #fdfdfd;
  padding-top: 22px;
  padding-bottom: 22px;
  padding-left: 16px;
  padding-right: 16px;
  line-height: 1.36;
  font-size: 14px;
  font-weight: normal;
  margin: auto;
}

#meditate-container .section-bottom {
  color: white;
  background-color: rgba(0,0,0,0.06);
  border-top: 0.8px solid rgba(0,0,0,0.08);
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 14px;
  padding-right: 14px;
}

#meditate-container #breathe-btn {
  font-size: 200%;
  margin: 2.5% 0 0 5%;
  display: inline-block;
  width: auto;
}
  </style>
  <link rel="stylesheet" href="/apps/breathing/static/breathe.css">
</head>

<body>
	
    <div class="breathing_container">
      <div id="meditate-container" class="section-container">
      <div id="circles-container">
        <div id="static-circle"></div>
        <div id="animating-circle"></div>
        <svg width="96px" height="96px" id="play-btn" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
          <path d="M8 5v14l11-7z"></path>
          <path d="M0 0h24v24H0z" fill="none"></path>
        </svg>
        <svg height="96px" width="96px" id="restart-btn" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
          <path d="M0 0h24v24H0z" fill="none"></path>
          <path d="M12 5V1L7 6l5 5V7c3.31 0 6 2.69 6 6s-2.69 6-6 6-6-2.69-6-6H4c0 4.42 3.58 8 8 8s8-3.58 8-8-3.58-8-8-8z"></path>
        </svg>
      </div>

      <div id="text-wrapper" class="PF">
        <div id="header-text" t-dd>1-minute breathing exercise</div>
        <div id="preroll-text" t-dd>Bring awareness to your breath</div>
        <div class="breathe-text" id="breathe-text-out" t-dd>Breathe out</div>
        <div class="breathe-text" id="breathe-text-in" t-dd>Breathe in</div>
      </div>
    </div>
    </div>


<script>
  $('body').addClass('PFC-orange');

<?php if($_GET['client']){?>
  $('.header').addClass('hidden');
<?}?>
  
</script>

  <script src="/apps/breathing/static/breathe.js"></script>


</body></html>