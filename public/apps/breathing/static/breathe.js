
  var playBtn = document.getElementById('play-btn');
  var animatingCircle = document.getElementById('animating-circle');
  var closeBtn = document.getElementById('close-btn');
  var fullScreenBtn = document.getElementById('fullscreen-btn');
  var mainContainer = document.getElementById('main-container');
  var meditateContainer = document.getElementById('meditate-container');

  var KEY_CODE_ESC = 27;

  /**
   * CSS classnames for the breathing exercise animation.
   */
  var CSS_BREATHE_ANIM_INIT = 'breathing-anim-initialized';
  var CSS_BREATHE_ANIM_PLAYING = 'breathing-anim-playing';
  var CSS_BREATHE_ANIM_RESTART = 'breathing-anim-restart';

  /**
   * The delay between a click on the start button and the start of the
   * breathing exercise, in ms.
   */
  var BREATHE_EXERCISE_START_DELAY_MS = 750;

  /**
   * The duration of the breathing exercise, in ms.
   *
   * The breathing exercise consists of a 5 seconds introductory (preroll)
   * guiding text, where the user is asked to bring awareness to their breath,
   * followed by 6 breathing cycles (a cycle being one inhale and one exhale).
   * One breathing cycle is defined as 10 seconds (in the CSS). Therefore, the
   * total duration of the breathing exercise is 5 secs preroll plus
   * 6 breathing cycles of 10s each, amounting to 65 seconds in total.
   *
   * Note: The details of the animation are explained at the top of the
   * animations section in the CSS.
   */
  var BREATHE_EXERCISE_DURATION_MS = 65000;


  /**
   * Starts the breathing exercise.
   */
  var startBreathingExercise = function() {
    if (meditateContainer.classList.contains(CSS_BREATHE_ANIM_INIT) ||
        meditateContainer.classList.contains(CSS_BREATHE_ANIM_PLAYING)) {
      return;
    }
    initAnim();
    $('#header').addClass('hidden');
  };


  /**
   * Plays the initialization phase of the animation, i.e. where the play button
   * fades out and becomes the breathing circle.
   */
  var initAnim = function() {
    meditateContainer.classList.remove(CSS_BREATHE_ANIM_RESTART);
    meditateContainer.classList.add(CSS_BREATHE_ANIM_INIT);
    setTimeout(function() {
      playAnim();
    }, BREATHE_EXERCISE_START_DELAY_MS);
  };


  /**
   * Plays the main phase of the animation, i.e. the pulsating circle
   * simulating breath cycles of breathing in and breathing out.
   */
  var playAnim = function() {
    meditateContainer.classList.remove(CSS_BREATHE_ANIM_INIT);
    meditateContainer.classList.add(CSS_BREATHE_ANIM_PLAYING);
    setTimeout(function() {
      stopAnim();
    }, BREATHE_EXERCISE_DURATION_MS);
  };


  /**
   * Stops the animation and shows the replay button.
   */
  var stopAnim = function() {
    meditateContainer.classList.remove(CSS_BREATHE_ANIM_PLAYING);
    meditateContainer.classList.add(CSS_BREATHE_ANIM_RESTART);
    $('#header').removeClass('hidden');
  };


  /**
   * Takes care of all the initialization necessary for breathing exercise
   * page.
   */
  var initPage = function() {
    animatingCircle.addEventListener('click', function() {
      startBreathingExercise();
    });

    // implement this here manually so that users who are used to use ESC
    // to exit fullscreen modes will not feel lost.
    document.addEventListener('keydown', function(e) {
      if (e.keyCode == KEY_CODE_ESC) {
        chrome.windows.getCurrent(function(w) {
          chrome.windows.update(w.id, { state: 'normal' });
        });
      }
    });

  };

  initPage();