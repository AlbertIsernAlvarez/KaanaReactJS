<html>

<head>
  <style>

  .keep_container {
  	display: flex;
  	flex-direction: column;
  	flex: 1 auto;
  }

  .keep_container>.PF-tabs>.tabs>.tab>.PF-button {
  	margin: .5em auto;
  }

.keep_container .PF-grid {
	margin: auto;
	max-width: 60em;
}

.keep_container .PF-grid .PF-card .info p {
	-webkit-line-clamp: 6;
}

.keep_container [data-name='tabskeep-notes'] .PF-grid .PF-card .info p {
  font-size: 20px;
}

.keep_container [data-name='tabskeep-tasks'] .PF-grid>.PF-card {
display: flex;
    flex-direction: row;
}

.keep_container [data-name='tabskeep-tasks'] .PF-grid>.PF-card .PF-icon:last-child {
  margin-right: .5em;
}
  
  </style>
</head>

<body>

	<div class="keep_container">

  <div class="PF-tabs">
    <div class="tabs">
      <div class="tab" data-name="tabskeep-notes"></div>
      <div class="tab" data-name="tabskeep-reminders"></div>
      <div class="tab" data-name="tabskeep-tasks"></div>
    </div>

    <div class="PF PF-tabbar bottom shadow">
      <div class="container center">
        <ul id="bottombar">
          <li class="ripple active" data-for="tabskeep-notes" data-taburl="./apps/<?=$app?>/pages/notes/index.php?<?=$server_querystring?>" onclick="keepchangecolor('yellow');">
            <i class="material-icons">dashboard</i>
            <span t-dd>Notes</span>
          </li>
          <li class="ripple" data-for="tabskeep-reminders" data-taburl="./apps/<?=$app?>/pages/reminders/index.php?<?=$server_querystring?>" onclick="keepchangecolor('purple');">
            <i class="material-icons">date_range</i>
            <span t-dd>Reminders</span>
          </li>
          <li class="ripple" data-for="tabskeep-tasks" data-taburl="./apps/<?=$app?>/pages/tasks/index.php?<?=$server_querystring?>" onclick="keepchangecolor('blue');">
            <i class="material-icons">done_all</i>
            <span t-dd>Tasks</span>
          </li>
          
        </ul>
        <div class="slider"></div>
      </div>
    </div>
  </div>
  </div>

  <script> 
  keepchangecolor('yellow');
  function keepchangecolor(color){
    $("body[class*='PFC-']").removeClass (function (index, css) { return (css.match (/(^|\s)PFC-\S+/g) || []).join(' '); });
    $('body').addClass('PFC-'+color);
  }

  </script>

</body>

</html>