<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Create task</h1>
</div>
<form class="PF PF-form" id="form-task-create" action="./apps/keep/pages/tasks/resources/insert.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<textarea placeholder=" " name="text" ><?=$task_text?></textarea>
		<span t-dd>Text</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Create</button>
</form>

<script>

  $("#form-task-create").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Creating task');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>