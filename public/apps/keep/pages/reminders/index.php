<div class="PF PF-button" onclick="windowdd('./apps/keep/pages/reminders/resources/create.php?<?=$server_querystring?>', 'fit');" t-dd>Create reminder</div>
<div class="PF PF-grid" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); " id="keep-reminders">
<?php
$reminders_select = mysqli_query($con,"SELECT * FROM keep_reminders WHERE status IS NULL AND user_id='$user_mismo_id' ORDER BY id DESC LIMIT 25");
while($row_reminders = mysqli_fetch_array($reminders_select))
  {
  	$reminders_id = $row_reminders['id'];
  	$reminders_text = $row_reminders['text'];
  	?>

<div class="PF PF-card" keep-reminder="<?=$reminders_id?>" onclick="windowdd('./apps/keep/pages/reminders/resources/edit.php?reminder=<?=$reminders_id?>&<?=$server_querystring?>', 'fit');">
	<div class="info">
		<p keep-reminder-data-text><?=replaceemojis($reminders_text)?></p>
	</div>
</div>
<?}?>
</div>