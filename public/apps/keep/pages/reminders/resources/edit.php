<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Edit reminder</h1>
	<div class="PF PF-icon ripple closewindowdd" onclick="$.get('./apps/keep/pages/reminders/resources/delete.php?<?=$server_querystring?>', function( data ) { eval(data); });"><i class="material-icons">delete_outline</i></div>
</div>
<form class="PF PF-form" id="form-reminder-update" action="./apps/keep/pages/reminders/resources/update.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<textarea placeholder=" " name="text" ><?=$reminder_text?></textarea>
		<span t-dd>Text</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

  $("#form-reminder-update").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Saving reminder');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>