<html><head>
<style>
  
  .wrapper {
    text-align:center;
  }

.section-container-saves {
    width: 100%;
    height: auto;
    position: relative;
    margin: auto;
    text-align: center;
    display: inline-block;
    padding: 0;
    padding-top: 1rem;
    max-width: 80rem;
}

.section-container-saves .card-save {
    width: 15.1%;
	  max-width: calc(50% - 2rem);
    margin: 0.5rem;
    display: inline-block;
    background: #fff;
    padding: 0rem;
    vertical-align: top;
    margin-bottom: 0.5rem;
    border-radius: 1em;
    overflow: hidden;
    transition: 0.5s;
    cursor: pointer;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    height: 15rem;
}
  
@media screen and (max-width: 1200px) {
 .section-container-saves .card-save {
    width: calc((-1 * 1%) + (100% / 6));
  }
}
@media screen and (max-width: 992px) {
 .section-container-saves .card-save {
    width: calc((-1 * 1%) + (100% / 5));
  }
}
@media screen and (max-width: 768px) {
 .section-container-saves .card-save {
    width: calc((-1 * 1%) + (100% / 2));
  }
}
	
.section-container-saves .card-save__imagen  {
    width: 100%;
    background-color: rgba(255, 255, 255, 0.58);
    height:10rem;
  display:inline-flex;
  vertical-align: middle;
    display: inline-flex;
    align-items: center;
    justify-content: center;
}
  
  .section-container-saves .card-save__imagen .letter {
    width: 100%;
    max-height: 100%;
    text-align: center;
    font-size: 5rem;
    color: rebeccapurple;
    vertical-align: middle;
    display: inline-flex;
    align-items: center;
    justify-content: center;
  }
  
 .section-container-saves .card-save__info {
    padding:0.5rem;
  }
  
.section-container-saves .card-save__author {
    text-align: left;
    margin: 0;
    font-size: 0.8rem;
    color: white;
    display: block;
    text-decoration: none;
    padding: 0.5rem;
    opacity:0.8;
    overflow:hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
.section-container-saves .card-save__author:hover {
  text-decoration: underline;
}
.section-container-saves .card-save__title {
    text-align: left;
    display: inline-block;
    font-size: 1rem;
    color: white;
    text-decoration: none;
    width: calc(100% - 1rem);
    overflow: hidden;
    padding: 0.5rem;
    padding-top: 0;
    white-space: nowrap;
    text-overflow: ellipsis;
}
.section-container-saves .card-save__title:hover {
  text-decoration: underline;
}
  
  .edit-save {
        box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
        width:calc(100% - 1rem);
        background:rebeccapurple;
        border-radius:2px;
        overflow:hidden;
    display:inline-flex;
    margin:0.5rem;
    text-align:left;
    max-height:calc(100% - 10rem);
      }
  
  .edit-save .imagen {
    width:100%;
    max-height:100%;
    background:rgba(255, 255, 255, 0.58);
    display: inline-flex;
    align-items: center;
    justify-content: center;
  }
  
  .edit-save .imagen .letter {
    width: 100%;
    max-height: 100%;
    text-align: center;
    font-size: 10rem;
    color: rebeccapurple;
    vertical-align: middle;
    display: inline-flex;
    align-items: center;
    justify-content: center;
  }
  
  .edit-save .info-top {
    min-height:15rem;
  }
  
  .edit-save .info-top, .edit-save .info-bottom {
    width: calc(100% - 2rem);
    display: inline-flex;
    padding:0.5rem;
    margin:0.5rem;
  }
  
  .edit-save .info-bottom i {
    vertical-align: bottom;
    align-items: flex-end;
    display: inline-grid;
  }
  
  .edit-save .info .info-container {
    width:calc(100% - 2.5rem);
  }
  
  .edit-save .info {
    width:100%;
    padding:0.5rem;
    font-size:1rem;
    padding:0.5rem;
    color:white;
    overflow:auto;
  }
  
  .edit-save .info h1 {
    padding:0.5rem;
    font-size:1.5rem;
  }
  
  .edit-save .info a {
    display: block;
    padding:0.5rem;
    padding-top:0;
    text-decoration: initial;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    color:rgba(255,255,255,0.87);
    font-size:14px;
  }
  
  .edit-save .info .addtag {
    border-radius:50px;
    padding:0.5rem 1rem;
    background:white;
    display:inline-flex;
    align-items:center;
    font-size:1rem;
    color:black;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    border:none;
  }
  
    .edit-save .info p {
      width:100%;
    padding:0.5rem;
    font-size:1rem;
  }
	
	@media (max-width: 75em) {
		.edit-save {
			display:inline-block;
			max-height:none;
		}
	}
	
</style></head><body>

<section class="section-container-saves" id="saves">
  
   <?php 
  $kiosco_news = mysqli_query($con,"SELECT * FROM marcadores WHERE user_id='$user_id_mismo' ORDER BY id DESC");
  while($row_kiosco_news = mysqli_fetch_array($kiosco_news))
  { 
  $id_kiosco_news = $row_kiosco_news["id"];
	$titulo_kiosco_news = $row_kiosco_news["titulo"];
	$fecha_kiosco_news = $row_kiosco_news["fecha"];
  $url_kiosco_news = $row_kiosco_news["url"];
  $imagen_kiosco_news = $row_kiosco_news["imagen"];
  $descripcion_kiosco_news = $row_kiosco_news["descripcion"];
  $color_kiosco_news = $row_kiosco_news["color"];
  $tags_kiosco_news = $row_kiosco_news["tags"];
    
    $letra_save = ucwords(substr($titulo_kiosco_news, 0, 1));
  ?>
    <div class="card-save" onclick="editsave('<?=$id_kiosco_news?>');" id="save<?=$id_kiosco_news?>" style="background-color:<?=$color_kiosco_news?>;"><div class="card-save__imagen" style="background-image:url('');" ><div class="letter" style="color:<?=$color_kiosco_news?>;" ><?=$letra_save?></div></div><div class="card-save__info" ><a class="card-save__author" href="<?=$url_kiosco_news?>" target="_blank" ><?=$url_kiosco_news?></a><a class="card-save__title"><?=$titulo_kiosco_news?></a></div></div>
  
  <?}?>
  
  </section>
  
<script>
    $(function() {
        $('.PF.PF-tabbar .container').addClass('center');
      });
  
    function editsave(id){
      $('.edit-save').remove();
      var saveid = '#save' + id;
      $.get("./resources/apps/save/edit.php?id=" + id, function (data) {
        $(saveid).after(data);
      });
    }
    
  </script>

</body></html>