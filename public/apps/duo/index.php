<html>

<head>
  <style>
    .duoContainer {
      display: flex;
      flex: 1 auto;
      height: calc(90vh - 2em);
    }
    
    .messages-list {
      width: 350px;
      flex: 1 auto;
      max-width: 25rem;
      display: -webkit-box;
      display: -moz-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-direction: column;
      flex-direction: column;
      overflow: hidden;
    }
    
    .messages-duo {
      flex: 1 auto;
      display: flex;
      flex-direction: column;
      background-color: rgb(var(--PF-color-primary));
      margin: .5em;
      border-radius: 1em;
      position: relative;
      overflow: hidden;
    }
    
    .messages-list-header {
      display: flex;
      align-items: center;
      -webkit-justify-content: space-between;
      justify-content: space-between;
      height: 55px;
      margin: 6px 0 0 16px;
      padding: 0 0 0 8px;
    }
    
    .messages-list-content {
      display: flex;
      flex-direction: column;
      overflow: auto;
      flex: 1;
    }
    
    .messages-list-new .PF-icon {
      -webkit-box-shadow: 0 0 2px rgba(0, 0, 0, 0.12), 0 2px 2px rgba(0, 0, 0, 0.24);
      box-shadow: 0 0 2px rgba(0, 0, 0, 0.12), 0 2px 2px rgba(0, 0, 0, 0.24);
      background-color: var(--PF-color-bg-second-default);
    }
    
    .messages-no-duo {
      -webkit-align-items: center;
      align-items: center;
      display: -webkit-box;
      display: -moz-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-direction: column;
      flex-direction: column;
      -webkit-box-flex: 1;
      -webkit-flex: 1;
      flex: 1 auto;
      -webkit-justify-content: center;
      justify-content: center;
      margin: 0 24px;
      overflow: auto;
    }
    
    .messages-no-duo img {
      height: 256px;
      width: 256px;
    }
    
    .messages-no-duo h3 {
      color: rgba(0, 0, 0, .87);
      margin: 32px auto 8px;
    }
    
    .messages-no-duo p {
      color: rgba(0, 0, 0, .74);
      max-width: 420px;
      text-align: center;
    }
    
    @media screen and (max-width: 560px) {
      .duoContainer .messages-list {
        max-width: 100%;
        width: 100%;
        border-right: 0;
      }
      .duoContainer.inCall .messages-list {
        display: none;
      }
      .duoContainer .messages-duo {
        width: 100%;
        border-right: 0;
        left: 0;
        top: 0;
        z-index: 10;
        margin: 0;
        border-radius: 0;
      }
      .duoContainer:not(.inCall) .messages-duo {
        display: none;
      }
      .duoContainer.inCall .messages-duo {
        margin: 0;
        border-radius: 0;
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
      }
    }
  </style>
</head>

<body>
  <div class="duoContainer">
    <div class="messages-list">
      <div class="PF PF-toolbar">
        <h1>Duo</h1>
      </div>
      <div class="messages-list-content">
        <!-----------<ul class="PF PF-list">
          <li class="messages-list-new" id="make-call">
            <div class="PF PF-icon ripple"><i class="material-icons">add</i></div>
            <div class="data">
              <h1>New Conversation</h1>
            </div>
          </li>
        </ul>------------>
        <div class="PF-grid selector center" id="conversations"></div>
      </div>
    </div>
    <div class="messages-duo" id="duoscreen"></div>
  </div>
  <script>
    $('#conversations').load('./apps/duo/resources/contacts.php?<?=$server_querystring?>');
    $('#duoscreen').load('./apps/duo/pantallas/videocall.php?<?=$server_querystring?>');
  </script>
</body>

</html>