<html>

<head>
  <style>
  @media (max-width: 45em) {
  .PF-window.open {
    max-height: 35em;
    margin-bottom: 0;
  }
  }
  </style>
</head>

<body>

    <div class="PF PF-content full noborder center">
      <div class="container">
        <div class="PF PF-image transparent" style="background-image:url('./apps/duo/resources/images/permission_error.svg');"></div>
        <h1 t-dd>Give microphone & camera permissions</h1>
      </div>
    </div>
    
    <div class="PF PF-card">
      <div class="info">
        <p>To let people you call hear you & see you, give Duo access to your microphone and camera.</p>
      </div>
    </div>


</body>

</html>