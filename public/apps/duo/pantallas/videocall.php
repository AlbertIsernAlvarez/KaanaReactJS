<html lang="en">

<head>
	<style>
		#my-video {
			width: 100%;
			height: 100%;
			position: absolute;
			right: 0em;
			bottom: 0em;
			z-index: 0;
			transition: .5s;
			object-fit: cover;
		}
		
		#my-video.active {
			width: 10em;
			height: auto;
			right: 1em;
			bottom: 1em;
			border-radius: 1em;
			z-index: 2;
		}
		
		#their-video {
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0;
			left: 0;
			opacity: 0;
		}
		
		#their-video.show {
			opacity: 1;
		}
		
		.videos {
			padding: 25px;
			text-align: center;
		}
		
		.accesories {
			z-index: 1;
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-image: linear-gradient(transparent, black 97%);
		}
		
		.accesories .call {
			position: absolute;
			left: 1em;
			bottom: 3em;
			background-color: green;
		}
		
		.accesories .endCall {
			position: absolute;
			left: 1em;
			right: 1em;
			bottom: 1em;
			margin: auto;
			background-color: #F44336;
			border-radius: 10em;
			padding: .7em;
			width: fit-content;
			color: white;
			opacity: 0;
			pointer-events: none;
		}

		.duoContainer.inCall .messages-duo .accesories .endCall {
			opacity: 1;
			pointer-events: all;
		}
		
		#my-id {
			font-weight: bold;
		}
	</style>
</head>

<body>

	<script src="https://kaana.io/resources/js/peerjs.min.js" type="text/javascript"></script>

	<div class="videos" id="video-container">
		<video id="their-video" autoplay=""></video>
		<video id="my-video" muted="true" autoplay=""></video>
	</div>

	<div class="accesories">
		<a class="PF PF-button shadow endCall" id="end-call"><i class="material-icons">call_end</i></a>
	</div>
	<script>
		var callAudioIncoming = new Audio('//kaana.io/apps/duo/resources/audio/incoming.mp3?not');
		var callAudioConnected = new Audio('//kaana.io/apps/duo/resources/audio/connected.mp3?not');

		// Compatibility shim
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

		// PeerJS object
		var peer = new Peer("<?=$user_mismo_id?>", {
			debug: 3,
			config: {
				'iceServers': [{
						url: 'stun:stun.l.google.com:19302'
					} // Pass in optional STUN and TURN server for maximum network compatibility
				]
			}
		});

		peer.on('open', function() {
			console.log(peer.id);
		});

		// Receiving a call
		peer.on('call', function(call) {
			window.call = call;
			alertdd.show('Someone is calling', null, {
				"title": "Answer",
				"class": "duoAnswer close"
			}, {
				"title": "Finish",
				"class": "duoDecline close"
			});
			callAudioIncoming.loop = true;
			callAudioIncoming.currentTime = 0;
			callAudioIncoming.play();
			setTimeout(function(){
				$('.duoDecline').click();
			}, 20000);
			
		});

		peer.on('error', function(err) {
			alertdd.show("Can't make this call right now");
			/*alert(err.message);*/
		});

		// Click handlers setup
		$(function() {

			$('#end-call').click(function() {
				window.existingCall.close();
			});

			$(document).on("click", ".duoDecline", function() {
				duoCallDecline();
			});

			$(document).on("click", ".duoAnswer", function() {
				duoCallAnswer();
			});

			// Get things started
			initDuo();
		});

		function initDuo() {
			// Get audio/video stream
			navigator.getUserMedia({
				audio: true,
				video: true
			}, function(stream) {
				// Set your video displays
				var video = document.querySelector('#my-video');
				video.srcObject = stream;
				video.onloadedmetadata = function(e) {
					video.play();
				};

				window.localStream = stream;
				closewindowdd();
			}, function() {
				windowdd('./apps/duo/resources/windows/permisions.php?<?=$_SERVER["QUERY_STRING"];?>', 'fit');
			});
		}

		function step3(call) {
			// Hang up on an existing call if present
			if (window.existingCall) {
				window.existingCall.close();
			}

			// Wait for stream on the call, then set peer video display
			call.on('stream', function(stream) {
				$('#my-video').addClass('active');
				$('#their-video').addClass('show');
				$('.duoContainer').addClass('inCall');
				var video = document.querySelector('#their-video');
				video.srcObject = stream;
				video.onloadedmetadata = function(e) {
					video.play();
				};
			});

			call.on('close', function() {
				alertdd.show("Call ended");
				callAudioIncoming.pause();
				callAudioConnected.currentTime = 0;
				callAudioConnected.play();
				duoDefaultState();
			});

			// UI stuff
			window.existingCall = call;
		}

		function duoDefaultState(){
			$('#my-video').removeClass('active');
			$('#their-video').removeClass('show');
			$('.duoContainer').removeClass('inCall');
		}

		function duoCall(userid) {
			// Initiate a call!
			var call = peer.call(userid, window.localStream);
			step3(call);
		}

		function duoCallAnswer() {
			// Answer the call automatically (instead of prompting user) for demo purposes
			window.call.answer(window.localStream);
			callAudioIncoming.pause();
			callAudioConnected.currentTime = 0;
			callAudioConnected.play();
			step3(call);
		}

		function duoCallDecline() {
			// Answer the call automatically (instead of prompting user) for demo purposes
			if (window.existingCall) {
				window.existingCall.close();
			}
			duoDefaultState();
			callAudioIncoming.pause();
			callAudioConnected.currentTime = 0;
			callAudioConnected.play();
		}

		<?php if($_GET['call']){?> setTimeout(function(){ duoCall("<?=$_GET['call']?>"); }, 3000); <?}?>
		
	</script>


</body>

</html>