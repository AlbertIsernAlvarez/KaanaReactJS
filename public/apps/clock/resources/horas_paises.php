<html>

<head>
  <style>
    .clocks {
      width: 100%;
      text-align: left;
      margin: auto;
      transition:0.5s;
    }
    
    .clocks .PF-bottombar {
      border-bottom: solid 1px rgba(var(--PF-color-on-surface), .1);
      box-shadow: none;
    }
    
    .clocks #content {
      width: 100%;
      padding: 0.5rem;
    }

    .timezone {
      position: relative;
      overflow: hidden;
      padding: 0.5rem;
      margin: 0;
      border-bottom: solid 1px rgba(var(--PF-color-on-surface), .1);
      display: inline-flex;
      width: 100%;
      align-items: center;
      color: rgb(var(--PF-color-on-surface));
    }
    
    .timezone:last-child {
      border-bottom: none;
    }

    .timezone .first {
      width: 100%;
    }

    .timezone attr {
      font-size: 1em;
    }

    .timezone__location {
      margin: 0;
      font-size: 1.5em;
      font-weight: 200;
    }

    .time {
      font-size: 2em;
      font-weight: 200;
      white-space: nowrap;
    }

    .time small {
      font-size: 0.6em;
    }
  </style>
</head>

<body>
  <div class="clocks">
    <div id="content"  >
      
    </div>
  </div>

  <script type="text/template" id="timezone">
    <div class="timezone">
      <div class="first">
        <h1 class="timezone__location">{{name}}</h1>
        <attr><strong>{{when}}</strong></attr>
      </div>
      <div class="time" id="time-{{name}}">{{time}}</div>
    </div>
  </script>
  <script src="//kaana.io/apps/clock/js/moment.min.js"></script>
  <script src="//kaana.io/apps/clock/js/moment-timezone-with-data-2012-2022.js"></script>
  <script src="//kaana.io/apps/clock/js/mustache.min.js"></script>
  <script>
    var timezones = [{
        name: 'Madrid',
        timezone: 'Europe/Madrid'
      },
      {
        name: 'Athens',
        timezone: 'Europe/Athens'
      },
      {
        name: 'New York',
        timezone: 'America/New_York'
      },
      {
        name: 'Hong Kong',
        timezone: 'Asia/Hong_Kong'
      },
      {
        name: 'London',
        timezone: 'Europe/London'
      },
      {
        name: 'Paris',
        timezone: 'Europe/Paris'
      },
      {
        name: 'Shanghai',
        timezone: 'Asia/Shanghai'
      },
      {
        name: 'Frankfurt',
        timezone: 'Europe/Madrid'
      },
                     {
        name: 'Paris',
        timezone: 'Europe/Paris'
      }
    ];

    var template = $('#timezone').html();
    Mustache.parse(template);

    var display = function() {
      var totalcontent = "";
      var content = $('#content');
      for (var i = 0; i < timezones.length; i++) {
        var tz = timezones[i];
        var m = moment();
        tz.time = m.tz(tz.timezone).format('h:mm a');
        tz.when = m.format('dddd');
        totalcontent = totalcontent + (Mustache.render(template, tz));
      }
      content.html(totalcontent);
      content.css("min-height", content.height() + "px");
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      // add a zero in front of numbers<10
      m = checkTime(m);
      s = checkTime(s);
      //document.getElementById('timeclock').innerHTML = h + ":" + m + ":" + s;
    }

    display();
    setInterval(display, 1000);

    function checkTime(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
  </script>
</body>

</html>