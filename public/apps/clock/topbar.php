<li class="ripple" onclick="$('.clock_container').empty().load('./apps/clock/pantallas/alarm.php');">
  <i class="material-icons">alarm</i>
  <span t-dd>Alarm</span>
</li>

<li class="ripple active" onclick="$('.clock_container').empty().load('./apps/clock/pantallas/clock.php');">
  <i class="material-icons">access_time</i>
  <span t-dd>Clock</span>
</li>

<li class="ripple" onclick="$('.clock_container').empty().load('./apps/clock/pantallas/timer.php');">
  <i class="material-icons">restore</i>
  <span t-dd>Timer</span>
</li>

<li class="ripple" onclick="$('.clock_container').empty().load('./apps/clock/pantallas/stopwatch.php');" >
  <i class="material-icons">timer</i>
  <span t-dd>Stopwatch</span>
</li>