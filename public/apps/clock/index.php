<html>

<head>
  <style>
    .header {
      box-shadow: 0px 1px 3px 1px rgba(60, 64, 67, .15);
    }

    .clock_container {
      display: flex;
      flex-direction: column;
      flex: 1;
      max-width: 50rem;
      margin: auto;
      width: 100%;
    }

    .clock_container .top {
      display: flex;
      align-items: center;
      flex: 1;
      text-align: center;
      min-height: fit-content;
    }

    .clock_container .top .time_data {
      padding: 0.5rem;
      margin: auto;
      width: calc(100% - 1rem);
    }

    .clock_container .top .time_data h1 {
      padding: 0.5rem;
      font-size: 3rem;
      margin: auto;
      color: var(--PF-color-on-surface));
      padding-bottom: 0;
    }

    .clock_container .top .time_data p {
      padding: 0.5rem;
      font-size: 1.5rem;
      margin: auto;
      padding-top: 0;
    }

    .clock_container .content {
      min-height: fit-content;
      border: solid 1px rgba(var(--PF-color-on-surface), .1);
      border-radius: 1em;
      margin-bottom: 1rem;
    }
  </style>
</head>

<body>
  <div class="clock_container">
    
  </div>
  
  <script>
  
    $('.clock_container').load('./apps/clock/pantallas/clock.php');
  
  </script>

</body>

</html>