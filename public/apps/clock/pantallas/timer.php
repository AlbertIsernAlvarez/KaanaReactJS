<html>

<head>
  <style>
    #maintitle {
      margin-bottom: 50px;
      padding: 10px;
      background-color: #3f51b5;
      color: #fff;
      font-size: 40px;
      box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.16), 0px 2px 10px 0px rgba(0, 0, 0, 0.12);
    }

    #cHolder {
      max-width: 500px;
      margin: auto;
    }

    #cBreak,
    #cWork {
      max-width: 150px;
      padding: 20px;
    }

    .inner_holder {
      margin: auto;
      overflow: auto;
      width: 350px;
    }

    .cbut {
      background: none;
      border: none;
    }

    #clock {
      padding: 1rem;
      width: 100%;
      margin: auto;
      font-size: 4rem;
      transition: all 0.2s ease;
    }

    #clock.inactive {
      color: #9E9E9E;
    }

    .work-time {
      color: var(--PF-color-original-default);
    }

    .break-time {
      color: green;
    }

    #clock b {
      font-size: 20px;
    }

    .accent {
      background-color: #e91e63;
    }

    .center {
      text-align: center;
      display: flex;
      flex-direction: column;
      margin:auto;
    }

    .top-space {
      margin-top: 20px;
      text-align: right;
    }

    .floating-button {
      position: absolute;
      bottom: 20px;
      right: 5%;
    }

    .pop-down {
      margin-bottom: -35px;
    }

    .round-button {
      color: #fff;
      height: 70px;
      width: 70px;
      border-radius: 50%;
      box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.26);
      border: none;
      transition: all 0.2s ease;
    }

    .round-button:active {
      width: 60px;
      height: 60px;
      background-color: #c2185b;
    }

    .round-button:hover {
      width: 80px;
      height: 80px;
      margin-top: -10px;
    }

    .fa-fw {
      width: 2em;
      color: #BADFFF;
    }

    .fa-fw:hover {
      color: #fff;
    }

    #timerbody ul {
      column-count: 3;
      width: fit-content;
      margin: auto;
      box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 5px 0 rgba(0, 0, 0, .23);
      border-radius: 1em;
      padding: 0.5rem;
    }

    #timerbody ul li {
      width: 5rem;
      height: 5rem;
      text-align: center;
      display: flex;
      align-items: center;
    }

    #timerbody ul li span {
      margin: auto;
      font-size: 2rem;
    }
  </style>
</head>

<body>

  <div id="timerbody" class="center">
    <div class="top">
      <div class="time_data">
        <h1 id="clock" class="work-time inactive" >1:00</h1>
      </div>
    </div>
    <div>
      <ul>
        <li><span>1</span></li>
        <li><span>2</span></li>
        <li><span>3</span></li>
        <li><span>4</span></li>
        <li><span>5</span></li>
        <li><span>6</span></li>
        <li><span>7</span></li>
        <li><span>8</span></li>
        <li><span>9</span></li>
      </ul>
      <div id="cHolder" class="row">
        <div class="inner_holder">
          <div id="cBreak" class="col-xs-5">Break
            <div class="row">
              <button id="break_minus" class="col-xs-4 cbut">-</button>
              <div id="break_view" class="col-xs-4">1</div>
              <button id="break_plus" class="col-xs-4 cbut">+</button>
            </div>
          </div>
          <div class="col-xs-2"></div>
          <div id="cWork" class="col-xs-5 center">Work
            <div class="row">
              <button id="work_minus" class="col-xs-4 cbut">-</button>
              <div id="work_view" class="col-xs-4">1</div>
              <button id="work_plus" class="col-xs-4 cbut">+</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="playbtnalarm"></div>

  <script>
    var workTime = 25;
    var breakTime = 5;
    var timerClock = $('#clock');
    var workPlus = $('#work_plus');
    var workMinus = $('#work_minus');

    var breakPlus = $('#break_plus');
    var breakMinus = $('#break_minus');

    var timerTime = workTime * 60;
    var timerInterval = 0;
    var timerClock;

    var alarmSound = new Audio('http://demo.tutorialzine.com/2015/04/material-design-stopwatch-alarm-and-timer/assets/06_Urban_Beat.mp3');

    workPlus.on('click', function(e) {
      if (workTime < 60) {
        workTime++;
        $('#work_view').empty().append(workTime);
        resetTimer();
      }
    });

    workMinus.on('click', function(e) {
      if (workTime > 1) {
        workTime--;
        $('#work_view').empty().append(workTime);
        resetTimer();
      }
    });

    breakPlus.on('click', function(e) {
      if (breakTime < 60) {
        breakTime++;
        $('#break_view').empty().append(breakTime);
      }
    });

    breakMinus.on('click', function(e) {
      if (breakTime > 1) {
        breakTime--;
        $('#break_view').empty().append(breakTime);
      }
    });
    // Clicking on the clock.
    timerClock.on('click', function(e) {
      if (timerClock.hasClass('inactive')) {
        if (timerTime > 0) {
          startTimer();
        }
      } else {
        pauseTimer();
      }

    });

    function startTimer() {

      // Prevent multiple intervals going on at the same time.
      clearInterval(timerInterval);

      // Every 1000ms (1 second) decrease the set time until it reaches 0.
      timerInterval = setInterval(function() {
        timerTime--;
        timerClock.text(returnFormattedToSeconds(timerTime));

        if (timerTime <= 0) {
          alarmSound.play();
          pauseTimer();
          startBreakTimer();
          timerClock.removeClass(work - time);
        }
      }, 1000);

      //timerInput.prop('disabled', true);
      timerClock.removeClass('inactive');
      timerClock.addClass('work-time');
    }

    function startBreakTimer() {

      // Prevent multiple intervals going on at the same time.
      clearInterval(timerInterval);
      timerTime = breakTime * 60;

      // Every 1000ms (1 second) decrease the set time until it reaches 0.
      timerInterval = setInterval(function() {
        timerTime--;
        timerClock.text(returnFormattedToSeconds(timerTime));

        if (timerTime <= 0) {
          alarmSound.play();
          timerClock.removeClass('break-time');
          pauseTimer();
          resetTimer();
        }
      }, 1000);

      //timerInput.prop('disabled', true);
      timerClock.removeClass('inactive');
      timerClock.addClass('break-time');
    }


    function pauseTimer() {
      clearInterval(timerInterval);

      //timerInput.prop('disabled', false);
      timerClock.addClass('inactive');
    }

    // Reset the clock with the previous valid time.
    // Useful for setting the same alarm over and over.
    function resetTimer() {
      pauseTimer();
      if (timerClock.hasClass('break-time')) {
        timerClock.removeClass('break-time');
      }

      if (workTime) {
        timerTime = workTime * 60;
        timerClock.text(returnFormattedToSeconds(timerTime));
      }
    }


    function returnFormattedToSeconds(seconds) {
      var min = Math.floor(seconds / 60);
      var sec = seconds % 60;
      return min + ":" + (sec >= 10 ? sec : "0" + sec);
    }
  </script>
</body>

</html>