<html>
	<head>
		<style>
			
			body {
				background:black;
			}
			
			.fit_container {
				width:100%;
				min-height:100%;
				max-width:40rem;
				margin:auto;
			}
			
			.fit_container .information {
		width: 100%;
    display: inline-block;
    position: relative;
    text-align: center;
    padding-top: 1rem;
			}
			
			.fit_container .information .ball {
    border-radius: 100px;
    background: #4CAF50;
    padding: 1rem;
    width: 10rem;
    height: 10rem;
    display: inline-flex;
    align-items: center;
    text-align: center;
    margin: 0.5rem;
    position: relative;
    margin-top: 2.5rem;
		cursor:pointer;
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
			}
			
			.fit_container .information .ball h1 {
    margin: auto;
    color: white;
			}
			
			.fit_container .information .ball::before {
    color: white;
    position: absolute;
    top: -2rem;
    left: 0;
    right: 0;
    margin: auto;
			}
			
			/*WALK*/
			
			.fit_container .information .ball.correr {
		background:#FFC107;
			}
			
			.fit_container .information .ball.correr::before {
    content: "Correr";
			}
			
			/*BICICLETA*/
			
			.fit_container .information .ball.bicicleta {
    background: #FF5722;
    width: 8rem;
    height: 8rem;
			}
			
			.fit_container .information .ball.bicicleta::before {
    content: "Bicicleta";
			}
			
			/*CAMINAR*/
			
			.fit_container .information .ball.caminar {
    background: #9C27B0;
    width: 6rem;
    height: 6rem;
			}
			
			.fit_container .information .ball.caminar::before {
    content: "Caminar";
			}
			
			.fit_container .activity {
				
			}
			
			.fit_container .activity .block {
				width:calc(100% - 2rem);
				margin-left:2rem;
			}
			
			.fit_container .activity .block .title {
    font-size: 1rem;
    color: white;
    padding: 1.5rem;
    position: relative;
    padding-left: 0;
			}
			
			.fit_container .activity .block .activity_block {
				display:inline-flex;
				width:100%;
			}
			
			.fit_container .activity .block .activity_block .bar {
    content: " ";
    width: 1rem;
    height: 10rem;
    background: #4CAF50;
    margin-right: -2rem;
    z-index: -1;
		box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
			}
			
			.fit_container .activity .block .activity_block .img {
    width: 3rem;
    height: 5rem;
    background: white;
    border-radius: 50px;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    display: inline-flex;
    align-items: center;
    text-align: center;
			}
			
		.fit_container .activity .block .activity_block .img i {
				margin:auto;
				font-size:1.5rem;
			}
			
			.fit_container .activity .block .activity_block .info {
    		margin-left: 1rem;
				padding: 0.5rem;
				color:white;
				margin-top:0.5rem;
			}
			
			.fit_container .activity .block .activity_block .info h1 {
    		font-size:1.5rem;
			}
			
			.fit_container .activity .block .activity_block .info .map {
width: 90%;
    height: 10rem;
    border-radius: 20px;
    margin: 1rem;
    margin-left: 0;
    background-size: cover;
    background-position: center;
			}
			
		</style>
	</head>
	<body>
		
		<div class="fit_container" >
			
			<div class="information" >
				<div class="ball correr" >
					<h1>6KM</h1>
				</div>
				
				<div class="ball bicicleta" >
					<h1>18KM</h1>
				</div>
				
				<div class="ball caminar" >
					<h1>1KM</h1>
				</div>
			</div>
			
			<div class="activity" >
				<div class="block" >
					
					<h1 class="title" >Hoy</h1>
					
					<div class="activity_block" >
						<div class="bar" ></div>
						<div class="img" >
							<i class="material-icons">&#xE566;</i>
						</div>
						<div class="info" >
							<h1>Mollet del Valles</h1>
							<p>Corriste durante 1.5 KM en 1:12h</p>
						</div>
					</div>
					
					<div class="activity_block" >
						<div class="bar" style="background: -webkit-linear-gradient(top, #4caf50 3rem, #FFEB3B, #4caf50); background-size: cover; padding-bottom: 9rem;" ></div>
						<div class="img" >
							<i class="material-icons">&#xE52F;</i>
						</div>
						<div class="info" >
							<h1>Mollet del Valles</h1>
							<p>Fuiste en bicicleta durante 2.3 KM en 2:46h</p>
							<div class="map" style="background-image:url('https://www.wired.com/wp-content/uploads/2016/11/GoogleMap-1-200x100-e1479510769475.jpg');">
								
							</div>
						</div>
					</div>
					
					<div class="activity_block" >
						<div class="bar" ></div>
						<div class="img" >
							<i class="material-icons">&#xE566;</i>
						</div>
						<div class="info" >
							<h1>Mollet del Valles</h1>
							<p>Corriste durante 0.7 KM en 34 minutos</p>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
	</body>
</html>