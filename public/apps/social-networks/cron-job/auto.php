<?php
function curl_get_file_contents($URL)
{
    $c = curl_init();
    curl_setopt($c, CURLOPT_TIMEOUT, 10);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    $contents = curl_exec($c);
    curl_close($c);
    if ($contents) return $contents;
    else return false;
}

function getSocialFeed($feed_url, $socialnetwork, $username)
{
    global $con;
    global $datetime;
    $sql_insert_socialnetworks = "UPDATE socialnetworks SET last_update='$datetime' WHERE socialnetwork='$socialnetwork'; ";
    if ($content = curl_get_file_contents($feed_url))
    {
        $xml = simplexml_load_string($content);

        $total_socialnetworks = 0;
        $total_socialnetworks_total = 0;
        foreach ($xml
            ->channel->item as $entry)
        {
            $namespaces = $entry->getNamespaces(true);
            if($socialnetwork != 'twitter'){ $title = mysqli_real_escape_string($con, $entry->title); }
            $url = $entry->link;
            $author = mysqli_real_escape_string($con, clean_input($entry->author));
            if (!$author)
            {
                $dc = $entry->children('http://purl.org/dc/elements/1.1');
                $author = mysqli_real_escape_string($con, clean_input($dc->creator)); // echoes the content of the <dc:creator/> tag
                
            }
            
            $description = mysqli_real_escape_string($con, $entry->description);
            $pubDate = mysqli_real_escape_string($con, clean_input(strftime("%Y-%m-%d %H:%M:%S", strtotime($entry->pubDate))));

            if (($title or $description) and $url and $pubDate)
            {
            	$sql_insert_socialnetworks .= "INSERT IGNORE INTO socialnetworks_feed (socialnetwork, username, title, text, images, url, date) VALUES ('$socialnetwork', '$username', '$title', '$description', '$images', '$url', '$pubDate'); ";
            }
        }
        if ($sql_insert_socialnetworks){
            mysqli_multi_query($con, $sql_insert_socialnetworks);
        }
    }
}

include_once (__DIR__ . "/networks.php");

?>
