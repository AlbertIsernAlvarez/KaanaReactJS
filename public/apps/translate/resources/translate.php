<?php

header('Content-Type: text/html; charset=UTF-8');

//FROM
$translate_from = $_POST['from'];
if($_GET['from']){
	$translate_from = $_GET['from'];
}
if($translate_from == null){
	$translate_from = "en-US";
}

//TO
$translate_to = $_POST['to'];
if($_GET['to']){
	$translate_to = $_GET['to'];
}
if($translate_to == null){ $translate_to = $language; }

//TEXT
$translate_text = $_POST['text'];
if($_GET['text']){
	$translate_text = $_GET['text'];
}

echo json_encode(translate($translate_text, "en-US", $language, "json", true));

exit();
?>