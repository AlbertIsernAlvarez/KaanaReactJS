<?php

$translations_language = $_POST['to'];
if($translations_language !== 'en-US'){

$translations_array = array();

foreach ($_POST['arraytexts'] as &$value) {
	$value = translate($value, "en-US", $translations_language, "json", true);
	array_push($translations_array, $value);
}

header('Content-Type: text/html; charset=UTF-8');
echo json_encode($translations_array);

}
exit();
?>