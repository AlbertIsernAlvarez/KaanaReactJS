<?php 

$language = $_COOKIE['language'];
$texto_traducir = $_GET['text'];
if($texto_traducir == ''){ $texto_traducir = $_POST['text']; }
$language_from = $_GET['from'];
$language_to = $_GET['to'];
if($language_to == ''){ $language_to = $language; }

include_once(__DIR__.'/../../../resources/acceso_db.php');
header('Content-Type: text/html; charset=UTF-8');

$texto_procesar = $texto_traducir;
$text_traduccion = $texto_traducir;


function andpalabra($word, $language_from, $language_to){
  $word = cleartext($word);
  $and = " texto LIKE '$word' $and_from AND language1='$language_from' AND language2='$language_to' AND traduccion!='' OR traduccion LIKE '$word' $and_from AND language1='$language_from' AND language2='$language_to' AND texto!='' ";
  $and_reverse = " traduccion LIKE '$word' $and_from AND language2='$language_from' AND language1='$language_to' AND texto!='' OR texto LIKE '$word' $and_from AND language2='$language_from' AND language1='$language_to' AND traduccion!='' ";
  return $and . " OR " . $and_reverse;
}

function cleartext($cadena){
  $cadena = utf8_decode($cadena);
  $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
  $cadena = str_replace('.', '', $cadena);
  $cadena = str_replace(',', '', $cadena);
  $cadena = str_replace('?', '', $cadena);
  $cadena = str_replace('¿', '', $cadena);
  $cadena = str_replace('!', '', $cadena);
  $cadena = str_replace('¡', '', $cadena);
  $cadena = str_replace('#', '', $cadena);
  $cadena = str_replace('/', '', $cadena);
  $cadena = str_replace(':', '', $cadena);
  $cadena = str_replace(';', '', $cadena);
  $cadena = str_replace('>', '', $cadena);
  $cadena = str_replace('<', '', $cadena);
  $cadena = str_replace('$', '', $cadena);
  $cadena = str_replace('%', '', $cadena);
  $cadena = str_replace('^', '', $cadena);
  $cadena = str_replace('&', '', $cadena);
  $cadena = str_replace('*', '', $cadena);
  $cadena = str_replace('(', '', $cadena);
  $cadena = str_replace(')', '', $cadena);
  
  //SE SUPONE QUE BORRA LOS EMOJIS PERO HAY UN ERROR Y ES QUE AL TRADUCIR DEJA LAS MAYUSCULAS EN MINUSCULAS POR ESO LO HE DESHABILITADO
  //$cadena = preg_replace('/[[:^print:]]/', '', $cadena);
    //$cadena = strtolower($cadena);
    return utf8_encode($cadena);
}




if($language_from != $language_to){
  
//COMPROBAMOS LA FRASE ENTERA
  $texto_procesar = cleartext($texto_procesar);
$select_traductor_entera = andpalabra($texto_procesar, $language_from, $language_to);
$traductor_traduccion = mysqli_query($con,"SELECT * FROM traductor WHERE $select_traductor_entera ORDER BY length(texto) DESC, length(traduccion) DESC LIMIT 5");
while($row_traductor_traduccion = mysqli_fetch_array($traductor_traduccion))
{
  $texto_traductor_traduccion = $row_traductor_traduccion["texto"];
  $traduccion_traductor_traduccion = $row_traductor_traduccion["traduccion"];
  $text_traduccion = preg_replace("/\b$texto_traductor_traduccion\b/i", "$traduccion_traductor_traduccion", "$text_traduccion");
}

  
  
  
  
  //VAMOS A RESTARLE UNA PALABRA Y BUSCAR LA TRADUCCION
  $texto_restada_original = cleartext($text_traduccion);
  $texto_restada = explode(' ', $texto_restada_original);
  foreach($texto_restada AS $word_arestar)
  {
    $texto_restada_original = preg_replace("/\b$word_arestar \b/i", "", "$texto_restada_original");
    $texto_restada_original = preg_replace("/\b $word_arestar\b/i", "", "$texto_restada_original");
    
    $select_traductor_restada = andpalabra($texto_restada_original, $language_from, $language_to);
    
    $traductor_traduccion = mysqli_query($con,"SELECT * FROM traductor WHERE $select_traductor_restada ORDER BY length(texto) DESC, length(traduccion) DESC LIMIT 10");
    while($row_traductor_traduccion = mysqli_fetch_array($traductor_traduccion))
    {
      $language1_traductor_traduccion = $row_traductor_traduccion["language1"];
      $texto_traductor_traduccion = $row_traductor_traduccion["texto"];
      
      $traduccion_traductor_traduccion = $row_traductor_traduccion["traduccion"];
      
      if($language1_traductor_traduccion == $language_from){
        $text_traduccion = preg_replace("/\b$texto_traductor_traduccion\b/i", "$traduccion_traductor_traduccion", "$text_traduccion");
      } else {
        $text_traduccion = preg_replace("/\b$traduccion_traductor_traduccion\b/i", "$texto_traductor_traduccion", "$text_traduccion");
      }
    }
  }
    
  
  
  
  

//VAMOS A RESTARLE UNA PALABRA Y BUSCAR LA TRADUCCION (AL REVES DE LA ANTERIOR)
  $texto_restada_original = cleartext($text_traduccion);
  $texto_restada = explode(' ', $texto_restada_original);
  foreach(array_reverse($texto_restada) AS $word_arestar)
  {
    $texto_restada_original = preg_replace("/\b $word_arestar\b/i", "", "$texto_restada_original");
    $texto_restada_original = preg_replace("/\b$word_arestar\b/i ", "", "$texto_restada_original");
    
    $select_traductor_restada = andpalabra($texto_restada_original, $language_from, $language_to);
    
    $traductor_traduccion = mysqli_query($con,"SELECT * FROM traductor WHERE $select_traductor_restada ORDER BY length(texto) DESC, length(traduccion) DESC LIMIT 10");
    while($row_traductor_traduccion = mysqli_fetch_array($traductor_traduccion))
    {
      $language1_traductor_traduccion = $row_traductor_traduccion["language1"];
      $texto_traductor_traduccion = $row_traductor_traduccion["texto"];
      
      $traduccion_traductor_traduccion = $row_traductor_traduccion["traduccion"];
      
      if($language1_traductor_traduccion == $language_from){
        $text_traduccion = preg_replace("/\b$texto_traductor_traduccion\b/i", "$traduccion_traductor_traduccion", "$text_traduccion");
      } else {
        $text_traduccion = preg_replace("/\b$traduccion_traductor_traduccion\b/i", "$texto_traductor_traduccion", "$text_traduccion");
      }
    }
  }
  

  
  
  //VAMOS A BUSCAR AHORA PALABRA POR PALABRA
  $texto_words = explode(' ', $text_traduccion);
  foreach(array_reverse($texto_words) AS $word)
  {
    $select_traductor_restada = andpalabra($word, $language_from, $language_to);
    
    $traductor_traduccion = mysqli_query($con,"SELECT * FROM traductor WHERE $select_traductor_restada ORDER BY length(texto) DESC, length(traduccion) DESC LIMIT 10");
    while($row_traductor_traduccion = mysqli_fetch_array($traductor_traduccion))
    {
      $language1_traductor_traduccion = $row_traductor_traduccion["language1"];
      $texto_traductor_traduccion = $row_traductor_traduccion["texto"];
      
      $traduccion_traductor_traduccion = $row_traductor_traduccion["traduccion"];
      
      if($language1_traductor_traduccion == $language_from){
        $text_traduccion = preg_replace("/\b$texto_traductor_traduccion\b/i", "$traduccion_traductor_traduccion", "$text_traduccion");
      } else {
        $text_traduccion = preg_replace("/\b$traduccion_traductor_traduccion\b/i", "$texto_traductor_traduccion", "$text_traduccion");
      }
    }
  }

 
}

if($texto_procesar{0} === strtoupper($texto_procesar{0})){
      $text_traduccion = ucfirst($text_traduccion);
    } else {
      $text_traduccion = lcfirst($text_traduccion);
    }

echo $text_traduccion;
?>