<html>

<head>
  <style>
    
    .headerbajo {
      height: 16rem;
      display: block!important;
      background-color:#497ed6!important;
    }

    .traductor {
      width: 100%;
      max-width: 60rem;
      margin: 0 auto;
    }

    .traductor table {
      width: 100%;
      border-radius: 3px;
      border-spacing: 0;
    }
    
    .traductor tr {
    display: inline-flex;
    width: 100%;
    }

    .traductor td {
    padding: 0.5rem;
    vertical-align: top;
    width: 100%;
    display: inline-block;
    }

    .traductor .textarea {
    width: 100%;
    height: 100%;
    min-height: 15rem;
    border: none;
    padding: 1rem;
    font-size: 1rem;
    resize: none;
    box-shadow: 0px 1px 3px 1px rgba(60,64,67,.15);
    border-radius: 1em;
    text-align: left;
    background: white;
    outline: none;
    font-family: Kaana Sans, Roboto, DumDarac Emojis, sans-serif;
    }

    .traductor select {
    padding: 0.5rem;
    border-radius: 1em;
    background: #FFFFFF;
    margin-bottom: 1rem;
    color: black;
      border:none;
    outline: none;
    font-size: 1rem;
    }

    .traductor select option {
      padding: 0.5rem;
      border-radius: 3px;
      margin-bottom: 1rem;
      box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
      outline: none;
      border: none;
      font-size: 1rem;
    }

    .traductor .escribiendo {
      color: #ABABAB;
    }

    .traductor .seccion_traducir {
    text-align: center;
    column-count: 2;
    column-width: auto;
    display: inline-flex;
    width: 100%;
    }
  </style>
</head>

<body>
  <div class="traductor">
    <table>
      <tbody>
        <tr class="seccion_traducir">
          <td>
            <select name="idioma1" id="idioma1">
              <option value="es" selected>Español</option>
              <option value="en">English</option>
              <option value="fr">Français</option>
              <option value="ca">Catala</option>
              <option value="ku">kurmanji</option>
              <option value="ru">Русо</option>
            </select>
            <div contenteditable="true" id="atraducir" class="textarea" ></div>
              <div class="significado_traducciones"></div>
          </td>
          <td>
            <select name="idioma2" id="idioma2">
              <option value="es">Español</option>
              <option value="en" selected>English</option>
              <option value="fr">Français</option>
              <option value="ca">Catala</option>
              <option value="ku">kurmanji</option>
              <option value="ru">Русо</option>
            </select>
            <div contenteditable="true" id="traducidotexto" class="traducido textarea" ></div>
              <div id="traducidotextoedit" style="display:none;"></div>
              <div class="significado_traducciones"></div>
          </td>
        </tr>
        <tr>
          <td>
            <p>Definiciones de: <b id="textodefinicion"></b></p>
          </td>
          <td>
            <p>Traducciones de: <b id="textotraducciones"></b></p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  
  <script>
    
      $('#atraducir').on( "keyup", function() { traducirtexto(); });
      $('#idioma1').change(function() { traducirtexto(); });
      $('#idioma2').change(function() { traducirtexto(); });
      $('#traducidotexto').on( "keyup", function() { edittext(); });
      
      var traducirtextotimeout;
      function traducirtexto(){
        var idioma1 = $('#idioma1').val();
        var idioma2 = $('#idioma2').val();
        var texttranslate = $('#atraducir').text();
        var texttranslatetraducido = $('#traducidotexto').text();
        var dataString = 'text=' + texttranslate;
        clearTimeout(traducirtextotimeout);
        traducirtextotimeout = setTimeout(function() {
          document.getElementById("textodefinicion").innerHTML = texttranslate;
          document.getElementById("textotraducciones").innerHTML = texttranslate;
          if (texttranslate != '') {
            $("#traducidotexto").addClass("escribiendo");
            $.ajax({
              type: "POST",
              url: "./apps/translate/resources/traducir.php?from=" + idioma1 + "&to=" + idioma2,
              data: dataString,
              cache: false,
              success: function(html) {
                $("#traducidotexto").empty().html(html).show();
                setTimeout(function() {
                  $("#traducidotexto").removeClass("escribiendo");
                }, 1000);
              }
            });
          }
          return false;
        }, 1000);
      }
    
    function edittext(){
      var editartexto_idioma1 = $('#idioma1').val();
        var editartexto_idioma2 = $('#idioma2').val();
        var editartexto_texttranslate = $('#atraducir').text();
        var editartexto_texttranslatetraducido = $('#traducidotexto').text();
        var editartexto_dataString = 'textedit=' + editartexto_texttranslatetraducido;
        
        if (editartexto_texttranslatetraducido != '') {
          $.ajax({
            type: "POST",
            url: "./apps/traductor/resources/editar.php?from=" + editartexto_idioma1 + "&to=" + editartexto_idioma2 + "&text=" + editartexto_texttranslate,
            data: editartexto_dataString,
            cache: false,
            success: function(htmledit) {
              $('#traducidotextoedit').empty().html(htmledit);
            }
          });
        }
        return false;
    }
  </script>
</body>

</html>