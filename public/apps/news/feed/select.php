<?php
$select_news = "SELECT *, SUBSTRING(`text`, 1, 250) AS text FROM news t1 WHERE id NOT IN('" . readfeedloaded($con, $user_mismo_id, $feed_unique, "news") . "') ";

if ($topic) {
    $select_news_algorithm[] = "(LOWER(title) LIKE LOWER('%$topic%') OR LOWER(topic) IN(LOWER('$topic')) OR LOWER(source) IN(LOWER('$topic')) )";
}

if ($source) {
    $select_news_algorithm[] = "(LOWER(source) IN(LOWER('$source')))";
}

if ($related) {
    $select_news_related   = array();
    $select_news_related[] = "(LOWER(title) LIKE LOWER('%$related%') OR LOWER(topic) IN(LOWER('$related')) OR LOWER(source) IN(LOWER('$related')) )";
    foreach (topicsRelated($related) as &$topic) {
        $select_news_related[] = "(LOWER(title) LIKE LOWER('%$topic%') OR LOWER(topic) IN(LOWER('$topic')) OR LOWER(source) IN(LOWER('$topic')) )";
    }
    $select_news_algorithm[] = " (" . implode(" OR ", $select_news_related) . ") ";
}

if ($q) {
    $select_news_algorithm[] = "(LOWER(title) LIKE LOWER('%$q%') OR LOWER(topic) IN (LOWER('$q')) OR LOWER(source) IN (LOWER('$q')) )";
}

if($user_mismo_languages){
    $select_news_algorithm[] = " language IN('$user_mismo_languages') ";
} elseif($language) {
    $select_news_algorithm[] = " language='$language' ";
}

if (!$topic and !$q) {
    $moreless_less_topics_exclude_group = array();
    $moreless_less                      = mysqli_query($con, "SELECT LOWER(topics) AS topics FROM moreless WHERE user_id='$user_mismo_id' AND moreless='less' AND topics IS NOT NULL ORDER BY date DESC");
    while ($row_moreless_less = mysqli_fetch_array($moreless_less)) {
        $moreless_less_topics         = $row_moreless_less['topics'];
        $moreless_less_topics         = explode(",", $moreless_less_topics);
        $moreless_less_topics_exclude = array();
        foreach ($moreless_less_topics as $moreless_less_topic) {
            $moreless_less_topic = mysqli_real_escape_string($con, $moreless_less_topic);
            if ($moreless_less_topic) {
                $moreless_less_topic = strtolower($moreless_less_topic);
                $moreless_less_topics_exclude[] = " (LOWER(title) NOT LIKE '%$moreless_less_topic%') ";
            }
        }
        if ($moreless_less_topics_exclude) {
            //GROUP OF TOPICS TOGETHER THAT THE USER DOES NOT WANT TO SEE
            $moreless_less_topics_exclude_group[] = " (" . implode(" AND ", $moreless_less_topics_exclude) . ") ";
        }
    }
    if ($moreless_less_topics_exclude_group) {
        //GROUP OF TOPICS THAT WE HAVE GROUPED BEFORE THE USER DOES NOT WANT TO SEE
        $select_news_algorithm[] = " (" . implode(" AND ", $moreless_less_topics_exclude_group) . ") ";
    }
}

if ($select_news_algorithm) {
    $select_news .= " AND (" . implode(" AND ", $select_news_algorithm) . ") ";
}

$select_news .= " GROUP BY title ORDER BY date DESC LIMIT 30";

?>