<div class="PF PF-toolbar center" style=" box-shadow: none; ">
  <h1>Newsstand</h1>
</div>
<div class="PF-tabs">
  <div class="PF PF-tabbar shadow">
    <div class="container center">
      <ul>
        <li class="ripple active" data-for="tabsnewsnewsstands-1" data-taburl="./apps/<?=$app?>/resources/newsstand.php?newsstand=featured&<?=$server_querystring?>"><span>Featured</span></li>
        <li class="ripple" data-for="tabsnewsnewsstands-2" data-taburl="./apps/<?=$app?>/resources/newsstand.php?newsstand=popular&<?=$server_querystring?>"><span>Popular</span></li>
      </ul>
      <div class="slider"></div>
    </div>
  </div>
  <div class="tabs">
    <div class="tab" data-name="tabsnewsnewsstands-1"></div>
    <div class="tab" data-name="tabsnewsnewsstands-2"></div>
  </div>
</div>