<?php
$news_headlines = array("World", "Business", "Technology", "Entertainment", "Sports", "Science", "Health");
?>
<div class="PF-tabs">
  <div class="PF PF-tabbar shadow">
    <div class="container center">
      <ul>
        <li class="ripple active" data-for="tabsnewsrelateds-lastest" data-taburl="./apps/<?=$app?>/resources/load-feed.php?type=latest&<?=$server_querystring?>"><span t-dd>Latest</span></li>
        <?php foreach($news_headlines as $headline){
          if($language != 'en-US'){
            $headline_translated = translate($headline, "en-US", $language, null, true);
          } else {
            $headline_translated = $headline;
          }
        ?>
          <li class="ripple" data-for="tabsnewsrelateds-<?=str_replace(' ', '', $headline);?>" data-taburl="./apps/<?=$app?>/resources/load-feed.php?related=<?=$headline_translated?>&<?=$server_querystring?>"><span t-dd><?=$headline?></span></li>
        <?}?>
      </ul>
      <div class="slider"></div>
    </div>
  </div>
  <div class="tabs">
    <div class="tab" data-name="tabsnewsrelateds-lastest"></div>
    <?php foreach($news_headlines as $headline){?>
      <div class="tab" data-name="tabsnewsrelateds-<?=str_replace(' ', '', $headline);?>"></div>
    <?}?>
  </div>
</div>