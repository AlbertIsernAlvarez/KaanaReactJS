<?php
function curl_get_file_contents($URL)
{
    $c = curl_init();
    curl_setopt($c, CURLOPT_TIMEOUT, 10);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    $contents = curl_exec($c);
    curl_close($c);
    if ($contents) return $contents;
    else return false;
}

function getFeed($feed_url, $fuente, $language, $datetime, $topic_getfeed)
{
    $sql_insert_news = "UPDATE news_rss SET updated_at='$datetime' WHERE source='$fuente'; ";
    global $con;
    if ($content = curl_get_file_contents($feed_url))
    {
        $xml = simplexml_load_string($content);

        $total_news = 0;
        $total_news_total = 0;
        foreach ($xml
            ->channel->item as $entry)
        {
            $namespaces = $entry->getNamespaces(true);

            $image = null;

            if (isset($entry->children($namespaces['media'])
                ->content
                ->attributes()
                ->url))
            {
                $image = trim((string)$entry->children($namespaces['media'])
                    ->content
                    ->attributes()
                    ->url);
            }

            if (!$image)
            {
                $image = mysqli_real_escape_string($con, clean_input($entry->enclosure['url']));
            }

            $title = mysqli_real_escape_string($con, clean_input($entry->title));

            if (!$fuente)
            {
                $fuente = mysqli_real_escape_string($con, clean_input($xml
                    ->channel
                    ->title));
            }

            //$language = $xml->channel->language;
            $url = $entry->link;
            $author = mysqli_real_escape_string($con, clean_input($entry->author));
            if (!$author)
            {
                $dc = $entry->children('http://purl.org/dc/elements/1.1');
                $author = mysqli_real_escape_string($con, clean_input($dc->creator)); // echoes the content of the <dc:creator/> tag
                
            }
            $etiquetas = mysqli_real_escape_string($con, clean_input($entry->category));
            $description = mysqli_real_escape_string($con, clean_input($entry->description));
            $descripcion_subtitle = mysqli_real_escape_string($con, clean_input($entry->subtitle));
            $pubDate = mysqli_real_escape_string($con, clean_input($entry->pubDate));
            $pubDate = strftime("%Y-%m-%d %H:%M:%S", strtotime($pubDate));

            if ($title and $description and $url and $pubDate)
            {
            	$sql_insert_news .= "INSERT IGNORE INTO news (url, description, image, language, title, text, source, author, date, tags, topic) VALUES ('$url', '$descripcion_subtitle', '$image', '$language', '$title', '$description', '$fuente', '$author', '$pubDate', '$etiquetas', '$topic_getfeed'); ";
            }
        }
        if ($sql_insert_news)
        {
            mysqli_multi_query($con, $sql_insert_news);
        }
    }
}

include_once (__DIR__ . "/sources.php");

?>
