<?php
$kiosco_news = mysqli_query($con,"SELECT * FROM news WHERE id='$id_get' LIMIT 1");
while($row_kiosco_news = mysqli_fetch_array($kiosco_news))
{
  $id_kiosco_news = $row_kiosco_news["id"];
  $source_kiosco_news = $row_kiosco_news["source"];
	$titulo_kiosco_news = $row_kiosco_news["title"];
	$fecha_kiosco_news = $row_kiosco_news["date"];
	$descripcion_kiosco_news = $row_kiosco_news["description"];
  $imagen_kiosco_news = $row_kiosco_news["image"];
  $texto_kiosco_news = $row_kiosco_news["text"];
  $author_kiosco_news = $row_kiosco_news["author"];
  
  $find=array("’","“","”"); //special characters other then characters starting with & and ending with ;
    $texto_kiosco_news=htmlspecialchars_decode($texto_kiosco_news);
    $texto_kiosco_news=nl2br($texto_kiosco_news);
    $texto_kiosco_news=str_replace($find,"",$texto_kiosco_news);
    $texto_kiosco_news=preg_replace('/\&.*\;/','',$texto_kiosco_news); //Replace all words starting with & and ending with ; with ''
    $texto_kiosco_news=preg_replace("/(\r?\n){2,}/",'', $texto_kiosco_news); //Replace all newline characters with ''    
    $texto_kiosco_news=preg_replace( "/\s+/", " ", $texto_kiosco_news);//Replace multiple spaces with single space
    $texto_kiosco_news=trim($texto_kiosco_news);
  
  
  $texto_kiosco_news = preg_replace('#<br />(\s*<br />)+#', '<br />', $texto_kiosco_news);
  $texto_kiosco_news = preg_replace('/(<a href="[^"]+")>/is','\\1 target="_blank">',$texto_kiosco_news);
  $texto_kiosco_news = preg_replace("%_self%", "_blank", "$texto_kiosco_news");
  $texto_kiosco_news = preg_replace("/<a(.*?)>/", "<a$1 target=\"_blank\">", $texto_kiosco_news);
  $texto_kiosco_news = preg_replace('#(<br\s?/?>)+#', '<br>', $texto_kiosco_news);
  $texto_kiosco_news = str_replace("<br>", "</p><p>", $texto_kiosco_news);;
  $fuente_kiosco_news = $row_kiosco_news["fuente"];
	$etiquetas_kiosco_news = $row_kiosco_news["etiquetas"];
	$url_kiosco_news = $row_kiosco_news["url"];
	
}

?>
<html>

<head>
  <style>

    article .bloque {
      width: 100%;
      padding-top: 0.5rem;
      display: inline-block;
    }
    
    article .textokiosco * {
      text-align: left;
      /*display:inline-block;*/
    }

    article {
      text-align: left;
      font-weight: 500;
      width: 100%;
      display: flex;
      flex-direction: column;
      overflow: hidden;
      flex: 1 auto;
    }

    article iframe {
      float: left;
      max-width: 100%;
      margin: 0rem!important;
      margin-bottom: 1rem!important;
      max-width: 100%!important;
      width: 100%;
      border-radius: 4px;
      box-shadow: none;
      display: block;
      padding: 0px;
    }

    article .article-header {
      width: 100%;
      max-width: 55rem;
      margin: auto;
      max-height: 55vh;
      background-size: cover;
      background-position: top center;
      border: solid 1px #eee;
    }

    article .first-article-header {
      display: inline-flex;
      position: relative;
      width: 100%;
      border: none;
      overflow: hidden;
      padding: 1.5rem 0;
      text-align: center;
    }

    article .first-article-header .bg {
      width: 120%;
      height: 120%;
      position: absolute;
      left: -10%;
      top: -10%;
      filter: blur(50px);
      background-size: cover;
      background-position: center;
      z-index: 0;
    }

    article .first-article-header img {
      z-index: 1;
      max-width: 100%;
      max-height: 15rem;
      width: auto;
      height: auto;
      display: inline-block;
      border-radius: 0.5em;
      position: relative;
      margin: auto;
    }

    article .article-header.second-hero {
      background-attachment: fixed;
      margin-bottom: -2rem;
    }

    article .article-content {
      width: 100%;
      margin: 0 auto;
      display: inline-block;
      text-align: center;
      overflow: hidden;
      overflow-y: auto;
      height: 100%;
    }

    article .article-content .article-meta {
    }

    article .article-content .textokiosco {
      /*white-space: pre-line;*/
      padding: 2rem;
      text-align: left;
      width: 100%;
      font-weight: 500;
    }

    article .article-content .textokiosco figure figcaption {
    width: 100%;
    font-size: 16px;
    padding-top: 0;
    margin: 0;
    padding: .7rem;
    border-radius: 1em;
    }

    article .article-content .textokiosco figure img {
      float: left;
      border-radius: 0;
    }

    article .article-content .textokiosco img {
      margin: 0!important;
      margin-bottom: 1rem!important;
      max-width: 100%;
      max-height: 100%;
      width: auto!important;
      height: auto!important;
      display: inline-block;
      border-radius:1em;
    }

    article .article-content .quote-bubble {
      display: block;
      float: left;
    }

    article .article-content .quote-bubble svg {
      margin: 53px 0 0 -60px;
    }

    article .article-content .textokiosco figure {
      width: 100%;
      background: rgb(var(--PF-color-on-surface), .05);
      border-radius: 1em;
      overflow: hidden;
      margin-bottom: 1rem;
    }

    article .article-content .textokiosco ul {
    padding: .5rem;
    border-radius: 1em;
    border: solid 1px #ddd;
    margin-top: .5rem;
    }

    article .article-content .textokiosco ul li {
      width: 100%;
      padding: .5rem;
    }

    article .article-content .textokiosco .feedflare, article .article-content .textokiosco .feedflare + img,
    article .article-content .textokiosco .related-entries, article .article-content .textokiosco .related-entries + img {
      display: none;
    }

    article .article-content .textokiosco h2,
    article .article-content .textokiosco h3,
    article .article-content .textokiosco h4 {
    margin: 0;
    margin-top: 0.5rem;
    margin-bottom: .5rem;
    }

    article .article-content blockquote {
      padding: 1rem 1rem;
      margin: 1rem;
      background: #333;
      color: white;
      font-size: 1rem;
      text-align: left;
      width: calc(100% - 2rem)!important;
      overflow: hidden;
      border-radius: 0.5em;
      line-height: 1.7em;
    }

    article .article-content blockquote p {
      padding: 0;
    }

    article .article-content blockquote * {
      max-width: 100%!important;
      color: white!important;
    }

    article b {
      font-weight: bold;
    }

    article .headlogo {
    width: 5rem;
    height: 5rem;
    margin: auto;
    margin-top: 0.5rem;
    }

    article .headline {
      font-size: 1.4rem;
      font-weight: 900;
      border: 0 solid transparent;
      padding: 0.5rem;
      align-self: center;
      text-align: center;
      text-decoration: none;
      word-break: break-word;
      -webkit-flex-basis: auto;
      flex-basis: auto;
      line-height: inherit;
      box-sizing: inherit;
      display: inline-block;
      width: calc(100% - 2em);
      max-width: 20em;
    }

    article .article-meta .bloque p {
      font-size: 1rem;
      font-weight: 900;
      border: 0 solid transparent;
      padding: 0.5rem;
      align-self: center;
      text-align: center;
      text-decoration: none;
      word-break: break-word;
      -webkit-flex-basis: auto;
      flex-basis: auto;
      line-height: inherit;
      box-sizing: inherit;
      display: inline-block;
      width: calc(100% - 48px);
    }
    
    article .article-meta  .PF-image + .bloque p {
      
    }

    article .subheadline {
      font-weight: 700;
      font-size: 30px;
      text-transform: uppercase;
      margin-top: 60px;
    }

    article .article-meta {
      text-align: center;
    }

    article .article-meta time {
      font-size: 1rem;
    }

    article .textokiosco p {
    padding: 0;
    font-size: 16px;
    line-height: 1.7em;
    text-align: justify!important;
    text-justify: inter-word;
    font-weight: 500;
    display: inline-block;
    width: 100%;
    word-break: break-word;
    white-space: pre-wrap;
    }
    
    article .textokiosco p:not(:empty) + p {
      padding-top: 1rem;
    }
    
    article .textokiosco p:empty {
      display:none;
    }

    article a span {
      font-weight: 900;
    }

    article blockquote {
      text-align: left;
      /*text-transform: uppercase;*/
      color: #a0a0a0;
      font-weight: 400;
      font-size: 31px;
      margin: 0;
    }

    article twitterwidget {
      width: 100%;
      margin: auto;
    }

    @media (max-width: 75em) {
      article .article-content h2 {
        font-size: 1.5rem;
      }
    }
  </style>
</head>

<body>

  <div class="PF PF-toolbar">
    <?php if(!$subpage){?>
      <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
      <?}?>
      <h1><?=$source_kiosco_news?></h1>
      <a href="<?=$url_kiosco_news?>" target="_blank"><div class="PF PF-icon ripple"><i class="material-icons">open_in_new</i></div></a>
  </div>
  <article>
    <section class="article-content first-article-content">
      <?php if($imagen_kiosco_news != ''){?>
      <header class="article-header first-article-header">
        <div class="bg bgmove" style="background-image:url('<?=$imagen_kiosco_news?>');"></div>
        <img KZoom src="<?=$imagen_kiosco_news?>" class="resized resized KaanaZoom-image PF shadow sh64" />
        <div class="hero"></div>
      </header>
      <?}?>
      <div class="article-meta">
          <div class="bloque">
            <h2 class="headline"><?=$titulo_kiosco_news?></h2>
          </div>
        <?php if(isset($fecha_kiosco_news)){?>
          <div class="bloque"><p><time datetime="<?=$fecha_kiosco_news?>" prettydate><?=$fecha_kiosco_news?></time> <?php if($author_kiosco_news){?>by <span><?=$author_kiosco_news?><?}?></span></p></div>
        <?}?>
      </div>
      <?php if($descripcion_kiosco_news != ''){?><blockquote><?=$descripcion_kiosco_news?></blockquote><?}?>
          <div class="textokiosco">
            <p><?php echo $texto_kiosco_news; ?></p>
          </div>
      <div class="PF PF-toolbar transparent">
        <div reactionsdd content-id="<?=$id_kiosco_news?>" content-app="news"></div>
      </div>
      
    </section>
    
  </article>

</body>

</html>