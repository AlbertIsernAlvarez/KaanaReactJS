<?php

$dom = new DOMDocument();
@$dom->loadHTMLFile($url_kiosco_news);
$nodes = $dom->getElementsByTagName('link');
foreach ($nodes as $node)
{
	if($node->getAttribute('rel') === 'amphtml')
	{
		$amp_url = $node->getAttribute('href');
		$amp = true;
		?>
		<iframe style="width:100%; height:100%; border:none;" allow="<?=$amp_url?>" allowfullscreen="true" allow="autoplay" onerror="ampfailed('<?=$amp_url?>');" src="<?=$amp_url?>"></iframe>

		<script>
		$.get("<?=$amp_url?>").fail(function () { ampfailed('<?=$url_kiosco_news?>'); });
		</script>

		<?
	} else {
		$amp = false;
	}
}

if(!$amp){?>
<script>
  ampfailed(url);
</script>
<?}?>

<script>

	function ampfailed(url){
  window.open(url, "_blank");
  setTimeout(function(){
  $('#body').removeClass('overflowhidden');
  $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closewindowdd');
  $('#ddwindow .PF-progress.loading').show();
  $('#ddwindow').removeClass('open');
  }, 1000);
	}
	
</script>