<html>

<head>
	<link rel="stylesheet" type="text/css" href="//kaana.io/apps/jobs/resources/style.css?version=<?=$cache_version?>">
</head>

<body>

	<div class="kj-h-has-floating-action kj-wrapper">
		<header role="banner" class="kj-header is-visible has-main-nav has-subnav">
			<div class="kj-header__bar">
				<div class="kj-container kj-container--full-width">
					<div class="kj-header__lockup">
						<a href="/" title="Kaana jobs" aria-label="Kaana jobs" class="kj-header__logo">
							<div class="kj-header__product-logo"><span aria-hidden="true" class="kj-header__product-logo-text">Jobs</span></div>
						</a> 
						<a href="#jump-content" class="kj-header__jump-to-content kj-button kj-button--secondary">Skip to content</a></div>
					<nav role="navigation" class="kj-header__nav">
						<ul role="menubar" class="kj-header__nav-list">
							<li role="none" class="kj-header__nav-list-item"><a opendd-href="?app=team" data-gtm-ref="navigation-teams" role="menuitem" t-dd>Team</a></li>
							<li role="none" class="kj-header__nav-list-item"><a opendd-href="?app=jobs&sp=howwehire" data-gtm-ref="navigation-how-we-hire" role="menuitem" t-dd>How we hire</a></li>
							<li role="none" class="kj-header__nav-list-item"><a opendd-href="?app=jobs&sp=students" data-gtm-ref="navigation-students" role="menuitem" t-dd>Students</a></li>
						</ul>
					</nav>
					<div class="kj-header__sign-in-out"><a opendd-href="?app=jobs&sp=profile" data-gtm-ref="navigation-cta-view-applications" rel="noopener" target="_blank" class="kj-header__view-applications-link kj-h-hidden--until-large">My applications</a>
						<div>
							<div class="kj-user-menu">
								<div class="kj-pop-over">
									<button aria-label="User menu" class="kj-pop-over__trigger kj-h-larger-tap-target kj-h-unstyled-button">
										<div class="kj-user-menu__avatar kj-user-menu__avatar--placeholder">
											<svg width="24" height="24" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
												<g>
													<path d="M0 0h24v24H0z" fill="none"></path>
													<path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path>
												</g>
											</svg>
										</div>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kj-header__bar-subnav">
					<div class="kj-container kj-container--full-width">
						<nav role="navigation" class="kj-header__nav kj-h-flex kj-header__nav--align-right">
							<ul role="menubar" class="kj-header__nav-list">
								<li role="none" class="kj-header__nav-list-item"><a opendd-href="?app=jobs&sp=saved" t-dd>Saved jobs</a></li>
								<li role="none" class="kj-header__nav-list-item"><a opendd-href="?app=jobs&sp=alerts" t-dd>Position alerts</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div id="kj-header__drawer" aria-label="Navigation drawer" aria-hidden="true" tabindex="0" class="kj-header__drawer" style="display: none;">
				<div class="kj-header__drawer-content">
					<div class="kj-header__drawer-bar kj-h-flex-row kj-h-flex-row--center">
						<div class="kj-header__lockup kj-h-flex">
							<svg width="100%" height="100%" viewBox="0 0 74 24" aria-label="Kaana" role="img" xmlns="http://www.w3.org/2000/svg" class="kj-header__company-logo-img" alt="Kaana">
								<g>
									<path fill="#4285F4" d="M9.24 8.19v2.46h5.88c-.18 1.38-.64 2.39-1.34 3.1-.86.86-2.2 1.8-4.54 1.8-3.62 0-6.45-2.92-6.45-6.54s2.83-6.54 6.45-6.54c1.95 0 3.38.77 4.43 1.76L15.4 2.5C13.94 1.08 11.98 0 9.24 0 4.28 0 .11 4.04.11 9s4.17 9 9.13 9c2.68 0 4.7-.88 6.28-2.52 1.62-1.62 2.13-3.91 2.13-5.75 0-.57-.04-1.1-.13-1.54H9.24z"></path>
									<path fill="#EA4335" d="M25 6.19c-3.21 0-5.83 2.44-5.83 5.81 0 3.34 2.62 5.81 5.83 5.81s5.83-2.46 5.83-5.81c0-3.37-2.62-5.81-5.83-5.81zm0 9.33c-1.76 0-3.28-1.45-3.28-3.52 0-2.09 1.52-3.52 3.28-3.52s3.28 1.43 3.28 3.52c0 2.07-1.52 3.52-3.28 3.52z"></path>
									<path fill="#4285F4" d="M53.58 7.49h-.09c-.57-.68-1.67-1.3-3.06-1.3C47.53 6.19 45 8.72 45 12c0 3.26 2.53 5.81 5.43 5.81 1.39 0 2.49-.62 3.06-1.32h.09v.81c0 2.22-1.19 3.41-3.1 3.41-1.56 0-2.53-1.12-2.93-2.07l-2.22.92c.64 1.54 2.33 3.43 5.15 3.43 2.99 0 5.52-1.76 5.52-6.05V6.49h-2.42v1zm-2.93 8.03c-1.76 0-3.1-1.5-3.1-3.52 0-2.05 1.34-3.52 3.1-3.52 1.74 0 3.1 1.5 3.1 3.54.01 2.03-1.36 3.5-3.1 3.5z"></path>
									<path fill="#FBBC05" d="M38 6.19c-3.21 0-5.83 2.44-5.83 5.81 0 3.34 2.62 5.81 5.83 5.81s5.83-2.46 5.83-5.81c0-3.37-2.62-5.81-5.83-5.81zm0 9.33c-1.76 0-3.28-1.45-3.28-3.52 0-2.09 1.52-3.52 3.28-3.52s3.28 1.43 3.28 3.52c0 2.07-1.52 3.52-3.28 3.52z"></path>
									<path fill="#34A853" d="M58 .24h2.51v17.57H58z"></path>
									<path fill="#EA4335" d="M68.26 15.52c-1.3 0-2.22-.59-2.82-1.76l7.77-3.21-.26-.66c-.48-1.3-1.96-3.7-4.97-3.7-2.99 0-5.48 2.35-5.48 5.81 0 3.26 2.46 5.81 5.76 5.81 2.66 0 4.2-1.63 4.84-2.57l-1.98-1.32c-.66.96-1.56 1.6-2.86 1.6zm-.18-7.15c1.03 0 1.91.53 2.2 1.28l-5.25 2.17c0-2.44 1.73-3.45 3.05-3.45z"></path>
								</g>
							</svg>
							<div class="kj-header__product-logo"><span class="kj-header__product-logo-text">Jobs</span></div>
						</div>
					</div>
				</div> <a href="https://www.Kaana.com/about/jobs/applications/" data-gtm-ref="navigation-cta-view-applications" rel="noopener" target="_blank" class="kj-header__view-applications-link">My applications</a></div>
			<div id="kj-header__drawer-backdrop" aria-hidden="true"
			class="kj-header__drawer-backdrop kj-h-backdrop" style="display: none;"></div>
		</header>
		<div id="jump-content" tabindex="-1" class="kj-main">
			<div class="kj-guided-search kj-main" role="main">
				<div class="kj-guided-search__wrapper">
					<ol role="presentation" class="kj-guided-search__progress kj-progress-dots">
						<li class="kj-progress-dots__dot kj-h-larger-tap-target kj-progress-dots__dot--active"></li>
						<li class="kj-progress-dots__dot kj-h-larger-tap-target"></li>
						<li class="kj-progress-dots__dot kj-h-larger-tap-target"></li>
					</ol>
					<div class="kj-guided-search__questions">
						<button aria-label="Previous question" data-gtm-ref="guided-search-step-q-previous" type="button" class="kj-button kj-button--secondary kj-button--icon kj-h-hidden--until-medium kj-guided-search__prev" style="display: none;">
							<svg width="9" height="14" viewBox="0 0 9 14" aria-label="Back arrow" role="img" xmlns="http://www.w3.org/2000/svg" class="kj-icon" alt="Back arrow">
								<path d="M8.496 11.333L4.162 7l4.334-4.333L6.444.616.06 7l6.384 6.384z" fill-rule="nonzero"></path>
							</svg>
						</button>
						<ol class="kj-guided-search__steps">
							<li class="kj-guided-search__step kj-guided-search__step--active">
								<div class="kj-guided-search__question-wrapper">
									<h1 class="kj-guided-search__title kj-heading kj-heading--alpha" t-dd>Find your next job.</h1>
									<div class="kj-guided-search__question">
										<label for="q" class="kj-guided-search__question-label kj-heading kj-heading--alpha" t-dd>What do you want to do?</label>
									</div>
								</div>
								<div class="kj-guided-search__answer-wrapper">
									<input t-dd id="jobssearch" placeholder="Software engineering, Design, Sales" autocomplete="off" type="text" class="kj-guided-search__answer">
									<button type="button" class="kj-button kj-button--primary kj-h-hidden--from-medium kj-guided-search__next"><span class="kj-h-larger-tap-target">Next</span></button>
								</div>
							</li>
							<li class="kj-guided-search__step">
								<div class="kj-guided-search__question-wrapper">
									<div class="kj-guided-search__question">
										<p class="kj-guided-search__question-confirmation kj-heading kj-heading--alpha" style="display: none;" t-dd>Sounds good.</p>
										<label for="location" class="kj-guided-search__question-label kj-heading kj-heading--alpha" t-dd>Where do you want to work?</label>
									</div>
								</div>
								<div class="kj-guided-search__answer-wrapper kj-guided-search__answer-wrapper--widget">
									<div><span></span>
										<div class="kj-autocomplete">
											<div class="kj-autocomplete__input-wrapper">
												<svg width="26" height="26" viewBox="0 0 26 26" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-autocomplete__icon kj-icon kj-guided-search__answer-icon">
													<g>
														<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>
														<path d="M0 0h24v24H0z" fill="none"></path>
													</g>
												</svg>
												<input id="location" data-gtm-ref="" placeholder="San Francisco, London, Michigan" type="text" autocomplete="off" class="kj-autocomplete__input kj-traditional-input kj-guided-search__answer kj-guided-search__answer--location">
												<ul class="kj-autocomplete__list kj-h-unstyled-list" style="display: none;">
													<li class="kj-autocomplete__result kj-autocomplete__result--disabled">
														No matches found, try another location
													</li>
												</ul>
											</div>
											<p class="kj-autocomplete__limit-text kj-h-text-align-center" style="display: none;">You can choose 4 locations at a time</p>
										</div>
									</div>
									<button data-gtm-ref="guided-search-step-q-next" type="button" class="kj-button kj-button--primary kj-h-hidden--from-medium kj-guided-search__next"><span class="kj-h-larger-tap-target">Next</span></button>
								</div>
							</li>
							<li class="kj-guided-search__step">
								<div class="kj-guided-search__question-wrapper">
									<div class="kj-guided-search__question">
										<label for="skills" class="kj-guided-search__question-label kj-heading kj-heading--alpha">Any relevant skills or experience you�d like to share?</label>
									</div>
								</div>
								<div class="kj-guided-search__answer-wrapper kj-guided-search__answer-wrapper--widget">
									<input id="skills" name="skills" placeholder="Computer programming, Finance degree, UX" autocomplete="off" type="text" class="kj-guided-search__answer"> <a href="/jobs/results/?company=Kaana&amp;company=YouTube&amp;employment_type=FULL_TIME&amp;jlo=&amp;q=nurse&amp;sort_by=relevance" class="kj-button kj-button--primary kj-h-hidden--from-medium kj-guided-search__next" data-gtm-ref="guided-search-step-finish"
									type="button"><span class="kj-h-larger-tap-target">Finish</span></a></div>
							</li>
						</ol>
						<ul class="kj-guided-search__actions kj-action-group kj-h-unstyled-list kj-h-hidden--until-medium">
							<li class="kj-action-group__item">
								<button data-gtm-ref="guided-search-step-q-next" type="button" class="kj-button kj-button--primary kj-button--raised kj-guided-search__next--desktop">Next</button>
							</li>
							<li class="kj-action-group__item"><a opendd-href="?app=jobs&sp=jobs" class="kj-link" data-gtm-ref="guided-search-skip-to-all-job-results">Skip to jobs</a></li>
						</ul> <a opendd-href="?app=jobs&sp=jobs" class="kj-button kj-button--raised kj-button--secondary kj-h-hidden--from-medium kj-guided-search__skip kj-sticky-button kj-sticky-button--mobile">Skip to results</a></div>
					<div class="kj-guided-search__result" style="top: 343px;">
						<?php
						$available_jobs=mysqli_query($con, "SELECT count(id) AS available FROM jobs");
						while($row_jobs_available=mysqli_fetch_array($available_jobs)) {
						$available_jobs=$row_jobs_available['available'];
						}
						?>
						<div class="kj-guided-search__result-count kj-guided-search__result-count--active PFC-green"><?=$available_jobs?></div>
						<div class="kj-guided-search__result-label"><span>Available jobs</span>
						</div>
						<div><a opendd-href="?app=jobs&sp=jobs" class="kj-button kj-button--primary kj-h-hidden--until-medium">Skip to jobs</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script>

  $("#jobssearch").on("keyup paste submit", function(e) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13') {
        openddgo("?p=search&q=" + $(this).val());
      }
      return false;
      e.preventDefault();
    });
</script>



</body>

</html>