<?php
$job=mysqli_query($con, "SELECT * FROM jobs WHERE id='$id_get' LIMIT 1");
while($row_job_job=mysqli_fetch_array($job)) {
  $id_job=$row_job_job['id'];
  $title_job = $row_job_job['title'];
  $description_job=$row_job_job['description'];
  $about_job=$row_job_job['about'];
  $location_job=$row_job_job['location'];
?>
<div>
	<div>
		<div itemscope="itemscope" itemtype="http://schema.org/JobPosting" class="kj-card kj-card--expanded kj-job-detail kj-job-detail--loaded">
			<div class="kj-card__header kj-job-detail__header">
				<ul class="kj-action-group kj-h-unstyled-list kj-card__actions kj-h-hidden--until-medium">
					<li class="kj-action-group__item">
						<div class="kj-pop-over kj-job-share-pop-over kj-pop-over--show-arrow">
							<button aria-label="Share Interaction Designer" class="kj-pop-over__trigger kj-h-larger-tap-target kj-h-unstyled-button kj-button kj-button--icon kj-button--secondary">
								<svg width="28" height="28" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon kj-icon--share">
									<g>
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path d="M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z"></path>
									</g>
								</svg>
								Share
							</button>
						</div>
					</li>
					<li class="kj-action-group__item">
						<div class="kj-pop-over kj-pop-over--show-arrow">
							<div class="kj-pop-over__trigger">
								<button aria-label="Save job: Interaction Designer" data-gtm-ref="job-results-card-save" class="kj-button kj-button--icon kj-button--secondary kj-action-bookmark kj-h-larger-tap-target">
									<svg width="28" height="28" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
										<g>
											<path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"></path>
											<path d="M0 0h24v24H0z" fill="none"></path>
										</g>
									</svg> <span class="kj-h-hidden--until-small">Save</span></button>
							</div>
						</div>
					</li>
				</ul>
				<h1 itemprop="title" class="kj-card__title kj-job-detail__title kj-heading kj-heading--beta"><?=$title_job?></h1>
				<ul class="kj-job-tags kj-job-detail__tags">
					<li itemprop="hiringOrganization" class="kj-job-tags__team">
						<svg width="18" height="18" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
							<g>
								<path d="M15 11V5l-3-3-3 3v2H3v14h18V11h-6zm-8 8H5v-2h2v2zm0-4H5v-2h2v2zm0-4H5V9h2v2zm6 8h-2v-2h2v2zm0-4h-2v-2h2v2zm0-4h-2V9h2v2zm0-4h-2V5h2v2zm6 12h-2v-2h2v2zm0-4h-2v-2h2v2z"></path>
								<path d="M0 0h24v24H0z" fill="none"></path>
							</g>
						</svg>
						Kaana
					</li>
					<li itemprop="jobLocation" class="kj-job-tags__location">
						<svg width="16" height="18" viewBox="0 0 18 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
							<g>
								<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>
								<path d="M0 0h24v24H0z" fill="none"></path>
							</g>
						</svg>
						<?=$location_job?>
					</li>
					<li itemprop="" class="kj-job-tags__locationsCount">+ 1 more location</li>
				</ul>
				<div class="kj-card__meta kj-job-detail__meta">
					<time datetime="2019-04-11T00:11:01.245Z" aria-label="Published 3 months ago" itemprop="publishDate" class="kj-card__meta-item kj-card__datetime">3 months ago</time>
					<a href="https://www.Kaana.com/about/careers/applications/signin?jobId=CiQAL2FckcUGZ4eD6cmjEkj2Yee5Dj9CEyrWX5xS3IKCHungHJMSOkjPMA3T93-SAp8vzmoG4MpZ811gRCEKRu1fHOssIxQFLEBdnCweTfstpFQHVfC23sWidB2AOxwp8xQ%3D_V2&amp;jobTitle=Interaction+Designer&amp;loc=US"
					aria-label="Apply for this job" data-gtm-ref="job-detail-card-apply-top" rel="noopener" target="_blank" class="kj-button kj-button--primary kj-button--raised kj-button--icon kj-job-detail__header-cta">
						<svg width="17" height="17" viewBox="0 0 17 17" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon kj-icon--left">
							<g>
								<path d="M16.4566369,1.85784717 L15.1442015,0.544782251 C14.4181734,-0.181594084 13.2360507,-0.181594084 12.5100226,0.544782251 L11.1975872,1.85784717 L9.88515179,3.17091208 L0,13.0514927 L0,17 L3.94661427,17 L13.822458,7.11941934 L15.1348934,5.80635442 L16.4473288,4.49328951 C17.182665,3.75760066 17.182665,2.5842235 16.4566369,1.85784717 Z M3.17404591,15.1374966 L1.86161051,15.1374966 L1.86161051,13.8244317 L11.1882791,4.48397699 L12.5007145,5.79704191 L3.17404591,15.1374966 Z"></path>
							</g>
						</svg>
						Apply
					</a>
				</div>
			</div>
			<div class="kj-card__content">
				<div class="kj-job-detail__section kj-job-detail__instruction">
					<div class="kj-job-detail__instruction-icon">
						<svg width="20" height="20" viewBox="0 0 20 20" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
							<path d="M9 15h2V9H9v6zm1-15C4.48 0 0 4.48 0 10s4.48 10 10 10 10-4.48 10-10S15.52 0 10 0zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM9 7h2V5H9v2z" fill-rule="nonzero"></path>
						</svg>
					</div>
					<p class="kj-job-detail__instruction-description"><b>Please include URLs for an online portfolio in addition to a resume. Submissions without a portfolio included will not be considered.</b>
						<br>
						<br>Note: By applying to this job your application is automatically submitted to the following locations: <b>Kirkland, WA, USA; <?=$location_job?></b></p>
				</div>
				<div class="kj-job-detail__section kj-job-detail__section--qualifications">
					<h2 class="kj-h-visually-hidden">Qualifications</h2>
					<div itemprop="qualifications" class="kj-job-qualifications kj-user-generated-content">
						<p>Minimum qualifications:</p>
						<ul>
							<li>BA/BS degree in Design (e.g., interaction, graphic, visual communications, product, industrial), HCI, CS or a related field or equivalent practical experience.</li>
							<li>3 years of experience in a related field.</li>
						</ul>
						<br>
						<p>Preferred qualifications:</p>
						<ul>
							<li>MA or MS degree in Design (e.g., interaction, graphic, visual communications, product, industrial), Human-Computer Interaction, Computer Science, or a related field.</li>
							<li>Demonstrated experience in designing usable web-based interfaces.</li>
							<li>Expert HTML skills.</li>
							<li>Strong, clean visual design sense.</li>
							<li>Knowledge of JavaScript for rapid prototyping purposes.</li>
							<li>Excellent leadership, communication and teamwork skills.</li>
						</ul>
					</div>
				</div>
				<div class="kj-job-detail__section kj-job-detail__section--description">
					<div class="kj-accordion__item kj-accordion__item--active">
						<div role="heading" aria-level="2">
							<div id="about" aria-controls="accordion-about" aria-expanded="true" role="button" tabindex="0" class="kj-accordion__header">
								<h2>About the job</h2>
							</div>
						</div>
						<div aria-expanded="true" aria-labelledby="about" id="accordion-about" role="region" class="kj-accordion__content">
							<div class="kj-accordion__content-inner">
								<div>
									<div itemprop="description" class="kj-user-generated-content">
										<p><?=$about_job?></p>
										<div>
											<br>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kj-job-detail__section kj-job-detail__section--responsibilities">
					<div class="kj-accordion__item kj-accordion__item--active">
						<div role="heading" aria-level="2">
							<div id="responsibilities" aria-controls="accordion-responsibilities" aria-expanded="true" role="button" tabindex="0" class="kj-accordion__header">
								<h2>Responsibilities</h2>
							</div>
						</div>
						<div aria-expanded="true" aria-labelledby="responsibilities" id="accordion-responsibilities" role="region" class="kj-accordion__content">
							<div class="kj-accordion__content-inner">
								<div>
									<div itemprop="responsibilities" class="kj-user-generated-content">
										<ul>
											<li>Help to define the user model and user interface for new and existing Kaana products and features.</li>
											<li>Develop high level and/or detailed storyboards, mockups and prototypes to effectively communicate interaction and design ideas.</li>
											<li>Gauge the usability of new and existing products and making constructive suggestions for change.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kj-job-detail__section kj-job-detail__section--location">
					<div class="kj-accordion__item kj-accordion__item--active">
						<div role="heading" aria-level="2">
							<div id="location" aria-controls="accordion-location" aria-expanded="true" role="button" tabindex="0" class="kj-accordion__header">
								<h2>Location</h2>
							</div>
						</div>
						<div aria-expanded="true" aria-labelledby="location" id="accordion-location" role="region" class="kj-accordion__content">
							<div class="kj-accordion__content-inner">
								<div>
									<div class="kj-office-map">
										<a href="https://www.Kaana.com/maps/search/?api=1&amp;query=Seattle%2C%20WA%2C%20USA" aria-label="View location on Kaana Maps" data-gtm-ref="job-detail-office-location-map" rel="noopener" target="_blank" class="kj-office-map__location-link kj-job-detail__location-link">
											<picture>
												<source srcset="https://maps.Kaanaapis.com/maps/api/staticmap?center=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;key=AIzaSyBt7dRu-48vSk1eg6v4bZbTNs43pOKiS40&amp;maptype=roadmap&amp;markers=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;size=1168x324&amp;zoom=12&amp;signature=ps_sPvkjgFUcz72GpSwau1rebxc=, https://maps.Kaanaapis.com/maps/api/staticmap?center=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;key=AIzaSyBt7dRu-48vSk1eg6v4bZbTNs43pOKiS40&amp;maptype=roadmap&amp;markers=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;scale=2&amp;size=1024x324&amp;zoom=12&amp;signature=LGHj2cxqA3QhLZJTwZmYSkwFGho= 2x"
												media="(min-width: 600px)">
												<source srcset="https://maps.Kaanaapis.com/maps/api/staticmap?center=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;key=AIzaSyBt7dRu-48vSk1eg6v4bZbTNs43pOKiS40&amp;maptype=roadmap&amp;markers=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;size=600x400&amp;zoom=12&amp;signature=D1jnhP7KcjQ4mOvW3HCzt_s7wc8=, https://maps.Kaanaapis.com/maps/api/staticmap?center=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;key=AIzaSyBt7dRu-48vSk1eg6v4bZbTNs43pOKiS40&amp;maptype=roadmap&amp;markers=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;scale=2&amp;size=600x400&amp;zoom=12&amp;signature=LHlLQ_7VV0axWi1ctKvQwOcu_UM= 2x"> <img src="https://maps.Kaanaapis.com/maps/api/staticmap?center=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;key=AIzaSyBt7dRu-48vSk1eg6v4bZbTNs43pOKiS40&amp;maptype=roadmap&amp;markers=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;size=1168x324&amp;zoom=12&amp;signature=ps_sPvkjgFUcz72GpSwau1rebxc="
												srcset="https://maps.Kaanaapis.com/maps/api/staticmap?center=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;key=AIzaSyBt7dRu-48vSk1eg6v4bZbTNs43pOKiS40&amp;maptype=roadmap&amp;markers=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;size=1168x324&amp;zoom=12&amp;signature=ps_sPvkjgFUcz72GpSwau1rebxc= ,https://maps.Kaanaapis.com/maps/api/staticmap?center=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;key=AIzaSyBt7dRu-48vSk1eg6v4bZbTNs43pOKiS40&amp;maptype=roadmap&amp;markers=601+N.+34th+Street%2C+Seattle%2C+WA+98103%2C+USA&amp;scale=2&amp;size=1024x324&amp;zoom=12&amp;signature=LGHj2cxqA3QhLZJTwZmYSkwFGho=  2x"
												alt="Map showing <?=$location_job?>"></picture>
										</a>
										<a href="https://www.Kaana.com/maps/search/?api=1&amp;query=Seattle%2C%20WA%2C%20USA" data-gtm-ref="job-detail-office-location-address" rel="noopener" target="_blank" class="kj-button kj-button--icon kj-button--secondary kj-job-detail__location-address-link">
											<svg width="24" height="24" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
												<g>
													<path d="M21.71 11.29l-9-9a.996.996 0 0 0-1.41 0l-9 9a.996.996 0 0 0 0 1.41l9 9c.39.39 1.02.39 1.41 0l9-9a.996.996 0 0 0 0-1.41zM14 14.5V12h-4v3H8v-4c0-.55.45-1 1-1h5V7.5l3.5 3.5-3.5 3.5z"></path>
													<path d="M0 0h24v24H0z" fill="none"></path>
												</g>
											</svg> <span><?=$location_job?></span>
											<svg width="18" height="18" viewBox="0 0 12 14" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon kj-icon--next">
												<path d="M8.496 11.333L4.162 7l4.334-4.333L6.444.616.06 7l6.384 6.384z" fill-rule="nonzero"></path>
											</svg>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kj-job-detail__section kj-font-size-small">
					<p t-dd>All jobs shown are not remunerated.</p>
				</div>
				<div class="kj-job-detail__section--apply-bottom-container">
					<a href="https://www.Kaana.com/about/careers/applications/signin?jobId=CiQAL2FckcUGZ4eD6cmjEkj2Yee5Dj9CEyrWX5xS3IKCHungHJMSOkjPMA3T93-SAp8vzmoG4MpZ811gRCEKRu1fHOssIxQFLEBdnCweTfstpFQHVfC23sWidB2AOxwp8xQ%3D_V2&amp;jobTitle=Interaction+Designer&amp;loc=US"
					aria-label="Apply for this job" data-gtm-ref="job-detail-card-apply-bottom" rel="noopener" target="_blank" class="kj-button kj-button--primary kj-button--raised kj-button--icon kj-job-detail__floating-cta kj-sticky-button">
						<svg width="17" height="17" viewBox="0 0 17 17" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon kj-icon--left">
							<g>
								<path d="M16.4566369,1.85784717 L15.1442015,0.544782251 C14.4181734,-0.181594084 13.2360507,-0.181594084 12.5100226,0.544782251 L11.1975872,1.85784717 L9.88515179,3.17091208 L0,13.0514927 L0,17 L3.94661427,17 L13.822458,7.11941934 L15.1348934,5.80635442 L16.4473288,4.49328951 C17.182665,3.75760066 17.182665,2.5842235 16.4566369,1.85784717 Z M3.17404591,15.1374966 L1.86161051,15.1374966 L1.86161051,13.8244317 L11.1882791,4.48397699 L12.5007145,5.79704191 L3.17404591,15.1374966 Z"></path>
							</g>
						</svg>
						Apply
					</a>
				</div>
			</div>
		</div>
		<div class="kj-skeleton">
			<div class="kj-card__header">
				<ul class="kj-action-group kj-h-unstyled-list kj-card__actions">
					<li class="kj-action-group__item">
						<div class="kj-pop-over kj-job-share-pop-over kj-pop-over--show-arrow">
							<button aria-label="Share Interaction Designer" class="kj-pop-over__trigger kj-h-larger-tap-target kj-h-unstyled-button kj-button kj-button--icon kj-button--secondary">
								<svg width="28" height="28" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon kj-icon--share">
									<g>
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path d="M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z"></path>
									</g>
								</svg>
								Share
							</button>
						</div>
					</li>
					<li class="kj-action-group__item">
						<div class="kj-pop-over kj-pop-over--show-arrow">
							<div class="kj-pop-over__trigger">
								<button aria-label="Save job: Interaction Designer" data-gtm-ref="job-results-card-save" class="kj-button kj-button--icon kj-button--secondary kj-action-bookmark kj-h-larger-tap-target">
									<svg width="28" height="28" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
										<g>
											<path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"></path>
											<path d="M0 0h24v24H0z" fill="none"></path>
										</g>
									</svg> <span class="kj-h-hidden--until-small">
Save
</span></button>
							</div>
						</div>
					</li>
				</ul>
				<h2 itemprop="title" class="kj-card__title kj-heading kj-heading--beta">
Interaction Designer
</h2>
				<ul class="kj-job-tags kj-h-flex">
					<li itemprop="hiringOrganization" class="kj-job-tags__team">
						<svg width="18" height="18" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
							<g>
								<path d="M15 11V5l-3-3-3 3v2H3v14h18V11h-6zm-8 8H5v-2h2v2zm0-4H5v-2h2v2zm0-4H5V9h2v2zm6 8h-2v-2h2v2zm0-4h-2v-2h2v2zm0-4h-2V9h2v2zm0-4h-2V5h2v2zm6 12h-2v-2h2v2zm0-4h-2v-2h2v2z"></path>
								<path d="M0 0h24v24H0z" fill="none"></path>
							</g>
						</svg>
						Kaana
					</li>
					<li itemprop="jobLocation" class="kj-job-tags__location">
						<svg width="16" height="18" viewBox="0 0 18 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
							<g>
								<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>
								<path d="M0 0h24v24H0z" fill="none"></path>
							</g>
						</svg>
						<?=$location_job?>
					</li>
					<li itemprop="" class="kj-job-tags__locationsCount">
						+ 1 more location
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="kj-job kj-site-footer__job-jobs--condensed" style="">
	<footer class="kj-site-footer--condensed">
		<div class="kj-site-footer__legal-policies--condensed">
			<ul role="list" class="kj-site-footer__legal-links">
				<li class="kj-site-footer__legal-link"><a opendd-href="?app=privacy" data-gtm-ref="footer-privacy-link" aria-label="Click to learn more about our privacy terms">Privacy</a></li>
				<li class="kj-site-footer__legal-link"><a opendd-href="?app=terms" data-gtm-ref="footer-terms-link" aria-label="Click to learn more about our terms and conditions">Terms</a></li>
			</ul>
		</div>
	</footer>
</div>
<?}?>