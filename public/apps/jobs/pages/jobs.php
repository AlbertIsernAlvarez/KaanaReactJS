<html>
<head>
	<link rel="stylesheet" type="text/css" href="//kaana.io/apps/jobs/resources/style.css?version=<?=$cache_version?>">
</head>

<body>
	
	<div class="kj-wrapper">
		<div id="jump-content" tabindex="-1" class="kj-main">
			<div class="kj-l-split">
				<aside class="kj-l-split__sidebar kj-sidebar kj-sidebar--header-hidden kj-sidebar--has-subnav kj-h-hidden--until-medium">
					<div class="kj-sidebar__inner">
						<div>
							<div class="kj-sidebar__header kj-sidebar--job-details kj-h-flex-row">
								<div>
									<button class="kj-sidebar__header-button kj-button--secondary kj-h-flex router-link-active kj-button kj-button--icon kj-button--back kj-h-larger-tap-target" onclick="window.history.back();">
										<svg width="24" height="24" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
											<g>
												<path d="M0 0h24v24H0z" fill="none"></path>
												<path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path>
											</g>
										</svg>
										<span t-dd>Back</span>
									</button>
								</div>
							</div>
							<div class="kj-sidebar__content">
								<ul id="search-results-sidebar" class="kj-search-results-sidebar kj-h-unstyled-list">
									<?php
									if($id_get){$select_jobs = "id='$id_get'"; } else { $select_jobs = "id!=''"; }
$job=mysqli_query($con, "SELECT * FROM jobs WHERE $select_jobs");
while($row_job_job=mysqli_fetch_array($job)) {
  $id_job=$row_job_job['id'];
  $title_job = $row_job_job['title'];
  $description_job=$row_job_job['description'];
  $about_job=$row_job_job['about'];
  $location_job=$row_job_job['location'];
?>
									<li class="kj-search-results-sidebar__item">
										<a opendd-href="?app=jobs&sp=jobs&id=<?=$id_job?>"
										class="kj-card router-link-exact-active kj-card--active kj-card--small" aria-label="Interaction Designer" itemscope="itemscope" itemtype="http://schema.org/JobPosting" data-gtm-vis-first-on-screen-10240128_28="553425" data-gtm-vis-total-visible-time-10240128_28="100"
										data-gtm-vis-has-fired-10240128_28="1" data-gtm-vis-first-on-screen-10583280_28="553441" data-gtm-vis-total-visible-time-10583280_28="100" data-gtm-vis-has-fired-10583280_28="1">
											<div class="kj-card__header">
												<ul class="kj-action-group kj-h-unstyled-list kj-card__actions">
													<li class="kj-action-group__item">
														<div class="kj-pop-over kj-pop-over--show-arrow">
															<div class="kj-pop-over__trigger">
																<button aria-label="Save job: Interaction Designer" data-gtm-ref="job-results-card-save" class="kj-button kj-button--icon kj-button--secondary kj-action-bookmark kj-h-larger-tap-target">
																	<svg width="18" height="18" viewBox="0 0 24 24" role="presentation" xmlns="http://www.w3.org/2000/svg" class="kj-icon">
																		<g>
																			<path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"></path>
																			<path d="M0 0h24v24H0z" fill="none"></path>
																		</g>
																	</svg>
																</button>
															</div>
														</div>
													</li>
												</ul>
												<h2 itemprop="title" class="kj-card__title kj-heading kj-heading--gamma"><?=$title_job?></h2>
												<div class="kj-h-flex-row kj-h-flex-row--center">
													<ul class="kj-job-tags kj-h-flex kj-job-tags--no-icons">
														<li>Kaana</li>
														<li><?=$location_job?></li>
													</ul>
													<div class="kj-card__meta kj-h-text-align-right">
														<time datetime="2019-04-11T00:11:01.245Z" itemprop="publishDate" aria-label="Published 3 months ago" class="kj-card__meta-item kj-card__datetime">3 months ago</time>
													</div>
												</div>
											</div>
										</a>
									</li>
									<?}?>
								</ul>
								<div class="kj-sidebar__pagination">
									<div class="kj-h-flex-row">
										<p class="kj-h-flex">
											Page 1
										</p>
										<div class="kj-action-group kj-action-group--center">
											<a class="kj-link kj-link--on-grey kj-action-group__item kj-h-larger-tap-target" t-dd>Previous</a>
											<a class="kj-link kj-link--on-grey kj-action-group__item kj-h-larger-tap-target" t-dd>Next</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</aside>
				<main class="kj-l-split__main kj-h-flex kj-h-flex-column" id="job">
					<script>$('#job').load('./apps/jobs/resources/job.php?<?=$server_querystring;?>');</script>
				</main>
			</div>
		</div>
	</div>



</body>

</html>