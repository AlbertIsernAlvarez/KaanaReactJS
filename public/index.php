<!DOCTYPE html>
<?php
if(!$window_id){
    $window_id = mt_rand(1000, 1000000000);
}
include_once(__DIR__."/resources/meta.php");
?>

<html>
<head>
  <?php 
  include_once(__DIR__."/meta-info.php"); ?>
</head>
<body translate="no" id="body" class="bodydd <?php if(isset($_COOKIE['darkmode']) && $_COOKIE['darkmode']){?>PF-dark<?}?>" <?php if($window){?>window="<?=$window?>"<?}?> <?php if($client){?>client="<?=$client?>"<?}?>>
  <script>
    var windowid = "<?=$window_id?>";
    var xhr = [];
  </script>
  <script src="/resources/js/jquery-3.4.1.min.js"></script>
  <script src="/resources/js/jquery-ui.min.js"></script>
  <script src="/resources/js/jquery.initialize.min.js"></script>

  <script src="/resources/js/paperflower.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/dynamicManifest.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/PF-alerts.js?version=<?=$cache_version?>"></script>
  <script src="/resources/js/autosize.js"></script>
  <script async defer src="/resources/js/moment.min.js"></script>
  <script async defer src="/resources/js/zoom.min.js"></script>
  <script async defer src="/resources/js/prettydate.js"></script>
  <script async src="/resources/js/jquery.visible.min.js"></script>
  <script async src="/resources/js/jquery.form.min.js"></script>
  <script async defer src="/resources/js/memories.js?version=<?=$cache_version?>"></script>
  <script async defer src="/resources/feed/initialize.js?version=<?=$cache_version?>"></script>
  
  <!----DD FUNCTIONS---->
  <script defer src="/resources/dd-functions/opendd.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/no-ios.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/language-name.js?version=<?=$cache_version?>"></script>
  
  <script>var language = "<?=$language?>";</script>
  <script async defer src="/resources/dd-functions/loading.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/t-dd.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/rgdd.js?version=<?=$cache_version?>"></script>
  <script async src="/resources/dd-functions/searchdd.js?version=<?=$cache_version?>"></script>
  <script async src="/resources/dd-functions/repeated-br.js?version=<?=$cache_version?>"></script>
  <script async defer src="/resources/dd-functions/lastactivity.js?version=<?=$cache_version?>"></script>
  <script async defer src="/resources/dd-functions/location.js?version=<?=$cache_version?>"></script>
  <script async defer src="/resources/dd-functions/fileSelector.js?version=<?=$cache_version?>"></script>
  <script async defer src="/resources/more-less/initialize.js?version=<?=$cache_version?>"></script>
  <script async defer src="/resources/dd-functions/followButton.js?version=<?=$cache_version?>"></script>
  
  <?php if($_GET['window'] === 'app' and $_GET['client'] === 'android'){?>
  <script async src="/resources/dd-functions/android-sound.js?version=<?=$cache_version?>"></script>
  <?}
  
  include_once(__DIR__."/resources/logo-loading.php");
  include_once(__DIR__."/main.php");
  include_once(__DIR__."/resources/ripple.php");
  if($user_mismo_id){
    include_once(__DIR__."/resources/highlight/highlight.php");
    include_once(__DIR__."/resources/glance/initialize.php");
    include_once(__DIR__."/resources/reactions/js.php");
  }

  if($_GET['window'] !== 'app' and $_GET['client'] !== 'android'){
    include_once(__DIR__."/resources/offline.php");
  }
  include_once(__DIR__."/resources/funciones/darkmode.php");
  ?>

<script src="/upup.min.js"></script>
<script>
UpUp.start({
  'content-url': 'offline.php',
  'assets': ['favicon.ico', 'imgs/logo/logo.svg', 'imgs/logo/logo_app.png', 'resources/css/paperflower.css', 'resources/css/paperflower/colors.css', 'resources/css/paperflower/button.css', 'resources/css/paperflower/progress.css', 'resources/css/style.css'],
  'service-worker-url': '/upup.sw.min.js'
});
</script>

</body>

</html>