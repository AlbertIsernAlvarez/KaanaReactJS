
  <meta name="trustpilot-one-time-domain-verification-id" content="8weA4adqc8kGLX6gVdQdbQZdJbI1ebq6QQ306XfY" />
  <meta name="google-site-verification" content="L0T0_6VCNrT3slPROGy2XjzhW8lqVCYTBZ-l5XnEm1U" />
  <link rel="manifest" href="/manifest.json">
  <meta name="application-name" content="Kaana">

  <title itemprop='name'>Kaana</title>
  <meta name="description" content="Discover amazing things and connect with passionate people.">
  <meta name="theme-color" content="white">

  <!-- Twitter Card data -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="Kaana">
  <meta name="twitter:title" content="Kaana">
  <meta name="twitter:description" content="Discover amazing things and connect with passionate people.">
  <meta name="twitter:image:src" content="//img.kaana.io/logo/logometa.png">
  <!-- Open Graph data -->
  <meta property="og:title" content="Kaana" />
  <meta property="og:author" content="Kaana" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="//kaana.io/" />
  <meta property="og:image" content="//img.kaana.io/logo/logometa.png" />
  <meta property="og:description" content="Discover amazing things and connect with passionate people.">
  <meta property="og:site_name" content="Kaana" />


  <meta property="og:type" content="website">
  <link rel="canonical" href="//kaana.io/">
  <link rel=”alternate” hreflang=”en” href=”//kaana.io/l/en”>
  <link rel=”alternate” hreflang=”es” href=”//kaana.io/l/es”>
  <link rel=”alternate” hreflang=”fr” href=”//kaana.io/l/fr”>
  <link rel=”alternate” hreflang=”ca” href=”//kaana.io/l/ca”>
  <link rel=”alternate” hreflang=”ar” href=”//kaana.io/l/ar”>
  <link rel=”alternate” hreflang=”tr” href=”//kaana.io/l/tr”>
  <link rel=”alternate” hreflang=”ku” href=”//kaana.io/l/ku”>
  <link rel=”alternate” hreflang=”gr” href=”//kaana.io/l/gr”>