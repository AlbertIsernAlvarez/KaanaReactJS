<?php

//ALLOWED PAGES ANONYMOUS
$allowed_pages_anonymous = array("login", "anonymous", "register", "recover", "error", "information", "donate", "feedback-support", "languages", "help", "common-passwords", "lastest-updates", "about", "about-old", "applications", "team", "countdown", "bugs-report", "privacy", "terms", "gamma", "accounts", "search", "profile", "about-new", "invitation");

//ALLOWED PAGES ANONYMOUS ONLY IF NO USER LOGGED IN
$allowed_pages_onlynotloggedin = array("login", "anonymous", "register", "recover", "error", "countdown", "gamma");

//NOT ALLOWED APPS IF ANONYMOUS
$not_allowed_apps_anonymous = array("pay", "photos", "files", "jobs", "home");

if($app and !in_array($app, $not_allowed_apps_anonymous) or $app and $user_mismo_id){
  $tipo_screen = 'apps';
  $screen_dd = $app;
} elseif($pagina and in_array($pagina, $allowed_pages_anonymous) or $pagina and $user_mismo_id){
  $tipo_screen = 'pantallas';
  $screen_dd = $pagina;
} elseif($dev){
  $tipo_screen = 'dev';
  $screen_dd = $dev;
} else {
  if($user_id_mismo){
    $pagina = "home";
    $tipo_screen = "pantallas";
    $screen_dd = $pagina;
	} else {
    if(!$_COOKIE['about']){
    $pagina = "about";
    } else {
      $pagina = "login";
    }
    $tipo_screen = "pantallas";
    $screen_dd = $pagina;
	}
}

$directorio_app = "." . getcwd();
if($user_id_mismo){
  include_once(__DIR__."/resources/user/activity.php");
}
?>

<html>
	<head>
    <style>
      .<?=$screen_dd?>-pageelementhide {
        display:none!important;
      }
    </style>
	</head>
	<body>
    <script>
      $(function() {
        $('#meta-info').load('/meta-info.php?<?=$server_querystring;?>');
        <?php if($pagina != 'search' and $q){?>
        setTimeout(function(){ openddgo('?p=search&q=<?=$q?>'); }, 1000);
        <?} elseif($pagina === 'search' and $q){?>
        $('#buscador').val('<?=$q?>');
        <?}?>
      });
    </script>
    
    <div class="dd_screen" >
      <?php
      if($subpage and file_exists(__DIR__."/$tipo_screen/$screen_dd/pages/$subpage/index.php")){
        include_once(__DIR__."/$tipo_screen/$screen_dd/pages/$subpage/index.php");
      } elseif(file_exists(__DIR__."/$tipo_screen/$screen_dd/index.php")) {
        include_once(__DIR__."/$tipo_screen/$screen_dd/index.php");
      } else {
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        include_once(__DIR__."/pantallas/hole/index.php");
      }
      
      if(file_exists(__DIR__."/$tipo_screen/$screen_dd/menu.php")){?>
      <script> $('.side-menu-left #menuleft').empty().load('<?=$tipo_screen?>/<?=$screen_dd?>/menu.php?<?=$server_querystring?>'); </script>
      <?} elseif($pagina or $app){?>
      <script> $('.side-menu-left #menuleft').empty().load('./resources/menus/default.php?<?=$server_querystring?>'); </script>
      <?}?>
    </div>
  </body>
</html>