{
  "schemaVersion": 1,
  "label": "Version",
  "message": "<?=ucwords($last_version_channel)?> <?=$last_version_name?> (<?=$last_version_number?>)",
  "color": "orange"
}