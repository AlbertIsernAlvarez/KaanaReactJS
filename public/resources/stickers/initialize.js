$.initialize("[sticker]", function() {
  var thisSticker = $(this);
  var id = thisSticker.attr("data-id");
  if(id) {
  	$.post("./resources/stickers/sticker.php", {
  		id: id
  	})
  	.done(function(data) {
  		thisSticker.replaceWith(data);
  	});
  }
});