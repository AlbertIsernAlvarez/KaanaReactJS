<html>

<head>
  <style>

  </style>
</head>

<body>
  <div class="highlight" id="highlight">
    <span class="logo_highlight"></span>
    <div class="content" id="highlight_content"></div>
  </div>
  <script>
    $(document).ready(function() {
        var highlight = $('#highlight');
        $(highlight).on('click', function(e) {
          $(highlight).fadeOut();
        });


        var timeouthighlight;
        $(document.body).on('mousedown mouseup dblclick mousemove', function(evt) {
          clearTimeout(timeouthighlight);
          timeouthighlight = setTimeout(function() {
            if (document.getSelection() != '') {
              var s = document.getSelection(),
                r = s.getRangeAt(0);
              if (r && s.toString()) {
                var p = r.getBoundingClientRect();

                if (p.left || p.top) {
                  highlight.css({
                    left: (p.left + (p.width / 2)) - (highlight.width() / 2),
                    top: (p.top - highlight.height() - 5),
                    display: 'flex'
                  });
                }

                var highlight_text = s.toString().replace(/<\/?[^>]+(>|$)/g, "");

                $.ajax({
                  type: "POST",
                  url: "./resources/highlight/results.php",
                  data: "text=" + highlight_text + "&<?=$server_querystring?>",
                  success: function(data) {
                    $("#highlight_content").html(data);
                    highlight.fadeIn();
                    return false;
                  }
                });


              }

            } else {
              highlight.fadeOut();
            }
          }, 500);
        });
        $(window).scroll(function() {
          if (highlight.is(":visible")) {
            window.getSelection().removeAllRanges();
            setTimeout(function() {highlight.fadeOut();}, 500);
          }
        });
      }

    );

    function copyhighlight() {
      document.execCommand('copy');
      alertdd.show('Copied');
      window.getSelection().removeAllRanges();
    }
  </script>
</body>

</html>