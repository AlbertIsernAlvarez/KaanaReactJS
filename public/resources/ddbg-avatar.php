<style>
  .ddbg-avatar {
    background: radial-gradient(at 0 0, #2196F3 0, rgba(255, 255, 255, 0) 70%, rgba(255, 255, 255, 0) 100%), radial-gradient(circle at 170% 0, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 0) 39%, #00BCD4 77%, #E91E63 92%, #F44336 100%), linear-gradient(135deg, #2196F3 0, #2196F3 24%, #9C27B0 100%);
    background-image: url('<?=$user_mismo_avatar?>');
    background-size: cover;
    background-position: center;
  }
</style>