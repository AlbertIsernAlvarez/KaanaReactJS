<?php


if(!$user_nameid and !$user_id){
  $user_id = $user_mismo_id;
  $user_nameid = $user_mismo;
}

if($user_id or $user_nameid){
$user_sql = mysqli_query($con, "SELECT * FROM users WHERE user_id='$user_id' OR username='$user_nameid' LIMIT 1");
while($row_user=mysqli_fetch_array($user_sql)) {
  $user_id = $row_user["user_id"];
  $user_status = $row_user["status"];
	$user = $row_user["username"];
	$user_username = $row_user["username"];
	$user_ciudad = $row_user["ciudad"];
	$user_trabajo = $row_user["trabajo"]; 
	$user_escuela = $row_user["escuela"]; 
	$user_biography = $row_user["biography"];
	$user_telefono = $row_user["telefono"];
	$user_name = $row_user["name"];
	$user_lastname = $row_user["lastname"];
	$user_email = $row_user["email"];
  $user_color = $row_user["color"];
	$user_avatar = $row_user["avatar"];
  $user_gender = $row_user["gender"];
	$user_ip = $_SERVER['REMOTE_ADDR'];
	$user_nacimiento = $row_user["fecha_nacimiento"];
	$user_web = $row_user["website"];
	$user_verificado = $row_user["verificado"];
	$user_lastactivity = $row_user["lastactivity"];
	if(round(abs(strtotime($datetime) - strtotime($user_lastactivity)) / 60) <= 5){
		$user_lastactivity = "online";
	}
	$user_estado = $row_user["estado"];
	$user_estado = replaceemojis($user_estado);
  $user_signup_date = $row_user["signup_date"]; 
	$user_bloqueado = $row_user["bloqueado"];
	$user_timezone = $row_user["timezone"];

	$user_language_array = json_decode($row_user["language"], true);
	$user_language = $user_language_array[0];
	
  $user_languages = json_decode($row_user["languages"], true);
  if(is_array($user_languages)){
  	$user_languages = array_unique(array_merge($user_language_array, $user_languages));
  	$user_languages = implode(", ", $user_languages);
	}
  
  $user_estado = preg_replace("/((http|https|www)[^\s]+)/", '<a target="_blank" href="$1">$0</a>', $user_estado);
  $user_estado = preg_replace("/href=\"www/", 'href="http://www', $user_estado);
  $user_estado = preg_replace("/(@[^\s]+) /", '<a opendd-href="?p=profile&u=$1">$0</a>', $user_estado);
  $user_estado = preg_replace("/#([^\s]+)/", '<a opendd-href="?p=topics&hashtag=$1">$0</a>', $user_estado);
  $user_estado = preg_replace("/&q=#/", '&q=', $user_estado);
  
  if($user_name) {$user_name = ucwords($user_name);} else {$user_name = ucwords($user);}

if($user_avatar) {
	$user_avatar = "//" . $settings_domain . "/apps/photos/see.php?id=" . $user_avatar;
} else {
	$user_avatar = "//kaana.io/apps/photos/see.php?id=user.svg";
}


$user_karma_total = mysqli_query($con, "SELECT SUM(amount) AS total FROM karma WHERE user_id='$user_id' LIMIT 1") or die();
while($row_user_karma_total=mysqli_fetch_array($user_karma_total)) {
	$user_karma = $row_user_karma_total["total"];
	$user_karma = number_format($user_karma);
}

$user_privatelist = array();
$user_privatelist_sql = mysqli_query($con, "SELECT * FROM user_lists WHERE user_id='$user_id' LIMIT 1") or die();
while($user_privatelist_row=mysqli_fetch_array($user_privatelist_sql)) {
	$user_privatelist[] = $user_privatelist_row["contentid"];
}
  
  if(!$user_color){
    $user_color = "blue";
  }

}

if($user_nameid and !$user_id){
  header("Location: /");
}
}


?>