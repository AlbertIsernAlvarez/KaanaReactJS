<?php
if(!isset($_SESSION)){ session_start(); }

if(!isset($_SESSION["users"])){
	$_SESSION["users"] = array();
}

if(isset($_SESSION["users"]["current"])){
$session_current = $_SESSION["users"]["current"];
}

$user_mismo_timezone = "Europe/Madrid";

$user_same = mysqli_query($con, "SELECT * FROM users WHERE user_id='$session_current' AND user_id IS NOT NULL LIMIT 1") or die();
while($row_user_same=mysqli_fetch_array($user_same)) {
  $user_mismo_id = $row_user_same["user_id"];
  $user_mismo_name = $row_user_same["name"];
	$user_mismo_lastname = $row_user_same["lastname"];
	$user_mismo = $row_user_same["username"];
	$user_mismo_username = $row_user_same["username"];
	$user_mismo_password = $row_user_same["password"];
	$user_mismo_status = $row_user_same["status"];
	$user_mismo_ciudad = $row_user_same["city"];
	$user_mismo_trabajo = $row_user_same["trabajo"]; 
	$user_mismo_biography = $row_user_same["biography"];
	$user_mismo_email = $row_user_same["email"];
  $user_mismo_color = $row_user_same["color"];
	$user_mismo_avatar = $row_user_same["avatar"];
  $user_mismo_avatar_id = $user_mismo_id;
  $user_mismo_gender = $row_user_same["gender"];
	$user_mismo_ip = $_SERVER['REMOTE_ADDR'];
	$user_mismo_birth_date = $row_user_same["fecha_nacimiento"];
	$user_mismo_web = $row_user_same["website"];
	$user_mismo_verificado = $row_user_same["verificado"];
	$user_mismo_lastactivity = $row_user_same["lastactivity"];
	$user_mismo_estado = $row_user_same["estado"];
  $user_mismo_signup_date = $row_user_same["signup_date"]; 
	$user_mismo_bloqueado = $row_user_same["bloqueado"];
	if($row_user_same["timezone"]){ $user_mismo_timezone = $row_user_same["timezone"]; }
	$user_mismo_coordinates = $row_user_same["coordinates"];
	$user_mismo_channel = $row_user_same["channel"];
	
	$user_mismo_language_array = json_decode($row_user_same["language"], true);
	$user_mismo_language = $user_mismo_language_array[0];
	
  $user_mismo_languages = json_decode($row_user_same["languages"], true);
  $user_mismo_languages = array_unique(array_merge($user_mismo_language_array, $user_mismo_languages));
  $user_mismo_languages = implode("','", $user_mismo_languages);
  
  $user_mismo_estado = preg_replace("/((http|https|www)[^\s]+)/", '<a target="_blank" opendd-href="$1">$0</a>', $user_mismo_estado);
  $user_mismo_estado = preg_replace("/href=\"www/", 'href="http://www', $user_mismo_estado);
  $user_mismo_estado = preg_replace("/(@[^\s]+) /", '<a opendd-href="?p=profile&u=$1">$0</a>', $user_mismo_estado);
  $user_mismo_estado = preg_replace("/#([^\s]+)/", '<a opendd-href="?p=topics&hashtag=$1">$0</a>', $user_mismo_estado);
  $user_mismo_estado = preg_replace("/&q=#/", '&q=', $user_mismo_estado);
  
  //OLD
  $user_id_mismo = $row_user_same["user_id"];
	}

if($user_mismo_id){
	$user_mismo_karma_total = mysqli_query($con, "SELECT SUM(amount) AS total FROM karma WHERE user_id='$user_mismo_id' LIMIT 1") or die();
	while($row_user_mismo_karma_total=mysqli_fetch_array($user_mismo_karma_total)) {
		$user_mismo_karma = $row_user_mismo_karma_total["total"];
		$user_mismo_karma = number_format($user_mismo_karma);
	}
}

if($user_mismo_id){
  include_once(__DIR__."/create-folders.php");
}


if($user_mismo_avatar) {
	$user_mismo_avatar = "//" . $settings_domain . "/apps/photos/see.php?id=" . $user_mismo_avatar;
} else {
	$user_mismo_avatar = "//kaana.io/apps/photos/see.php?id=user.svg";
}

if(!$user_mismo_color){
  $user_mismo_color = "blue";
}

if ($user_mismo_name) {$user_mismo_name = ucwords($user_mismo_name);} else {$user_mismo_name = ucwords($user_mismo);}

$user_mismo_fullname = $user_mismo_name . " " . $user_mismo_lastname;

if($user_mismo_nacimiento !== ''){
	$parts_nacimiento_user_mismo = explode('-', $user_mismo_nacimiento);
	$ano_nacimiento_user_mismo = $parts_nacimiento_user_mismo[0];
	$mes_nacimiento_user_mismo = $parts_nacimiento_user_mismo[1];
	$dia_nacimiento_user_mismo = $parts_nacimiento_user_mismo[2];
}


$user_mismo_topics_array = array();
$user_mismo_topics_select = mysqli_query($con, "SELECT id_seguir FROM user_follows WHERE user_id='$user_mismo_id' AND type='topic'");
while($row_news_topics = mysqli_fetch_array($user_mismo_topics_select))
{
  $user_mismo_topics_array[] = $row_news_topics["id_seguir"];
}
$user_mismo_topics = implode(",", $user_mismo_topics_array);
$user_mismo_topics_in = "'" . implode("','", $user_mismo_topics_array) . "'";


$account_switch = $_GET['account-switch'];
if($account_switch and $account_switch !== $user_mismo_id){
	$public_accounts_switch = array('10', '73');
  if(in_array($account_switch, array_keys($_SESSION["users"])) or (in_array($account_switch, $public_accounts_switch))){
    $_SESSION["users"]["current"] = $account_switch;
    if(!$_SESSION["users"]["$account_switch"]){
      $_SESSION["users"]["$account_switch"] = array();
    }
  }
  $urlactual_accountswitch = str_replace("?account-switch=$account_switch&", "?", $server_url);
	$urlactual_accountswitch = str_replace("?account-switch=$account_switch", "", $urlactual_accountswitch);
	$urlactual_accountswitch = str_replace("&account-switch=$account_switch", "", $urlactual_accountswitch);
	header('Location: '.$urlactual_accountswitch);
}
?>