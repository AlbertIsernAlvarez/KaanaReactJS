<html>
  <head>
    <?php include_once('../../meta.php'); ?>
    <style></style>
  </head>
  <body>
    <script src="/resources/js/jquery-3.3.1.js"></script>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/jquery-ui.min.js"></script>
    <script src="/resources/js/autosize.js"></script>
    <script src="/resources/js/jquery.initialize.min.js"></script>
    <script src="/resources/js/jquery.hidden-dimension.js"></script>
    <script src="/resources/js/paperflower.js"></script>
    <?php include($_GET['p'].'.php'); ?>
  </body>
</html>