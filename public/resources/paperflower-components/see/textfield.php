<a class="title" href="./see/?p=textfield" target="_top">Textfield - Filled</a>
    <div class="textfields">
        <label class="PF-textfield filled">
            <input placeholder=" ">
            <span>Empty</span>
        </label>
        <label class="PF-textfield filled">
            <input placeholder=" " value="Some text">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" "></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" ">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>
    <div class="textfields">
        <label class="PF-textfield filled">
            <input placeholder=" " disabled="">
            <span>Empty</span>
        </label>
        <label class="PF-textfield filled">
            <input placeholder=" " value="Some text" disabled="">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" " disabled=""></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" " disabled="">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>

    <a class="title" href="./see/?p=textfield" target="_top">Textfield - Outlined</a>
    <div class="textfields">
        <label class="PF-textfield outlined">
            <input placeholder=" ">
            <span>Empty</span>
        </label>
        <label class="PF-textfield outlined">
            <input placeholder=" " value="Some text">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" "></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" ">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>
    <div class="textfields">
        <label class="PF-textfield outlined">
            <input placeholder=" " disabled="">
            <span>Empty</span>
        </label>
        <label class="PF-textfield outlined">
            <input placeholder=" " value="Some text" disabled="">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" " disabled=""></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" " disabled="">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>

    <a class="title" href="./see/?p=textfield" target="_top">Textfield - Standard</a>
    <div class="textfields">
        <label class="PF-textfield standard">
            <input placeholder=" ">
            <span>Empty</span>
        </label>
        <label class="PF-textfield standard">
            <input placeholder=" " value="Some text">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" "></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" ">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>
    <div class="textfields">
        <label class="PF-textfield standard">
            <input placeholder=" " disabled="">
            <span>Empty</span>
        </label>
        <label class="PF-textfield standard">
            <input placeholder=" " value="Some text" disabled="">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" " disabled=""></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" " disabled="">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>