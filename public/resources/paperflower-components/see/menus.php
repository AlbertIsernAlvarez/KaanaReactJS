<html><head>
  
<style>
  
.PF-menu-trigger {
  cursor: pointer;
  position: absolute;
  -webkit-user-select: none;
      -ms-user-select: none;
          user-select: none;
  -moz-user-select: none;
}
.PF-menu-trigger:first-of-type {
  top: 15px;
  left: 15px;
}
.PF-menu-trigger:nth-of-type(2) {
  top: 15px;
  right: 15px;
}
.PF-menu-trigger:nth-of-type(3) {
  bottom: 15px;
  left: 15px;
}
.PF-menu-trigger:last-of-type {
  bottom: 15px;
  right: 15px;
}

.PF-menu {
  position: absolute;
  background: rgb(var(--PF-color-surface));
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
  border-radius: 3px;
  padding: 10px 0;
  transition: all 0.2s cubic-bezier(0.785, 0.135, 0.15, 0.86);
  -webkit-transform: scale(0);
          transform: scale(0);
  pointer-events: none;
  opacity: 0;
}
.PF-menu.-is-open {
  -webkit-transform: scale(1);
          transform: scale(1);
  pointer-events: auto;
  opacity: 1;
}
.PF-menu--top-left {
  top: 150%;
  left: 0;
  -webkit-transform-origin: top left;
          transform-origin: top left;
}
.PF-menu--top-right {
  top: 150%;
  right: 0;
  -webkit-transform-origin: top right;
          transform-origin: top right;
}
.PF-menu--bottom-left {
  bottom: 150%;
  left: 0;
  -webkit-transform-origin: bottom left;
          transform-origin: bottom left;
}
.PF-menu--bottom-right {
  bottom: 150%;
  right: 0;
  -webkit-transform-origin: bottom right;
          transform-origin: bottom right;
}
.PF-menu a {
    padding: 10px 30px 10px 15px;
    display: block;
    cursor: pointer;
    transition: all 0.2s ease;
    text-decoration: none;
    color: #444444;
    word-break: normal;
}
.PF-menu a:hover {
  background: #eee;
}
</style></head><body>

<div class="PF-menu-trigger">top left
  <ul class="PF-menu PF-menu--top-left">
    <li class="ripple"><a href="#">Home</a></li>
    <li class="ripple"><a href="#">Messages</a></li>
    <li class="ripple"><a href="#">Archive</a></li>
    <li class="ripple"><a href="#">Home</a></li>
    <li class="ripple"><a href="#">Messages</a></li>
    <li class="ripple"><a href="#">Archive</a></li>
  </ul>
</div>
<div class="PF-menu-trigger">top right
  <ul class="PF-menu PF-menu--top-right">
    <li class="ripple"><a href="#">Home</a></li>
    <li class="ripple"><a href="#">Messages</a></li>
    <li class="ripple"><a href="#">Archive</a></li>
  </ul>
</div>
<div class="PF-menu-trigger">bottom left
  <ul class="PF-menu PF-menu--bottom-left">
    <li class="ripple"><a href="#">Home</a></li>
    <li class="ripple"><a href="#">Messages</a></li>
    <li class="ripple"><a href="#">Archive</a></li>
  </ul>
</div>
<div class="PF-menu-trigger">bottom right
  <ul class="PF-menu PF-menu--bottom-right">
    <li class="ripple"><a href="#">Home</a></li>
    <li class="ripple"><a href="#">Messages</a></li>
    <li class="ripple"><a href="#">Archive</a></li>
    <li class="ripple"><a href="#">Home</a></li>
    <li class="ripple"><a href="#">Messages</a></li>
    <li class="ripple"><a href="#">Archive</a></li>
  </ul>
</div>
<script>
  var menuTriggers = document.querySelectorAll('.PF-menu-trigger');
var menus = document.querySelectorAll('.PF-menu');

menuTriggers.forEach(function (menuTrigger) {
	menuTrigger.addEventListener('click', function (e) {
		var currentMenu = menuTrigger.firstElementChild;

		menus.forEach(function (menu) {
			if (menu !== currentMenu) {
				menu.classList.remove('-is-open');
			}
		});

		if (e.target.parentNode.parentNode !== currentMenu) {
			currentMenu.classList.toggle('-is-open');
		}
	});
});

document.querySelector('body').addEventListener('click', function (e) {
	if (!e.target.closest('.PF-menu-trigger') && !e.target.classList.contains('menu')) {
		menus.forEach(function (menu) {
			if (menu.classList.contains('-is-open')) {
				menu.classList.remove('-is-open');
			}
		});
	}
});
</script>
</body></html>