<a class="title" href="./see/?p=buttons" target="_top">Button - Contained</a>
    <div><button class="PF-button">Contained</button></div>
    <div><button class="PF-button" disabled="">Contained</button></div>

    <a class="title" href="./see/?p=buttons" target="_top">Button - Outlined</a>
    <div><button class="PF-button-outlined">Outlined</button></div>
    <div><button class="PF-button-outlined" disabled="">Outlined</button></div>

    <a class="title" href="./see/?p=buttons" target="_top">Button - Text</a>
    <div><button class="PF-button-text">Text</button></div>
    <div><button class="PF-button-text" disabled="">Text</button></div>