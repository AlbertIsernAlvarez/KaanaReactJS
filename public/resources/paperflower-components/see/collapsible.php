<html>

<head>
  <style>

    .PF-collapsible {
    max-width: 560px;
      width:100%;
    margin: auto;
    border: 1px solid #eee;
    border-radius: 1em;
    }

    .PF-collapsible>li {
      border-bottom: 1px solid #eee;
      position: relative;
    }
    
    .PF-collapsible>li:last-child {
      border-color: transparent;
    }

    .PF-collapsible>li>.collapsible-header {
      width: 100%;
      display: block;
      cursor: pointer;
      font-weight: 600;
      line-height: 3;
      font-size: 14px;
      font-size: 0.875rem;
      text-indent: 15px;
      user-select: none;
    }
    
    .PF-collapsible>li>.collapsible-body {
      display: none;
      padding: 10px 25px 30px;
      color: #6b97a4;
      font-size: 13px;
      font-size: 0.8125rem;
      line-height: 2;
      padding: 10px;
    }

    .PF-collapsible>li>.collapsible-header:after {
    width: 10px;
    height: 10px;
    border-right: 2px solid #4a6e78;
    border-bottom: 2px solid #4a6e78;
    position: absolute;
    right: 1em;
    content: " ";
    top: 1.2em;
    transform: rotate(-45deg);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    }

    .collapsible-header.active:after {
      transform: rotate(45deg);
      -webkit-transition: all 0.2s ease-in-out;
      -moz-transition: all 0.2s ease-in-out;
      transition: all 0.2s ease-in-out;
    }
  </style>
</head>

<body>

  <ul class="PF-collapsible">
    <li>
      <div class="collapsible-header" >Lorem ipsum</div>
      <div class="collapsible-body">
      <ul class="PF PF-collection">
    <li class="collection-item ripple">
      <div class="PF PF-avatar ripple"></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
    <li class="collection-item ripple">
      <div class="PF PF-icon ripple"><i class="material-icons">folder</i></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
    <li class="collection-item ripple">
      <div class="PF PF-icon ripple"><i class="material-icons">insert_chart</i></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
    <li class="collection-item ripple">
      <div class="PF PF-icon ripple"><i class="material-icons">play_arrow</i></div>
      <div class="data" >
        <p>First Line</p>
        <span>Second Line</span>
      </div>
    </li>
  </ul>
      </div>
    </li>
    <li>
      <div class="collapsible-header" >Lorem ipsum</div>
      <div class="collapsible-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, ipsum, fuga, in, obcaecati magni ullam nobis voluptas fugiat tenetur voluptatum quas tempora maxime rerum neque deserunt suscipit provident cumque et mollitia ex aspernatur
        porro minus sapiente voluptatibus eos at perferendis repellat odit aliquid harum molestias ratione pariatur adipisci. Aliquid, iure.</div>
    </li>
    <li>
      <div class="collapsible-header" >Lorem ipsum</div>
      <div class="collapsible-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, ipsum, fuga, in, obcaecati magni ullam nobis voluptas fugiat tenetur voluptatum quas tempora maxime rerum neque deserunt suscipit provident cumque et mollitia ex aspernatur
        porro minus sapiente voluptatibus eos at perferendis repellat odit aliquid harum molestias ratione pariatur adipisci. Aliquid, iure.</div>
    </li>
  </ul>
  <!-- / PF-collapsible -->
  <script>
    (function($) {
      $('.PF-collapsible > li:eq(0) .collapsible-header').addClass('active').next().slideDown();

      $('.PF-collapsible .collapsible-header').click(function(j) {
        var dropDown = $(this).closest('li').find('.collapsible-body');

        $(this).closest('.PF-collapsible').find('.collapsible-body').not(dropDown).slideUp();

        if ($(this).hasClass('active')) {
          $(this).removeClass('active');
        } else {
          $(this).closest('.PF-collapsible').find('.collapsible-header.active').removeClass('active');
          $(this).addClass('active');
        }

        dropDown.stop(false, true).slideToggle();

        j.preventDefault();
      });
    })(jQuery);
  </script>
</body>

</html>