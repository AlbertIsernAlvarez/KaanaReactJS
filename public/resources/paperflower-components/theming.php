<html>
  <head>
    <style>
    
/* PaperFlower Theming */
.PF-theming {
    position: fixed;
    left: 0;
    bottom: 0;
    border-radius: 0 8px 0 0;
    padding: 24px 24px 36px;
    color: #000;
    background-color: #eee;
    box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
    transform: none;
    transition: transform 0.4s;
  z-index: 9999;
}

.PF-theming.hidden {
    transform: translatey(80%);
}

#toggle {
    margin-bottom: 16px;
}

#toggle > span {
    font-size: 20px;
}

.PF-theming > label {
    display: block;
    color: rgba(0, 0, 0, 0.87);
}

.PF-theming > label > input::-webkit-slider-runnable-track {
    background-color: transparent !important;
    background: linear-gradient(to right, #000, #fff);
}

.PF-theming > label > input::-moz-range-track {
    background-color: transparent !important;
    background: linear-gradient(to right, #000, #fff);
}

.PF-theming > label > input::-moz-range-progress {
    background-color: transparent !important;
}

.PF-theming > label > input::-ms-fill-lower,
.PF-theming > label > input::-ms-fill-upper {
    background-color: transparent !important;
}

.PF-theming > label > input::-ms-track {
    background: linear-gradient(to right, #000, #fff);
}

#primary > input::-webkit-slider-runnable-track {
    background: linear-gradient(to right, #f00, #ff0, #0f0, #0ff, #00f, #f0f, #f00);
}

#primary > input::-moz-range-track {
    background: linear-gradient(to right, #f00, #ff0, #0f0, #0ff, #00f, #f0f, #f00);
}

#primary > input::-ms-track {
    background: linear-gradient(to right, #f00, #ff0, #0f0, #0ff, #00f, #f0f, #f00);
}

/* Overriding */
#on-primary {
    --PF-color-primary: var(--PF-color-on-primary);
}

#surface {
    --PF-color-primary: var(--PF-color-surface);
}

#on-surface {
    --PF-color-primary: var(--PF-color-on-surface);
}

@media only screen and (max-width: 680px) {
    .PF-theming {
        transform: translate(-100%, 70%);
    }
}
    
    </style>
  </head>
  <body>
<div class="PF-theming">
            <label id="toggle" class="PF-switch">
                <input type="checkbox" checked="">
                <span>Colors</span>
            </label>
            <label id="primary" class="PF-slider">
                <input type="range" min="0" max="360" value="207">
                <span>Primary</span>
            </label>
            <label id="on-primary" class="PF-slider">
                <input type="range" min="0" max="255" value="255">
                <span>On Primary</span>
            </label>
            <label id="surface" class="PF-slider">
                <input type="range" min="0" max="255" value="255">
                <span>Surface</span>
            </label>
            <label id="on-surface" class="PF-slider">
                <input type="range" min="0" max="255" value="0">
                <span>On Surface</span>
            </label>
        </div>
    
    
<script>document.querySelectorAll('.checkboxes > label:last-child > input').forEach(item => item.indeterminate = true);

const progress = document.querySelector('.progresses > progress');
setInterval(() => {
    progress.value = progress.value === 100 ? 0 : progress.value + 10;
}, 1000);

// https://codegolf.stackexchange.com/a/150252
const hsl2rgb = (H, S, L) => [5, 3, 1].map(i => A(L * 2) * S * ([1, Y, 0, 0, Y, 1][(i - ~H) % 6] - .5) + L, Y = (A = n => n > 1 ? 2 - n : n)((H /= 60) % 2));

document.querySelector('#primary > input').oninput = event => {
    const rgb = hsl2rgb(event.target.value, 0.897, 0.541);
    document.body.style.setProperty('--PF-color-primary', `${Math.round(rgb[0] * 255)}, ${Math.round(rgb[1] * 255)}, ${Math.round(rgb[2] * 255)}`);
};

document.querySelector('#on-primary > input').oninput = event => {
    const value = event.target.value;
    document.body.style.setProperty('--PF-color-on-primary', `${value}, ${value}, ${value}`);
};

document.querySelector('#surface > input').oninput = event => {
    const value = event.target.value;
    document.body.style.setProperty('--PF-color-surface', `${value}, ${value}, ${value}`);
};

document.querySelector('#on-surface > input').oninput = event => {
    const value = event.target.value;
    document.body.style.setProperty('--PF-color-on-surface', `${value}, ${value}, ${value}`);
};

const PFtheming = document.querySelector('.PF-theming');

document.querySelector('#toggle > input').onchange = event => {
    if (event.target.checked) {
        PFtheming.classList.remove('hidden');
    } else {
        PFtheming.classList.add('hidden');
    }
};
</script>
    
  </body>
</html>