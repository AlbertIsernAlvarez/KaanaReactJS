<!-----
                                        
                                        
              `.-::::--.                
          .:+oooooossssss+:.            
        -+oooooooosssssssssso-          
      .+ooooooooossssssssssssso.        
     -ooooooooooosssssssssssssys-       
    .ooooo:-+oooso::ossssssssyyyy.      
    /oooo:  `ooss.  -sssssssyyyyyo      
    ooooo:  `osss.  :sssssssyyyyyy      
    oooooo/:osssso//sssssssyyyyyyy`     
    /oooooooosssssssssssssyyyyyyys      
    .ooooooosssssssssssssyyyyyyyys/o    
     -ooooosssssssssssssyyyyyyyyyys:    
      .+oossssssssssssssyyyyyyyys:      
        -+sssssssssssssyyyyyys/.        
          .:+sssssssssyyyo/-`           
              .-:::/::-.                
                                        
                                        
Kaana
kaana.io
Wow! We guess that you are a developer.
Now Kaana is recruiting a CRAZY developer.
Pleases contact us via e-mail: team@kaana.io
------->
<link rel="dns-prefetch" href="https://kaana.io">
<link rel="dns-prefetch" href="https://img.kaana.io">
<link rel="preconnect" href="https://kaana.io">
<link rel="preconnect" href="https://img.kaana.io">
<link rel="manifest" id="dynamicManifest">
<meta name="google-site-verification" content="L0T0_6VCNrT3slPROGy2XjzhW8lqVCYTBZ-l5XnEm1U" />
<meta http-equiv="content-language" content="<?=$language?>" />
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<link href="https://kaana.io/" rel="canonical">
<meta name="google" content="notranslate" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:type" content="website">
<link rel=”alternate” hreflang=”en” href=”//kaana.io/l/en” >
<link rel=”alternate” hreflang=”es” href=”//kaana.io/l/es” >
<link rel=”alternate” hreflang=”fr” href=”//kaana.io/l/fr” >
<link rel=”alternate” hreflang=”ca” href=”//kaana.io/l/ca” >
<link rel=”alternate” hreflang=”ar” href=”//kaana.io/l/ar” >
<link rel=”alternate” hreflang=”tr” href=”//kaana.io/l/tr” >
<link rel=”alternate” hreflang=”ku” href=”//kaana.io/l/ku” >
<link rel=”alternate” hreflang=”gr” href=”//kaana.io/l/gr” >
<meta name="theme-color" content="#ffffff">

<!--------SOCIAL----------->

<!-------TWITTER-------->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@Kaana">

<!------ meta otros------->
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/style.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/kaana-internal.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/followbutton.css?version=<?=$last_version_cacheversion?>" async>
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/more-less.css?version=<?=$last_version_cacheversion?>" async>
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/memories.css?version=<?=$last_version_cacheversion?>" async>
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/filters.css?version=<?=$last_version_cacheversion?>" async>
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/animate.css?version=<?=$last_version_cacheversion?>" async>
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/colors.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/page.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/cards.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/link.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/select.css?version=<?=$last_version_cacheversion?>">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/button.css?version=<?=$last_version_cacheversion?>">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/checkbox.css?version=<?=$last_version_cacheversion?>">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/radio.css?version=<?=$last_version_cacheversion?>">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/switch.css?version=<?=$last_version_cacheversion?>">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/progress.css?version=<?=$last_version_cacheversion?>">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/textfield.css?version=<?=$last_version_cacheversion?>">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/grid.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/chips.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/collapsible.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/collection.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/feed-multisource.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/smart-cards.css?version=<?=$last_version_cacheversion?>">
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/reactions.css?version=<?=$last_version_cacheversion?>" async>
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/highlight.css?version=<?=$last_version_cacheversion?>" async>
<link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/glance.css?version=<?=$last_version_cacheversion?>" async>