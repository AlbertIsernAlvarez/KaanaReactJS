<?php
function topicsRelated($topic)
{
    global $con;
	global $language;
    $topics_related = array();
    $topics_related_sql = mysqli_query($con, "SELECT * FROM topics WHERE LOWER(title) IN(SELECT related FROM topics_related WHERE LOWER(title) IN(LOWER('$topic')) AND language IN('$language') ORDER BY id DESC) ORDER BY demand DESC LIMIT 20");
    while ($topics_related_sql_row = mysqli_fetch_array($topics_related_sql))
    {
        $topics_related[] = $topics_related_sql_row["title"];
    }
    return $topics_related;
}
?>