<?php
$feed_unique = $_POST['unique'];

include_once(__DIR__.'/feeds-loaded.php');

if(!$app and $pagina != 'genius' and ($user_mismo_id or $user_id === '1' or $user_id === '639')){
include_once(__DIR__.'/feeds/posts/posts.php');
}

if(!$pagina or $q or $pagina == 'genius' or $pagina == 'topics' or $app == 'news'){
  include_once(__DIR__.'/../../apps/news/feed/news.php');
}

include_once(__DIR__.'/feeds/smart-cards/smart-cards.php');

/*include(__DIR__.'/feeds/socialnetworks/socialnetworks.php');*/

$feed_query = mysqli_query($con,"SELECT * FROM feed t1 WHERE feed_unique='$feed_unique' AND user_id='$user_mismo_id' ORDER BY weight DESC LIMIT 20");

if(mysqli_num_rows($feed_query)>0){?>
<div class="block">
	<?php
	$ids_feedsloaded_insert = array();
  while($row_feed = mysqli_fetch_array($feed_query)){
    $feed_item_class = null;
    $feed_item_userid = $row_feed["userid"];
    $feed_item = $row_feed["item"];
    $feed_item_id = $row_feed["item_id"];
    $feed_name = $row_feed["feed"];
    $feed_item_type = $row_feed["type"];
    $feed_item_title = $row_feed["title"];
    $feed_item_title = strip_tags(html_entity_decode(replaceemojis($feed_item_title)));
    $feed_item_text = strip_tags(html_entity_decode(replaceemojis($row_feed["text"])));
    $feed_item_text_links = $row_feed["text"];
    $feed_item_images = $row_feed["images"];
    if(isset($feed_item_images)){ list($feed_item_images_size_width, $feed_item_images_size_height) = getimagesize($feed_item_images); }
    $feed_item_source = $row_feed["source"];
    $feed_item_date = $row_feed["date"];
    $feed_item_contentid = $row_feed["contentid"];
    $feed_item_reply = $row_feed["reply"];
    $feed_item_location_name = $row_feed["location_name"];
    $feed_item_location_coordinates = $row_feed["location_coordinates"];

    if(!$feed_item_type){
      $feed_item_text = preg_replace("/https?\:\/\/[^\" ]+/i", "", $feed_item_text);
    }

    if($feed_name == 'news'){
      if($feed_item_images and $feed_item_images_size_width <= 300){ $feed_item_class .= " flex "; }
      $feed_item_title_onclick = "windowdd('./apps/news/resources/article.php?id=" . $feed_item_id . "', 'fit');";
      $feed_item_image_onclick = $feed_item_title_onclick;
      $feed_item_text_onclick = $feed_item_title_onclick;
      $feed_item_span_onclick = "openddgo('?app=news&sp=source&source=".$feed_item_source."');";
    }

      addfeedloaded($con, $user_mismo_id, $feed_unique, $feed_name, $feed_item_id);

      switch ($feed_item) {
        case "post":
          include(__DIR__."/items/post/post-template.php");
          break;
        case "fullcoverage":
          include(__DIR__."/items/fullcoverage/fullcoverage.php");
          break;
        case "smart-card":
          include(__DIR__."/items/smart-cards/$feed_item_type.php");
          break;
        case "informative-cards":
          include(__DIR__."/items/informative-cards/card.php");
          break;
        default:
          include(__DIR__."/items/card/card.php");
    }

  }

mysqli_query($con, "DELETE FROM feed WHERE user_id='$user_mismo_id' AND feed_unique='$feed_unique'");

  ?>
</div>
<?}?>