<?php foreach ($block[0]['fullcoverage'] as &$fullcoverage) {?>
      <div class="fullcoverage" data-id="<?=$fullcoverage['id']?>">
        <div class="content">
          <div class="pages">
            <?php foreach ($fullcoverage['screens'] as &$fullcoverage_screens) {?>
            <div class="screen" data-id="<?=$fullcoverage_screens['id']?>"></div>
            <?}?>
          </div>
          <div class="progress-container">
            <?php foreach ($fullcoverage['screens'] as &$fullcoverage_progress) {?>
            <progress value="100" max="100" class="PF-progress linear ripple"></progress>
            <?}?>
          </div>
        </div>
        <div class="PF button ripple">View full coverage</div>
      </div>
    <?}?>