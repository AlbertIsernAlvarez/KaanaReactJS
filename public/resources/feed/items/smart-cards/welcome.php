<html>

<head>
  <style>
    .welcome_kaana {
    width: 100%;
    height: auto;
    background-color: rgb(var(--PF-color-surface));
    border-radius: 1em;
    }

    .welcome_kaana .block {
      width: 100%;
      background: linear-gradient(to top, var(--PF-color-bg-first-default), var(--PF-color-default) 90%);
      text-align: center;
      border-radius: 20px;
      margin-bottom: 1rem;
      padding: .5em;
    }

    .welcome_kaana .block .head {
      padding-top: 1rem;
    }

    .welcome_kaana .block .head img {
      background: rgb(var(--PF-color-surface));
      border-radius: 100px;
    }

    .welcome_kaana .block .head h1 {
      font-size: 34px;
      line-height: 40px;
      padding-right: 0;
      padding-left: 0;
      vertical-align: top;
      font-weight: 900;
    }

    .welcome_kaana .block .head p {
    font-size: 18px;
    line-height: 26px;
    padding-right: 0;
    padding-left: 3px;
    padding-bottom: 0px;
    padding-top: 0px;
    vertical-align: top;
    font-weight: 400;
    text-align: center;
    padding: .5em 1.5em;
    rgb(var(--PF-color-surface))-space: pre-wrap;
    }

    .welcome_kaana .block .bigimage {
      width: calc(100% - 1em);
      height: auto;
      background: rgb(var(--PF-color-primary), .1);
      border-radius: 1em;
      margin: .5em;
    }

    /*CONVERSACION*/
    
    .welcome_kaana .block .conversacion {
    	margin: .5em;
    	background-color: rgb(var(--PF-color-primary), .1);
    border-radius: 1em;
    }

    .welcome_kaana .block .conversacion .mensaje {
      width: 100%;
      padding: 0;
      padding-left: 1rem;
      padding-right: 1rem;
      position: relative;
    }

    .welcome_kaana .block .conversacion .mensaje .kmk {
      position: absolute;
      top: -2rem;
      left: 1rem;
      padding: 1.5rem;
      background: url(http://img.kaana.io/logo/logo.png);
      background-repeat: no-repeat;
      background-size: 2.5rem;
      background-color: rgb(var(--PF-color-surface));
      border-radius: 50px;
      background-position: center;
    }

    .welcome_kaana .block .conversacion .mensaje.user {
      text-align: right;
    }
    
    .welcome_kaana .block .conversacion .mensaje.kamiku_conversacion {
      text-align: left;
    }

    .welcome_kaana .block .conversacion .mensaje .bubble {
-webkit-text-size-adjust: none;
    background-color: rgb(var(--PF-color-surface));
    border: 0;
    border-collapse: collapse;
    border-spacing: 0;
    color: rgb(var(--PF-color-on-surface));
    display: inline-block;
    font-size: 15px;
    font-weight: 400;
    margin: 0;
    text-align: left;
    text-decoration: none;
    border-radius: 1.5em;
    padding: .7em;
    position: relative;
    margin-bottom: .5em;
    }

    .welcome_kaana .block .conversacion .mensaje.kamiku_conversacion .bubble {
      background-color: rgb(var(--PF-color-surface));
    }

    .welcome_kaana .block .conversacion .mensaje.kamiku_conversacion .bubble.first {
      margin-top: 2rem;
    }

    .welcome_kaana .block .conversacion .mensaje.kamiku_conversacion .bubble.first:before {
      content: " ";
      position: absolute;
      top: -1.5rem;
      left: 1.5rem;
      width: 0;
      height: 0;
      border: .8rem solid transparent;
      border-color: transparent transparent rgb(var(--PF-color-surface)) transparent;
    }

    .welcome_kaana .block .conversacion .mensaje.kamiku_conversacion .bubble.image {
      padding: 0;
      border: none;
      background: transparent;
      border-radius: 0;
      box-shadow: none;
    }

    .welcome_kaana .block .conversacion .mensaje.kamiku_conversacion .bubble.image img {
      width: auto;
      max-width: 100%;
      margin: 0;
    }
  </style>
</head>

<body>

  <div class="welcome_kaana">

    <div class="block PF shadow">

      <div class="head">
        <img src="//img.kaana.io/smart-cards/feed/welcome/welcome-faces.gif" alt="Kaana" style="width: 85px;  border:0">
        <h1 t-dd>Welcome to Kaana</h1>
        <p t-dd>Find new ways to adventure</p>
      </div>

      <img kzoom class="bigimage" src="//img.kaana.io/smart-cards/feed/welcome/welcome-big.png" /> 

      <div class="conversacion" style=" display: none; ">
        <div class="mensaje kamiku_conversacion">
          <div class="kmk PF shadow"></div>
          <div class="bubble PF shadow first">
            <p>Hi. I'm Kaana.</p>
          </div>
        </div>
        <div class="mensaje user">
          <div class="bubble PF shadow">
            <p>Hi, how do I get started?</p>
          </div>
          <div class="bubble PF shadow">
            <p>I am looking forward to using this platform.</p>
          </div>
        </div>
        <div class="mensaje kamiku_conversacion">
          <div class="kmk PF shadow"></div>
          <div class="bubble PF shadow first">
            <p>I recommend that you start following the topics you might like.</p>
          </div>
          <div class="bubble PF shadow image">
            <img src="https://hsto.org/storage2/213/e2f/ea2/213e2fea2046987cc8cbedf27e24840a.png">
          </div>
        </div>
      </div>

    </div>

    <div class="block PF shadow" style=" display: none; ">
      <div class="conversacion">
        <div class="mensaje kamiku_conversacion">
          <div class="kmk PF shadow"></div>
          <div class="bubble PF shadow first">
            <p>Hi, how can I help!? </p>
          </div>
        </div>
        <div class="mensaje user">
          <div class="bubble PF shadow" onclick="verkamikupreguntar('Play some music');">
            <p>Play some music</p>
          </div>
        </div>
        <div class="mensaje kamiku_conversacion">
          <div class="kmk PF shadow"></div>
          <div class="bubble PF shadow first">
            <p>Ok, playing! </p>
          </div>
          <div class="bubble PF shadow image">
            <img src="https://assistant.google.com/static/images/platforms/phones/entertainment.png">
          </div>
        </div>
      </div>
    </div>

  </div>

</body>

</html>