<?php 
include_once(__DIR__."/../../../../dd-functions/Parsedown.php");
?>
<div class="text">
	<?php if($feed_item_title){?><h1 class="title"><?=$feed_item_title?></h1><?}
	if($feed_item_markdown){
		$Parsedown = new Parsedown();
		echo $Parsedown->text($feed_item_text);
	} elseif(strlen($feed_item_text) >= 180){?>
		<p><?=$feed_item_text?></p>
	<?} else {?>
		<h1><?=$feed_item_text?></h1>
	<?}?>
</div>