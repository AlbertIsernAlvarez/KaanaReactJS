<?php
preg_match_all('/https?\:\/\/[^\" ]+/i', $feed_item_text_links, $match_link_p);
if($match_link_p){
$links_implode = implode("' AND url LIKE '", $match_link_p[0]);
$links_query = mysqli_query($con, "SELECT * FROM links WHERE url LIKE '$links_implode' ORDER BY date DESC LIMIT 1");
  while($row_links = mysqli_fetch_array($links_query)){
  	$links_item_url = $row_links["url"];
  	$links_item_url_domain = str_ireplace('www.', '', parse_url($links_item_url, PHP_URL_HOST));
    $links_item_title = $row_links["title"];
    $links_item_description = $row_links["description"];
    $links_item_image = $row_links["image"];

    //if(preg_match('/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i',$links_item_url)){
    	//$links_item_embed_video = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"100%\" height=\"315\" src=\"//www.youtube.com/embed/$1?showinfo=0&loop=1&rel=0\" frameborder=\"0\" allowfullscreen></iframe>",$links_item_url);
    //}
    ?>
		<a class="PF PF-link shadow" href="<?=$links_item_url?>" target="_blank">
			<?php
			if($links_item_embed_video){
				echo $links_item_embed_video;
			} elseif($links_item_image){?>
				<div class="PF PF-image" style="background-image: url('<?=$links_item_image?>');"></div>
			<?}?>
			<div class="data">
				<?php if($links_item_url_domain){?><div class="source"><p><?=$links_item_url_domain?></p></div><?}?>
				<?php if($links_item_title){?><h1><?=$links_item_title?></h1><?}?>
			</div>
		</a>
<?}
}?>