<?php
if($feed_item_userid){
        $feed_item_userdata_query = mysqli_query($con,"SELECT * FROM users WHERE user_id='$feed_item_userid' LIMIT 1");
        while($row_feed_item_userdata = mysqli_fetch_array($feed_item_userdata_query)){
          $feed_item_userdata_id = $row_feed_item_userdata["user_id"];
          $feed_item_userdata_username = $row_feed_item_userdata["username"];
          $feed_item_userdata_name = $row_feed_item_userdata["name"];
          $feed_item_userdata_lastname = $row_feed_item_userdata["lastname"];
          $feed_item_userdata_avatar = $row_feed_item_userdata["avatar"];
          if($feed_item_userdata_avatar) {
          	$feed_item_userdata_avatar = "//" . $settings_domain . "/apps/photos/see.php?id=" . $feed_item_userdata_avatar;
          } else {
          	$feed_item_userdata_avatar = "//user.kaana.io/user.svg";
          }
          if($feed_item_userdata_name){
            $feed_item_userdata_fullname = $feed_item_userdata_name . " " . $feed_item_userdata_lastname;
          } else {
            $feed_item_userdata_fullname = $feed_item_userdata_username;
          }
        }
 }
?>

	<div class="post" data-post='<?=$feed_item_id?>'>
		<div class="top">
				<div class="data">
					<div class="PF-avatar ripple <?php if($feed_item_userdata_id == $user_id_mismo) {?>ddbg-avatar<?}?>" onclick="downmenudd('./pantallas/profile/preview/menu.php?id=<?=$feed_item_userdata_id?>');" style="background-image: url('<?=$feed_item_userdata_avatar?>')" rgdd></div>
					<div class="info">
						<h1><b><?=$feed_item_userdata_fullname?></b><?php if($feed_item_user_action){?> <span><?=$feed_item_user_action?></span><?}?></h1>
						<p><span prettydate><?=$feed_item_date?></span><?php if($feed_item_location_name){?> · <span onclick="searchdd('<?=$feed_item_location_name?>');"><?=$feed_item_location_name?></span><?}?></p>
					</div>
					<div more-less data-type="post" data-id="<?=$feed_item_id?>" data-userid="<?=$feed_item_userdata_id?>"></div>
					<div class="PF PF-icon ripple menu-icon" onclick="downmenudd('/resources/feed/feeds/posts/resources/menu.php?id=<?=$feed_item_id?>');"><i class="material-icons">more_horiz</i></div>
				</div>
			</div>
		<div class="container">
			<div class="content">
				<?php
				if($feed_item_title or $feed_item_text){
					include(__DIR__."/content/text.php");
				}
		
		if($feed_item_type){
			include(__DIR__."/content/" . $feed_item_type . ".php");
		} elseif($feed_item_text_links){
			include(__DIR__."/content/link.php");
		}

		?>
			</div>
			<div class="bottom">
				<div reactionsdd content-id="<?=$feed_item_id?>" content-app="posts"></div>
				<ul class="bar">
					<li opendd-href="?p=new-post&action=reply&post=<?=$feed_item_id?>">
						<div><i class="material-icons bd"> reply </i><span t-dd>Reply</span></div>
					</li>
					<li opendd-href="?p=new-post&action=repost&post=<?=$feed_item_id?>">
						<div><i class="material-icons"> repeat </i><span t-dd>Repost</span></div>
					</li>
					<li onclick="alertdd.show('This function is not yet available.');">
						<div><i class="material-icons bd"> bookmark </i><span t-dd>Save</span></div>
					</li>
				</ul>
			</div>
			<div class="replies">
				<?php include(__DIR__."/resources/replies.php"); ?>
			</div>
		</div>
	</div>