<?php
  $feed_unique = $_POST['unique'];
  $feedsloaded_date_delete = date("Y-m", strtotime($datetime));
  
  mysqli_query($con, "DELETE FROM feed WHERE user_id='$user_mismo_id' ");
  mysqli_query($con, "DELETE FROM feeds_loaded WHERE user_id='$user_mismo_id' AND date<'$feedsloaded_date_delete' ");
?>

<div class="feed-multisource <?=$_POST['classes']?>" data-unique="<?=$_POST['unique']?>" data-currenturl="<?=$_POST['currenturl']?>">
  <?php if($user_mismo_id and (!$q and $pagina !== 'search' and !$_POST['hideform'])){?>
    <div class="PF new-post-smallform shadow ripple" opendd-href="?p=new-post<?php if($topic){?>&topic=<?=$topic?><?}?>">
      <div class="PF-avatar ripple ddbg-avatar" rgdd></div>
      <p>
        <?php include(__DIR__."/../../pantallas/new-post/resources/text-form.php"); ?>
      </p>
      <div class="PF PF-icon ripple" opendd-href="?p=new-post&action=add-image"><i class="material-icons">image</i></div>
    </div>
    <?php include(__DIR__."/../../pantallas/new-post/resources/special.php"); ?>
  <?}?>
  <div class="blocks blocks_feed"></div>
  <div class="blocks loadingPlaceholder">
    <div class="block loading">
      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <div class="container"><span>Author name</span></div>
            <div class="container title"><h1>This is the title</h1></div>
            <div class="container text"><p>This is the text description of this card</p></div>
          </div>
        </div>
        <div class="footer">
          <div class="container"><span prettydate><?=$datetime?></span></div>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>
      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <div class="container"><span>Author name</span></div>
            <div class="container title"><h1>This is the title</h1></div>
            <div class="container text"><p>This is the text description of this card</p></div>
          </div>
        </div>
        <div class="footer">
          <div class="container"><span prettydate><?=$datetime?></span></div>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>
      <div class="card">
        <div class="content">
          <div class="PF PF-image"></div>
          <div class="data">
            <div class="container"><span>Author name</span></div>
            <div class="container title"><h1>This is the title</h1></div>
            <div class="container text"><p>This is the text description of this card</p></div>
          </div>
        </div>
        <div class="footer">
          <div class="container"><span prettydate><?=$datetime?></span></div>
          <div class="space"></div>
          <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
        </div>
      </div>
    </div>
  </div>
    <div class="PF shadow feed-card no-content">
      <div class="cover contain" style="background-image: url('//img.kaana.io/smart-cards/feed/empty.png'); margin-top: 2em;"></div>
      <div class="head">
        <?php if(!$user_mismo_id){?>
        <p t-dd>Login to see the feed</p>
        <?} else {?>
        <h1 t-dd>There's nothing to show</h1>
        <?}?>
      </div>
    </div>
</div>