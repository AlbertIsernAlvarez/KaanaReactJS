(function() {

  window.nameinput = {
    maxlength: 35,
    lettersonly: true,
    regex: /^[a-zA-Z0-9 ]+$/,
    notblank: true
  };

  window.emailinput = {
    malength: 35,
    regex: /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/
  };

  window.isblank = function(str) {
    if (str.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  window.tooLong = function(string, maxlength) {
    if (string.length <= maxlength) {
      return false;
    } else {
      return true;
    }
  };

  window.tooShort = function(string, minlength) {
    if (string.length >= minlength) {
      return false;
    } else {
      return true;
    }
  };

  window.validateText = function(text, min, max) {
    var increment, totalitems, validitems;
    validitems = 0;
    totalitems = 4;
    if (min == null) {
      min = 3;
    }
    if (max == null) {
      max = 35;
    }
    increment = function() {
      return validitems++;
    };
    if (!isblank(text)) {
      increment();
    }
    if (!tooShort(text, min)) {
      increment();
    }
    if (!tooLong(text, max)) {
      increment();
    }
    if (nameinput.regex.test(text)) {
      increment();
    }
    if (validitems < totalitems) {
      return false;
    } else {
      return true;
    }
  };

  window.validateEmail = function(email) {
    var increment, totalitems, validitems;
    validitems = 0;
    totalitems = 1;
    increment = function() {
      return validitems++;
    };
    if (emailinput.regex.test(email)) {
      increment();
    }
    if (validitems < totalitems) {
      return false;
    } else {
      return true;
    }
  };

  window.isPhone = function(input) {
    var regex;
    regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})\s*[-. ]?\s*([0-9]{4})$/;
    return regex.test(input);
  };

  window.is_int = function(value) {
    if (parseFloat(value) === parseInt(value) && !isNaN(value)) {
      return true;
    } else {
      return false;
    }
  };

  $ = jQuery;

  $.fn.extend({
    multistep: function(options) {
      var currentStep, form, log, settings, specialClasses, step;
      settings = {
        debug: true,
        progressbar: true
      };
      settings = $.extend(settings, options);
      log = function(msg) {
        if (settings.debug) {
          return typeof console !== "undefined" && console !== null ? console.log(msg) : void 0;
        }
      };
      this.addClass('multistep-form');
      this.find('.step').first().css('display', 'block');
      this.find('.step:not(:first)').append('<a href="#" class="back"></a>');
      window.valid = false;
      form = this;
      step = $('.step', this);
      currentStep = 0;
      specialClasses = ["phone", "zip", "email"];
      window.specialRegEx = new RegExp(specialClasses.join("|"));
      $("input:not([type=image],[type=button],[type=submit],[type=radio],[type=checkbox])", form).keyup(function() {
        var min, skip, _ref;
        min = (_ref = ($(this)).attr('min')) != null ? _ref : 3;
        if ((($(this)).attr('class') != null) && ((($(this)).attr('class')).match(specialRegEx) != null)) {
          skip = true;
        }
        if (!(($(this)).attr('optional') === "yes" || ($(this)).val().length < min || (skip != null))) {
          if (validateText($(this).val(), min, $(this).attr('max'))) {
            return $(this).removeClass('error').addClass('success').attr('valid', 'true');
          } else {
            return $(this).removeClass('success').addClass('error').attr('valid', 'false');
          }
        }
      });
      ($('input.phone', form)).change(function() {
        if (isPhone($(this).val())) {
          return $(this).removeClass('error').addClass('success').attr('valid', 'true');
        } else {
          return $(this).removeClass('success').addClass('error').attr('valid', 'false');
        }
      });
      ($('input.email', form)).change(function() {
        if (validateEmail($(this).val())) {
          $(this).removeClass('error').addClass('success').attr('valid', 'true');
        } else {
          $(this).removeClass('success').addClass('error').attr('valid', 'false');
        }
        if ($(this).val() != null) {
          return $(this).addClass('success');
        } else {
          return $(this).addClass('error');
        }
      });
      ($('input.zip', form)).change(function() {
        if (is_int($(this).val()) && $(this).val().length === 5) {
          return $(this).removeClass('error').addClass('success').attr('valid', 'true');
        } else {
          return $(this).removeClass('success').addClass('error').attr('valid', 'false');
        }
      });
      ($('select', form)).change(function() {
        if (($(this).val() != null) && ($(this)).val() !== '') {
          return $(this).removeClass('error');
        } else {
          return $(this).addClass('error');
        }
      });
      ($('textarea', form)).keyup(function() {
        if (($(this).val() != null) && ($(this)).val() !== '') {
          return $(this).removeClass('error').addClass('success').attr('valid', 'true');
        } else {
          return $(this).removeClass('success').addClass('error').attr('valid', 'false');
        }
      });
      return $('.step .submit', form).click(function(e) {
        var progress, thisStep;
        thisStep = $(this).closest('.step');
        e.preventDefault();
        window.totalitems = 0;
        window.validitems = 0;
        $('input:not([type=image],[type=button],[type=submit],[type=radio],[type=checkbox]):visible', thisStep).each(function() {
          window.totalitems++;
          if ($(this).attr('valid') === 'true' || ($(this)).attr('optional') === "yes") {
            window.validitems++;
          } else {
            $(this).removeClass('success').addClass('error').focus();
            false;
          }
          return log(validitems + " " + totalitems);
        });
        ($('textarea:visible', thisStep)).each(function() {
          window.totalitems++;
          if ($(this).attr('valid') === 'true' || ($(this)).attr('optional') === "yes") {
            return window.validitems++;
          } else {
            $(this).removeClass('success').addClass('error').focus();
            return false;
          }
        });
        $('select:visible', thisStep).each(function() {
          window.totalitems++;
          if (($(this).val() != null) && $(this).val() !== "") {
            window.validitems++;
            return $(this).removeClass('error');
          } else {
            return $(this).addClass('error');
          }
        });
        if (window.totalitems === window.validitems) {
          if (thisStep.attr('id') !== "last-step") {
            thisStep.slideUp();
            thisStep.next('.step').slideDown();
            currentStep++;
          } else {
            if ((options != null ? options.beforeSubmit : void 0) != null) {
              options.beforeSubmit();
            }
            form.submit();
          }
        }
        return $('.back', form).click(function(e) {
          e.preventDefault();
          log('clicked');
          ($(this)).closest('.step').slideUp();
          ($(this)).closest('.step').prev('.step').slideDown();
          currentStep--;
        });
      });
    }
  });

}).call(this);

$(function() {
  $('.multistep').multistep();
});