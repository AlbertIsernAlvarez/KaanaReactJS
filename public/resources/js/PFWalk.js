
var _logenv = {
    MSG: false,
    PFWalk_CONTENT: true,
    PFWalk_CONTENT_TOP: false,
    PFWalk_LOCK: false,
    PFWalk_SCROLL: true,
    ALL: true
};
function _log(context, message) {
    if(!!_logenv[context] || _logenv.ALL) console.log(context +': '+ message);
}

(function($) {

    var PFWalk_DEFAULT_COLOR = '#1A237E';
    var PFWalk_TRANSITION_DURATION = 500;
    var PFWalk_MIN_SIZE = 60;
    var PFWalk_PADDING = 20;
    var PFWalk_COMPONENT =
        "<div id='PFWalk-bounds'><div id='PFWalk-wrapper'>" +
        "<div id='PFWalk-content-wrapper'>" +
        "<div id='PFWalk-content'></div>" +
        "<button id='PFWalk-action' class='PF PF-button'><div class='inside' ><p></p></div></button>" +
        "</div>" +
        "</div></div>";

    var PFWalkWrapper = {};
    var PFWalkContentWrapper = {};
    var PFWalkContent = {};
    var PFWalkActionButton = {};
  var PFWalkActionButtonText = {};
    var PFWalkUpdateHandler = null;

    /**
     * A Object that configures an PFWalkpoint.
     * @typedef {object} WalkPoint
     * @property {string} target A jQuery selector of the element that the PFWalk will focus;
     * @property {string} content A HTML code that will be inserted on the PFWalk-content container;
     * @property {string} [color] A CSS (rgb, rgba, hex, etc.) color specification that will paint the PFWalk. #2196F3 is default;
     * @property {string} [acceptText] The text of the accept button of the PFWalk;
     * @property {function} [onSet] A function that will be called when the PFWalk content is setted;
     * @property {function} [onClose] A function that will be called when the PFWalk is accepted;
     */

    /***
     * Open the PFWalker to a PFWalkpoint.
     * @param {WalkPoint} PFWalkPoint The configuration of the PFWalkpoint
     */
    $.fn.PFWalk = function (PFWalkPoint) {
        var target = this;

        disableScroll();
        PFWalkWrapper.removeClass('closed');
        setWalker(target, PFWalkPoint);
    };

    /***
     * Set the opened PFWalker to a target with the properties from PFWalkPoint.
     * @param {string|HTMLElement|JQueryElement} target A query or a Element to target the PFWalker
     * @param {WalkPoint} PFWalkPoint The properties for this PFWalker
     */
    function setWalker(target, PFWalkPoint) {
        target = $(target);
        _log('MSG', '-------------------------------------');
        _log('MSG', 'Setting a PFWalk to #' + target[0].id);
        _log('PFWalk_SETUP', 'Properties:\n' + JSON.stringify(PFWalkPoint, null, 2));

        setupListeners(target, PFWalkPoint.onClose);

        PFWalkContentWrapper.css('display', 'none');
        locateTarget(target, function () {
            setProperties(PFWalkPoint.content, PFWalkPoint.color, PFWalkPoint.acceptText);
            PFWalkWrapper.css('display', 'block');
            renderFrame(target, function () {
                PFWalkWrapper.addClass('opened');
                renderContent(target, function() {
                    PFWalkContentWrapper.css('display', '');
                });
            });
        });

        _log('MSG', 'Walk created. Calling onSet() (if exists)');
        if (!!PFWalkPoint.onSet) PFWalkPoint.onSet(PFWalkContent);
    }

    /***
     * Close the PFWalker and flush its Listeners.
     */
    function closeWalker() {
        _log('MSG', 'Closing Walker');

        flushListeners();
        enableScroll();
        PFWalkWrapper.css({marginTop: '-500px', marginLeft: '-500px'});
        PFWalkWrapper.addClass('closed');
        setTimeout(function () {
            PFWalkWrapper.css('display', 'none');
            PFWalkWrapper.removeClass('opened');
        }, PFWalk_TRANSITION_DURATION);

        _log('MSG', 'Walker Closed!');
    }

    /***
     * Set the properties for the PFWalk.
     * @param {string} content The content that will be displayed in the PFWalk
     * @param {string} color A CSS valid color
     * @param {string} acceptText The text that will be displayed in the accept button
     */
    function setProperties(content, color, acceptText) {
        color = !!color ? color : PFWalk_DEFAULT_COLOR;
        PFWalkContent.html(content);
        PFWalkContent.attr('t-dd', 'update');
      PFWalkWrapper.removeClass(function (index, css) {
	return (css.match (/\bPFC-\S+/g) || []).join(' '); // removes anything that starts with "page-"
}).addClass('PFC-' + color);
        PFWalkActionButtonText.text(acceptText);
        PFWalkActionButtonText.attr('t-dd', 'update');
    }

    /***
     * Create the function that updates the PFWalker to a target
     * @param {JQueryElement} target  The target to set the update function
     * @returns {function} Update handler to call in the listeners
     */
    function createUpdateHandler(target) {
        _log('PFWalk_UPDATE', 'Creating UpdateHandler for #' +target[0].id);

        var updateHandler =  function () {
            _log('MSG', 'Updating and rendering');
            locateTarget(target, function () {
                renderFrame(target, function () {
                    renderContent(target);
                });
            });


        };
        updateHandler.toString = function () {
            return 'updateHandler -> #' + target[0].id;
        };
        return updateHandler;
    }

    /***
     * Setup the update listeners (onResize, MutationObserver) and the close callback.
     * @param {JQueryElement} target The target to set the listeners
     * @param {function} onClose Close callback
     */
    function setupListeners(target, onClose) {
        if(!!PFWalkUpdateHandler) flushListeners();
        PFWalkUpdateHandler = createUpdateHandler(target);

        $(window).on('resize', PFWalkUpdateHandler);
        PFWalkActionButton.on('click', function actionCallback(){
            if (!!onClose) onClose();
            if (!!$.PFWalk._points && !!$.PFWalk._points[$.PFWalk._currentIndex + 1]) {
                $.PFWalk._currentIndex++;
                setWalker($.PFWalk._points[$.PFWalk._currentIndex].target, $.PFWalk._points[$.PFWalk._currentIndex]);
            } else {
                $.PFWalk._currentIndex = 0;
                $.PFWalk._points = null;
                if($.PFWalk._callback) $.PFWalk._callback();
                $.PFWalk._callback = null;
                closeWalker();
            }
            PFWalkActionButton.off('click', actionCallback);
        });
    }

    /***
     * Clean the listeners with the actual updateHandler
     */
    function flushListeners() {
        _log('PFWalk_UPDATER', 'Flushing handlers\n' + PFWalkUpdateHandler);
        if(!!$.PFWalk._mutationObserver) $.PFWalk._mutationObserver.disconnect();
        $.PFWalk._mutationObserver = null;
        $(window).off('resize', PFWalkUpdateHandler);
    }

    /***
     * Move the Walker to a target
     * @param {JQueryElement} target
     */
    function locateTarget(target, locateCallback) {
        var position = target.offset();
        var positionMode = window.getComputedStyle(target[0])['position'];
        var windowHeight = $(window).height();
        var documentHeight = $(document.body).height();

        var positionOutOfBounds = (positionMode == 'absolute' || positionMode == 'relative') &&
            parseInt(window.getComputedStyle(target[0])['top']) > documentHeight; // Test if the position of the target is out of the document height by a forced position;

        _log('PFWalk_LOCK', 'Moving Walker to:\n' + JSON.stringify(position, null, 2));
        _log('PFWalk_SCROLL', 'documentHeight: ' +documentHeight);

        var scrollTo = (position.top - (windowHeight / 2));
        _log('PFWalk_LOCK', 'Trying to centralize the target in the screen: \n ' + JSON.stringify({
            scrollTo: scrollTo,
            targetY: position.top,
            windowHeightPer2: windowHeight / 2,
            targetPositionMode: positionMode,
            positionOutOfBounds: positionOutOfBounds
        }, null, 2));

        if (scrollTo > 0 && positionMode != 'fixed') {
            _log('PFWalk_LOCK', 'Scrolling to ' +scrollTo);
            _log('PFWalk_SCROLL', 'scrollTo + windowHeight: ' +(scrollTo + windowHeight));
            if (scrollTo + windowHeight > documentHeight && !positionOutOfBounds) scrollTo = documentHeight - windowHeight; // Setting the scroll limit by the document's height
            _log('PFWalk_LOCK', 'Corrected scroll amount: ' +scrollTo);

            $('body,html').animate({
                scrollTop: scrollTo
            }, PFWalk_TRANSITION_DURATION, function () {
                locateCallback();
            });
        } else {
            _log('PFWalk_LOCK', 'Resetting scroll');
            $('body,html').animate({
                scrollTop: 0
            }, PFWalk_TRANSITION_DURATION, function () {
                locateCallback();
            });
        }

    }

    function renderFrame(target, renderCallback) {
        var position = target.offset();
        var height = target.outerHeight();
        var width = target.outerWidth();

        var holeSize = height > width ? height : width; // Catch the biggest measure
        if (holeSize < PFWalk_MIN_SIZE) holeSize = PFWalk_MIN_SIZE; // Adjust with default min measure if it not higher than it
        _log('PFWalk_LOCK', 'Walk hole size ' +holeSize+ 'px');

        PFWalkWrapper.css({
            'height': (holeSize + PFWalk_PADDING) + 'px',
            'width': (holeSize + PFWalk_PADDING) + 'px',

            'margin-left': -((holeSize + PFWalk_PADDING) / 2) + 'px',
            'margin-top': -((holeSize + PFWalk_PADDING) / 2) + 'px',

            'left': (position.left + (width / 2)) + 'px',
            'top': (position.top + (height / 2)) + 'px',
        });

        setTimeout(function () {
            renderCallback();
        }, 250);
    }

    function renderContent(target, renderCallback) {
        var position = target.offset();

        var itCanBeRenderedInRight = position.left + (PFWalkWrapper.outerWidth() - PFWalk_PADDING) + PFWalkContentWrapper.outerWidth() < $(window).outerWidth();
        var itCanBeRenderedInLeft = (position.left - PFWalk_PADDING) - PFWalkContentWrapper.outerWidth() > 0;

        var itCanBeRenderedInTop = PFWalkWrapper[0].getBoundingClientRect().top - PFWalkContentWrapper.outerHeight() > 0;
        var itCanBeRenderedInBottom = PFWalkWrapper[0].getBoundingClientRect().top + PFWalkWrapper.outerHeight() + PFWalkContentWrapper.outerHeight() < $(window).outerHeight();

        _log('PFWalk_CONTENT', 'itCanBeRenderedInRight: ' +itCanBeRenderedInRight);
        _log('PFWalk_CONTENT', 'itCanBeRenderedInLeft: ' +itCanBeRenderedInLeft);
        _log('PFWalk_CONTENT', 'itCanBeRenderedInTop: ' +itCanBeRenderedInTop);
        _log('PFWalk_CONTENT', 'itCanBeRenderedInBottom: ' +itCanBeRenderedInBottom);

        var positionLeft = '100%';
        var positionTop = '100%';
        var marginTop = 0;
        var marginLeft = 0;
        var textAlign = 'left';

        if (!itCanBeRenderedInRight) {
            positionLeft = itCanBeRenderedInLeft ? '-'+ PFWalkContentWrapper.outerWidth() +'px': (itCanBeRenderedInBottom ? '0%':  '25%');
            textAlign = itCanBeRenderedInLeft ? 'right' : 'center';
            marginTop = itCanBeRenderedInLeft ? 0 : (itCanBeRenderedInBottom ? '20px' : '-20px');
        }
        if (!itCanBeRenderedInBottom) {
            positionTop = itCanBeRenderedInTop ? '-'+ PFWalkContentWrapper.outerHeight() +'px': PFWalkWrapper.outerHeight() / 2 - PFWalkContentWrapper.outerHeight() / 2 + 'px';
            marginLeft = itCanBeRenderedInTop ? 0 : (!itCanBeRenderedInRight ? '-20px' : '20px');
        }

        PFWalkContentWrapper.css({
            'left': positionLeft,
            'top': positionTop,
            'text-align': textAlign,
            'margin-top': marginTop,
            'margin-left': marginLeft
        });

        if(renderCallback) renderCallback();
    }

    $.PFWalk = function (PFWalkPoints, callback) {
        $.PFWalk._points = PFWalkPoints;
        $.PFWalk._currentIndex = 0;
        $.PFWalk._callback = callback;

        $(PFWalkPoints[0].target).PFWalk(PFWalkPoints[0]);
    };

    /**
     * Global variable that holds the current PFWalk configuration.
     * @type {WalkPoint[]}
     */
    $.PFWalk._points = null;

    /**
     * Global variable that holds the current point index in _PFWalkPoints array.
     * @type {number}
     */
    $.PFWalk._currentIndex = 0;

    /**
     * Global variable that holds the MutationObserver that listen body modifications.
     * @type {MutationObserver}
     */
    $.PFWalk._mutationObserver = null;

    function init() {
        $('body').append(PFWalk_COMPONENT);
        PFWalkWrapper = $('#PFWalk-wrapper');
        PFWalkContentWrapper = $('#PFWalk-content-wrapper');
        PFWalkContent = $('#PFWalk-content');
        PFWalkActionButton = $('#PFWalk-action');
      PFWalkActionButtonText = $('#PFWalk-action .inside p');
    }
    init();

    //Locking scroll
    // left: 37, up: 38, right: 39, down: 40,
    // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
    var keys = {
        37: 1,
        38: 1,
        39: 1,
        40: 1,
        32: 1,
        33: 1,
        34: 1
    };

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
        $('html').css({
            'height': '100vh',
            'overflow': 'hidden'
        });
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove = preventDefault; // mobile
        document.onkeydown = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        $('html').css({
            'height': '',
            'overflow': ''
        });
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }
    window.enableScroll = enableScroll;
})(window.$);