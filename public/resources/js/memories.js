$(function() {

  $('.memories--item_image').on('mousedown', function(e) {
    $(this).addClass('pressed');
  });

  $('.memories--item_image').on('mouseup', function(e) {
    $(this).removeClass('pressed');
  });

});