$(document).on('click', '.PF-bottombar ul li', function(e) {
  $('.PF-bottombar ul li').removeClass('active');
  $(this).addClass('active');
});

$(document).on('click', '.PF-tabbar ul li:not(.not)', function(e) {
  $('.PF-tabbar ul li').removeClass('active');
  $(this).addClass('active');
  tabSlider(this);
});



/*SELECT*/

function PFSelect(selector) {

  var _self = this;
  _self.selector = selector;
  _self.option = {
    focusClass: "is-focused",
    checkedClass: "is-checked"
  };


  _self.init = function() {
    jQuery(selector).each(function() {
      var $selectHolder = jQuery(this);
      var $select = $selectHolder.find("select");
      // check if input already initialized
      if ($selectHolder.data("PF-select")) {
        return;
      }
      // add event listeners
      $select.on("change focus", _self.checkValue);
      $select.on("focus blur", _self.checkFocus);
      // trigger blur event after selection
      $select.on("change", function() {
        jQuery(this).blur();
      });
      // check on init
      _self.checkValue.call($select);

      $selectHolder.data("PF-select", true);
    });
  };

  _self.isChecked = function($select) {
    if ($select.find(":selected").length && $select.val() !== "") {
      return true;
    }
    return false;
  };

  _self.isFocused = function($select) {
    return $select.is(":focus");
  };

  _self.checkValue = function() {
    var $select = jQuery(this);
    var $selectHolder = $select.closest(_self.selector);

    // if value is not empty
    if (_self.isChecked($select)) {
      $selectHolder.addClass(_self.option.checkedClass);
    } else {
      $selectHolder.removeClass(_self.option.checkedClass);
    }
  };

  _self.checkFocus = function() {
    var $select = jQuery(this);
    var $selectHolder = $select.closest(_self.selector);

    // delay :focus check since on IOS its not being applied immediately
    setTimeout(function() {
      // if select has focus
      if (_self.isFocused($select)) {
        $selectHolder.addClass(_self.option.focusClass);
      } else {
        $selectHolder.removeClass(_self.option.focusClass);
      }
    }, 0);
  };

  _self.init();
}

$.initialize(".PF-select", function() {
  new PFSelect(this);
});

/*SELECT*/

$.initialize(".PF-tabbar ul li.active", function() {
  tabSlider(this);
});

function tabSlider(este) {
  var liactual;
  var litotal = 0;
  var oldwidth = $(this).outerWidth() + $(this).parent().parent().children('.slider').outerWidth();

  if (!$(this).closest('.PF-tabbar').hasClass('circle')) {
    $(este).parent('ul').children('li').removeClass('ripple');
  }

  $(este).parent('ul').children('li').each(function() {

    if ($(this).hasClass('active')) {
      liactual = $(this).outerWidth();

      if (liactual < 50) {
        litotal = litotal - 10;
        liactual = 70;
      }

      if ($(this).closest('.PF-tabbar').hasClass('circle')) {
        $(this).parent().parent().children('.slider').css({
          'width': oldwidth
        }).delay(1000).css({
          'transform': 'translate(' + litotal + 'px, 0)',
          'width': liactual + 10
        });
      } else {
        litotal = litotal + 15;
        $(this).parent().parent().children('.slider').css({
          'width': oldwidth
        }).delay(1000).css({
          'transform': 'translate(' + litotal + 'px, 0)',
          'width': liactual - 50
        });
      }

      return true;
    } else {
      if ($(this).css('display') != 'none') {
        if ($(this).closest('.PF-tabbar').hasClass('circle')) {
          litotal = litotal + 10 + $(this).outerWidth();
        } else {
          litotal = litotal + $(this).outerWidth();
        }
      }
    }
  });

}


/*HIDEDCONTENT*/

$(document).on('click', '.PF-hidedcontent .PF-button.openclose', function(e) {
  $(this).parent().parent().toggleClass('open');
});

/*HIDEDCONTENT*/

/*TABS*/

$.initialize(".PF-tabbar ul li.active", function() {
  var thistab = $(this);
  var tabfor = $(this).attr('data-for');
  var PFtabs = $(this).closest('.PF-tabs');
  var tabs = PFtabs.children('.tabs');
  var taburl = $(this).attr('data-taburl');
  tabs.children(".tab:not('" + tabfor + "')").removeClass('active');
  tabs.children("[data-name='" + tabfor + "']").addClass('active');
  if (taburl && !$(this).hasClass('tabloaded')) {
    Kloading("start");
    tabs.children("[data-name='" + tabfor + "']").load(taburl, function() {
      Kloading("end");
    });
  }
  $(this).addClass('tabloaded');
});

$.initialize(".PF-tabbar ul li[preload], .PF-tabs[preload] .PF-tabbar li, .PF-tabs.hideempty .PF-tabbar li", function() {
  var thistab = $(this);
  var tabfor = $(this).attr('data-for');
  var PFtabs = $(this).closest('.PF-tabs');
  var tabs = PFtabs.children('.tabs');
  var taburl = $(this).attr('data-taburl');
  if (taburl && !$(this).hasClass('tabloaded')) {
    tabs.children("[data-name='" + tabfor + "']").load(taburl, function() {
      if (PFtabs.hasClass('hideempty') && tabs.children("[data-name='" + tabfor + "']").html().length == 0) {
        thistab.hide();
        thistab.parent().children('li').first().click();
      }
    });

  }
  $(this).addClass('tabloaded');
});

$(document).on('click', '.PF-tabs .PF-tabbar li', function(e) {
  var thistab = $(this);
  var tabfor = $(this).attr('data-for');
  var PFtabs = $(this).closest('.PF-tabs');
  var tabs = PFtabs.children('.tabs');
  var taburl = $(this).attr('data-taburl');
  var taballowreload = $(this).attr('tab-allowreload');
  tabs.children(".tab:not('" + tabfor + "')").removeClass('active');
  tabs.children("[data-name='" + tabfor + "']").addClass('active');
  if (taburl && !$(this).hasClass('tabloaded') || taburl && taballowreload != null) {
    Kloading("start");
    tabs.children("[data-name='" + tabfor + "']").load(taburl, function() {
      Kloading("end");
    });
  }
  $(this).addClass('tabloaded');
});

/*TABS*/


/*DOWNMENU*/

function downmenudd(url, scrolldiv) {
  Kloading("start");
  if (url !== '') {

    if (scrolldiv != null) {
      var top = $(scrolldiv).offset().top - 100;
      $('html,body').animate({
        scrollTop: top
      }, 1000);
    }

    $.get(url, function(data) {
        $('#body').addClass('overflowhidden');
        Kloading("end");
        $('#downmenudd').addClass('open');
        $('.side-menu-overlay').addClass('closedownmenudd').addClass('show');
        $('#downmenudd').html(data);
      })
      .fail(function() {
        $('#body').removeClass('overflowhidden');
        Kloading("end");
        $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closedownmenudd');
        $('#downmenudd').removeClass('open');
      });
  } else {
    $('#body').removeClass('overflowhidden');
    $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closedownmenudd');
    Kloading("start");
    $('#downmenudd').removeClass('open');
  }

}

$(document).on('click', '.closedownmenudd, #downmenudd li', function() {
  $('#body').removeClass('overflowhidden');
  $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closedownmenudd');
  Kloading("end");
  $('#downmenudd').removeClass('open');
});

/*DOWNMENU*/





/*TEXTAREA*/

$.initialize(".PF-textfield textarea", function() {
  autosize($(this));
});





$.initialize(".PF-collapsible", function() {
  $(this).children('li').first().addClass('active').find('.collapsible-body').slideDown();
});

$(document).on('click', '.PF-collapsible .collapsible-header', function(e) {
  var dropDown = $(this).closest('li').find('.collapsible-body');

  $(this).closest('.PF-collapsible').find('.collapsible-body').not(dropDown).slideUp();

  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
  } else {
    $(this).closest('.PF-collapsible').find('.collapsible-header.active').removeClass('active');
    $(this).addClass('active');
  }

  dropDown.stop(false, true).slideToggle();

  e.preventDefault();
});