<div class="more-less reset">
  <div class="ball">
    <div class="stick"></div>
    <div class="circle"></div>
    <div class="ripple-outward"></div>
  </div>
  <div class="both PF shadow">
    <ul>
      <li class="PF PFC-blue ripple more">
        <div class="more-icon"></div>
        <span t-dd>More</span>
      </li>
      <li class="PF PFC-red ripple less">
        <span t-dd>Less</span>
        <div class="less-icon"></div>
      </li>
    </ul>
  </div>
</div>