<?php

if(!isset($language) and isset($_COOKIE["language"])){
	$language = $_COOKIE["language"];
}
if(isset($_GET['language'])){ $language_get = $_GET['language']; } else { $language_get = null; }
if(isset($_COOKIE['languageoriginal'])){ $language_original = $_COOKIE['languageoriginal']; } else { $language_original = null; }
if(isset($_COOKIE['languageordenador'])){ $language_ordenador = $_COOKIE['languageordenador']; } else { $language_ordenador = null; }

//CHECK IF LANGUAGE EXIST
$language_exist = substr($language_get, 0, 2);
$language_exist=mysqli_query($con,"SELECT * FROM languages WHERE code IN ('$language_exist') AND enabled='1' LIMIT 1");
if(!mysqli_num_rows($language_exist)) {
	$language_get = null;
}

//CHECK IF COUNTRY AND LANGUAGE EXIST
$country_exist = substr($language_get, 3);
$country_exist=mysqli_query($con,"SELECT * FROM languages_countries WHERE LOWER(code) IN (LOWER('$country_exist')) AND enabled='1' LIMIT 1");
if(!mysqli_num_rows($country_exist)) {
	$language_get = null;
}

if($language_get){
	$urlactuallanguageget = str_replace("?language=$language_get&", "?", $server_url);
	$urlactuallanguageget = str_replace("?language=$language_get", "", $urlactuallanguageget);
	$urlactuallanguageget = str_replace("&language=$language_get", "", $urlactuallanguageget);
	header('Location: '.$urlactuallanguageget);
}

if($language_get){
	unset($_COOKIE['language']);
	setcookie("language", $language_get, null, "/");
	$language = $language_get;
}

if(!$language){
	//IF THE LANGUAGE IS EMPTY AND DOESNT GET A NEW ONE THIS WILL BE THE DEFAULT
	if(!$language_get){
		$language_get = "en-US";
	}
	setcookie("language", $language_get, null, "/");
	setcookie("languageoriginal", $language_get, null, "/");
	setcookie("languageordenador", $language_get, null, "/");
	$language = $language_get;
}

if($language_get and !$user_mismo_id){
	unset($_COOKIE['languageordenador']);
	setcookie("languageordenador", $language_get, null, "/");
}

//IF SESSION AND NEW LANGUAGE EXIST UPDATE LANGUAGE OF THE USER
if($user_mismo_id and $language_get){
	$language_get_array = json_encode(array("$language_get"));
	mysqli_query($con, "UPDATE users SET language='$language_get_array' WHERE user_id='$user_mismo_id'");
}

//IF SESSION EXIST SET LANGUAGE OF THE USER
if($user_mismo_id and $user_mismo_language){
	$language = $user_mismo_language;
}

if($language == 'ca-CT'){
	$language = "ca-ES";
}

if(isset($_GET["lang"])){
	$language = $_GET["lang"];
}

?>