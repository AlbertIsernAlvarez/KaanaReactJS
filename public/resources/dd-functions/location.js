if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(updatePosition);
}

function updatePosition(position) {
	$.post( "./pantallas/account/update/basic-info/location.php", { latitude: position.coords.latitude, longitude: position.coords.longitude } );
}