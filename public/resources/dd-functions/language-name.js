$.initialize("[language-name]", function() {
	$(this).load('./pantallas/languages/language-name.php?lang=' + $(this).text());
	$(this).attr('language-name', '');
});

$(document).on("DOMSubtreeModified", "[language-name]", function() {
	if ($(this).attr('language-name') == 'update') {
		$(this).load('./pantallas/languages/language-name.php?lang=' + $(this).text());
		$(this).attr('language-name', '');
	}
});