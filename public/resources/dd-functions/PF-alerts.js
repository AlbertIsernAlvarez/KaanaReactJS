var alertdd = function() {
      var holder, elem, animIn, animOut, that = {};
      that.init = function(options) {
        if (typeof elem === "undefined") $("<div>", {
          id: "PF-alertsWrapper",
          class: "PF-alertsWrapper"
        }).appendTo("body");
        elem = $(options.selector);
        animIn = typeof options.animationIn === "undefined" ? "fadeInUp" : options.animationIn;
        animOut = typeof options.animationOut === "undefined" ? "fadeOutDown" : options.animationOut;
      };
      that.clear = function() {
        $(".PF-alert").remove();
      };
      that.show = function(text, duration, action1, action2, classes, contentclasses) {
        if (text !== null) {
          var ttl, dismissable;
          ttl = typeof duration === "undefined" ? 6E3 : duration;
          dismissable = parseInt(ttl) ? false : true;
          holder = $("<div>", {
            "class": "PF-alert PF-alertOpacity " + classes,
            css: {
              "display": "none"
            }
          });
          holder.attr("data-text", text);
          holder.appendTo(elem);
          $("<span>", {
            css: {
              "display": "none"
            }
          }).appendTo(holder);
          $("<span>", {
            "class": "PF-alertContent " + contentclasses,
            html: text
          }).appendTo(holder);
          if (typeof action1 === "undefined") {
            $("<div>", {
              "class": "PF-alertActionWrapper",
              css: {
                "display": "none"
              }
            }).appendTo(holder);
            holder.find(".PF-alertActionWrapper").append($("<span>", {
              "class": "PF-alertAction",
              "role": "link"
            }));
            holder.find(".PF-alertActionWrapper").append($("<span>", {
              "class": "PF-alertAction PF-alertSpacer",
              "role": "link",
              css: {
                "display": "none"
              }
            }))
          } else {
            $("<div>", {
              "class": "PF-alertActionWrapper"
            }).appendTo(holder);
            holder.find(".PF-alertActionWrapper").append($("<span>", {
              "class": "PF-alertAction " + action1["class"],
              "role": "link",
              "onclick": action1["onclick"],
              html: action1.title
            }).attr("data-pass", action1.passData));
            if (typeof action2 === "undefined") holder.find(".PF-alertActionWrapper").append($("<span>", {
              "class": "PF-alertAction PF-alertSpacer",
              "role": "link",
              css: {
                "display": "none"
              }
            }));
            else holder.find(".PF-alertActionWrapper").append($("<span>", {
              "class": "PF-alertAction PF-alertSpacer " + action2["class"],
              "role": "link",
              "onclick": action2["onclick"],
              html: action2.title
            }).attr("data-pass", action2.passData))
          }
          if (dismissable) {
            holder.append($("<button>", {
              "class": "PF-alertSpacer PF-alertTransparent",
              "title": "Dismiss"
            }));
            holder.find("button").append($("<i class='material-icons'>&#xE5CD;</i>", {
              "alt": "Dismiss"
            }));
          }
          /*TRANSLATE IT*/
          holder.find(".PF-alertContent").attr('t-dd', '');
          holder.find(".PF-alertActionWrapper span").attr('t-dd', '');
          if (jQuery.fn.animateCSS) {
            holder.animateCSS(animIn, {
              duration: 400
            });
            if (!dismissable) holder.animateCSS(animOut, {
              duration: 600,
              delay: ttl,
              callback: function() {
                $(this).delay(200).remove()
              }
            });
            else holder.find("button").on("click", function(e) {
              e.preventDefault();
              $(this).parent().animateCSS(animOut, {
                duration: 600,
                callback: function() {
                  $(this).delay(200).remove()
                }
              })
            })
          } else {
            holder.slideDown(200);
            if (!dismissable) holder.delay(ttl).slideUp(200, function() {
              $(this).remove()
            });
            else holder.children("button").on("click", function(e) {
              e.preventDefault();
              $(this).parent().slideUp(200, function() {
                $(this).remove()
              })
            })
          }
          $('.PF-alert[data-text="'+text+'"]:not(:last):first').remove();
        }
      };
      return that
    }();


    // INIT AND USAGE
    alertdd.init({
      "selector": "#PF-alertsWrapper"
    });

    window.addEventListener("offline", offlinealert);
    window.addEventListener("online", onlinealert);

    function offlinealert() {
      window.stop();
      $('.offlinenotification i').click();
      alertdd.show('You are offline', '', '', '', 'offlinenotification', 't-dd');
    }

    function onlinealert() {
      $('.offlinenotification i').click();
    }
    
    $(document).on('click', '.PF-alert .close', function(e) {
        e.preventDefault();
        $(this).parent().parent().slideUp(200, function() {
          $(this).remove();
        });
      });

    /*
      alertdd.show('Kaana Notification', 'dismissable or 4E3 as integer', {
        "title": "Support Links with Jquery",
        "class": "jq-your-class",
        "passData": "your data to data-pass attribute"
      }, {
        "title": "Did I mention Dual Links are supported?"
      });

      setTimeout(function() {
        alertdd.show('Hello Mate!');
      }, 1000);

      setTimeout(function() {
        alertdd.show('Did you like it?');
      }, 1500);

      setTimeout(function() {
        alertdd.show('Kaana but we re-wrote it!');
      }, 1000);

      setTimeout(function() {
        alertdd.show('But we love it!', 'permanent', {
          "title": "who we are?",
          "class": "jq-out",
          "passData": "vayes"
        });
      }, 2500);

      $(document).on('click', '.jq-out', function(e) {
        e.preventDefault();
        var url = 'https://kaana.io/';
        window.open(url, '_blank');
      });
      */