var MediaStream = null;
var openddajax = null;
var firstopendd = null;
var lasturlopendd = null;

function cleanopendd() {
  $('.contenedor').empty();
  $('#bottombar').empty();
  $('#menuleft').empty();
  $('.PF-hidedcontent.open').removeClass('open');
  $('#header').removeClass('shadow');
  $('#body').removeClass('surface-enabled');
  $('.PF-tabbar.full').removeClass('full');
  $('.PF-tabbar.flex').removeClass('flex');
  $('#header').removeClass('hidden');
  Kloading("end");
  $('#downmenudd').removeClass('open');
  $('#body').removeClass('overflowhidden');

  if ($('.search_container').length === 0) {
    $('#buscador').val('');
  }

  //WINDOWDD
  $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closewindowdd');
  $('#side-menu-left').removeClass('active');
  $('#ddwindow .PF-progress.loading').show();
  $('#ddwindow').removeClass('open');
  //WINDOWDD

  $("body[class*='PFC-']").removeClass(function(index, css) {
    return (css.match(/(^|\s)PFC-\S+/g) || []).join(' ');
  });
  if (MediaStream !== null) {
    MediaStream.stop();
  }
}

function opendd(queabrir, click) {
  if (navigator.onLine) {
    if (queabrir === '/') {
      queabrir = "";
    }
    openddajax = $.ajax({
      url: "./screen.php" + queabrir,
      type: "get",
      cache: false,
      contentType: false,
      processData: false,
      beforeSend: function() {
        Kloading("start");
        if (openddajax != null) {
          openddajax.abort();
        }
        for (var x = 0; x < xhr.length; x++) {
          xhr[x].abort();
          xhr.splice(x, 1);
        }
      },
      success: function(data) {
        $('html').animate({
          scrollTop: 0
        }, "slow", function() {
          cleanopendd();
          $('.contenedor').off().empty().html(data).find("script").each(function(i) {
            eval($(this).text());
            return false;
          });
        });

      },
      error: function(xhr, ajaxOptions, thrownError) {
        if (thrownError !== 'abort' && thrownError) {
          alertdd.show('Error: ' + thrownError);
        }
      }
    }).always(function( data ) {
      Kloading("end");
    });
  } else {
    alertdd.show("There' s no internet connection");
  }
  lasturlopendd = queabrir;
  return false;
}

$(document).on("click", "[opendd-href], .opendd", function(e) {

  e.preventDefault();

  var $this = $(this),
    title = $this.text();

  if ($(this).attr('opendd-href')) {
    var url = $(this).attr('opendd-href');
  } else {
    var url = $(this).attr('href');
  }
  if (!url) {
    url = "/";
  }
  if (lasturlopendd !== url) {
    history.pushState({
      url: url,
      title: "title"
    }, "title", url);
  }

  opendd(url);
  return false;
});

var timeoutloadopenddgo = null;

function openddgo(url) {
  if (!url) {
    url = "/";
  }
  clearTimeout(timeoutloadopenddgo);
  timeoutloadopenddgo = setTimeout(function() {
    if (lasturlopendd !== url) {
      history.pushState({
        url: url,
        title: "title"
      }, "title", url);
    }
    opendd(url);
    return false;
  }, 500);
}


var timeoutloadpopstate = null;
window.onpopstate = function(e) {
  clearTimeout(timeoutloadpopstate);
  timeoutloadpopstate = setTimeout(function() {
    var state = e.state;
    if (state !== null) {
      opendd(state.url);
    } else {
      opendd("");
    }
  }, 500);
}