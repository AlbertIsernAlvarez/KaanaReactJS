document.querySelector('#dynamicManifest').setAttribute('href', '/resources/dynamicManifest.json');

function genDynamicManifest(app) {
		$.get("/apps/"+app+"/assets/manifest.json", function(data, status){
			if(data){
				document.querySelector('#dynamicManifest').setAttribute('href', '/apps/'+app+'/assets/manifest.json');
			}
		});
		
	if(!myDynamicManifest){
		var myDynamicManifest = {
			"short_name": "Kaana",
			"name": "Kaana",
			"icons": [{
				"src": "https://img.kaana.io/logo/app/96.png?not",
				"sizes": "96x96",
				"type": "image/png"
			}, {
				"src": "https://img.kaana.io/logo/app/144.png?not",
				"sizes": "144x144",
				"type": "image/png"
			}, {
				"src": "https://img.kaana.io/logo/app/192.png?not",
				"sizes": "192x192",
				"type": "image/png"
			}, {
				"src": "https://img.kaana.io/logo/logo_app.png?not",
				"sizes": "512x512",
				"type": "image/png"
			}],
			"start_url": "/?client=web",
			"display": "standalone",
			"background_color": "#ffffff",
			"theme_color": "#ffffff",
			"orientation": "portrait"
		}
	}

/*
	if (myDynamicManifest) {
		const stringManifest = JSON.stringify(myDynamicManifest);
		const blob = new Blob([stringManifest], {
			type: 'application/json'
		});
		const manifestURL = URL.createObjectURL(blob);
		document.querySelector('#dynamicManifest').setAttribute('href', manifestURL);
	}
	*/

}