$.initialize("[follow-button]", function() {
  var thisfollowbutton = $(this);
  var text = thisfollowbutton.children('.follow-text');
  var id = thisfollowbutton.attr("data-id");
  var type = thisfollowbutton.attr("data-type");
  var classes = thisfollowbutton.attr("data-classes");
  var size = thisfollowbutton.attr("data-size");
  if (thisfollowbutton.attr("data-id") != null && thisfollowbutton.attr("data-type") != null) {
    Kloading("start");
    $.post("./resources/followbutton/followbutton.php", {
        id: id,
        type: type,
        classes: classes,
        size: size
      })
      .done(function(data) {
        thisfollowbutton.replaceWith(data);
        Kloading("end");
      });
  }
});

$.initialize(".follow-button-container", function() {
  followbuttonstate($(this));
});

$(document).on('click', '.follow-button-container .follow-button', function(e) {
  followbuttonupdate($(this).parent());
  e.stopPropagation();
  e.preventDefault();
});

function followbuttonupdate(thisfollowbutton) {
  var id = thisfollowbutton.attr("data-id");
  var type = thisfollowbutton.attr("data-type");
  var classes = thisfollowbutton.attr("data-classes");
  var size = thisfollowbutton.attr("data-size");
  var thisfollowbutton_all = $('.follow-button-container[data-unique="' + type + '.' + id + '"]');
  var thisfollowbuttonbtn = thisfollowbutton_all.children('.follow-button');
  var text = thisfollowbuttonbtn.children('.follow-text');
  Kloading("start");
  $.post("./resources/followbutton/update.php", {
      id: id,
      type: type,
      classes: classes,
      size: size
    })
    .done(function(data) {
      followbuttonstate(thisfollowbutton, 'update');
      Kloading("end");
    });
}

function followbuttonstate(thisfollowbutton, action) {
  var id = thisfollowbutton.attr("data-id");
  var type = thisfollowbutton.attr("data-type");
  var classes = thisfollowbutton.attr("data-classes");
  var size = thisfollowbutton.attr("data-size");
  var thisfollowbutton_all = $('.follow-button-container[data-unique="' + type + '.' + id + '"]');
  var thisfollowbuttonbtn = thisfollowbutton_all.children('.follow-button');
  var text = thisfollowbuttonbtn.children('.follow-text');
  var followingnumbers = $('[data-followingnumbers="' + type + '.' + id + '"]');
  var followingnumbers_now = $('[data-followingnumbers="' + type + '.' + id + '"]').text();
  Kloading("start");
  $.post("./resources/followbutton/state.php", {
      id: id,
      type: type,
      classes: classes,
      size: size
    })
    .done(function(data) {
      if (data == false) {
        thisfollowbuttonbtn.removeClass("following");
        text.attr("t-dd", "update").text("Follow");
        if (action == 'update' && followingnumbers_now > 0) {
          followingnumbers.text(--followingnumbers_now);
        }
      } else {
        thisfollowbuttonbtn.addClass("following");
        text.attr("t-dd", "update").text("Following");
        if (action == 'update') {
          followingnumbers.text(++followingnumbers_now);
        }
      }
      Kloading("end");
    });
}