<?php
function urlinfo($url)
{
  global $con;

  $ch = curl_init();
  
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  
  $html = curl_exec($ch);
  curl_close($ch);
  
  $doc = new DOMDocument();
  @$doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
  $nodes = $doc->getElementsByTagName('title');
  
  $title = $nodes->item(0)->nodeValue;
  $metas = $doc->getElementsByTagName('meta');
  
  for ($i = 0; $i < $metas->length; $i++)
  {
    $meta = $metas->item($i);
    if($meta->getAttribute('name') == 'description')
      $description = $meta->getAttribute('content');
    if($meta->getAttribute('property') == 'og:image')
      $image = $meta->getAttribute('content');
    if($meta->getAttribute('name') == 'keywords')
      $keywords = $meta->getAttribute('content');
  }

  if(!$title or !$image){
  preg_match("/<title>(.+)<\/title>/siU", file_get_contents($url), $title_matches);
  if($title_matches[1]){ $title = $title_matches[1]; }
  $tags = get_meta_tags($url);
  if($tags['description']){ $description = $tags['description']; }
  if($tags['author']){ $author = $tags['author']; }
  if($tags['keywords']){ $keywords = $tags['keywords']; }
  if($tags['twitter:image']){ $image = $tags['twitter:image']; }
  }

  $url_escape = mysqli_real_escape_string($con, $url);
  $title_escape = mysqli_real_escape_string($con, $title);
  $description_escape = mysqli_real_escape_string($con, $description);
  $image_escape = mysqli_real_escape_string($con, $image);
  $author_escape = mysqli_real_escape_string($con, $author);
  $keywords_escape = mysqli_real_escape_string($con, $keywords);
  mysqli_query($con, "INSERT INTO links (url, title, description, image, author, keywords) VALUES('$url_escape', '$title_escape', '$description_escape', '$image_escape', '$author_escape', '$keywords_escape'); ");

  return array(
    "domain" => $domain,
    "title" => $title,
    "description" => $description,
    "author" => $author,
    "image" => $image,
    "keywords" => $keywords);
  
}
?>