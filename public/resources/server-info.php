<?php
if($_SERVER["QUERY_STRING"]){ $server_querystring = $_SERVER["QUERY_STRING"]; }

if($window_id){
	$server_querystring .= "&windowid=" . $window_id;
}

$server_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER["HTTP_HOST"]."/?".$server_querystring;
?>