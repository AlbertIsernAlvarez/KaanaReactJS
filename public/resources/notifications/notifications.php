<html>

<head>
  <style>
    .PF-notifications {
    max-width: 23rem;
    width: 100%;
    height: 100%;
    max-height: 100%;
    position: fixed;
    right: -100vw;
    top: 0;
    bottom: 0;
    z-index: 9991;
    transition: .2s;
    padding: 1em;
    display: flex;
    pointer-events: none;
    opacity: 0;
    }

    .PF-notifications.open {
      right: 0;
      opacity: 1;
    }

    .PF-notifications>.container {
      height: fit-content;
      margin: auto;
      border-radius: 1em;
      display: flex;
      width: 100%;
    }

    .PF-notifications>.container>.groups {
    overflow: auto;
    width: 100%;
    height: fit-content;
    margin: auto;
    background-color: rgb(var(--PF-color-surface));
    transition: .2s;
    border-radius: 1em;
    background-position: center;
    background-size: 10rem;
    background-repeat: no-repeat;
    max-height: 100%;
    pointer-events: all;
    }

    .PF-notifications>.container>.groups>.group {
      width: 100%;
    }
    
    .PF-notifications>.container>.groups:empty {
      background-image: url('//img.kaana.io/notificaciones/campana.gif');
      min-height: 20em;
    }
    
    .PF-notifications .container>.groups .PF-notification {
      overflow: hidden;
      user-select: none;
      width: 100%;
      display: flex;
      flex-direction: column;
      position: relative;
      background: rgb(var(--PF-color-surface));
      /*box-shadow: 0 1px 1.5px 0 rgba(0, 0, 0, .12), 0 1px 1px 0 rgba(0, 0, 0, .24);*/
      color: rgb(var(--PF-color-on-surface));
    }
    
    .PF-notifications .container>.groups .PF-notification:first-child {
      border-radius: 1em 1em 0 0;
    }
    
    .PF-notifications .container>.groups .PF-notification:last-child {
      border-radius: 0 0 1em 1em;
    }
    
    .PF-notifications .container>.groups .PF-notification+.PF-notification:not(.music) {
      border-top: solid 1px rgba(0, 0, 0, 0.1);
    }
    
    .PF-notification .info,
    .PF-notification .image {
      position: relative;
    }
    
    .PF-notification .info {
      background-color: rgb(var(--PF-color-surface));
      width: 100%;
      z-index: 2;
      padding: 0.5rem;
      border: none;
      display: flex;
      flex-direction: column;
    }
    
    .PF-notification.music .info {
      background-color: rgb(var(--PF-color-primary));
    }
    
    .PF-notification .image:after {
      background: linear-gradient(to right, rgb(var(--PF-color-primary)), transparent);
      content: '';
      height: 100%;
      position: absolute;
      top: 0px;
      left: 0px;
      width: 100%;
    }
    
    .PF-notification.settings .info #controls,
    .PF-notification.settings .info .specifics {
      display: none;
    }
    
    .PF-notification .content {
      width: 100%;
      display: inline-flex;
    }
    
    .PF-notification .info .specifics {
      padding: 0.5rem;
      width: 100%;
      font-size: 16px;
    }
    
    .PF-notification .info .specifics h1,
    .PF-notification .settings .settings-contents .details h1 {
      font-size: inherit;
      font-weight: 500;
      margin: 0px;
      overflow: hidden;
      display: -webkit-box;
      -webkit-box-orient: vertical;
    }
    
    .PF-notification:not(.full) .info .specifics h1,
    .PF-notification:not(.full) .settings .settings-contents .details h1 {
      -webkit-line-clamp: 2;
    }
    
    .PF-notification .info .specifics p,
    .PF-notification .settings .settings-contents .details p {
      color: rgba(var(--PF-color-on-surface), .8);
      font-size: 0.9em;
      overflow: hidden;
      display: -webkit-box;
      -webkit-box-orient: vertical;
    }
    
    .PF-notification:not(.full) .info .specifics p,
    .PF-notification:not(.full) .settings .settings-contents .details p {
      -webkit-line-clamp: 2;
    }
    
    .PF-notification .info #controls {
      padding: 0.5rem;
      width: 100%;
      display: flex;
      align-items: center;
    }
    
    .PF-notification .info #controls i {
      display: inline-block;
      font-size: 20px;
      margin: 0px 10px;
    }
    
    .PF-notification .info #controls i:first-child {
      margin-left: 0px;
    }
    
    .PF-notification .image {
      background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/1468070/odesza-in-return.jpg");
      background-position: center;
      background-size: cover;
      right: 0px;
      content: 0px;
      min-width: 90px;
    }
    
    .PF-notification .settings {
      background-color: rgb(var(--PF-color-surface));
      height: fit-content;
      display: none;
      position: relative;
      transition: all 0.5s, border-radius .75s;
      width: 100%;
      z-index: 11;
      flex: 1 auto;
      text-align: center;
    }
    
    .PF-notification.settings .settings {
      display: block;
    }
    
    .PF-notification .settings .settings-contents {
      height: fit-content;
      position: relative;
      transition: all 0.25s;
      padding: 0.5rem;
      padding-top: 0;
    }
    
    .PF-notification .settings .settings-contents .details {
      padding: 0.5rem;
      padding-top: 0;
    }
    
    .PF-notification.music .settings .settings-contents,
    .PF-notification.music .settings .settings-contents .details {
      padding: 0.5rem;
    }
    
    .PF-notification .settings .settings-contents .details h1 {
      font-weight: 500;
      margin: 0px;
    }
    
    .PF-notification .settings button {
      width: 100%;
      padding: 1rem;
      background-color: rgba(var(--PF-color-primary), .1);
      border: 2px solid rgb(var(--PF-color-primary));
      border-radius: 1em;
      cursor: pointer;
      color: rgb(var(--PF-color-primary));
    }
    
    .PF-notification.alerting .settings button:first-of-type,
    .PF-notification.silent .settings button:last-of-type {
      background-color: rgba(var(--PF-color-primary), .8);
      color: rgb(var(--PF-color-surface));
    }
    
    .PF-notification .settings button:first-of-type {
      margin-top: .5rem;
      margin-bottom: .5rem;
    }
    
    .PF-notification .service {
      display: flex;
      align-items: center;
      padding: 0.5rem 0;
      width: 100%;
      font-size: 12px;
    }
    
    .PF-notification .service i,
    .PF-notification .service h1 {
      font-size: inherit;
      margin: 0px;
      display: inline;
      vertical-align: middle;
    }
    
    .PF-notification .service i {
      margin-left: 0.5rem;
      margin-right: 0.5rem;
      font-size: 16px;
      cursor: pointer;
      transition: .2s;
    }

    .PF-notification.full .service i.downup {
      transform: rotate(180deg);
    }
    
    .PF-notification .service h1 {
      font-weight: 500;
      font-size: 13px;
      white-space: nowrap;
      display: flex;
      align-items: center;
    }
    
    .PF-notification .service h1.name {}
    
    .PF-notification .service h1.details {
      color: rgba(var(--PF-color-on-surface), .8);
    }
    
    .PF-notification .service h1.details:before {
      content: " • ";
      margin-right: 0.5rem;
      margin-left: 0.5rem;
      font-size: 10px;
      color: rgba(var(--PF-color-on-surface), .8);
    }

    @media (max-width: 40em) {
      .PF-notifications {
        max-width: 100%;
        padding: 0;
        padding-top: 20%;
        right: initial;
        bottom: -100vh;
        top: initial;
      }
      .PF-notifications.open {
        bottom: 0;
      }
      .PF-notifications>.container {
        margin-bottom: 0;
        border-radius: 1em 1em 0 0;
      }
      .PF-notifications>.container>.groups {
        height: 100%;
      }
    }
  </style>
</head>

<body>
  <div class="PF PF-notifications" id="notifications">
    <div class="container PF shadow sh64">
      <div class="groups"></div>
    </div>
  </div>

  <!----------<div class="PF-notification music PF-dark PFC-orange" id="PF-notification" onclick="$(this).toggleClass('settings');">
      <div class="content">
        <div class="info">
          <div class="service">
            <i class="material-icons">music_note</i>
            <h1 class="name">Kaana Play Music</h1>
            <h1 class="details">In Return</h1>
            <i class="material-icons downup">&#xE313;</i>
          </div>
          <div class="specifics">
            <h1>Always this day</h1>
            <p>Aloa aloe</p>
          </div>
          <div id="controls"><i class="material-icons">thumb_up</i><i class="material-icons">fast_rewind</i><i class="material-icons">play_arrow</i><i class="material-icons">fast_forward</i><i class="material-icons">thumb_down</i></div>
        </div>
        <div class="image"></div>
      </div>
      <div class="settings">
        <div class="settings-contents">
          <div class="details">
            <h1>Playback</h1>
            <p>1 out of 5 notification categories from this app</p>
          </div>
        </div>
      </div>
    </div>
    <div class="PF-notification" id="PF-notification" onclick="$(this).toggleClass('settings');">
      <div class="info">
        <div class="service">
          <i class="material-icons">music_note</i>
          <h1 class="name">Kaana Play Music</h1>
          <h1 class="details">In Return</h1>
          <i class="material-icons downup">&#xE313;</i>
        </div>
        <div class="specifics">
          <h1>Always this day</h1>
          <p>Aloa aloe</p>
        </div>
      </div>
      <div class="settings">
        <div class="settings-contents">
          <div class="details">
            <h1>Playback</h1>
            <p>1 out of 5 notification categories from this app</p>
          </div>
        </div>
      </div>
    </div>
    <div class="PF-notification" id="PF-notification">
      <div class="info">
        <div class="service">
          <i class="material-icons">music_note</i>
          <h1 class="name">Kaana Play Music</h1>
          <h1 class="details">In Return</h1>
          <i class="material-icons downup">&#xE313;</i>
        </div>
        <div class="specifics">
          <h1>Always this day</h1>
          <p>Aloa aloe</p>
        </div>
      </div>
      <div class="settings">
        <div class="settings-contents">
          <div class="details">
            <h1>Playback</h1>
            <p>1 out of 5 PF-notification categories from this app</p>
          </div>
        </div>
      </div>
    </div>
    ---------------->



  <script>

    $(document).on("click", ".opennotifications", function() {
      $('#notifications').addClass('open');
      $('.side-menu-overlay').addClass('show');
      $('#body').addClass('overflowhidden');
    });
  
    $(document).on("click", "#notifications .PF-notification .downup", function() {
      $(this).closest('.PF-notification').toggleClass('full');
    });

    $(document).on("contextmenu", "#notifications .PF-notification", function() {
      $(this).closest('.PF-notification').toggleClass('settings');
      return false;
    });

    var notificationsdiv = $('#notifications>.container>.groups');
    var notifications = [];

    function addnotifications(data) {

      data.forEach(function(notificationdata) {

        var id = notificationdata.id;
        var icon = notificationdata.icon;
        var activity = notificationdata.activity;
        var channel = notificationdata.channel;
        var title = notificationdata.title;
        var text = notificationdata.text;
        var content = notificationdata.content;
        var action = notificationdata.action;
        var date = notificationdata.date;
        var groupation = notificationdata.groupation;

        var notificationtemplate = $("<div class='PF-notification'></div>");

        notificationtemplate.append("<div class='info'></div>");

        notificationtemplate.children('.info').append("<div class='service'></div>");
        notificationtemplate.children('.info').append("<div class='specifics'></div>");

        if (icon) {
          notificationtemplate.children('.info').children('.service').append("<i class='material-icons'>" + icon + "</i>");
        }

        if (activity) {
          notificationtemplate.children('.info').children('.service').append("<h1 class='name'>" + activity + "</h1>");
        }

        if (channel) {
          notificationtemplate.children('.info').children('.service').append("<h1 class='details'>" + channel + "</h1>");
        }

        notificationtemplate.children('.info').children('.service').append("<i class='material-icons downup'>&#xE313;</i>");

        if (title) {
          notificationtemplate.children('.info').children('.specifics').append("<h1>" + title + "</h1>");
        }

        if (text) {
          notificationtemplate.children('.info').children('.specifics').append("<p>" + text + "</p>");
        }

        notificationtemplate.append("<div class='settings'></div>");
        notificationtemplate.children('.settings').append("<div class='settings-contents'></div>");
        notificationtemplate.children('.settings').children('.settings-contents').append("<div class='details'></div>");

        if (activity) {
          notificationtemplate.children('.settings').children('.settings-contents').children('.details').append("<h1>" + activity + " Notifications</h1>");
        }

        if (channel) {
          notificationtemplate.children('.settings').children('.settings-contents').children('.details').append("<p>" + channel + "</p>");
        }

        notificationtemplate.children('.settings').children('.settings-contents').children('.details').append("<button class='alerting PFC-blue ripple'>Alerting</button>");
        notificationtemplate.children('.settings').children('.settings-contents').children('.details').append("<button class='silent PFC-orange ripple'>Silent</button>");

        if (groupation === 'alerting') {
          notificationtemplate.addClass('alerting');
        } else if (groupation === 'silent') {
          notificationtemplate.addClass('silent');
        }

        //IF NOT EXISTS
        if (jQuery.inArray(id, notifications) === -1) {
          if (groupation === 'alerting') {
            notificationsdiv.prepend(notificationtemplate);
          } else if (groupation === 'silent') {
            notificationsdiv.append(notificationtemplate);
          }
          notifications.push(id);
        }

      });
    }

    var notificationsinterval = true;
    setInterval(function() {
      if(notificationsinterval == true){
      $.ajax({
        type: 'POST',
        url: './resources/notifications/sync.php',
        data: {
          nonotifications: notifications
        },
        beforeSend: function() {
          notificationsinterval = false;
        },
        success: function(data) {
          if (data) {
            addnotifications(data);
          }
          notificationsinterval = true;
        },
        error: function(xhr, status, error) {
          console.log("Notifications error: " + error);
        }
      });
      }
    }, 1500);
  </script>

</body>

</html>