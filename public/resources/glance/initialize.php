<script>
	$.initialize("[glance]", function() {
		var glance = $(this);
		/*var id = glance.attr("data-id");*/
		var id = Math.floor((Math.random() * 1000000) + 1000);
		var type = glance.attr("data-type");
		if (id) {
			$.post("./resources/glance/glance.php?<?=$server_querystring?>", {
					id: id,
					type: type
				})
				.done(function(data) {
					glance.replaceWith(data);
					setInterval(glance_start(id), 10000);
				});
		}
	});


function glance_start(id){
	<?php if($language === 'en-US'){?>
	$(".glance[data-id='"+id+"']>.container .text").html("<?=ucfirst($user_mismo_name)?>'s briefing");
	<?} else {?>
	$(".glance[data-id='"+id+"']>.container .text").html("<span t-dd>That's your briefing,</span> <?=ucfirst($user_mismo_name)?> ");
	<?}?>
	$(".glance[data-id='"+id+"']>.container .date").text(moment().format('dddd, MMMM D'));
}

</script>