<?php
$user_mismo_weather = mysqli_query($con,"SELECT currently FROM weather WHERE user_id='$user_mismo_id' LIMIT 1");
while($row_user_mismo_weather = mysqli_fetch_array($user_mismo_weather))
  {

    $user_mismo_weather_array = json_decode($row_user_mismo_weather['currently'], true);
    $user_mismo_weather_temperature = $user_mismo_weather_array['temperature'];
    $user_mismo_weather_temperature = substr($user_mismo_weather_temperature, 0, strpos($user_mismo_weather_temperature, "."));
    $user_mismo_weather_icon = $user_mismo_weather_array['icon'];
    $user_mismo_weather_icon_extension = ".png";
    if (in_array($user_mismo_weather_icon, array("clear-day", "partly-cloudy-day", "rain", "moon", "thunderstorms", "clear-night"))) {
      $user_mismo_weather_icon_extension = ".svg";
    }
  }
?>

<div class="PF glance block" data-id="<?=$_POST['id']?>">
  <div class="container">
    <h1 class="up text"></h1>
    <div class="down" ><p opendd-href="?p=genius&section=glance"><span class="date"></span></p><?php if($user_mismo_weather_temperature){?><p opendd-href="?app=weather"><span class="weather-icon" style="background-image: url('//img.kaana.io/weather/<?=$user_mismo_weather_icon.$user_mismo_weather_icon_extension?>');"></span><span><?=$user_mismo_weather_temperature?>ºC</span></p><?}?></div>
  </div>
</div>