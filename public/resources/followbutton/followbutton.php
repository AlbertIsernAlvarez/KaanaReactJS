<?php
$followbutton_id = $_POST['id'];
$followbutton_type = $_POST['type'];
$followbutton_size = $_POST['size'];
$followbutton_classes = $_POST['classes'];
if($followbutton_id == $user_mismo_id and $followbutton_type == 'user'){
  $followbutton_hide = "yes";
}
if($user_mismo_id and $followbutton_hide == null){?>
<div class="PF follow-button-container <?=$followbutton_classes?>" data-unique="<?=$followbutton_type?>.<?=$followbutton_id?>" data-id="<?=$followbutton_id?>" data-type="<?=$followbutton_type?>" data-size="<?=$followbutton_size?>" data-classes="<?=$followbutton_classes?>" >
  <a class="PF follow-button shadow">
    <svg xmlns="http://www.w3.org/2000/svg">
      <g>
        <rect class="plus__line1" width="2" height="12" x="5" y="0"></rect>
        <rect class="plus__line2" width="12" height="2" x="0" y="5"></rect>
      </g>
    </svg>
      <span class="follow-text" t-dd>Loading</span>
    </a>
</div>
<?}?>