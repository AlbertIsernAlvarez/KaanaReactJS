<?php

if(isset($_GET['os'])){ $client_os = $_GET['os']; }

if(!function_exists('browser_agent')){
    function browser_agent(){
        $browsers = ["Firefox", "Chrome", "Safari", "Opera", "MSIE", "Trident", "Edge"];
        $http_user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browser = null;

        foreach ($browsers as $browser) {
            if(strpos($http_user_agent, $browser) !== false) {
                $browser = $browser;
                break;
            }
        }

        switch ($browser) {
            case 'MSIE':
                $browser = 'Internet Explorer';
                break;
            case 'Trident':
                $browser = 'Internet Explorer';
                break;
            case 'Edge':
                $browser = 'Internet Explorer';
                break;
        }

        if($browser) {
			return $browser; 
		} else {
			return false;
		}
	}
}

$client_browser = browser_agent();

if(!function_exists('device_agent')){
	function device_agent($type_device){
	    $http_user_agent = $_SERVER['HTTP_USER_AGENT'];
		$device = '';
		
		if(stristr($http_user_agent,'ipad')) {
			$device = "ipad";
			$mobile = true;
		} else if(stristr($http_user_agent,'iphone') || strstr($http_user_agent,'iphone')) {
			$device = "iphone";
			$mobile = true;
		} else if(stristr($http_user_agent,'blackberry')) {
			$device = "blackberry";
			$mobile = true;
		} else if(stristr($http_user_agent,'android')) {
			$device = "android";
			$mobile = true;
		} else if(stristr($http_user_agent,'windows')) {
			$device = "windows";
		} else if(stristr($http_user_agent,'linux')) {
			$device = "linux";
		}

		if($type_device === 'mobile') {
		    if($mobile){
		        return $device;
		    } else {
		        return false;
		    }
		} elseif($type_device === 'laptop') {
			if($laptop){
		        return $device;
		    } else {
		        return false;
		    } 
		} elseif($type_device === 'tablet') {
		    if($tablet){
		        return $device;
		    } else {
		        return false;
		    }
		} elseif($device) {
			return $device; 
		} else {
			return false;
		}
	}
}

?>