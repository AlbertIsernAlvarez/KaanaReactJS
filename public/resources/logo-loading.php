<div class="logo-loading" id="logoloading">
  <html>

  <head>
    <style>
      body {
        overflow: hidden;
      }
      
      .logo-loading {
        margin: auto;
        text-align: center;
        background-color: rgba(var(--PF-color-background), .95);
        position: fixed;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
        z-index: 100000;
        display: flex;
        align-items: center;
        flex-direction: column;
      }
      
      .logo-loading .group {
        margin: auto;
        width: fit-content;
        height: fit-content;
        <?php if($client and device_agent('mobile')) {
          ?> display: none;
          <?
        }
        ?>
      }
      
      .logo-loading .group .fantasma {
        width: 5em;
        height: 5em;
        margin: auto;
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        transform: rotate(0deg);
        -webkit-animation: animationfantasma 1.5s infinite ease;
        -moz-animation: animationfantasma 1.5s infinite ease;
        -ms-animation: animationfantasma 1.5s infinite ease;
        -o-animation: animationfantasma 1.5s infinite ease;
        animation: animationfantasma 1.5s infinite ease;
      }
      
      .logo-loading .group .loading {
        width: 6em;
        display: block;
        height: 3px;
        margin: 12px auto;
        border-radius: 2px;
        background-color: rgba(var(--PF-color-on-surface), .4);
        position: relative;
        overflow: hidden;
        z-index: 1;
        border-radius: 10em;
      }
      
      .logo-loading .group .loading:before {
        content: '';
        height: 3px;
        width: 5rem;
        position: absolute;
        -webkit-transform: translate(-34px, 0);
        -moz-transform: translate(-34px, 0);
        -ms-transform: translate(-34px, 0);
        -o-transform: translate(-34px, 0);
        transform: translate(-34px, 0);
        background-color: rgb(var(--PF-color-on-surface));
        /*background: -webkit-linear-gradient(left, #E91E63, #2196F3);*/
        border-radius: 2px;
        -webkit-animation: animation 1.5s infinite ease;
        -moz-animation: animation 1.5s infinite ease;
        -ms-animation: animation 1.5s infinite ease;
        -o-animation: animation 1.5s infinite ease;
        animation: animation 1.5s infinite ease;
      }
      
      @keyframes animation {
        0% {
          left: 0;
        }
        50% {
          left: 100%;
        }
        100% {
          left: 0;
        }
      }
      
      @keyframes animationfantasma {
        0% {
          transform: rotate(0deg);
        }
        25% {
          transform: rotate(5deg);
        }
        50% {
          transform: rotate(-5deg);
        }
        100% {
          transform: rotate(0deg);
        }
      }

      .logo-loading .group>h1 {
        margin-top: .1em;
        margin-bottom: .2em;
        font-size: 1.8em;
        font-weight: bold;
      }

      .logo-loading .group .version {
        font-size: .8em;
        position: absolute;
        right: 0;
        bottom: 0;
        padding: 1em;
        color: rgb(var(--PF-color-on-surface), .6);
      }
    </style>
  </head>

  <body>
    <div class="group">
      <div style="background-image:url('//img.kaana.io/logo/logo_app.png?not');" class="fantasma"></div>
      <h1>Kaana</h1>
      <span class="loading"></span>
      <p class="version"><span t-dd>Version</span> <span t-dd><?=ucwords($last_version_channel)?></span> <?=$last_version_name?> <b>(<?=$last_version_number?>)</b></p>
    </div>
  </body>
  </html>
</div>