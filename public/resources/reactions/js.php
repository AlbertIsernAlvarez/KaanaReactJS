<script>

const Reaction = ({ emoji, number, classes, contentid, app }) => `
	<div class="PF PF-chip ripple ${classes}" reaction reaction-emoji="${emoji}" reaction-id="${contentid}" reaction-app="${app}">
		<div class="inside">
			<i>${emoji}</i>
			<p>${number}</p>
		</div>
	</div>
	`;
  
    $.initialize("[reactionsdd]", function() {
    var content_id = $(this).attr('content-id');
    var content_app = $(this).attr('content-app');
    reactionsresults(content_id, content_app);
  });

  function reactionsresults(content_id, content_app){
    xhr.push( $.ajax({
      method: "POST",
      url: "./resources/reactions/results.php",
      data: "id=" + content_id + "&app=" + content_app
    }).done(function(data) {
      $("[reactionsdd][content-id='"+content_id+"'][content-app='"+content_app+"']").html(data.map(Reaction).join(''));
    }) );
  }
  


  $(document).on("click", "[reaction]", function() {
    var content_id = $(this).attr('reaction-id');
    var content_app = $(this).attr('reaction-app');
    var emoji = $(this).attr('reaction-emoji');
    xhr.push( $.ajax({
      method: "POST",
      url: "./resources/reactions/insert-update.php",
      data: "emoji=" + emoji + "&id=" + content_id + "&app=" + content_app
    }).done(function(data) {
      reactionsresults(content_id, content_app);
    }) );
  });
</script>