<html>
	<head>
		<style type="text/css">
			
			@media (max-width: 75em) {
			
			.kamiku:hover, .kamiku.open { 
				margin:0!important;
			border-radius:0!important;
			width:100%!important;
			height:100%!important;
				max-width:100%!important;
				max-height:100%!important;
				margin:0!important;
				display:block!important;
			}
			
			}

			
			.color_kamiku {
				background: white;
			}
			
			.kamiku {
				max-width: 3.5rem;
				max-height: 3.5rem;
				width: 100%;
				height: 100%;
				text-align: center;
				display: inline-flex;
				align-items: center;
				border-radius: 100% 100% 100% 10px;
				position: fixed;
				bottom: 0;
				left:0;
				margin: 0.5rem;
				box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
				color: black;
				font-size: 1.5rem;
				transition: 0.5s;
				cursor: pointer;
				z-index: 999;
				font-family:Roboto;
				font-weight:100;
			}
			
			.kamiku:hover, .kamiku.open {
				color:white;
				background: #3f51b5;
			}
			
			.kamiku * {
				font-family:Roboto;
				font-weight:100;
			}
			
			.kamiku i {
				font-family:Material Icons;
			}

			<?php if($usandomovil == ''){?>.kamiku:hover,<?}?> .kamiku.open {
				box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
				border-radius: 2px;
				max-width: 25rem;
				max-height: 35rem;
				cursor: default;
				padding: 0;
			}

			.kamiku_menu_abierto:hover {
				max-width: 40rem;
			}

			.kamiku .contenido_kamiku .pantalla_kamiku {
				width: 100%;
				max-width: 100%;
			}

			.cards_kamiku::-webkit-scrollbar {
    width: 10px;
    background-color: #212121;
    height: 5px;
  	box-shadow: none;
}
      
      .cards_kamiku::-webkit-scrollbar-thumb {
        background-color: #37474F;
}
			
			

	<?php if($usandomovil == ''){?>.kamiku:hover .kmk,<?}?> .kamiku.open .kmk {
		display: none;
	}

	.kamiku .kmk {
		margin: auto;
		transition: 0.2s;
	}
			
			
			.kamiku .alertas_kamiku {
			position: fixed;
    	left: 4rem;
    	bottom: 0rem;
			max-width: 20rem;
			transition:0.5s;
			width:auto;
			height:auto;
			padding:0.5rem;
			text-align:center;
			}

	.kamiku .alertas_kamiku .alerta_kamiku {
    background: #fff;
    border: 1.1px solid #e0e0e0;
    border-radius: 16.5px;
    color: #212121;
    display: inline-block;
    font-family: "Product Sans", sans-serif;
    font-size: 14px;
    font-weight: 400;
    line-height: 24px;
    margin: 2px 0;
    max-width: 268px;
    /* opacity: 0; */
    padding: 10px 15px;
    transition: 0.5s;
    text-align: left;
    vertical-align: middle;
    -webkit-font-smoothing: antialiased;
    animation-name: bubble;
    animation-duration: 0.5s;
    word-break: break-word;
    display: inline-flex;
    align-items: center;
	}
			
			.kamiku .alertas_kamiku .margen_alerta_kamiku {
				width:100%;
				text-align:left;
			}
			
			<?php if($usandomovil == ''){?>.kamiku:hover .alertas_kamiku .margen_alerta_kamiku,<?}?> .kamiku.open .alertas_kamiku .margen_alerta_kamiku {
				text-align:left;
			}

	<?php if($usandomovil == ''){?>.kamiku:hover .alertas_kamiku,<?}?> .kamiku.open .alertas_kamiku {
   		 left: 25.5rem;
	}
			
			<?php if($usandomovil == ''){?>.kamiku:hover .alertas_kamiku .alerta_kamiku,<?}?> .kamiku.open .alertas_kamiku .alerta_kamiku {
				border-radius: 5px 20px 20px 5px;
				padding-left: 0.5rem;
				text-align:left;
			}

	.kamiku_menu_abierto:hover .alertas_kamiku {
   		 left: 40.5rem;
	}
			

	.kamiku .alerta_kamiku i {
   		 display: none;
   		 padding-right: 0.5rem;
	}
      
      .kamiku .alerta_kamiku_nueva {
        background:#01579B;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
        bottom: -100%!important;
      }

      <?php if($usandomovil == ''){?>.kamiku:hover .alerta_kamiku_nueva,<?}?> .kamiku.open .alerta_kamiku_nueva {
        background:#01579B;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
        bottom: -100%!important;
      }

	.kamiku .menu_kamiku {
		display: none;
	}

	<?php if($usandomovil == ''){?>.kamiku:hover .contenido_kamiku .menu_kamiku,<?}?> .kamiku.open .contenido_kamiku .menu_kamiku {
    width: 3.5rem;
    height: 100%;
    background: rgba(0, 0, 0, 0.25);
    text-align: center;
    display: inline-block;
    transition: 1s;
	}

	.menu_kamiku_abierto {
		width: auto!important;
	}

	.kamiku .contenido_kamiku .menu_kamiku i {
    color: white;
    display: block;
    margin: 0.5rem;
    padding: 0.5rem;
    cursor: pointer;
    transition: 0.5s;
    max-width: 1.5rem;
	}

	.kamiku .contenido_kamiku .menu_kamiku i:hover {
	box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background: rgba(33, 33, 33, 0.51);
    color: white;
    border-radius: 100%;
	}

	.kamiku .contenido_kamiku {
		display: none;
	}

	<?php if($usandomovil == ''){?>.kamiku:hover .contenido_kamiku,<?}?> .kamiku.open .contenido_kamiku {
    display: inline-flex;
    width: 100%;
    height: 100%;
	}

	.kamiku .contenido_kamiku .cards_kamiku {
    width: 100%;
    height: calc(100% - 1rem);
    padding: 0.5rem;
    overflow-x: hidden;
    overflow-y: auto;
	  transition:0.2s;
		position:relative;
	}
			
	.titulokamiku {
    transition: 0.5s;
    font-size: 3.5rem;
    font-family: Product Sans;
	  font-weight:900;
    height: 5rem;
    padding-top: 1.5rem;
    animation:titulokamikuanimacion 1.5s 1s ease both;
    display: inline-flex;
    align-items: center;
    margin: 0;
		color:white;
	}

	.peqiniciotitulokamiku {
		font-size: 2rem;
		font-weight: 100;
		margin: 0;
		animation:peqtitulokamikuanimaciion 1s 0s ease both;
		padding: 0;
	}

@keyframes titulokamikuanimacion {
  from {
  	padding: 0;
    height: 100%;
    opacity:0;
  }
}

@keyframes peqtitulokamikuanimacion {
  from {
  	opacity: 0;
  	height: 100%;
  }
}
	.kamiku .talk_kamiku {
    display: none;
    width: 100%;
    height: 100%;
    position: relative;
    transition: 0.5s;
	}

			

		</style>
	</head>
	<body>
		
	<div class="kamiku color_kamiku" id="kamiku" <?php if($usandomovil == ''){?>onmouseover="$(this).removeClass('open');"<?} else {?> onclick="$(this).addClass('open');" <?}?> >
	<i class="material-icons kmk" id="iconokamiku">&#xE6DD;</i>
		
	<div class="alertas_kamiku" id="alertaskamiku" onclick="talkkamiku()" >
	
	</div>


	<div class="contenido_kamiku" id="contenidokamiku" >
	
	<div class="menu_kamiku" >
		<i class="material-icons" onclick="menukamiku()">menu</i>
		<?php if($usandomovil != ''){?><i class="material-icons" onclick="setTimeout(function(){ $('.kamiku').removeClass('open'); }, 1000);">&#xE5C4;</i><?}?>
		<i class="material-icons" onclick="iniciokamiku()" >&#xE871;</i>
		<i class="material-icons" onclick="talkkamiku()">bubble_chart</i>
		<i class="material-icons" onclick="cambiartitulokamiku('Tendencias')" >whatshot</i>
		<i class="material-icons" onclick="cambiartitulokamiku('Acerca de mi')">account_circle</i>
		<i class="material-icons" onclick="cambiartitulokamiku('Tus compras')">&#xE8CC;</i>
		<i class="material-icons" onclick="cambiartitulokamiku('Recordatorios')">access_time</i>
		<i class="material-icons" onclick="cambiartitulokamiku('Comentarios')">rate_review</i>
		<i class="material-icons" onclick="cambiartitulokamiku('Ayuda')">help</i>
	</div>	

		
		
	<div class="pantalla_kamiku cards_kamiku" >

	<h1 id="titulokamiku" class="titulokamiku" >Kamiku</h1>
		
		
		
		
		<div class="cardskamiku" >

		<div class="loadcardskamiku" id="loadcardskamiku" ></div>

		</div>
	</div>
		
		<iframe id="kamikutalk" src="https://kaana.io/dominio/kamiku/" class="pantalla_kamiku talk_kamiku kamikuiframe" style="border: 0; width: 100%; height: 100%;">Your browser doesn't support iFrames.</iframe>
		
	</div>

	</div>
		

	<script type="text/javascript">

	function kamikustart() {
		
	}
		
	function menukamiku() {
		$('.menu_kamiku').toggleClass('menu_kamiku_abierto');
		$('.kamiku').toggleClass('kamiku_menu_abierto');
	}

	function iniciokamiku() {
		document.getElementById('titulokamiku').innerHTML = 'Kamiku'; 
		$('#titulokamiku').removeClass('peqiniciotitulokamiku');
		$('.cards_kamiku').show();
		$('.card_kamiku_normal').show();
		$('.talk_kamiku').hide();
		document.getElementById("iconokamiku").innerHTML = "bubble_chart";
		$('.kamiku').removeClass('kamiku_telefono');
		$('.kmk_alerta').removeClass('vericono_kamiku');
	}

	function cambiartitulokamiku(titulokamiku){
		document.getElementById("titulokamiku").innerHTML = titulokamiku;
		$('#titulokamiku').addClass('peqiniciotitulokamiku');
		$('.cards_kamiku').show();
		$('.card_kamiku_normal').hide();
		$('.talk_kamiku').hide();
		$('.titulokamiku').hide();
		setTimeout(function(){ $('.titulokamiku').show();}, 500);
		document.getElementById("iconokamiku").innerHTML = "bubble_chart";
		$('.kmk_alerta').removeClass('vericono_kamiku');
	}
      
	function talkkamiku() {
		$('.cards_kamiku').hide();
		$('.talk_kamiku').show();
		document.getElementById("iconokamiku").innerHTML = "bubble_chart";
		$('.kamiku').addClass('kamiku_telefono');
		$('.kmk_alerta').addClass('vericono_kamiku');
	}

    $('#loadcardskamiku').load('./resources/now.php?paginaapp=<?=$pagina_app?>');  
		$( document ).ready(function() {
		setInterval(function(){ $('#loadcardskamiku').load('./resources/now.php?app=now'); }, 300000);
    });
		
		function crearkamikualertafirst(texto){
			$.get("./resources/kamiku/funciones/alertas.php?first=si&decir=" + texto, function (data) {
      $("#alertaskamiku").append(data).delay(5000).queue(function() { $(this).remove(); });});
		}
		
		function crearkamikualerta(texto){ 
			$('.kmkborrar').remove();
			$.get("./resources/kamiku/funciones/alertas.php?decir=" + texto, function (data) {
      $("#alertaskamiku").append(data).children(':first').removeClass('zoomInUp').addClass('zoomOutUp').addClass('kmkborrar');});
		}
		
		function crearkamikualertatemporal(texto){ 
			$('.kmkborrar').remove();
			$.get("./resources/kamiku/funciones/alertas.php?decir=" + texto, function (data) {
      $("#alertaskamiku").append(data).children(':first').removeClass('zoomInUp').addClass('zoomOutUp').addClass('kmkborrar');});
		}
		
		function verkamiku() {
			$('.kamiku').addClass('open');
		}
		
		function verkamikutalk() {
			talkkamiku();
			$('.kamiku').addClass('open');
		}
		
		function verkamikupreguntar(pregunta) {
			talkkamiku();
			$('.kamiku').addClass('open');
			
			$('#kamikutalk')[0].contentWindow.respuestapreguntar(pregunta);
		}
		
		<?php if($_GET['verkamiku'] != ''){?>verkamiku();<?}?>
		
	</script>
<div class="kamikuhover" ></div>
	</body>
</html>