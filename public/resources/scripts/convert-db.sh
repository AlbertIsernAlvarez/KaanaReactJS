#!/bin/bash
USER=dumdarac
PASSWORD=floquetdeneu123
DB=dumdarac

doAllTables() {
        # get the table names
        TABLENAMES=`mysql -u $USER -p$PASSWORD -D $DB -e "SHOW TABLES\G;"|grep 'Tables_in_'|sed -n 's/.*Tables_in_.*: \([_0-9A-Za-z]*\).*/\1/p'`
        # loop through the tables and convert them
        for TABLENAME in $TABLENAMES
        do
                echo "* Processing table: $TABLENAME"
                FIELDS=`mysql -u$USER -p$PASSWORD -D $DB -e "show columns from $TABLENAME WHERE type LIKE 'text'\G" | grep Field | sed 's/Field: //g'`
                for FIELD in $FIELDS
                do
                        mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD TEXT CHARACTER SET latin1;"
                        mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD LONGBLOB;"
                        mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD TEXT CHARACTER SET utf8;"
                done

                FIELDS=`mysql -u$USER -p$PASSWORD -D $DB -e "show columns from $TABLENAME WHERE type LIKE 'longtext'\G" | grep Field | sed 's/Field: //g'`
                for FIELD in $FIELDS
                do
                        mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD LONGTEXT CHARACTER SET latin1;"
                        mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD LONGBLOB;"
                        mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD LONGTEXT CHARACTER SET utf8;"
                done

                FIELDS=`mysql -u$USER -p$PASSWORD -D $DB -e "show columns from $TABLENAME WHERE type LIKE 'varchar%'\G" | grep Field | sed 's/Field: //g'`
                for FIELD in $FIELDS
                do
                	echo "** Processing field: $FIELD "
                       mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD VARCHAR(255) CHARACTER SET latin1;"
                       mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD LONGBLOB;"
                       mysql -u$USER -p$PASSWORD -D $DB -e "alter table $TABLENAME change $FIELD $FIELD VARCHAR(255) CHARACTER SET utf8;"
                done
        done
}

doAllTables

