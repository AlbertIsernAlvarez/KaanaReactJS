<script>
/*DARK MODE*/
<?php if(isset($_COOKIE['darkmode']) && $_COOKIE['darkmode']){?>
document.querySelector("meta[name=theme-color]").setAttribute("content", "#000000");
<?}?>
jQuery(document).ready(function($) {

if (document.cookie.indexOf('darkmode=') === -1) {
    var mql = window.matchMedia('(prefers-color-scheme: dark)')

    mediaqueryresponse(mql) // call listener function explicitly at run time

    mql.addListener(mediaqueryresponse) // attach listener function to listen in on state changes

    function mediaqueryresponse(mql) {
        if (document.cookie.indexOf('darkmode=') === -1) {
            if (mql.matches) {
                $('body').addClass('PF-dark');
                document.querySelector("meta[name=theme-color]").setAttribute("content", "#000000");
            } else {
                $('body').removeClass('PF-dark');
                document.querySelector("meta[name=theme-color]").setAttribute("content", "#ffffff");
            }
        }
    }
}

});
</script>