<?php

function uploadfile($file, $folder="", $name="", $extension="", $allowedfiletype=""){
  
  $file_to = $folder.$name.".".$extension;
  $uploadOk = 1;
  $imageFileType = $file['type'];
  
  // Check if image file is a actual image or fake image
  $check = getimagesize($file["tmp_name"]);
  if($check === false) {
    //"File is not an image.";
    $uploadOk = 0;
    return "ISNOIMAGE";
  }
  
  // Allow certain file formats
  if($imageFileType != "image/jpeg" && $imageFileType != "image/jpg" && $imageFileType != "jpeg") {
    //Sorry, only JPG, JPEG, files are allowed.
    $uploadOk = 0;
    return "ONLYTIPE";
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
    //Sorry, your file was not uploaded.
    return "NOTUPLOADED";
  } else {
    // if everything is ok, try to upload file
    if (move_uploaded_file($file["tmp_name"], $file_to)) {
        //The file ". basename( $_FILES["image"]["name"]). " has been uploaded.
      return true;
    } else {
        //Sorry, there was an error uploading your file.
      return "ERROR";
    }
  }
}

?>
