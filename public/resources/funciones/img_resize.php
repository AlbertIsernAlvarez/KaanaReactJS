<?php
function dd_img_resize($source, $newcopy, $width, $height, $ext) {
$source = str_replace("%20", " ", $source);
$width = str_replace("px", "", $width);
$height = str_replace("px", "", $height);
$mime = mime_content_type($source);

header('Content-type: ' . $mime);

if($mime != "image/svg" and $mime != "image/gif"){
$getimagesize = getimagesize($source);
list($width_original, $height_original) = $getimagesize;
$image = new Imagick($source);

if($height == null){
$ratio_orig = $width_original/$height_original;
$height = $height/$ratio_orig;
}

$compression = $_GET['compression'];
if($compression == ''){
  $compression = "50";
}

$quality = $_GET['quality'];
if($quality == ''){
  $quality = "50";
}


switch ($mime) {
    case "image/png":
        $image->setImageFormat('png');
        break;
    case "image/jpg":
        $image->setImageFormat('jpg');
        break;
    case "image/gif":
        $image->setImageFormat('gif');
        break;
    case "image/jpeg":
        $image->setImageFormat('jpeg');
        break;
}

$image->optimizeImageLayers();

// Compression and quality
$image->setImageCompression(Imagick::COMPRESSION_JPEG);
$image->setImageCompressionQuality(60);

$image->setOption('jpeg:extent', '100kb');

$image->stripImage();

$image->setInterlaceScheme(Imagick::INTERLACE_PLANE);

/*AUTO ROTATE IMAGE*/
switch ($image->getImageOrientation()) {
    case Imagick::ORIENTATION_TOPLEFT:
        break;
    case Imagick::ORIENTATION_TOPRIGHT:
        $image->flopImage();
        break;
    case Imagick::ORIENTATION_BOTTOMRIGHT:
        $image->rotateImage("#000", 180);
        break;
    case Imagick::ORIENTATION_BOTTOMLEFT:
        $image->flopImage();
        $image->rotateImage("#000", 180);
        break;
    case Imagick::ORIENTATION_LEFTTOP:
        $image->flopImage();
        $image->rotateImage("#000", -90);
        break;
    case Imagick::ORIENTATION_RIGHTTOP:
        $image->rotateImage("#000", 90);
        break;
    case Imagick::ORIENTATION_RIGHTBOTTOM:
        $image->flopImage();
        $image->rotateImage("#000", 90);
        break;
    case Imagick::ORIENTATION_LEFTBOTTOM:
        $image->rotateImage("#000", -90);
        break;
    default: // Invalid orientation
        break;
    }
    $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);

// If 0 is provided as a width or height parameter,
// aspect ratio is maintained
$image->thumbnailImage($width, $height);

echo $image;


$image->clear();
$image->destroy();

} else {
    readfile($source);
}
}

if($_GET['file']){
  dd_img_resize(__DIR__."/../../".$_GET['file'], null, $_GET['w'], $_GET['h'], null);
}

?>