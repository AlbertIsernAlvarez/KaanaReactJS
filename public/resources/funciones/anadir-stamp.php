<?php
$url = $_GET['url'];
// Cargar la estampa y la foto para aplicarle la marca de agua
$im = imagecreatefromjpeg($url);

// Primero crearemos nuestra imagen de la estampa manualmente desde GD
$estampa = imagecreatetruecolor(100, 70);
imagefilledrectangle($estampa, 0, 0, 99, 69, 0x0000FF);
imagefilledrectangle($estampa, 9, 9, 90, 60, 0xFFFFFF);
$im = imagecreatefrompng('../../imgs/logo/logotransparente.png');
imagestring($estampa, 5, 20, 20, 'libGD', 0x0000FF);
imagestring($estampa, 3, 20, 40, '(c) 2007-9', 0x0000FF);

// Establecer los márgenes para la estampa y obtener el alto/ancho de la imagen de la estampa
$margen_dcho = 10;
$margen_inf = 10;
$sx = imagesx($estampa);
$sy = imagesy($estampa);

// Fusionar la estampa con nuestra foto con una opacidad del 50%
imagecopymerge($im, $estampa, imagesx($im) - $sx - $margen_dcho, imagesy($im) - $sy - $margen_inf, 0, 0, imagesx($estampa), imagesy($estampa), 50);

// Guardar la imagen en un archivo y liberar memoria
imagepng($im, 'foto_estampa.png');
imagedestroy($im);

?>