<?php
function translate($text, $from, $to, $format, $contribute)
{
    $text_original = $text;
    global $con;
    global $language;
    
    if($format === "json"){
    	$translation = new stdClass();
    }
    
    if ($text and $from and $to) {
    	$text = mysqli_real_escape_string($con, $text);
        
        $translation_sql = mysqli_query($con, "SELECT * FROM traductor WHERE (language1='$from' AND language2='$to' OR language1='$to' AND language2='$from') AND (texto='$text' OR traduccion='$text') ORDER BY length(texto) DESC, length(traduccion) DESC LIMIT 1");
        while ($row_translation_sql = mysqli_fetch_array($translation_sql)) {
            $translation_id  = $row_translation_sql["id"];
            $translation_demand = $row_translation_sql["demand"];
            $translation_status  = $row_translation_sql["status"];

            $translation_texto = $row_translation_sql["texto"];
            $translation_texto_traduccion = $row_translation_sql["traduccion"];

            if ($from != $row_translation_sql["language1"]) {
            	$translation_result = $translation_texto;
            } else {
            	$translation_result = $translation_texto_traduccion;
            }

            if($format === "json"){
            	$translation->id = $row_translation_sql["id"];
            	$translation->status = $row_translation_sql["status"];
            	$translation->original = $text;
            	$translation->translation = $translation_result;
            	return $translation;
            } elseif($translation_result) {
            	return $translation_result;
            }
        }
        if($from != $to){
            if($contribute === true and $translation_id == null){
                mysqli_query($con, "INSERT INTO traductor (texto, language1, language2, status, demand) VALUES('$text', '$from', '$to', 'pending', '1') ");
            }
        }

        if(!$format) {
            return $text_original;
        }
        
    }
}
?>