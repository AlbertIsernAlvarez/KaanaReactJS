<html>

<head>
  <link rel="stylesheet prefetch" href="https://kaana.io/resources/playerdd/css.css">
  <style>
    body {
      background: black;
      width: 100%;
      height: 100%;
      display: inline-flex;
      align-items: center;
      margin: 0;
    }

    .container {
      margin: auto;
    }

    .playerdd {
      max-width:100vw;
      max-height:100vh;
      border-radius: 1em;
    }
  </style>
</head>

<body>
  <script src="//kaana.io/resources/js/jquery-3.3.1.js"></script>
  <script src="//kaana.io/resources/playerdd/js.js"></script>
  <div class="container">
    <audio controls id="player">
  <source src="//kaana.io/<?=$_GET['file'];?>?not" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
  </div>
  <script>
    document.addEventListener('DOMContentLoaded', () => {
        // This is the bare minimum JavaScript. You can opt to pass no arguments to setup.
        const player = new playerdd('#player'); // Bind event listener
      }

    );
  </script>
</body>

</html>