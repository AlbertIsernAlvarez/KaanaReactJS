<html>

<head>
  <link rel="stylesheet prefetch" href="https://kaana.io/resources/playerdd/css.css">
  <style>
    body {
      background: black;
      width: 100%;
      height: 100%;
      display: inline-flex;
      align-items: center;
      margin: 0;
    }
  </style>
</head>

<body>
  <script src="//kaana.io/resources/js/jquery-3.3.1.js"></script>
  <script src="//kaana.io/resources/playerdd/js.js"></script>
  <video controls playsinline class="player-dd" src="//kaana.io/<?=$_GET['file'];?>?not">
      <source src="//kaana.io/<?=$_GET['file'];?>?not" type="video/mp4">
 <a href="//kaana.io/<?=$_GET['file'];?>?not" download>Download</a></video>
  <script>
    const players = Array.from(document.querySelectorAll('.player-dd')).map(p => new playerdd(p));
    $(document).bind("DOMSubtreeModified", function() {
        const players = Array.from(document.querySelectorAll('.player-dd')).map(p => new playerdd(p)); // Bind event listener
      });
  </script>
</body>

</html>