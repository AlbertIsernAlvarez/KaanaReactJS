<?php
if(!isset($_SESSION)){ session_start(); }
include_once(__DIR__."/server-info.php");
include_once(__DIR__."/settings.php");
include_once(__DIR__."/acceso_db.php");
include_once(__DIR__."/funciones/clean_input.php");
include_once(__DIR__."/funciones/replaceemojis.php");
include_once(__DIR__."/funciones/translate.php");
include_once(__DIR__."/emails/send.php");
include_once(__DIR__."/server-info.php");
include_once(__DIR__."/only-variables.php");
if(isset($_SESSION['users']['current'])){
  include_once(__DIR__."/user/data.php");
  include_once(__DIR__."/user/timezone.php");
  include_once(__DIR__."/user/error-log.php");
}
include_once(__DIR__."/user/data-public.php");
include_once(__DIR__."/language.php");
include_once(__DIR__."/errors.php");
include_once(__DIR__."/version/info.php");
include_once(__DIR__."/update-statistics.php");

if($app and file_exists(__DIR__."/../apps/$app/preppend.php")){
	include_once(__DIR__."/../apps/$app/preppend.php");
}

if($pagina and file_exists(__DIR__."/../pantallas/$pagina/preppend.php")){
	include_once(__DIR__."/../pantallas/$pagina/preppend.php");
}

include_once(__DIR__."/topics/related.php");


function contains($string, array $array) {
    $count = 0;
    foreach($array as $value) {
        if (false !== stripos($string,$value)) {
            ++$count;
        };
    }
    return $count == count($array);
}
?>