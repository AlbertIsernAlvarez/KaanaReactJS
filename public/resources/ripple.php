<html>

<head>
  <style>
    .ripple,
    .activeripple {
      overflow: hidden;
      transition: 0.5s;
      position: relative;
      -webkit-transform: translateZ(0);
      transform: translateZ(0);
    }

    .ripple .rippling {
      position: absolute;
      border-radius: 50%;
      background-color: rgb(var(--PF-color-primary));
      pointer-events: none;
      opacity: .4;
      transform: translate3d(-50%, -50%, 0);
      filter: blur(3px);
    }

    .PF-avatar.ripple .rippling {
      max-width: 100%;
      max-height: 100%;
    }
  </style>
</head>

<body>
  <script>
    $(function() {
        $("body").on("mousedown", ".ripple", function(e) {
          $(this).addClass('activeripple');
          var thiss = $(this);
          var $ripple = $('<div class="rippling" />'),
            $button = $(this),
            btnOffset = $button.offset(),
            xPos = e.pageX - btnOffset.left,
            yPos = e.pageY - btnOffset.top,
            size = 0,
            animateSize = parseInt(Math.max($button.width(), $button.height()) * Math.PI);
          if (thiss.hasClass("unbounded") || thiss.hasClass("PF-icon") || thiss.hasClass("PF-avatar")) {
            xPos = "50%";
            yPos = "50%";
          }
          $ripple.css({
            top: yPos,
            left: xPos,
            width: size,
            height: size,
            backgroundColor: $button.attr("ripple-color"),
            opacity: $button.attr("ripple-opacity")
          }).appendTo($button).animate({
            width: animateSize,
            height: animateSize,
            opacity: 0
          }, 500, function() {
            $(this).remove();
            $('.activeripple').removeClass('activeripple');
          });
        });
      }

    );
  </script>
</body>

</html>