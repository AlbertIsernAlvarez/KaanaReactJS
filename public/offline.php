<html>

<head>
  <title>Can't connect | Kaana</title>
  <link rel="dns-prefetch" href="https://kaana.io">
  <link rel="preconnect" href="https://kaana.io">
  <meta http-equiv="content-language" content="<?=$language?>" />
  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
  <link href="https://kaana.io/" rel="canonical">
  <meta name="google" content="notranslate" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="googlebot" content="noindex" />
  <meta name="robots" content="noindex">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/style.css">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/colors.css">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower.css">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/button.css">
  <link rel="stylesheet" type="text/css" href="//kaana.io/resources/css/paperflower/progress.css">
  <style>
    .offline_screen {
      position: fixed;
      left: 0;
      top: 0;
      max-width: 100%;
      width: 100%;
      height: 100%;
      color: rgb(var(--PF-color-on-surface));
      background-color: rgb(var(--PF-color-surface));
      display: flex;
      background-image: url(//img.kaana.io/logo/logo_app.png);
      background-size: 5em;
      background-position: top 2em left 2em;
      background-repeat: no-repeat;
      z-index: 99999999;
      border-radius: 0;
    }
    
    .offline_screen svg {
      width: 10em;
      margin: auto;
    }
    
    .offline_screen .PF-button {
      margin: .5em auto;
    }
  </style>
</head>

<body>
  <script src="/resources/js/jquery-3.4.1.min.js"></script>
  <script src="/resources/js/jquery-ui.min.js"></script>
  <script src="/resources/js/jquery.initialize.min.js"></script>

  <div class="PF PF-emptypage offline_screen" id="offlinescreen">
    <div class="container">
      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 295.2 225.28"><defs><style>.cls-1{opacity:0.38;isolation:isolate;fill:url(#radial-gradient);}.cls-2,.cls-3{fill:none;stroke-miterlimit:10;stroke-width:4.47px;}.cls-2{stroke:url(#linear-gradient);}.cls-3{stroke:url(#linear-gradient-2);}.cls-4{fill:#a27f68;}.cls-5{fill:#b3bac5;}.cls-6{fill:#0052cc;}.cls-7{fill:url(#linear-gradient-3);}.cls-8{fill:url(#linear-gradient-4);}.cls-9{fill:url(#linear-gradient-5);}.cls-10{fill:url(#linear-gradient-6);}.cls-11{fill:url(#linear-gradient-7);}.cls-12{opacity:0.5;}.cls-13{fill:#fff;}.cls-14{fill:url(#linear-gradient-8);}.cls-15{fill:url(#linear-gradient-9);}.cls-16{fill:url(#linear-gradient-10);}.cls-17{fill:url(#linear-gradient-11);}.cls-18{fill:url(#linear-gradient-12);}.cls-19{fill:url(#linear-gradient-13);}.cls-20{fill:url(#linear-gradient-14);}</style><radialGradient id="radial-gradient" cx="147.6" cy="155.03" r="96.52" gradientTransform="matrix(1, 0, 0, -1, 0, 268)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff0b2"/><stop offset="0.16" stop-color="#ffe587" stop-opacity="0.76"/><stop offset="0.32" stop-color="#ffdb5f" stop-opacity="0.53"/><stop offset="0.49" stop-color="#ffd33d" stop-opacity="0.34"/><stop offset="0.64" stop-color="#ffcd22" stop-opacity="0.19"/><stop offset="0.78" stop-color="#ffc810" stop-opacity="0.09"/><stop offset="0.91" stop-color="#ffc504" stop-opacity="0.02"/><stop offset="1" stop-color="#ffc400" stop-opacity="0"/></radialGradient><linearGradient id="linear-gradient" x1="31.32" y1="113.08" x2="-3.36" y2="113.08" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#c1c7d0"/><stop offset="1" stop-color="#c1c7d0" stop-opacity="0"/></linearGradient><linearGradient id="linear-gradient-2" x1="386.9" y1="113.08" x2="349.22" y2="113.08" gradientTransform="matrix(-1, 0, 0, 1, 647.78, 0)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#0065ff"/><stop offset="0.08" stop-color="#0266ff" stop-opacity="0.97"/><stop offset="0.19" stop-color="#076aff" stop-opacity="0.9"/><stop offset="0.32" stop-color="#1070ff" stop-opacity="0.78"/><stop offset="0.46" stop-color="#1d79ff" stop-opacity="0.62"/><stop offset="0.61" stop-color="#2d84ff" stop-opacity="0.41"/><stop offset="0.77" stop-color="#4092ff" stop-opacity="0.16"/><stop offset="0.86" stop-color="#4c9aff" stop-opacity="0"/></linearGradient><linearGradient id="linear-gradient-3" x1="193.33" y1="154.92" x2="260.88" y2="154.92" gradientTransform="matrix(1, 0, 0, -1, 0, 268)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#0052cc"/><stop offset="1" stop-color="#2684ff"/></linearGradient><linearGradient id="linear-gradient-4" x1="116.41" y1="133.72" x2="155.73" y2="133.72" gradientTransform="matrix(1, 0, 0, -1, 0, 268)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#7a869a"/><stop offset="1" stop-color="#dfe1e5"/></linearGradient><linearGradient id="linear-gradient-5" x1="116.41" y1="178.75" x2="155.73" y2="178.75" xlink:href="#linear-gradient-4"/><linearGradient id="linear-gradient-6" x1="183.22" y1="107.16" x2="183.22" y2="202.31" gradientTransform="matrix(1, 0, 0, -1, 0, 268)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2684ff"/><stop offset="1" stop-color="#b2d4ff"/></linearGradient><linearGradient id="linear-gradient-7" x1="2858.07" y1="154.92" x2="2925.62" y2="154.92" gradientTransform="translate(2956.93 268) rotate(180)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#7a869a"/><stop offset="1" stop-color="#ebecf0"/></linearGradient><linearGradient id="linear-gradient-8" x1="230.7" y1="666.36" x2="224.66" y2="672.59" gradientTransform="translate(0 -560.93)" gradientUnits="userSpaceOnUse"><stop offset="0.18" stop-color="#fff" stop-opacity="0.4"/><stop offset="1" stop-color="#fff"/></linearGradient><linearGradient id="linear-gradient-9" x1="223.87" y1="673.69" x2="216.88" y2="680.48" xlink:href="#linear-gradient-8"/><linearGradient id="linear-gradient-10" x1="2847.96" y1="107.17" x2="2847.96" y2="202.31" gradientTransform="translate(2956.93 268) rotate(180)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#c1c7d0"/><stop offset="1" stop-color="#f4f5f7"/></linearGradient><linearGradient id="linear-gradient-11" x1="151.75" y1="211.71" x2="151.75" y2="256.2" gradientTransform="matrix(1, 0, 0, -1, 0, 268)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff0b2"/><stop offset="1" stop-color="#ffc400"/></linearGradient><linearGradient id="linear-gradient-12" x1="124.35" y1="220.49" x2="134.7" y2="220.49" gradientTransform="matrix(1, 0, 0, -1, 0, 268)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffc400"/><stop offset="1" stop-color="#fff0b2"/></linearGradient><linearGradient id="linear-gradient-13" x1="121.71" y1="75.86" x2="121.71" y2="115.7" xlink:href="#linear-gradient-12"/><linearGradient id="linear-gradient-14" x1="147.08" y1="85.1" x2="147.08" y2="107.26" xlink:href="#linear-gradient-12"/></defs><title>integration-small</title><circle class="cls-1" cx="147.6" cy="112.97" r="96.52"/><line class="cls-2" x1="31.32" y1="113.08" x2="-3.36" y2="113.08"/><line class="cls-3" x1="260.88" y1="113.08" x2="298.56" y2="113.08"/><path class="cls-4" d="M50,126.11l-1.57-6.27a.74.74,0,0,1,.7-.74c.3.27.64.51.87.4s.13-.35.34-.48c.37-.23,1.06.25,1.27.4a3.1,3.1,0,0,1,.62.58l1,.34a1.08,1.08,0,0,1,.67.75L55,122c.54.09.76,2.26.78,2.34l-.41,2.79a.74.74,0,0,1-.52.91.77.77,0,0,1-.45,0l-4-1.43A.76.76,0,0,1,50,126.11Z"/><path class="cls-4" d="M55.06,131.16c1.18-.42,4,3.41,1-4a6,6,0,0,0-2.38-3.58c-1.92-1.18-3.47.27-5.12-1.07-.88-.72-.91-1.53-2-2-.79-.33-1.85-.38-2.05,0s.85,1.08,2.32,3S48,125.64,49.28,127c.54.58.93.88,2.1,2.11a9.77,9.77,0,0,0,.9.92C52.85,130.51,54.09,131.5,55.06,131.16Z"/><path class="cls-5" d="M28.48,104.15h0a3.24,3.24,0,0,1,3.24,3.24v11.42A3.24,3.24,0,0,1,28.48,122h0a3.23,3.23,0,0,1-3.23-3.23V107.39A3.24,3.24,0,0,1,28.48,104.15Z"/><path class="cls-6" d="M263.27,104.15h0a3.24,3.24,0,0,1,3.23,3.24v11.42a3.23,3.23,0,0,1-3.23,3.23h0a3.24,3.24,0,0,1-3.24-3.23V107.39A3.24,3.24,0,0,1,263.27,104.15Z"/><path class="cls-7" d="M218.83,155.13h-25.5V71h25.5a42,42,0,0,1,42,42.05h0a42,42,0,0,1-42,42.05Z"/><rect class="cls-8" x="116.41" y="131.06" width="39.32" height="6.44"/><rect class="cls-9" x="116.41" y="86.03" width="39.32" height="6.44"/><path class="cls-10" d="M187.37,160.84H173.12V65.69h14.25a6,6,0,0,1,6,6h0v83.23a6,6,0,0,1-6,6Z"/><path class="cls-11" d="M73.36,71h25.5v84.09H73.36a42,42,0,0,1-42-42h0a42,42,0,0,1,42-42Z"/><g id="Layer_2-2" data-name="Layer 2-2" class="cls-12"><g id="White"><path class="cls-13" d="M237,98.12H222.8a6.42,6.42,0,0,0,6.42,6.41h2.62v2.53a6.41,6.41,0,0,0,6.41,6.41V99.35A1.23,1.23,0,0,0,237,98.12Z"/><path class="cls-14" d="M229.78,105.4H215.56a6.42,6.42,0,0,0,6.42,6.42h2.61v2.52a6.42,6.42,0,0,0,6.42,6.41V106.64a1.23,1.23,0,0,0-1.23-1.24Z"/><path class="cls-15" d="M222.53,112.69H208.32a6.41,6.41,0,0,0,6.42,6.41h2.62v2.53a6.41,6.41,0,0,0,6.4,6.41V113.92A1.23,1.23,0,0,0,222.53,112.69Z"/></g></g><path class="cls-16" d="M104.82,65.69h14.26v95.14H104.82a6,6,0,0,1-5.95-6h0V71.65a6,6,0,0,1,5.95-6Z"/><path class="cls-17" d="M160.11,35.64l-4.18-23.41a.53.53,0,0,0-.61-.43.52.52,0,0,0-.41.38l-6,21.06L144,31.85a.52.52,0,0,0-.65.36.6.6,0,0,0,0,.23l4.19,23.42a.52.52,0,0,0,.6.42.53.53,0,0,0,.42-.37l6-21.06,4.86,1.39a.53.53,0,0,0,.65-.36A.65.65,0,0,0,160.11,35.64Z"/><path class="cls-18" d="M133.34,45.74l-8.6-8.17a.24.24,0,0,0-.33,0,.24.24,0,0,0,0,.24L128.16,48l-2.37.88a.23.23,0,0,0-.13.3.41.41,0,0,0,0,.09l8.6,8.17a.24.24,0,0,0,.33,0,.23.23,0,0,0,.05-.24L130.89,47l2.37-.89a.22.22,0,0,0,.13-.29A.19.19,0,0,0,133.34,45.74Z"/><path class="cls-19" d="M114.31,168l-3.56,23.51a.53.53,0,0,0,.44.6.54.54,0,0,0,.51-.22l12.44-18,4.16,2.87a.52.52,0,0,0,.73-.13.85.85,0,0,0,.09-.22l3.55-23.52a.52.52,0,0,0-.44-.59.51.51,0,0,0-.5.22l-12.45,18-4.16-2.88a.53.53,0,0,0-.73.14A.58.58,0,0,0,114.31,168Z"/><path class="cls-20" d="M142.9,172.27l5.52,10.5a.22.22,0,0,0,.31.1.23.23,0,0,0,.13-.21l-.33-10.89,2.54-.07a.23.23,0,0,0,.22-.24.36.36,0,0,0,0-.1l-5.52-10.5a.24.24,0,0,0-.44.11l.33,10.89-2.54.08a.22.22,0,0,0-.22.23A.3.3,0,0,0,142.9,172.27Z"/></svg>
      <h1 t-dd>Can't connect</h1>
      <p t-dd>You need an internet connection to use Kaana</p>
      <div class="PF PF-button" onclick="location.reload();" t-dd>Retry</div>
    </div>
  </div>

  <script>
    window.addEventListener("online", onlinescreen);
    function onlinescreen(){
      setInterval(function(){ location.reload(); }, 1000);
    }
  </script>

</body>

</html>